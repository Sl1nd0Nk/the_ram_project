//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ServicesPortalDBModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class UserEmailNotification
    {
        public int ID { get; set; }
        public string RecordType { get; set; }
        public int RecordID { get; set; }
        public int EmailNotificationID { get; set; }
        public bool NotificationSent { get; set; }
        public string EmailAddress { get; set; }
        public int PrincipalID { get; set; }
        public string EmailBody { get; set; }
        public string EmailSubject { get; set; }
    }
}
