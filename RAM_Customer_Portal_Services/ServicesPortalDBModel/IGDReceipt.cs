//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ServicesPortalDBModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class IGDReceipt
    {
        public int ID { get; set; }
        public int IGDStagingID { get; set; }
        public string SiteCode { get; set; }
        public string PrincipalCode { get; set; }
        public string ProdCode { get; set; }
        public string LineNumber { get; set; }
        public string ReceiptType { get; set; }
        public string MoveRef { get; set; }
        public string PORef { get; set; }
        public Nullable<System.DateTime> ReceiptDT { get; set; }
        public string StartEndStatus { get; set; }
        public Nullable<int> RejectCount { get; set; }
        public string ReasonCode { get; set; }
        public Nullable<int> AcceptCount { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }
    }
}
