﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ServicesPortalDBModel;
using ServicesWmsHostDBModel;
//using OCOSenderService.SOShipment;
using OCOSenderService.HAWSO;
using System.IO;
using System.Xml.Serialization;
using System.Xml;
using RAM.Utilities.Common;
using System.Configuration;

namespace OCOSenderService
{
    public partial class OCOSender : ServiceBase
    {
        WMSHostCustomerPortalEntities PortalDB;
        RAMWMSHOSTEntities WmsHostDB;
        private bool ProcessRunning = false;

        public OCOSender()
        {
            InitializeComponent();

            ProcessRunning = true;
            var threadStart = new ThreadStart(RunService);
            var thread = new Thread(threadStart);
            thread.Start();
        }

        #region OCO
        private Order AddOCOOrder(MsgIn_OCO OCO, Principal Prn)
        {
            Logger.Info("Adding - Order " + OCO.OrderNumber);

            if(Prn != null)
            {
                MsgOut_ORD MOrd = WmsHostDB.MsgOut_ORD.AsNoTracking().Where(a => a.OrderNumber == OCO.OrderNumber).ToList().LastOrDefault();

                if (MOrd != null)
                {
                    Customer Cust = null;
                    if (!String.IsNullOrEmpty(MOrd.CustAcc))
                    {
                        string CustAcc = MOrd.CustAcc.Trim();
                        Cust = PortalDB.Customers.AsNoTracking().Where(a => a.StoreCode == CustAcc).FirstOrDefault();
                    }

                    string CustName = MOrd.DeliveryAdd1 == null ? "" : String.IsNullOrEmpty(MOrd.DeliveryAdd1) ? "" : MOrd.DeliveryAdd1.Trim();
                    string Street1 = MOrd.DeliveryAdd2 == null ? "" : String.IsNullOrEmpty(MOrd.DeliveryAdd2) ? "" : MOrd.DeliveryAdd2.Trim();
                    string Street2 = MOrd.DeliveryAdd3 == null ? "" : String.IsNullOrEmpty(MOrd.DeliveryAdd3) ? "" : MOrd.DeliveryAdd3.Trim();

                    if (Cust == null)
                    {
                        if (!String.IsNullOrEmpty(CustName) && !String.IsNullOrEmpty(Street1) && !String.IsNullOrEmpty(Street2))
                        {
                            Cust = PortalDB.Customers.AsNoTracking().Where(a => a.CustomerName.StartsWith(CustName) && a.StreetAddress1.StartsWith(Street1) && a.StreetAddress2.StartsWith(Street2)).FirstOrDefault();
                        }
                    }

                    if (Cust == null)
                    {
                        CustName = String.IsNullOrEmpty(CustName) ? Street1 : CustName;

                        if (!String.IsNullOrEmpty(CustName))
                        {
                            Cust = PortalDB.Customers.AsNoTracking().Where(a => a.CustomerName == CustName).FirstOrDefault();
                        }
                    }

                    if (Cust == null)
                    {
                        CustName = MOrd.CompanyName != null? !String.IsNullOrEmpty(MOrd.CompanyName) ? MOrd.CompanyName.Trim() : "" : "";

                        if (!String.IsNullOrEmpty(CustName))
                        {
                            Cust = PortalDB.Customers.AsNoTracking().Where(a => a.CustomerName == CustName).FirstOrDefault();
                        }
                    }

                    if (Cust == null)
                    {
                        string BillTo = MOrd.BilledTo == null? "" : !String.IsNullOrEmpty(MOrd.BilledTo.Trim()) ? MOrd.BilledTo : "";
                        if (!String.IsNullOrEmpty(BillTo))
                        {
                            Cust = PortalDB.Customers.AsNoTracking().Where(a => a.CustomerID == BillTo).FirstOrDefault();
                        }
                    }

                    if (Cust != null)
                    {
                        LinkedCustomer LC = PortalDB.LinkedCustomers.AsNoTracking().Where(a => a.PrincipalID == Prn.PrincipalID && a.RAMCustomerID == Cust.CustomerID).FirstOrDefault();

                        if(LC == null)
                        {
                            LC = new LinkedCustomer();
                            LC.BillingRAMCustomerID = Cust.CustomerID;
                            LC.CourierName = String.IsNullOrEmpty(MOrd.CourierName) ? "" : MOrd.CourierName.ToUpper();
                            LC.CourierService = String.IsNullOrEmpty(MOrd.CourierService) ? "" : MOrd.CourierService.ToUpper();
                            LC.DebtorsCode = "";
                            LC.Deleted = false;
                            LC.EventDate = OCO.RecordDT;
                            LC.EventTerminal = "";
                            LC.EventUser = OCO.RecordSource;
                            LC.InsuranceRequired = MOrd.InsuranceRequired == null? false : MOrd.InsuranceRequired;
                            LC.KYC = String.IsNullOrEmpty(MOrd.KYC) ? "" : MOrd.KYC.ToUpper();
                            LC.PrincipalID = Prn.PrincipalID;
                            LC.RAMCustomerID = Cust.CustomerID;

                            PortalDB.LinkedCustomers.Add(LC);
                            PortalDB.SaveChanges();
                        }

                        Order MyOrd = new Order();
                        MyOrd.BillingCustomerDetailID = LC.CustomerDetailID;
                        MyOrd.BillingRAMCustomerID = Cust.CustomerID;
                        MyOrd.ClientConfirmed = false;
                        MyOrd.CourierName = String.IsNullOrEmpty(MOrd.CourierName) ? "" : MOrd.CourierName.ToUpper();
                        MyOrd.CourierService = String.IsNullOrEmpty(MOrd.CourierService) ? "" : MOrd.CourierService.ToUpper();
                        MyOrd.CustomerDetailID = LC.CustomerDetailID;
                        MyOrd.CustomerOrderNumber = String.IsNullOrEmpty(MOrd.CustOrderNumber) ? MOrd.OrderNumber.ToUpper() : MOrd.CustOrderNumber.ToUpper();
                        MyOrd.DateCreated = MOrd.DateCreated;
                        MyOrd.Deleted = false;
                        MyOrd.EventDate = MOrd.RecordDT;
                        MyOrd.EventTerminal = "";
                        MyOrd.EventUser = OCO.RecordSource;
                        MyOrd.IDNumber = String.IsNullOrEmpty(MOrd.IDNumber) ? "" : MOrd.IDNumber.ToUpper();
                        MyOrd.InsuranceRequired = MOrd.InsuranceRequired == null? false: MOrd.InsuranceRequired;
                        MyOrd.KYC = String.IsNullOrEmpty(MOrd.KYC) ? "" : MOrd.KYC.ToUpper();
                        MyOrd.OrderDiscount = (double)MOrd.OrderDiscount;
                        MyOrd.OrderVAT = (double)MOrd.OrderVAT;
                        MyOrd.PrincipalID = Prn.PrincipalID;
                        MyOrd.Priority = MOrd.Priority.ToString();
                        MyOrd.Processor = String.IsNullOrEmpty(MOrd.Processor) ? "" : MOrd.Processor.ToUpper();
                        MyOrd.RAMOrderNumber = MOrd.OrderNumber.ToUpper();
                        MyOrd.ReceiverRAMCustomerID = Cust.CustomerID;
                        MyOrd.SalesCategory = String.IsNullOrEmpty(MOrd.SalesCategory) ? "" : MOrd.SalesCategory.ToUpper();
                        MyOrd.SalesPerson = String.IsNullOrEmpty(MOrd.SalesPerson) ? "" : MOrd.SalesPerson.ToUpper();
                        MyOrd.Status = "NEW";
                        MyOrd.Submitted = true;
                        MyOrd.ValidateDelivery = false;
                        MyOrd.ValueAddedPackaging = false;

                        PortalDB.Orders.Add(MyOrd);
                        PortalDB.SaveChanges();

                        List<MsgOut_ORD_LN> MOrdLn = WmsHostDB.MsgOut_ORD_LN.AsNoTracking().Where(a => a.ORDID == MOrd.ORDID).ToList();
                        List<OrderLineItem> OrdLnList = new List<OrderLineItem>();

                        foreach(var i in MOrdLn)
                        {
                            ProductStaging Prod = PortalDB.ProductStagings.AsNoTracking().Where(a => a.ProdCode == i.ProdCode && a.PrincipalID == Prn.PrincipalID).FirstOrDefault();

                            if(Prod != null)
                            {
                                OrderLineItem ln = new OrderLineItem();
                                ln.Deleted = false;
                                ln.EventDate = OCO.RecordDT;
                                ln.EventTerminal = "";
                                ln.EventUser = OCO.RecordSource;
                                ln.LineNumber = i.LineNumber.ToString();
                                ln.LineText = String.IsNullOrEmpty(i.LineText)? "": i.LineText;
                                ln.LineType = i.LineType;
                                ln.OrderID = MyOrd.OrderID;
                                ln.ProductStagingID = Prod.ProductStagingID;
                                ln.Quantity = i.Quantity;
                                ln.SubmittedProductCode = "";
                                ln.SubmittedRAMOrderNumber = "";
                                ln.SubmittedToWMS = true;
                                ln.SubmittedUnitCost = i.UnitCost;
                                ln.SubmittedUnitDiscountAmount = i.Discount;
                                ln.SubmittedVAT = i.Vat;
                                ln.WMSHostOrderID = MOrd.ORDID;

                                OrdLnList.Add(ln);
                            }
                        }

                        if(OrdLnList.Count > 0)
                        {
                            PortalDB.OrderLineItems.AddRange(OrdLnList);
                            PortalDB.SaveChanges();
                            PortalDB.Dispose();
                            PortalDB = new WMSHostCustomerPortalEntities();

                            Logger.Info("Order " + OCO.OrderNumber + " Added");
                            return MyOrd;
                        }
                        else
                        {
                            PortalDB.Orders.Remove(MyOrd);
                            PortalDB.SaveChanges();
                        }
                    }
                }
            }

            Logger.Info("Order " + OCO.OrderNumber + " Not Added");
            return null;
        }

        private void ProcessOCOs(List<MsgIn_OCO> Lst)
        {
            foreach (var OCO in Lst)
            {
                try
                {
                    if (!ProcessRunning) break;

                    PortalDB = new WMSHostCustomerPortalEntities();

                    Logger.Info("Process OCO Msg - Order " + OCO.OrderNumber);

                    string ProcErr = "";
                    DateTime ProcStartDT = DateTime.Now;

                    Principal ChkPrn = PortalDB.Principals.AsNoTracking().Where(a => a.PrincipalCode == OCO.PrincipalCode).FirstOrDefault();
                    if(ChkPrn != null)
                    {
                        Order Ord = PortalDB.Orders.Where(a => a.RAMOrderNumber == OCO.OrderNumber && a.Deleted == false && a.PrincipalID == ChkPrn.PrincipalID).FirstOrDefault();

                        if (Ord == null)
                        {
                            Ord = AddOCOOrder(OCO, ChkPrn);
                        }

                        if (Ord != null)
                        {
                            OrderOCO OrdOco = new OrderOCO();
                            OrdOco.CreateDate = OCO.RecordDT;
                            OrdOco.OrderCost = OCO.OrderCost;
                            OrdOco.OrderDiscount = OCO.OrderDiscount;
                            OrdOco.OrderNumber = OCO.OrderNumber;
                            OrdOco.OrderVAT = OCO.OrderVAT;
                            OrdOco.PrincipalCode = OCO.PrincipalCode;
                            OrdOco.Priority = OCO.Priority;
                            OrdOco.SiteCode = OCO.SiteCode;
                            OrdOco.UserID = OCO.UserID;
                            OrdOco.Weight = OCO.Weight;

                            PortalDB.OrderOCOes.Add(OrdOco);
                            PortalDB.SaveChanges();

                            List<MsgIn_OCO_LN> OcoLnLst = WmsHostDB.MsgIn_OCO_LN.AsNoTracking().Where(a => a.OCOID == OCO.OCOID).ToList();

                            int ShippedTotalQty = 0;
                            if (OcoLnLst.Count > 0)
                            {
                                List<int> LineNumbers = OcoLnLst.Select(a => a.LineNumber).Distinct().ToList();

                                foreach (var i in LineNumbers)
                                {
                                    List<MsgIn_OCO_LN> LnLst = OcoLnLst.Where(a => a.LineNumber == i).ToList();

                                    int Quantity = 0;
                                    foreach (var n in LnLst)
                                    {
                                        Quantity += (int)n.QuantityPicked;
                                    }

                                    ShippedTotalQty += Quantity;

                                    OrderOCOLine OcoLine = new OrderOCOLine();
                                    OcoLine.Discount = LnLst[0].Discount;
                                    OcoLine.LineNumber = LnLst[0].LineNumber;
                                    OcoLine.OrderOCOID = OrdOco.ID;
                                    OcoLine.ProdCode = LnLst[0].ProdCode;
                                    OcoLine.UnitCost = LnLst[0].UnitCost;
                                    OcoLine.Vat = LnLst[0].Vat;
                                    OcoLine.Quantity = Quantity;

                                    PortalDB.OrderOCOLines.Add(OcoLine);
                                    PortalDB.SaveChanges();

                                    ProductStaging Prod = PortalDB.ProductStagings.Where(a => a.ProdCode == OcoLine.ProdCode && a.Deleted == false && a.PrincipalID == ChkPrn.PrincipalID).FirstOrDefault();
                                    bool AddSerials = false;
                                    if (Prod != null && Prod.Serialised == true) AddSerials = true;

                                    if (AddSerials)
                                    {
                                        List<OrderOCOLineSerial> OrdOcoLnSerList = new List<OrderOCOLineSerial>();
                                        List<Serial> PodSerList = new List<Serial>();
                                        List<string> MySerList = new List<string>();

                                        int SerCount = 0;
                                        foreach (var n in LnLst)
                                        {
                                            if (!String.IsNullOrEmpty(n.SerialNumber.Trim()))
                                            {
                                                OrderOCOLineSerial lnSer = new OrderOCOLineSerial();
                                                lnSer.OrderOCOLineID = OcoLine.ID;
                                                lnSer.SerialNumber = n.SerialNumber;

                                                OrdOcoLnSerList.Add(lnSer);

                                                if (OrdOcoLnSerList.Count == 1000)
                                                {
                                                    PortalDB.OrderOCOLineSerials.AddRange(OrdOcoLnSerList);
                                                    PortalDB.SaveChanges();
                                                    PortalDB.Dispose();
                                                    PortalDB = new WMSHostCustomerPortalEntities();
                                                    OrdOcoLnSerList = new List<OrderOCOLineSerial>();
                                                }

                                                MySerList.Add(n.SerialNumber);
                                            }
                                        }

                                        if (OrdOcoLnSerList.Count > 0)
                                        {
                                            PortalDB.OrderOCOLineSerials.AddRange(OrdOcoLnSerList);
                                        }

                                        List<Serial> ProdSerLst = PortalDB.Serials.AsNoTracking().Where(a => MySerList.Contains(a.SerialNumber) && a.ProductStagingID == Prod.ProductStagingID).ToList();
                                        List<Serial> UniqueLst = new List<Serial>();
                                        List<string> UniqueLstStr = new List<string>();
                                        List<int> AvailbleSerList = new List<int>();

                                        if (ProdSerLst.Count > 0)
                                        {
                                            foreach (var s in MySerList)
                                            {
                                                var ToAdd = ProdSerLst.Where(a => a.SerialNumber == s).LastOrDefault();
                                                if (ToAdd != null)
                                                {
                                                    UniqueLst.Add(ToAdd);
                                                }
                                            }
                                        }

                                        foreach (var u in UniqueLst)
                                        {
                                            if ((u.OrderID == null && u.Status != "SLA DELETE") || u.OrderID == Ord.OrderID)
                                            {
                                                AvailbleSerList.Add(u.ID);
                                                UniqueLstStr.Add(u.SerialNumber);
                                            }
                                        }

                                        foreach (var s in MySerList)
                                        {
                                            if (UniqueLstStr.IndexOf(s) < 0)
                                            {
                                                Serial ProdSer = new Serial();
                                                ProdSer.EventDate = OCO.RecordDT;
                                                ProdSer.ProductStagingID = Prod.ProductStagingID;
                                                ProdSer.SerialNumber = s;
                                                ProdSer.OrderID = Ord.OrderID;
                                                ProdSer.Status = "DISPATCHED";

                                                PodSerList.Add(ProdSer);

                                                if (PodSerList.Count == 1000)
                                                {
                                                    PortalDB.Serials.AddRange(PodSerList);
                                                    PortalDB.SaveChanges();

                                                    PortalDB.Dispose();
                                                    PortalDB = new WMSHostCustomerPortalEntities();
                                                    PodSerList = new List<Serial>();
                                                }
                                            }
                                        }

                                        if (PodSerList.Count > 0)
                                        {
                                            PortalDB.Serials.AddRange(PodSerList);
                                            PortalDB.SaveChanges();
                                            PortalDB.Dispose();
                                            PortalDB = new WMSHostCustomerPortalEntities();
                                        }

                                        if (AvailbleSerList.Count > 0)
                                        {
                                            List<Serial> ProdSerList = PortalDB.Serials.Where(a => AvailbleSerList.Contains(a.ID)).ToList();
                                            int Count = 0;
                                            foreach (var a in ProdSerList)
                                            {
                                                a.Status = "DISPATCHED";
                                                a.EventDate = OCO.RecordDT;
                                                a.OrderID = Ord.OrderID;
                                                Count++;
                                                SerCount++;

                                                if (Count == 1000)
                                                {
                                                    Count = 0;
                                                    PortalDB.SaveChanges();
                                                    PortalDB.Dispose();
                                                    PortalDB = new WMSHostCustomerPortalEntities();
                                                }
                                            }

                                            PortalDB.SaveChanges();
                                            PortalDB.Dispose();
                                            PortalDB = new WMSHostCustomerPortalEntities();
                                        }

                                        int StockQty = Prod.StockQuantity == null ? 0 : (int)Prod.StockQuantity;
                                        Prod.StockQuantity = StockQty - SerCount;
                                        Prod.StockQuantity = Prod.StockQuantity < 0 ? 0 : Prod.StockQuantity;
                                    }
                                    else
                                    {
                                        if (Prod != null)
                                        {
                                            int StockQty = Prod.StockQuantity == null ? 0 : (int)Prod.StockQuantity;
                                            Prod.StockQuantity = StockQty - OcoLine.Quantity;
                                            Prod.StockQuantity = Prod.StockQuantity < 0 ? 0 : Prod.StockQuantity;
                                        }
                                    }

                                    if (Prod != null)
                                    {
                                        if (Prod.StockQuantity < 0) Prod.StockQuantity = 0;
                                        //Prod.EventDate = DateTime.Now;
                                        PortalDB.Entry(Prod).State = System.Data.Entity.EntityState.Modified;
                                        PortalDB.SaveChanges();
                                    }
                                }
                            }

                            Principal Prn = PortalDB.Principals.AsNoTracking().Where(a => a.PrincipalID == Ord.PrincipalID && a.WebServiceIntegration == true).FirstOrDefault();

                            if (Prn != null) Ord.ClientConfirmed = false;

                            if (Ord.Status != "DISPATCHED" && Ord.Status != "ABANDONED") Ord.EventDate = OCO.RecordDT;

                            Ord.Status = ShippedTotalQty > 0 ? "DISPATCHED" : "ABANDONED";
                            PortalDB.Entry(Ord).State = System.Data.Entity.EntityState.Modified;
                            PortalDB.SaveChanges();

                            string[] aEUser = Ord.EventUser.Split('@');
                            string[] aUser = aEUser[0].Split('.');

                            string User = Char.ToUpper(aUser[0][0]) + aUser[0].Substring(1);
                            string Subject = "Order Cancelled Confirmation - RAM Warehouse";

                            string Body = String.Format("Hi {0}\n\nPlease be advised that order {1} (Tracking Number: {2}) has been cancelled by the warehouse.", User, Ord.CustomerOrderNumber, Ord.RAMOrderNumber);

                            if (Ord.Status == "DISPATCHED")
                            {
                                Subject = "Order Shipped Confirmation - RAM Warehouse";
                                Body = String.Format("Hi {0}\n\nPlease be advised that order {1} (Tracking Number: {2}) has been shipped by the warehouse.", User, Ord.CustomerOrderNumber, Ord.RAMOrderNumber);
                            }

                            CheckEmailNotificationRequired((int)Ord.PrincipalID, "ORDER_SHIPPED", "ORDER", Ord.OrderID, Ord.EventUser, Subject, Body);
                        }
                        else
                        {
                            ProcErr = "Order Not Found In The Customer Portal";
                        }
                    }
                    else
                    {
                        ProcErr = "Unknown Principal In The Customer Portal";
                    }

                    MsgIn_OCO UpdateOco = WmsHostDB.MsgIn_OCO.Find(OCO.OCOID);

                    UpdateOco.RecordState = !String.IsNullOrEmpty(ProcErr) ? "PROCESSERROR" : "PROCESSED";
                    UpdateOco.ProcStartDT = ProcStartDT;
                    UpdateOco.ProcEndDT = DateTime.Now;
                    UpdateOco.ProcErrors = ProcErr;
                    WmsHostDB.Entry(UpdateOco).State = System.Data.Entity.EntityState.Modified;
                    WmsHostDB.SaveChanges();

                    PortalDB.Dispose();
                }
                catch(Exception ex)
                {
                    Logger.Error(ex.Message);
                    if (!String.IsNullOrEmpty(ex.InnerException.Message))
                    {
                        Logger.Error(ex.InnerException.Message);
                    }
                }
            }
        }

        private void OCO_Msg_ToProcess_List()
        {
            try
            {
                WmsHostDB = new RAMWMSHOSTEntities();

                List<MsgIn_OCO> Lst = WmsHostDB.MsgIn_OCO.AsNoTracking().Where(a => a.RecordState == "READY").Take(100).OrderByDescending(a => a.RecordDT).ToList();

                ProcessOCOs(Lst);

                WmsHostDB.Dispose();
            }
            catch(Exception ex)
            {
                Logger.Error(ex.Message);
            }
        }
        #endregion

        #region Orders
        private void CheckEmailNotificationRequired(int PrincipalID, string Notification, string RecordType, int RecordID, string EmailAddress, string Subject, string Body)
        {
            try
            {
                if (!ProcessRunning) return;

                if (String.IsNullOrEmpty(EmailAddress) || !SMTPMailer.IsValidEmailAddress(EmailAddress)) return;

                PortalDB = new WMSHostCustomerPortalEntities();

                List<PrincipalAssignedNotification> PrnNotLst = PortalDB.PrincipalAssignedNotifications.Where(a => a.PrincipalID == PrincipalID).ToList();

                foreach (var n in PrnNotLst)
                {
                    EmailNotification EmailNot = PortalDB.EmailNotifications.Find(n.EmailNotificationID);

                    if(EmailNot != null)
                    {
                        if(EmailNot.Name == Notification)
                        {
                            UserEmailNotification UNot = PortalDB.UserEmailNotifications.AsNoTracking().Where(a => a.PrincipalID == PrincipalID && a.RecordType == RecordType && a.RecordID == RecordID && a.EmailAddress == EmailAddress && a.EmailNotificationID == EmailNot.ID).FirstOrDefault();

                            if(UNot == null)
                            {
                                UNot = new UserEmailNotification();
                                UNot.EmailAddress = EmailAddress;
                                UNot.EmailNotificationID = EmailNot.ID;
                                UNot.NotificationSent = false;
                                UNot.PrincipalID = PrincipalID;
                                UNot.RecordID = RecordID;
                                UNot.RecordType = RecordType;
                                UNot.EmailSubject = Subject;
                                UNot.EmailBody = Body;
                                PortalDB.UserEmailNotifications.Add(UNot);
                                PortalDB.SaveChanges();
                            }
                        }
                    }
                }

                PortalDB.Dispose();
            }
            catch(Exception ex)
            {
                Logger.Error(ex.Message);
            }
        }

        private void ProcessEachOrdFound(List<Order> lst)
        {
            foreach (var i in lst)
            {
                try
                {
                    if (!ProcessRunning) break;

                    WmsHostDB = new RAMWMSHOSTEntities();

                    MsgOut_ORD Ord = WmsHostDB.MsgOut_ORD.AsNoTracking().Where(a => a.OrderNumber == i.RAMOrderNumber && a.CustOrderNumber == i.CustomerOrderNumber).ToList().LastOrDefault();

                    if(Ord != null)
                    {
                        if(Ord.RecordState == "PROCESSED" || Ord.RecordState == "PROCESSERROR")
                        {
                            string NewStatus = "WMS SUBMITTED";
                            string SubmitError = "";
                            if(Ord.ProcErrors != "Success")
                            {
                                if (Ord.ProcErrors == "Unable to connect to the remote server")
                                {
                                    WmsHostDB.Dispose();
                                    continue;
                                }

                                NewStatus = "WMS SUBMIT FAILED";
                                SubmitError = Ord.ProcErrors;
                            }

                            PortalDB = new WMSHostCustomerPortalEntities();

                            Order POrd = PortalDB.Orders.Find(i.OrderID);

                            if(POrd != null)
                            {
                                POrd.Status = NewStatus;
                                POrd.RejectError = SubmitError;
                                PortalDB.SaveChanges();
                            }

                            PortalDB.Dispose();

                            string[] aEUser = i.EventUser.Split('@');
                            string[] aUser = aEUser[0].Split('.');

                            string User = Char.ToUpper(aUser[0][0]) + aUser[0].Substring(1);
                            string Subject = "Order Reject Confirmation - RAM Warehouse";

                            string Body = String.Format("Hi {0}\n\nPlease be advised that order {1} (Tracking Number: {3}) has been rejected by the warehouse due to the following error:\n\t{2}.", User, i.CustomerOrderNumber, SubmitError, i.RAMOrderNumber);

                            if (NewStatus == "WMS SUBMITTED")
                            {
                                Subject = "Order Receipt Confirmation - RAM Warehouse";
                                Body = String.Format("Hi {0}\n\nPlease be advised that order {1} (Tracking Number: {2}) has been received by the warehouse.", User, i.CustomerOrderNumber, i.RAMOrderNumber);
                            }

                            CheckEmailNotificationRequired((int)i.PrincipalID, "ORDER_RECEIPT", "ORDER", i.OrderID, i.EventUser, Subject, Body);
                        }
                    }

                    WmsHostDB.Dispose();
                }
                catch(Exception ex)
                {
                    Logger.Error(ex.Message);
                }
            }
        }

        private void ORD_CheckStatus()
        {
            try
            {
                if (!ProcessRunning) return;

                PortalDB = new WMSHostCustomerPortalEntities();

                List<Order> lst = PortalDB.Orders.AsNoTracking().Where(a => a.Deleted == false && (a.Status == null || a.Status == "NEW")).ToList();

                PortalDB.Dispose();

                ProcessEachOrdFound(lst);
            }
            catch(Exception ex)
            {
                Logger.Error(ex.Message);
            }
        }
        #endregion

        #region Order Email Notification
        private void ProcessEachEmailRecord(List<UserEmailNotification> lst)
        {
            foreach (var i in lst)
            {
                try
                {
                    if (!ProcessRunning) break;

                    SMTPMailer Mail = new SMTPMailer();

                    string Host = ConfigurationManager.AppSettings["SMTPHost"];
                    int Port = Convert.ToInt32(ConfigurationManager.AppSettings["SMTPPort"]);
                    string From = ConfigurationManager.AppSettings["FromWMSEmail"];

                    Mail.SendMailMessage(i.EmailAddress, From, "", "", i.EmailSubject, i.EmailBody, new List<string>(), Host, Port);

                    PortalDB = new WMSHostCustomerPortalEntities();

                    UserEmailNotification Rec = PortalDB.UserEmailNotifications.Find(i.ID);

                    if(Rec != null)
                    {
                        Rec.NotificationSent = true;
                        PortalDB.SaveChanges();
                    }

                    PortalDB.Dispose();
                }
                catch(Exception ex)
                {
                    Logger.Error(ex.Message);
                }
            }
        }

        private void ORD_CheckEmailsToSend()
        {
            try
            {
                if (!ProcessRunning) return;

                PortalDB = new WMSHostCustomerPortalEntities();

                List<UserEmailNotification> lst = PortalDB.UserEmailNotifications.AsNoTracking().Where(a => a.NotificationSent == false && a.RecordType == "ORDER").ToList();

                PortalDB.Dispose();

                ProcessEachEmailRecord(lst);

            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message);
            }
        }

        #endregion

        #region Helpers

        private static string GetXMLFromObject(object o)
        {
            StringWriter sw = new StringWriter();
            //XmlTextWriter tw = null;
            try
            {
                XmlSerializer serializer = new XmlSerializer(o.GetType());
                //tw = new XmlTextWriter(sw);
                XmlWriter xw = XmlWriter.Create(sw,
                                              new XmlWriterSettings()
                                              {
                                                  OmitXmlDeclaration = true
                                                   ,
                                                  ConformanceLevel = ConformanceLevel.Auto
                                                   ,
                                                  Indent = true
                                              });
                serializer.Serialize(xw, o);
            }
            catch (Exception)
            {
                //Handle Exception Code
            }
            finally
            {
                sw.Close();
            }
            return sw.ToString();
        }

        private void LogTransaction(object Req, object Res, bool Success, string TrType, string TrErr, string TrID, string PrincipalCode)
        {
            string ReqStr = GetXMLFromObject(Req);
            string ResStr = GetXMLFromObject(Res);

            WMSHostCustomerPortalEntities db = new WMSHostCustomerPortalEntities();

            WebServiceLog Log = new WebServiceLog();
            Log.TransactionError = TrErr;
            Log.TransactionDate = DateTime.Now;
            Log.TransactionID = TrID.ToUpper();
            Log.TransactionResult = !Success ? "FAILED" : "SUCCESS";
            Log.TransactionType = TrType.ToUpper();
            Log.TransactionXml = ReqStr + "\\n" + ResStr;
            Log.PrincipalCode = PrincipalCode.ToUpper();
            Log.Direction = "OUTGOING";
            db.WebServiceLogs.Add(Log);
            db.SaveChanges();

            db.Dispose();
        }

        #endregion

        #region Shipment
        private void ProcessEachAndSend(List<Order> lst)
        {
            foreach (var i in lst)
            {
                try
                {
                    if (!ProcessRunning) break;

                    PortalDB = new WMSHostCustomerPortalEntities();

                    Logger.Info("Process Client Shipment Msg - Order " + i.CustomerOrderNumber);

                    OrderOCO x = PortalDB.OrderOCOes.AsNoTracking().Where(a => a.OrderNumber == i.RAMOrderNumber).ToList().LastOrDefault();

                    if (x != null)
                    {
                        RAMWsSoapClient cl = new RAMWsSoapClient("RAMWsSoap");

                        List<OrderOCOLine> lnlist = PortalDB.OrderOCOLines.AsNoTracking().Where(a => a.OrderOCOID == x.ID).ToList();

                        if (lnlist.Count > 0)
                        {
                            OrderLine[] SoLns = new OrderLine[lnlist.Count];
                            int z = 0;
                            foreach (var y in lnlist)
                            {
                                OrderLine SoLn = new OrderLine();
                                SoLn.LineNumber = y.LineNumber.ToString();
                                SoLn.ProdCode = y.ProdCode;
                                SoLn.Quantity = (int)y.Quantity;
                                SoLn.UnitCost = (double)y.UnitCost;
                                SoLn.UnitDiscountAmount = (double)y.Discount;
                                SoLn.VAT = (double)y.Vat;

                                List<OrderOCOLineSerial> serlist = PortalDB.OrderOCOLineSerials.AsNoTracking().Where(a => a.OrderOCOLineID == y.ID).ToList();

                                SoLn.SerialNumbers = new string[serlist.Count];
                                int b = 0;
                                foreach (var a in serlist)
                                {
                                    SoLn.SerialNumbers[b++] = a.SerialNumber;
                                }

                                SoLns[z++] = SoLn;
                            }

                            try
                            {
                                SOShipmentRequest Req = new SOShipmentRequest();
                                Req.Lines = SoLns;
                                Req.OrderDiscount = (double)x.OrderDiscount;
                                Req.OrderNumber = i.CustomerOrderNumber;
                                Req.OrderTotal = (double)x.OrderCost;
                                Req.OrderVAT = (double)x.OrderVAT;
                                Req.PrincipalCode = x.PrincipalCode;
                                Req.Priority = x.Priority;
                                Req.UserID = x.UserID;
                                Req.Weight = (double)x.Weight;

                                Response Res = cl.SOShipment(x.PrincipalCode, i.CustomerOrderNumber, x.Priority, x.UserID, (double)x.Weight, (double)x.OrderCost, (double)x.OrderDiscount, (double)x.OrderVAT, SoLns);

                                Logger.Info(Res);

                                Logger.Info("Process Client Shipment Msg - Order " + i.CustomerOrderNumber + " | Response: " + Res.success);

                                LogTransaction(Req, Res, Res.success, "SO Shipment", Res.reasontext, i.CustomerOrderNumber, x.PrincipalCode);

                                if (Res.success)
                                {
                                    i.ClientConfirmed = true;
                                    i.EventDate = DateTime.Now;
                                    PortalDB.Entry(i).State = System.Data.Entity.EntityState.Modified;
                                    PortalDB.SaveChanges();
                                }
                            }
                            catch(Exception ex)
                            {
                                Logger.Error(ex.Message);
                            }
                        }
                    }
                    else
                    {
                        i.ClientConfirmed = null;
                    }

                    PortalDB.Dispose();
                }
                catch(Exception ex)
                {
                    Logger.Error(ex.Message);
                }
            }
        }

        private void ClientShipmentSend()
        {
            try
            {
                PortalDB = new WMSHostCustomerPortalEntities();

                List<Principal> PrnList = PortalDB.Principals.AsNoTracking().Where(a => a.WebServiceIntegration == true).ToList();

                PortalDB.Dispose();

                foreach (var i in PrnList)
                {
                    if (!ProcessRunning) break;

                    PortalDB = new WMSHostCustomerPortalEntities();

                    List<Order> lst = PortalDB.Orders.Where(a => a.Status == "DISPATCHED" && a.ClientConfirmed == false && a.PrincipalID == i.PrincipalID).Take(100).ToList();

                    PortalDB.Dispose();

                    ProcessEachAndSend(lst);
                }
            }
            catch(Exception ex)
            {
                Logger.Error(ex.Message);
            }
        }
        #endregion

        #region Main
        private void RunService()
        {
            Logger.Info("OCO Sender Service Started");

            while (ProcessRunning)
            {
                Logger.Info("Processing Submitted ORD Msgs");
                ORD_CheckStatus();

                Logger.Info("Processing ORD Emails To Send Msgs");
                ORD_CheckEmailsToSend();

                Logger.Info("Processing OCO Msgs");
                OCO_Msg_ToProcess_List();

                Logger.Info("Processing SO Shipment Msgs");
                ClientShipmentSend();

                Logger.Info(" System inteval of " + (30000 / 1000) + " seconds starting ");
                System.Threading.Thread.Sleep(30000);
                Logger.Info(" System inteval of " + (30000 / 1000) + " seconds ended ");
            }

            Logger.Info("OCO Sender Service Stopped");
        }
        #endregion

        protected override void OnStart(string[] args)
        {
        }

        protected override void OnStop()
        {
            ProcessRunning = false;
        }
    }
}
