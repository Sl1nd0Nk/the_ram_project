//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ServicesWmsHostDBModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class MsgIn_SLA
    {
        public int SLAID { get; set; }
        public System.DateTime RecordDT { get; set; }
        public string RecordState { get; set; }
        public string RecordSource { get; set; }
        public int ProcAttempts { get; set; }
        public Nullable<System.DateTime> ProcStartDT { get; set; }
        public Nullable<System.DateTime> ProcEndDT { get; set; }
        public string ProcErrors { get; set; }
        public string SiteCode { get; set; }
        public string PrincipalCode { get; set; }
        public string ProdCode { get; set; }
        public Nullable<int> QuantityChange { get; set; }
        public string ReasonCode { get; set; }
        public string AvailabilityState { get; set; }
        public Nullable<int> SerialCount { get; set; }
        public System.Guid rowguid { get; set; }
    }
}
