﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Core.Objects;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml;
using System.Xml.Serialization;
using HostClientServices.SharedServices_LocationsWS;
using HostClientServices.WMSHost;

namespace HostClientServices
{
    /// <summary>
    /// Summary description for ClientServices
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class ClientServices : System.Web.Services.WebService
    {
        #region Classes

        public class Response
        {
            public bool success { get; set; }
            public int reasoncode { get; set; }
            public string reasontext { get; set; }
        }

        public class POLine
        {
            public int OrderLineNo { get; set; }
            [XmlElementAttribute(IsNullable = true)]
            public string ProdCode { get; set; }
            public int ExpectedQuantity { get; set; }
            public decimal UnitCost { get; set; }
        }

        public class ReturnLine
        {
            [XmlElementAttribute(IsNullable = true)]
            public string ProdCode { get; set; }
            [XmlElementAttribute(IsNullable = true)]
            public string LineNumber { get; set; }
            [XmlElementAttribute(IsNullable = true)]
            public string LineType { get; set; }
            [XmlElementAttribute(IsNullable = true)]
            public string LineText { get; set; }
            public int Quantity { get; set; }
        }

        public class OrderLine
        {
            [XmlElementAttribute(IsNullable = true)]
            public string ProdCode { get; set; }
            [XmlElementAttribute(IsNullable = true)]
            public string LineNumber { get; set; }
            [XmlElementAttribute(IsNullable = true)]
            public string LineType { get; set; }
            public string LineText { get; set; }
            public int Quantity { get; set; }
            public decimal UnitCost { get; set; }
            public decimal VAT { get; set; }
            public decimal UnitDiscountAmount { get; set; }
        }

        #endregion

        #region Helpers

        private static string GetXMLFromObject(object o)
        {
            StringWriter sw = new StringWriter();
            //XmlTextWriter tw = null;
            try
            {
                XmlSerializer serializer = new XmlSerializer(o.GetType());
                //tw = new XmlTextWriter(sw);
                XmlWriter xw = XmlWriter.Create(sw,
                                              new XmlWriterSettings()
                                              {
                                                  OmitXmlDeclaration = true
                                                   ,
                                                  ConformanceLevel = ConformanceLevel.Auto
                                                   ,
                                                  Indent = true
                                              });
                serializer.Serialize(xw, o);
            }
            catch (Exception)
            {
                //Handle Exception Code
            }
            finally
            {
                sw.Close();
            }
            return sw.ToString();
        }

        private void LogTransaction(object Req, object Res, bool Success, string TrType, string TrErr, string TrID, string PrincipalCode)
        {
            string ReqStr = GetXMLFromObject(Req);
            string ResStr = GetXMLFromObject(Res);

            WMSHostCustomerPortalEntities db = new WMSHostCustomerPortalEntities();

            WebServiceLog Log = new WebServiceLog();
            Log.TransactionError = TrErr;
            Log.TransactionDate = DateTime.Now;
            Log.TransactionID = TrID.ToUpper();
            Log.TransactionResult = !Success ? "FAILED" : "SUCCESS";
            Log.TransactionType = TrType.ToUpper();
            Log.TransactionXml = ReqStr + "\\n" + ResStr;
            Log.PrincipalCode = PrincipalCode.ToUpper();
            Log.Direction = "INCOMING";
            db.WebServiceLogs.Add(Log);
            db.SaveChanges();

            db.Dispose();
        }

        #endregion

        #region Customer

        public class CCustomerUpdate
        {
            public string PrincipalCode { get; set; }
            public string CustomerID { get; set; }
            public string CustomerName { get; set; }
            public string ContactPerson { get; set; }
            public string IDNumber { get; set; }
            public string TelephoneNo { get; set; }
            public string CellNo { get; set; }
            public string FaxNo { get; set; }
            public string EmailAddress { get; set; }
            public string StreetAddress1 { get; set; }
            public string StreetAddress2 { get; set; }
            public string StreetAddress3 { get; set; }
            public string StreetAddress4 { get; set; }
            public string PostalCode { get; set; }
            public string LatsAndLongs { get; set; }
            public string DebtorsCode { get; set; }
            public string KYC { get; set; }
            public string CourierName { get; set; }
            public string CourierService { get; set; }
            public bool InsuranceRequired { get; set; }
            public bool IsActive { get; set; }
            public bool IsShipper { get; set; }
        }

        private string ValidateCustomerMandatoryFields(string PrincipalCode, string CustomerID, string CustomerName, string ContactPerson,
                                                       string TelephoneNo, string CellNo, string FaxNo, string EmailAddress, string StreetAddress1,
                                                       string StreetAddress2, string StreetAddress3, string StreetAddress4, string PostalCode)
        {
            string str = " Not Supplied";

            if (String.IsNullOrEmpty(PrincipalCode)) return "PricipalCode" + str;

            if (String.IsNullOrEmpty(CustomerID)) return "CustomerID" + str;

            if (String.IsNullOrEmpty(CustomerName)) return "CustomerName" + str;

            if (String.IsNullOrEmpty(ContactPerson)) return "ContactPerson" + str;

            if (String.IsNullOrEmpty(TelephoneNo)) return "TelephoneNo" + str;

            if (String.IsNullOrEmpty(CellNo)) return "CellNo" + str;

            if (String.IsNullOrEmpty(FaxNo)) return "FaxNo" + str;

            if (String.IsNullOrEmpty(EmailAddress)) return "EmailAddress" + str;

            if (String.IsNullOrEmpty(StreetAddress1)) return "StreetAddress1" + str;

            if (String.IsNullOrEmpty(StreetAddress2)) return "StreetAddress2" + str;

            //if (String.IsNullOrEmpty(StreetAddress3)) return "StreetAddress3" + str;

            //if (String.IsNullOrEmpty(StreetAddress4)) return "StreetAddress4" + str;

            if (String.IsNullOrEmpty(PostalCode)) return "PostalCode" + str;

            return "";
        }

        private string ValidateCustomerFieldLengths(string CustomerID, string CustomerName, string ContactPerson, string IDNumber,
                                               string TelephoneNo, string CellNo, string FaxNo, string EmailAddress, string StreetAddress1,
                                               string StreetAddress2, string StreetAddress3, string StreetAddress4, string PostalCode,
                                               string LatsAndLongs, string DebtorsCode, string KYC, string CourierName, string CourierService)
        {
            string str = "Invalid Field Length ";

            if (CustomerID.Length > 20) return str + "CustomerID";

            if (CustomerName.Length > 50) return str + "CustomerName";

            if (ContactPerson.Length > 50) return str + "ContactPerson";

            if (!String.IsNullOrEmpty(IDNumber) && IDNumber.Length > 13) return str + "IDNumber";

            if (TelephoneNo.Length > 20) return str + "TelephoneNo";

            if (CellNo.Length > 20) return str + "CellNo";

            if (FaxNo.Length > 20) return str + "FaxNo";

            if (EmailAddress.Length > 100) return str + "EmailAddress";

            if (StreetAddress1.Length > 40) return str + "StreetAddress1";

            if (StreetAddress2.Length > 40) return str + "StreetAddress2";

            if (!String.IsNullOrEmpty(StreetAddress3) && StreetAddress3.Length > 40) return str + "StreetAddress3";

            if (!String.IsNullOrEmpty(StreetAddress4) && StreetAddress4.Length > 40) return str + "StreetAddress4";

            if (PostalCode.Length > 4) return str + "PostalCode";

            if (!String.IsNullOrEmpty(LatsAndLongs) && LatsAndLongs.Length > 22) return str + "LatsAndLongs";

            if (!String.IsNullOrEmpty(DebtorsCode) && DebtorsCode.Length > 30) return str + "DebtorsCode";

            if (!String.IsNullOrEmpty(KYC) && KYC.Length > 3) return str + "KYC";

            if (!String.IsNullOrEmpty(CourierName) && CourierName.Length > 30) return str + "CourierName";

            if (!String.IsNullOrEmpty(CourierService) && CourierService.Length > 3) return str + "CourierService";

            return "";
        }

        [WebMethod]
        public Response CustomerUpdate([XmlElementAttribute(IsNullable = true)]string PrincipalCode,
                                       [XmlElementAttribute(IsNullable = true)]string CustomerID,
                                       [XmlElementAttribute(IsNullable = true)]string CustomerName,
                                       [XmlElementAttribute(IsNullable = true)]string ContactPerson,
                                       string IDNumber,
                                       [XmlElementAttribute(IsNullable = true)]string TelephoneNo,
                                       [XmlElementAttribute(IsNullable = true)]string CellNo,
                                       [XmlElementAttribute(IsNullable = true)]string FaxNo,
                                       [XmlElementAttribute(IsNullable = true)]string EmailAddress,
                                       [XmlElementAttribute(IsNullable = true)]string StreetAddress1,
                                       [XmlElementAttribute(IsNullable = true)]string StreetAddress2,
                                       [XmlElementAttribute(IsNullable = true)]string StreetAddress3,
                                       [XmlElementAttribute(IsNullable = true)]string StreetAddress4,
                                       [XmlElementAttribute(IsNullable = true)]string PostalCode,
                                       string LatsAndLongs,
                                       string DebtorsCode,
                                       string KYC,
                                       string CourierName,
                                       string CourierService,
                                       bool InsuranceRequired,
                                       bool IsActive,
                                       bool IsShipper)
        {
            CCustomerUpdate Rec = new CCustomerUpdate
            {
                CellNo = CellNo,
                ContactPerson = ContactPerson,
                CourierName = CourierName,
                CourierService = CourierService,
                CustomerID = CustomerID,
                CustomerName = CustomerName,
                DebtorsCode = DebtorsCode,
                EmailAddress = EmailAddress,
                FaxNo = FaxNo,
                IDNumber = IDNumber,
                InsuranceRequired = InsuranceRequired,
                IsActive = IsActive,
                IsShipper = IsShipper,
                KYC = KYC,
                LatsAndLongs = LatsAndLongs,
                PostalCode = PostalCode,
                PrincipalCode = PrincipalCode,
                StreetAddress1 = StreetAddress1,
                StreetAddress2 = StreetAddress2,
                StreetAddress3 = StreetAddress3,
                StreetAddress4 = StreetAddress4,
                TelephoneNo = TelephoneNo
            };

            WMSHostCustomerPortalEntities db = new WMSHostCustomerPortalEntities();

            Principal Prn;
            List<Zone> Zones;
            usp_Customer_Save_Result output;
            LinkedCustomer LC;
            string ZoneID = "";

            Response Res = new Response();
            Res.success = true;
            Res.reasoncode = 0;
            Res.reasontext = "";

            string Err = ValidateCustomerMandatoryFields(PrincipalCode, CustomerID, CustomerName, ContactPerson,
                                                         TelephoneNo, CellNo, FaxNo, EmailAddress, StreetAddress1,
                                                         StreetAddress2, StreetAddress3, StreetAddress4, PostalCode);

            if (!String.IsNullOrEmpty(Err))
            {
                Res.success = false;
                Res.reasoncode = 3;
                Res.reasontext = Err;

                LogTransaction(Rec, Res, Res.success, "Customer", Res.reasontext, CustomerName, PrincipalCode);

                db.Dispose();
                return Res;
            }

            Err = ValidateCustomerFieldLengths(CustomerID, CustomerName, ContactPerson, IDNumber,
                                               TelephoneNo, CellNo, FaxNo, EmailAddress, StreetAddress1,
                                               StreetAddress2, StreetAddress3, StreetAddress4, PostalCode,
                                               LatsAndLongs, DebtorsCode, KYC, CourierName, CourierService);

            if (!String.IsNullOrEmpty(Err))
            {
                Res.success = false;
                Res.reasoncode = 1;
                Res.reasontext = Err;

                LogTransaction(Rec, Res, Res.success, "Customer", Res.reasontext, CustomerName, PrincipalCode);

                db.Dispose();
                return Res;
            }

            try
            {
                Prn = db.Principals.AsNoTracking().Where(a => a.PrincipalCode == PrincipalCode).FirstOrDefault();
                if (Prn == null)
                {
                    Res.success = false;
                    Res.reasoncode = 4;
                    Res.reasontext = "Invalid Pricipal Code";

                    LogTransaction(Rec, Res, Res.success, "Customer", Res.reasontext, CustomerName, PrincipalCode);

                    db.Dispose();
                    return Res;
                }

                string Surb = StreetAddress3;
                int index = Surb.IndexOf("'");
                if (index >= 0)
                {
                    Surb = Surb.Insert(index, "'");
                }

                Surb = Surb.Trim();
                string Area = StreetAddress4;
                Area = Area.Trim();

                Zones = db.Zones.AsNoTracking().Where(a => a.Suburb.StartsWith(Surb) && a.Area == Area).Take(20).ToList();
                if (Zones.Count <= 0)
                {
                    Zones = db.Zones.AsNoTracking().Where(a => a.Suburb.StartsWith(Surb)).Take(20).ToList();
                    if (Zones.Count <= 0)
                    {
                        Res.success = false;
                        Res.reasoncode = 5;
                        Res.reasontext = "Invalid Address";

                        LogTransaction(Rec, Res, Res.success, "Customer", Res.reasontext, CustomerName, PrincipalCode);

                        db.Dispose();
                        return Res;
                    }
                }

                foreach (var i in Zones)
                {
                    string postcode = i.PostalCode;
                    if (postcode.Length < 4)
                    {
                        postcode.PadLeft(4, '0');
                    }

                    if (postcode == PostalCode)
                    {
                        ZoneID = i.ZoneID;
                        break;
                    }
                }

                if (String.IsNullOrEmpty(ZoneID))
                {
                    ZoneID = Zones[0].ZoneID;
                }

                ObjectParameter myres = new ObjectParameter("Result", typeof(string));
                ObjectParameter success = new ObjectParameter("Success", typeof(bool));
                ObjectParameter newCustomerID = new ObjectParameter("NewCustomerID", typeof(string));
                ObjectParameter ThisCustomerID = new ObjectParameter("CustomerID", CustomerID);

                ObjectResult<usp_Customer_Save_Result> Result = db.usp_Customer_Save(true, Prn.PrincipalID, ThisCustomerID, "", Prn.CustomerGroupID, CustomerName, TelephoneNo,
                                               FaxNo, CellNo, ContactPerson, EmailAddress, StreetAddress1, StreetAddress2,
                                               ZoneID, IsShipper, IsActive, "", "", "", false, myres, success, newCustomerID);

                output = new usp_Customer_Save_Result();
                foreach (var i in Result)
                {
                    output = i;
                }

                LC = db.LinkedCustomers.Where(a => a.RAMCustomerID == CustomerID).FirstOrDefault();
                if (LC != null)
                {
                    LC.DebtorsCode = String.IsNullOrEmpty(DebtorsCode) ? "" : DebtorsCode.ToUpper();
                    LC.KYC = String.IsNullOrEmpty(KYC) ? "" : KYC.ToUpper();
                    LC.CourierName = String.IsNullOrEmpty(CourierName) ? "" : CourierName.ToUpper();
                    LC.CourierService = String.IsNullOrEmpty(CourierService) ? "" : CourierService.ToUpper();
                    LC.InsuranceRequired = InsuranceRequired;

                    db.Entry(LC).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                }

                LogTransaction(Rec, Res, Res.success, "Customer", Res.reasontext, CustomerName, PrincipalCode);
            }
            catch (Exception ex)
            {
                Res.success = false;
                Res.reasoncode = 5;
                Res.reasontext = "Internal Server Error";

                LogTransaction(Rec, Res, Res.success, "Customer", ex.Message, CustomerName, PrincipalCode);
            }

            db.Dispose();
            return Res;
        }

        #endregion

        #region ItemUpdate

        public class CItemUpdate
        {
            public string PrincipalCode { get; set; }
            public string ProdCode { get; set; }
            public string EANCode { get; set; }
            public string ShortDesc { get; set; }
            public string LongDesc { get; set; }
            public bool Serialised { get; set; }
            public decimal UnitCost { get; set; }
            public decimal SalesPrice { get; set; }
            public bool ExpiryProduct { get; set; }
            public int LeadTimeDays { get; set; }
            public bool ProdActive { get; set; }
        }

        private string ValidateProductMandatoryFields(string PrincipalCode, string ProdCode, string ShortDesc, string LongDesc)
        {
            string str = " Not Supplied";

            if (String.IsNullOrEmpty(PrincipalCode)) return "PricipalCode" + str;

            if (String.IsNullOrEmpty(ProdCode)) return "ProdCode" + str;

            if (String.IsNullOrEmpty(ShortDesc)) return "ShortDesc" + str;

            if (String.IsNullOrEmpty(LongDesc)) return "LongDesc" + str;

            return "";
        }

        private string ValidateProductFieldLengths(string ProdCode, string EANCode, string ShortDesc, string LongDesc)
        {
            string str = "Invalid Field Length ";

            if (ProdCode.Length > 20) return str + "ProdCode";

            if (!String.IsNullOrEmpty(EANCode) && EANCode.Length > 20) return str + "EANCode";

            if (ShortDesc.Length > 20) return str + "ShortDesc";

            if (LongDesc.Length > 40) return str + "LongDesc";

            return "";
        }

        [WebMethod]
        public Response ItemUpdate([XmlElementAttribute(IsNullable = true)]string PrincipalCode,
                                   [XmlElementAttribute(IsNullable = true)]string ProdCode,
                                   string EANCode,
                                   [XmlElementAttribute(IsNullable = true)]string ShortDesc,
                                   [XmlElementAttribute(IsNullable = true)]string LongDesc,
                                   bool Serialised,
                                   decimal UnitCost,
                                   decimal SalesPrice,
                                   bool ExpiryProduct,
                                   int LeadTimeDays,
                                   bool ProdActive)
        {
            CItemUpdate Itm = new CItemUpdate
            {
                PrincipalCode = PrincipalCode,
                EANCode = EANCode,
                ExpiryProduct = ExpiryProduct,
                LeadTimeDays = LeadTimeDays,
                LongDesc = LongDesc,
                ProdActive = ProdActive,
                ProdCode = ProdCode,
                SalesPrice = SalesPrice,
                Serialised = Serialised,
                ShortDesc = ShortDesc,
                UnitCost = UnitCost
            };

            WMSHostCustomerPortalEntities db = new WMSHostCustomerPortalEntities();

            Principal Prn;

            Response Res = new Response();
            Res.success = true;
            Res.reasoncode = 0;
            Res.reasontext = "";

            string Err = ValidateProductMandatoryFields(PrincipalCode, ProdCode, ShortDesc, LongDesc);
            if (!String.IsNullOrEmpty(Err))
            {
                Res.success = false;
                Res.reasoncode = 3;
                Res.reasontext = Err;

                LogTransaction(Itm, Res, Res.success, "Product", Res.reasontext, ProdCode, PrincipalCode);

                db.Dispose();
                return Res;
            }

            Err = ValidateProductFieldLengths(ProdCode, EANCode, ShortDesc, LongDesc);
            if (!String.IsNullOrEmpty(Err))
            {
                Res.success = false;
                Res.reasoncode = 1;
                Res.reasontext = Err;

                LogTransaction(Itm, Res, Res.success, "Product", Res.reasontext, ProdCode, PrincipalCode);

                db.Dispose();
                return Res;
            }

            try
            {
                Prn = db.Principals.AsNoTracking().Where(a => a.PrincipalCode == PrincipalCode).FirstOrDefault();
                if (Prn == null)
                {
                    Res.success = false;
                    Res.reasoncode = 4;
                    Res.reasontext = "Invalid Pricipal Code";

                    LogTransaction(Itm, Res, Res.success, "Product", Res.reasontext, ProdCode, PrincipalCode);

                    db.Dispose();
                    return Res;
                }

                ProductStaging Prod = db.ProductStagings.AsNoTracking().Where(a => a.PrincipalID == Prn.PrincipalID && a.ProdCode == ProdCode).FirstOrDefault();
                bool Update = true;

                if (Prod == null) Update = false;

                if (!String.IsNullOrEmpty(EANCode))
                {
                    Prod = db.ProductStagings.AsNoTracking().Where(a => a.PrincipalID == Prn.PrincipalID && a.EANCode == EANCode && a.ProdCode != ProdCode).FirstOrDefault();
                    if (Prod != null)
                    {
                        Res.success = false;
                        Res.reasoncode = 5;
                        Res.reasontext = "Duplicate EAN Code";

                        LogTransaction(Itm, Res, Res.success, "Product", Res.reasontext, ProdCode, PrincipalCode);

                        db.Dispose();
                        return Res;
                    }
                }

                if (!Update)
                {
                    Prod = new ProductStaging();
                    Prod.PrincipalID = Prn.PrincipalID;
                    Prod.ProdActive = ProdActive;
                    Prod.ProdCode = ProdCode.ToUpper();
                    Prod.LongDesc = LongDesc.ToUpper();
                    Prod.ShortDesc = ShortDesc.ToUpper();
                    Prod.EANCode = EANCode.ToUpper();
                    Prod.Serialised = Serialised;
                    Prod.Deleted = false;
                    Prod.EventDate = DateTime.Now;
                    Prod.EventTerminal = "";
                    Prod.EventUser = "Webservice";
                    Prod.ExpiryProduct = ExpiryProduct;
                    Prod.LeadTimeDays = LeadTimeDays;
                    Prod.SalesPrice = (decimal)SalesPrice;

                    db.ProductStagings.Add(Prod);
                    db.SaveChanges();
                }
                else
                {
                    Prod = db.ProductStagings.Where(a => a.PrincipalID == Prn.PrincipalID && a.ProdCode == ProdCode).FirstOrDefault();

                    Prod.ProdActive = ProdActive;
                    Prod.Deleted = false;
                    Prod.EANCode = EANCode.ToUpper();
                    Prod.ProdCode = ProdCode.ToUpper();
                    Prod.EventDate = DateTime.Now;
                    Prod.EventTerminal = "";
                    Prod.EventUser = "Webservice";
                    Prod.ExpiryProduct = ExpiryProduct;
                    Prod.LeadTimeDays = LeadTimeDays;
                    Prod.LongDesc = LongDesc.ToUpper();
                    Prod.ShortDesc = ShortDesc.ToUpper();
                    Prod.SalesPrice = (decimal)SalesPrice;
                    Prod.Serialised = Serialised;

                    db.Entry(Prod).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                }

                LogTransaction(Itm, Res, Res.success, "Product", Res.reasontext, ProdCode, PrincipalCode);
            }
            catch (Exception ex)
            {
                Res.success = false;
                Res.reasoncode = 5;
                Res.reasontext = "Internal Server Error";

                LogTransaction(Itm, Res, Res.success, "Product", ex.Message, ProdCode, PrincipalCode);
            }

            db.Dispose();
            return Res;
        }

        #endregion

        #region PO

        public class CPurchaseOrder
        {
            public string PrincipalCode { get; set; }
            public string SupplierName { get; set; }
            public string PORef { get; set; }
            public DateTime PODate { get; set; }
            public DateTime ExpectedDeliveryDateTime { get; set; }
            public List<POLine> Line { get; set; }
        }

        private void ValidatePOLines(List<POLine> Line, int PrincipalID, out Response Res)
        {
            WMSHostCustomerPortalEntities db = new WMSHostCustomerPortalEntities();

            Res = new Response();
            Res.success = true;
            Res.reasontext = "";

            if (Line.Count <= 0)
            {
                Res.success = false;
                Res.reasontext = "No PO Lines Supplied";
                Res.reasoncode = 5;
                return;
            }

            foreach (var i in Line)
            {
                if (i.OrderLineNo <= 0)
                {
                    Res.success = false;
                    Res.reasontext = "Invalid Order Line Supplied";
                    Res.reasoncode = 6;
                    return;
                }

                if (String.IsNullOrEmpty(i.ProdCode))
                {
                    Res.success = false;
                    Res.reasontext = "Invalid Product Code at line " + i.OrderLineNo;
                    Res.reasoncode = 7;
                    return;
                }

                ProductStaging Prod = db.ProductStagings.AsNoTracking().Where(a => a.ProdCode == i.ProdCode && a.PrincipalID == PrincipalID).FirstOrDefault();

                if (Prod == null)
                {
                    Res.success = false;
                    Res.reasontext = "Invalid Product " + i.ProdCode + " For Principal";
                    Res.reasoncode = 8;
                    return;
                }

                if (i.ExpectedQuantity <= 0)
                {
                    Res.success = false;
                    Res.reasontext = "Invalid Expected Quantity for line " + i.OrderLineNo;
                    Res.reasoncode = 9;
                    return;
                }
            }
        }

        [WebMethod]
        public Response PurchaseOrder([XmlElementAttribute(IsNullable = true)]string PrincipalCode,
                                     string SupplierName,
                                     [XmlElementAttribute(IsNullable = true)]string PORef,
                                     DateTime PODate,
                                     DateTime ExpectedDeliveryDateTime,
                                     [XmlElementAttribute(IsNullable = true)] List<POLine> Line)
        {
            CPurchaseOrder Rec = new CPurchaseOrder
            {
                ExpectedDeliveryDateTime = ExpectedDeliveryDateTime,
                Line = Line,
                PODate = PODate,
                PORef = PORef,
                PrincipalCode = PrincipalCode,
                SupplierName = SupplierName
            };

            WMSHostCustomerPortalEntities db = new WMSHostCustomerPortalEntities();

            Principal Prn;

            Response Res = new Response();
            Res.success = true;
            Res.reasoncode = 0;
            Res.reasontext = "";

            if (String.IsNullOrEmpty(PrincipalCode))
            {
                Res.success = false;
                Res.reasoncode = 1;
                Res.reasontext = "No Principal Code Supplied";

                LogTransaction(Rec, Res, Res.success, "PO", Res.reasontext, PORef, PrincipalCode);

                db.Dispose();
                return Res;
            }

            try
            {
                Prn = db.Principals.AsNoTracking().Where(a => a.PrincipalCode == PrincipalCode).FirstOrDefault();
                if (Prn == null)
                {
                    Res.success = false;
                    Res.reasoncode = 2;
                    Res.reasontext = "Invalid Pricipal Code";

                    LogTransaction(Rec, Res, Res.success, "PO", Res.reasontext, PORef, PrincipalCode);

                    db.Dispose();
                    return Res;
                }

                //ProductStaging Prod = db.ProductStagings.AsNoTracking().Where(a => a.PrincipalID == Prn.PrincipalID && a.ProdCode == ProdCode).FirstOrDefault();

                if (String.IsNullOrEmpty(PORef))
                {
                    Res.success = false;
                    Res.reasoncode = 3;
                    Res.reasontext = "No PO Reference Supplied";

                    LogTransaction(Rec, Res, Res.success, "PO", Res.reasontext, PORef, PrincipalCode);

                    db.Dispose();
                    return Res;
                }

                ValidatePOLines(Line, Prn.PrincipalID, out Res);
                if (!Res.success)
                {
                    LogTransaction(Rec, Res, Res.success, "PO", Res.reasontext, PORef, PrincipalCode);

                    db.Dispose();
                    return Res;
                }

                InboundMaster PO = new InboundMaster();
                PO.PORef = PORef.ToUpper();
                PO.PODate = PODate == null ? DateTime.Now : PODate;
                PO.EventDate = DateTime.Now;
                PO.ExpectedDeliveryDateTime = ExpectedDeliveryDateTime == null ? DateTime.Now : ExpectedDeliveryDateTime;
                PO.PrincipalID = Prn.PrincipalID;
                PO.RecordSource = "Webservice";
                PO.SiteCode = "001";
                PO.EventUser = "webservice";
                PO.EventTerminal = "";
                PO.Deleted = false;
                PO.SupplierName = String.IsNullOrEmpty(SupplierName) ? "" : SupplierName.ToUpper();

                db.InboundMasters.Add(PO);
                db.SaveChanges();

                List<InboundMasterLineItem> LnLst = new List<InboundMasterLineItem>();
                foreach (var i in Line)
                {
                    ProductStaging Prod = db.ProductStagings.AsNoTracking().Where(a => a.ProdCode == i.ProdCode && a.PrincipalID == Prn.PrincipalID).FirstOrDefault();
                    InboundMasterLineItem ln = new InboundMasterLineItem();
                    ln.ExpectedQuantity = i.ExpectedQuantity;
                    ln.InboundMasterID = PO.InboundMasterID;
                    ln.InboundMasterLineItemID = 0;
                    ln.ProductStagingID = Prod.ProductStagingID;
                    ln.UnitCost = (double)i.UnitCost;
                    ln.Deleted = false;
                    ln.EventDate = DateTime.Now;
                    ln.EventTerminal = "";
                    ln.EventUser = "webservice";

                    LnLst.Add(ln);
                }

                if (LnLst.Count > 0)
                {
                    db.InboundMasterLineItems.AddRange(LnLst);
                }
                else
                {
                    db.InboundMasters.Remove(PO);

                    Res.success = false;
                    Res.reasoncode = 4;
                    Res.reasontext = "Internal Server Error";
                }

                db.SaveChanges();
                LogTransaction(Rec, Res, Res.success, "PO", Res.reasontext, PORef, PrincipalCode);
            }
            catch (Exception ex)
            {
                Res.success = false;
                Res.reasoncode = 4;
                Res.reasontext = "Internal Server Error";

                LogTransaction(Rec, Res, Res.success, "PO", ex.Message, PORef, PrincipalCode);
            }

            db.Dispose();
            return Res;
        }

        #endregion

        #region CustomerReturn

        public class CCustomerReturn
        {
            public string PrincipalCode { get; set; }
            public string CustomerID { get; set; }
            public string BillingCustomerID { get; set; }
            public string ReturnReferenceNumber { get; set; }
            public string ReturnWaybillNumber { get; set; }
            public string CustomerOrderNumber { get; set; }
            public string ReturnType { get; set; }
            public List<ReturnLine> Line { get; set; }
        }

        private string ValidateCustomerReturnMandatoryFields(string CustomerID, string BillingCustomerID, string PrincipalCode, string ReturnReferenceNumber)
        {
            string str = " Not Supplied";

            if (String.IsNullOrEmpty(PrincipalCode)) return "PricipalCode" + str;

            if (String.IsNullOrEmpty(CustomerID)) return "CustomerID" + str;

            if (String.IsNullOrEmpty(BillingCustomerID)) return "BillingCustomerID" + str;

            if (String.IsNullOrEmpty(ReturnReferenceNumber)) return "ReturnReferenceNumber" + str;

            return "";
        }

        private string ValidateCustomerReturnFieldLengths(string CustomerID, string BillingCustomerID, string ReturnReferenceNumber, string ReturnWaybillNumber, string CustomerOrderNumber, string ReturnType)
        {
            string str = "Invalid Field Length ";

            if (CustomerID.Length > 20) return str + "CustomerID";

            if (BillingCustomerID.Length > 20) return str + "BillingCustomerID";

            if (ReturnReferenceNumber.Length > 12) return str + "ReturnReferenceNumber";

            if (!String.IsNullOrEmpty(ReturnWaybillNumber) && ReturnWaybillNumber.Length > 50) return str + "ReturnWaybillNumber";

            if (!String.IsNullOrEmpty(CustomerOrderNumber) && CustomerOrderNumber.Length > 20) return str + "CustomerOrderNumber";

            if (!String.IsNullOrEmpty(ReturnType) && ReturnType.Length > 20) return str + "ReturnType";

            return "";
        }

        private void ValidateReturnOrderLines(List<ReturnLine> Line, int PrincipalID, out Response Res)
        {
            WMSHostCustomerPortalEntities db = new WMSHostCustomerPortalEntities();
            Res = new Response();
            Res.success = true;
            Res.reasontext = "";

            if (Line.Count <= 0)
            {
                Res.success = false;
                Res.reasontext = "No Order Lines Supplied";
                Res.reasoncode = 4;
                return;
            }

            foreach (var i in Line)
            {
                if (String.IsNullOrEmpty(i.LineNumber))
                {
                    Res.success = false;
                    Res.reasontext = "Invalid Order Line Supplied";
                    Res.reasoncode = 5;
                    return;
                }

                if (String.IsNullOrEmpty(i.ProdCode))
                {
                    Res.success = false;
                    Res.reasontext = "Invalid Product Code at line " + i.LineNumber;
                    Res.reasoncode = 6;
                    return;
                }

                if (String.IsNullOrEmpty(i.LineType))
                {
                    Res.success = false;
                    Res.reasontext = "Invalid Line Type at line " + i.LineNumber + ". Should Be P, C or S";
                    Res.reasoncode = 7;
                    return;
                }

                ProductStaging Prod = db.ProductStagings.AsNoTracking().Where(a => a.ProdCode == i.ProdCode && a.PrincipalID == PrincipalID).FirstOrDefault();
                if (i.LineType == "P" && Prod == null)
                {
                    Res.success = false;
                    Res.reasontext = "Invalid Product " + i.ProdCode + " For Principal";
                    Res.reasoncode = 8;
                    return;
                }

                if (i.Quantity <= 0)
                {
                    Res.success = false;
                    Res.reasontext = "Invalid Line Quantity at line " + i.LineNumber;
                    Res.reasoncode = 9;
                    return;
                }
            }
        }

        [WebMethod]
        public Response CustomerReturn([XmlElementAttribute(IsNullable = true)] string CustomerID,
                             [XmlElementAttribute(IsNullable = true)] string BillingCustomerID,
                             [XmlElementAttribute(IsNullable = true)]string PrincipalCode,
                             [XmlElementAttribute(IsNullable = true)] string ReturnReferenceNumber,
                             string ReturnWaybillNumber,
                             string CustomerOrderNumber,
                             string ReturnType,
                             [XmlElementAttribute(IsNullable = true)] List<ReturnLine> Line)
        {
            CCustomerReturn Rec = new CCustomerReturn
            {
                BillingCustomerID = BillingCustomerID,
                CustomerID = CustomerID,
                CustomerOrderNumber = CustomerOrderNumber,
                Line = Line,
                PrincipalCode = PrincipalCode,
                ReturnReferenceNumber = ReturnReferenceNumber,
                ReturnType = ReturnType,
                ReturnWaybillNumber = ReturnWaybillNumber
            };

            WMSHostCustomerPortalEntities db = new WMSHostCustomerPortalEntities();

            Principal Prn;
            LinkedCustomer LC;
            LinkedCustomer LC1;

            Response Res = new Response();
            Res.success = true;
            Res.reasoncode = 0;
            Res.reasontext = "";

            string Err = ValidateCustomerReturnMandatoryFields(CustomerID, BillingCustomerID, PrincipalCode, ReturnReferenceNumber);
            if (!String.IsNullOrEmpty(Err))
            {
                Res.success = false;
                Res.reasoncode = 3;
                Res.reasontext = Err;

                LogTransaction(Rec, Res, Res.success, "CustomerReturn", Res.reasontext, CustomerOrderNumber, PrincipalCode);

                db.Dispose();
                return Res;
            }

            Err = ValidateCustomerReturnFieldLengths(CustomerID, BillingCustomerID, ReturnReferenceNumber, ReturnWaybillNumber, CustomerOrderNumber, ReturnType);
            if (!String.IsNullOrEmpty(Err))
            {
                Res.success = false;
                Res.reasoncode = 1;
                Res.reasontext = Err;

                LogTransaction(Rec, Res, Res.success, "CustomerReturn", Res.reasontext, CustomerOrderNumber, PrincipalCode);

                db.Dispose();
                return Res;
            }

            try
            {
                Prn = db.Principals.AsNoTracking().Where(a => a.PrincipalCode == PrincipalCode).FirstOrDefault();
                if (Prn == null)
                {
                    Res.success = false;
                    Res.reasoncode = 4;
                    Res.reasontext = "Invalid Pricipal Code";

                    LogTransaction(Rec, Res, Res.success, "CustomerReturn", Res.reasontext, CustomerOrderNumber, PrincipalCode);

                    db.Dispose();
                    return Res;
                }

                LC = db.LinkedCustomers.AsNoTracking().Where(a => a.RAMCustomerID == CustomerID).FirstOrDefault();
                if (LC == null)
                {
                    Res.success = false;
                    Res.reasoncode = 5;
                    Res.reasontext = "Invalid CustomerID";

                    LogTransaction(Rec, Res, Res.success, "CustomerReturn", Res.reasontext, CustomerOrderNumber, PrincipalCode);

                    db.Dispose();
                    return Res;
                }

                LC1 = db.LinkedCustomers.AsNoTracking().Where(a => a.RAMCustomerID == BillingCustomerID).FirstOrDefault();
                if (LC1 == null)
                {
                    Res.success = false;
                    Res.reasoncode = 6;
                    Res.reasontext = "Invalid CustomerID";

                    LogTransaction(Rec, Res, Res.success, "CustomerReturn", Res.reasontext, CustomerOrderNumber, PrincipalCode);

                    db.Dispose();
                    return Res;
                }

                ValidateReturnOrderLines(Line, Prn.PrincipalID, out Res);
                if (!Res.success)
                {
                    LogTransaction(Rec, Res, Res.success, "CustomerReturn", Res.reasontext, CustomerOrderNumber, PrincipalCode);

                    db.Dispose();
                    return Res;
                }
            }
            catch (Exception ex)
            {
                Res.success = false;
                Res.reasoncode = 7;
                Res.reasontext = "Internal Server Error";

                LogTransaction(Rec, Res, Res.success, "CustomerReturn", ex.Message, CustomerOrderNumber, PrincipalCode);

                db.Dispose();
                return Res;
            }

            try
            {
                //ReturnOrder RO = db.ReturnOrders.AsNoTracking().Where(a => a.ReceiverRAMCustomerID == CustomerID && a.BillingRAMCustomerID == BillingCustomerID && a.PrincipalID == Prn.PrincipalID && a.ReturnReferenceNumber == ReturnReferenceNumber).FirstOrDefault();
                ReturnOrder RO = new ReturnOrder();
                RO.BillingCustomerDetailID = LC1.CustomerDetailID;
                RO.BillingRAMCustomerID = BillingCustomerID;
                RO.CustomerDetailID = LC.CustomerDetailID;
                RO.CustomerOrderNumber = String.IsNullOrEmpty(CustomerOrderNumber) ? "" : CustomerOrderNumber;
                RO.DateCreated = DateTime.Now;
                RO.Deleted = false;
                RO.EventDate = DateTime.Now;
                RO.EventTerminal = "";
                RO.EventUser = "webservice";
                RO.OrderDiscount = 0;
                RO.OrderVAT = 0;
                RO.PrincipalID = Prn.PrincipalID;
                RO.Priority = "3";
                RO.Processor = "";
                RO.RAMOrderNumber = CustomerOrderNumber;
                RO.ReceiverRAMCustomerID = CustomerID;
                RO.ReturnReferenceNumber = ReturnReferenceNumber;
                RO.ReturnType = String.IsNullOrEmpty(ReturnType) ? "PART ORDER" : (ReturnType != "PART ORDER" ? (ReturnType != "FULL ORDER" ? "PART ORDER" : ReturnType) : ReturnType);
                RO.ReturnWaybillNumber = String.IsNullOrEmpty(ReturnWaybillNumber) ? "" : ReturnWaybillNumber;
                RO.SalesCategory = "";
                RO.SalesPerson = "";
                RO.Submitted = false;
                RO.ValueAddedPackaging = false;

                db.ReturnOrders.Add(RO);
                db.SaveChanges();

                List<ReturnOrderLineItem> LnLst = new List<ReturnOrderLineItem>();

                foreach (var i in Line)
                {
                    ProductStaging Prod = db.ProductStagings.AsNoTracking().Where(a => a.ProdCode == i.ProdCode && a.PrincipalID == Prn.PrincipalID).FirstOrDefault();
                    ReturnOrderLineItem ln = new ReturnOrderLineItem();
                    ln.LineNumber = i.LineNumber;
                    ln.LineText = i.LineText;
                    ln.LineType = i.LineType;
                    ln.Deleted = false;
                    ln.EventDate = DateTime.Now;
                    ln.EventTerminal = "";
                    ln.EventUser = "webservice";
                    ln.OriginalOrderedQuantity = i.Quantity;
                    ln.ProductStagingID = Prod.ProductStagingID;
                    ln.Quantity = i.Quantity;
                    ln.ReturnOrderID = RO.ReturnOrderID;
                    ln.SubmittedProductCode = "";
                    ln.SubmittedRAMOrderNumber = "";
                    ln.SubmittedToWMS = false;
                    ln.SubmittedUnitDiscountAmount = 0;
                    ln.SubmittedVAT = 0;

                    LnLst.Add(ln);
                }

                if (LnLst.Count > 0)
                {
                    db.ReturnOrderLineItems.AddRange(LnLst);
                }
                else
                {
                    db.ReturnOrders.Remove(RO);
                    Res.success = false;
                    Res.reasoncode = 7;
                    Res.reasontext = "Internal Server Error";
                }

                db.SaveChanges();

                LogTransaction(Rec, Res, Res.success, "CustomerReturn", Res.reasontext, CustomerOrderNumber, PrincipalCode);
            }
            catch (Exception ex)
            {
                Res.success = false;
                Res.reasoncode = 7;
                Res.reasontext = "Internal Server Error";

                LogTransaction(Rec, Res, Res.success, "CustomerReturn", ex.Message, CustomerOrderNumber, PrincipalCode);
            }

            db.Dispose();
            return Res;
        }

        #endregion

        #region SalesOrder

        public class CSalesOrder
        {
            public string PrincipalCode { get; set; }
            public string CustomerID { get; set; }
            public string BillingCustomerID { get; set; }
            public string CustomerOrderNumber { get; set; }
            public int Priority { get; set; }
            public bool ValueAddedPackaging { get; set; }
            public string IDNumber { get; set; }
            public string SalesPerson { get; set; }
            public string SalesCategory { get; set; }
            public string Processor { get; set; }
            public string KYC { get; set; }
            public string CourierName { get; set; }
            public string CourierService { get; set; }
            public bool InsuranceRequired { get; set; }
            public decimal OrderDiscount { get; set; }
            public decimal OrderVAT { get; set; }
            public List<OrderLine> Line { get; set; }
        }

        private string ValidateSOMandatoryFields(string CustomerID, string BillingCustomerID, string PrincipalCode, string CustomerOrderNumber)
        {
            string str = " Not Supplied";

            if (String.IsNullOrEmpty(PrincipalCode)) return "PricipalCode" + str;

            if (String.IsNullOrEmpty(CustomerID)) return "CustomerID" + str;

            if (String.IsNullOrEmpty(BillingCustomerID)) return "BillingCustomerID" + str;

            if (String.IsNullOrEmpty(CustomerOrderNumber)) return "CustomerOrderNumber" + str;

            return "";
        }

        private string ValidateSOFieldLengths(string CustomerID, string BillingCustomerID, string CustomerOrderNumber, string IDNumber, string SalesPerson, string SalesCategory, string Processor, string KYC, string CourierName, string CourierService)
        {
            string str = "Invalid Field Length ";

            if (CustomerID.Length > 20) return str + "CustomerID";

            if (BillingCustomerID.Length > 20) return str + "BillingCustomerID";

            //if (OrderID.Length > 10) return str + "OrderID";

            if (!String.IsNullOrEmpty(CustomerOrderNumber) && CustomerOrderNumber.Length > 20) return str + "CustomerOrderNumber";

            if (!String.IsNullOrEmpty(IDNumber) && IDNumber.Length > 15) return str + "IDNumber";

            if (!String.IsNullOrEmpty(SalesPerson) && SalesPerson.Length > 6) return str + "SalesPerson";

            if (!String.IsNullOrEmpty(SalesCategory) && SalesCategory.Length > 6) return str + "SalesCategory";

            if (!String.IsNullOrEmpty(Processor) && Processor.Length > 10) return str + "Processor";

            if (!String.IsNullOrEmpty(KYC) && KYC.Length > 3) return str + "KYC";

            if (!String.IsNullOrEmpty(CourierName) && CourierName.Length > 3) return str + "CourierName";

            if (!String.IsNullOrEmpty(CourierService) && CourierService.Length > 3) return str + "CourierService";

            return "";
        }

        private void ValidateSalesOrderLines(List<OrderLine> Line, int PrincipalID, out Response Res)
        {
            Res = new Response();
            Res.success = true;
            Res.reasontext = "";

            if (Line.Count <= 0)
            {
                Res.success = false;
                Res.reasontext = "No Order Lines Supplied";
                Res.reasoncode = 6;
                return;
            }

            foreach (var i in Line)
            {
                WMSHostCustomerPortalEntities db = new WMSHostCustomerPortalEntities();

                if (String.IsNullOrEmpty(i.LineNumber) || !i.LineNumber.All(char.IsDigit))
                {
                    Res.success = false;
                    Res.reasontext = "Invalid Order Line Supplied";
                    Res.reasoncode = 3;
                    return;
                }

                if (String.IsNullOrEmpty(i.ProdCode))
                {
                    Res.success = false;
                    Res.reasontext = "Invalid Product Code at line " + i.LineNumber;
                    Res.reasoncode = 2;
                    return;
                }

                if (String.IsNullOrEmpty(i.LineType) || (i.LineType.ToUpper() != "P" && i.LineType.ToUpper() != "C" && i.LineType.ToUpper() != "S"))
                {
                    Res.success = false;
                    Res.reasontext = "Invalid Line Type at line " + i.LineNumber + ". Should Be P, C or S";
                    Res.reasoncode = 2;
                    return;
                }

                ProductStaging Prod = db.ProductStagings.AsNoTracking().Where(a => a.ProdCode == i.ProdCode && a.PrincipalID == PrincipalID).FirstOrDefault();
                if (i.LineType.ToUpper() == "P" && Prod == null)
                {
                    Res.success = false;
                    Res.reasontext = "Invalid Product " + i.ProdCode + " For Principal";
                    Res.reasoncode = 2;
                    return;
                }

                if (i.Quantity <= 0)
                {
                    Res.success = false;
                    Res.reasontext = "Invalid Line Quantity at line " + i.LineNumber;
                    Res.reasoncode = 2;
                    return;
                }

                string StockAvailble = "OK";

                ObjectParameter myres = new ObjectParameter("Result", typeof(string));
                ObjectResult<string> StockObject = db.usp_X_STOCK_Available((int)PrincipalID, i.ProdCode, (int)i.Quantity, myres);

                foreach (var x in StockObject)
                {
                    StockAvailble = x;
                }

                if (StockAvailble != "OK")
                {
                    Res.success = false;
                    Res.reasontext = "Not Enough Stock Availble For Product " + i.ProdCode;
                    Res.reasoncode = 10;
                    return;
                }
            }
        }

        private Order CreateOrderInPortal(LinkedCustomer LC, LinkedCustomer LC1, string CustomerID, string CourierName,
                                  string CourierService, string CustomerOrderNumber, string IDNumber, bool InsuranceRequired,
                                  string KYC, decimal OrderDiscount, decimal OrderVAT, Principal Prn, int Priority, string Processor,
                                  string OrderID, string BillingCustomerID, string SalesCategory, string SalesPerson,
                                  bool ValueAddedPackaging, List<OrderLine> Line, out string Error)
        {
            WMSHostCustomerPortalEntities db = new WMSHostCustomerPortalEntities();
            Order MyOrd;
            Error = "";

            try
            {
                MyOrd = new Order();
                MyOrd.BillingCustomerDetailID = LC.CustomerDetailID;
                MyOrd.BillingRAMCustomerID = CustomerID;
                MyOrd.ClientConfirmed = false;
                MyOrd.CourierName = String.IsNullOrEmpty(CourierName) ? "" : CourierName.ToUpper();
                MyOrd.CourierService = String.IsNullOrEmpty(CourierService) ? "" : CourierService.ToUpper();
                MyOrd.CustomerDetailID = LC1.CustomerDetailID;
                MyOrd.CustomerOrderNumber = CustomerOrderNumber.ToUpper();
                MyOrd.DateCreated = DateTime.Now;
                MyOrd.Deleted = false;
                MyOrd.EventDate = DateTime.Now;
                MyOrd.EventTerminal = "";
                MyOrd.EventUser = "webservice";
                MyOrd.IDNumber = String.IsNullOrEmpty(IDNumber) ? "" : IDNumber.ToUpper();
                MyOrd.InsuranceRequired = InsuranceRequired;
                MyOrd.KYC = String.IsNullOrEmpty(KYC) ? "" : KYC.ToUpper();
                MyOrd.OrderDiscount = (double)OrderDiscount;
                MyOrd.OrderVAT = (double)OrderVAT;
                MyOrd.PrincipalID = Prn.PrincipalID;
                MyOrd.Priority = Priority <= 0 ? "3" : Priority > 3 ? "3" : Priority.ToString();
                MyOrd.Processor = String.IsNullOrEmpty(Processor) ? "" : Processor.ToUpper();
                MyOrd.RAMOrderNumber = OrderID.ToUpper();
                MyOrd.ReceiverRAMCustomerID = BillingCustomerID;
                MyOrd.SalesCategory = String.IsNullOrEmpty(SalesCategory) ? "" : SalesCategory.ToUpper();
                MyOrd.SalesPerson = String.IsNullOrEmpty(SalesPerson) ? "" : SalesPerson.ToUpper();
                MyOrd.Status = "NEW";
                MyOrd.Submitted = true;
                MyOrd.ValidateDelivery = false;
                MyOrd.ValueAddedPackaging = ValueAddedPackaging;

                db.Orders.Add(MyOrd);
                db.SaveChanges();

                List<OrderLineItem> OrdLnLst = new List<OrderLineItem>();

                foreach (var i in Line)
                {
                    OrderLineItem Ordln = new OrderLineItem();
                    ProductStaging Prod = db.ProductStagings.AsNoTracking().Where(a => a.ProdCode == i.ProdCode && a.PrincipalID == Prn.PrincipalID).FirstOrDefault();

                    int ProdID = 0;
                    if (Prod != null)
                    {
                        ProdID = Prod.ProductStagingID;
                        Ordln.Deleted = false;
                        Ordln.EventDate = DateTime.Now;
                        Ordln.EventTerminal = "";
                        Ordln.EventUser = "webservice";
                        Ordln.LineNumber = i.LineNumber;
                        Ordln.LineText = String.IsNullOrEmpty(i.LineText) ? "" : i.LineText;
                        Ordln.LineType = i.LineType.ToUpper();
                        Ordln.OrderID = MyOrd.OrderID;
                        Ordln.ProductStagingID = ProdID;
                        Ordln.Quantity = i.Quantity;
                        Ordln.SubmittedProductCode = "";
                        Ordln.SubmittedRAMOrderNumber = "";
                        Ordln.SubmittedToWMS = true;
                        Ordln.SubmittedUnitCost = (double)i.UnitCost;
                        Ordln.SubmittedUnitDiscountAmount = (double)i.UnitDiscountAmount;
                        Ordln.SubmittedVAT = (double)i.VAT;

                        OrdLnLst.Add(Ordln);
                    }
                }

                if (OrdLnLst.Count > 0)
                {
                    db.OrderLineItems.AddRange(OrdLnLst);
                }
                else
                {
                    Order OrdToReMove = MyOrd;
                    MyOrd = null;
                    db.Orders.Remove(OrdToReMove);
                }

                db.SaveChanges();
            }
            catch (Exception ex)
            {
                Error = ex.Message;
                MyOrd = null;
            }

            db.Dispose();
            return MyOrd;
        }

        private Response SendOrdIntoWMS(string CustomerID, string BillingCustomerID, Order MyOrd, Principal Prn, List<OrderLine> Line)
        {
            WMSHostCustomerPortalEntities db = new WMSHostCustomerPortalEntities();

            Response Res = new Response();
            Res.success = true;
            Res.reasoncode = 0;
            Res.reasontext = "";

            try
            {
                vw_RCustomers rec = db.vw_RCustomers.AsNoTracking().Where(a => a.CustomerID == CustomerID).FirstOrDefault();
                Zone recZone = db.Zones.AsNoTracking().Where(a => a.ZoneID == rec.ZoneID).FirstOrDefault();

                vw_RCustomers inv = db.vw_RCustomers.AsNoTracking().Where(a => a.CustomerID == BillingCustomerID).FirstOrDefault();
                Zone invZone = db.Zones.AsNoTracking().Where(a => a.ZoneID == inv.ZoneID).FirstOrDefault();

                HostWSSoapClient cl = new HostWSSoapClient("HostWSSoap");

                ORDLine[] lns = new ORDLine[Line.Count];
                int x = 0;
                foreach (var i in Line)
                {
                    ORDLine ln = new ORDLine();
                    ln.LineNumber = Convert.ToInt32(i.LineNumber);
                    ln.LineText = i.LineText;
                    ln.LineType = i.LineType.ToUpper();
                    ln.ProductCode = i.ProdCode.ToUpper();
                    ln.Quantity = i.Quantity;
                    ln.UnitCost = (double)i.UnitCost;
                    ln.UnitDiscountAmount = (double)i.UnitDiscountAmount;
                    ln.VAT = (double)i.VAT;
                    lns[x++] = ln;
                }

                string Addr1 = inv.StreetAddress1.Length > 32 ? inv.StreetAddress1.Substring(0, 32) : inv.StreetAddress1;
                string Addr2 = inv.StreetAddress2.Length > 32 ? inv.StreetAddress2.Substring(0, 32) : inv.StreetAddress2;
                string Addr3 = invZone.Suburb.Length > 32 ? invZone.Suburb.Substring(0, 32) : invZone.Suburb;
                string Addr4 = invZone.Area.Length > 32 ? invZone.Area.Substring(0, 32) : invZone.Area;

                string DelAddr1 = rec.CustomerName.Length > 32 ? rec.CustomerName.Substring(0, 32) : rec.CustomerName;
                string DelAddr2 = rec.StreetAddress1.Length > 32 ? rec.StreetAddress1.Substring(0, 32) : rec.StreetAddress1;
                string DelAddr3 = rec.StreetAddress2.Length > 32 ? rec.StreetAddress2.Substring(0, 32) : rec.StreetAddress2;
                string DelAddr4 = recZone.Suburb.Length > 32 ? recZone.Suburb.Substring(0, 32) : recZone.Suburb;
                string DelAddr5 = invZone.Area.Length > 32 ? invZone.Area.Substring(0, 32) : String.IsNullOrEmpty(invZone.Area) ? invZone.PostalCode : invZone.Area;
                string DelAddr6 = rec.TelephoneNo;

                string IDNumber = String.IsNullOrEmpty(MyOrd.IDNumber) ? "" : MyOrd.IDNumber;
                string KYC = String.IsNullOrEmpty(MyOrd.KYC) ? "" : MyOrd.KYC;
                string CourierName = String.IsNullOrEmpty(MyOrd.CourierName) ? "" : MyOrd.CourierName;
                string CourierService = String.IsNullOrEmpty(MyOrd.CourierService) ? "" : MyOrd.CourierService;

                string CompanyName = rec.CustomerName.Length > 10 ? rec.CustomerName.Substring(0, 10) : rec.CustomerName;

                string StoreCode = rec.StoreCode == null ? "" : rec.StoreCode.Length > 20 ? rec.StoreCode.Substring(0, 20) : rec.StoreCode;

                cl.ORDAdd("Webservice", "001", Prn.PrincipalCode, MyOrd.RAMOrderNumber, Line.Count, DateTime.Now,
                          CompanyName, "", MyOrd.CustomerOrderNumber, MyOrd.ReceiverRAMCustomerID,
                          inv.CustomerName, Addr1, Addr2, Addr3, Addr4, invZone.PostalCode, inv.TelephoneNo,
                          false, "", false, MyOrd.Priority, false, MyOrd.SalesPerson, MyOrd.SalesCategory, MyOrd.Processor,
                          DelAddr1, DelAddr2, DelAddr3, DelAddr4, DelAddr5, DelAddr6, recZone.PostalCode, (double)MyOrd.OrderDiscount,
                          (double)MyOrd.OrderVAT, 0, lns, IDNumber, KYC, CourierName, CourierService,
                          (bool)MyOrd.InsuranceRequired, true, StoreCode);
            }
            catch (Exception ex)
            {
                Order Ord = db.Orders.Where(a => a.OrderID == MyOrd.OrderID).FirstOrDefault();

                if (Ord != null)
                {
                    List<OrderLineItem> Ordlnlist = db.OrderLineItems.Where(a => a.OrderID == MyOrd.OrderID).ToList();

                    if (Ordlnlist.Count > 0)
                    {
                        db.OrderLineItems.RemoveRange(Ordlnlist);
                    }

                    db.Orders.Remove(Ord);
                    db.SaveChanges();
                }

                Res.success = false;
                Res.reasoncode = 6;
                Res.reasontext = ex.Message;
            }

            db.Dispose();
            return Res;
        }

        [WebMethod]
        public Response SalesOrder([XmlElementAttribute(IsNullable = true)]string CustomerID,
                     [XmlElementAttribute(IsNullable = true)]string BillingCustomerID,
                     [XmlElementAttribute(IsNullable = true)]string PrincipalCode,
                     [XmlElementAttribute(IsNullable = true)]string CustomerOrderNumber,
                     int Priority,
                     bool ValueAddedPackaging,
                     string IDNumber,
                     string SalesPerson,
                     string SalesCategory,
                     string Processor,
                     string KYC,
                     string CourierName,
                     string CourierService,
                     bool InsuranceRequired,
                     decimal OrderDiscount,
                     decimal OrderVAT,
                     [XmlElementAttribute(IsNullable = true)] List<OrderLine> Line)
        {
            CSalesOrder Rec = new CSalesOrder
            {
                BillingCustomerID = BillingCustomerID,
                CourierName = CourierName,
                CourierService = CourierService,
                CustomerID = CustomerID,
                CustomerOrderNumber = CustomerOrderNumber,
                IDNumber = IDNumber,
                InsuranceRequired = InsuranceRequired,
                KYC = KYC,
                Line = Line,
                OrderDiscount = OrderDiscount,
                OrderVAT = OrderVAT,
                PrincipalCode = PrincipalCode,
                Priority = Priority,
                Processor = Processor,
                SalesCategory = SalesCategory,
                SalesPerson = SalesPerson,
                ValueAddedPackaging = ValueAddedPackaging
            };

            WMSHostCustomerPortalEntities db = new WMSHostCustomerPortalEntities();

            Principal Prn;
            LinkedCustomer LC;
            LinkedCustomer LC1;
            string OrderID = "";

            Response Res = new Response();
            Res.success = true;
            Res.reasoncode = 0;
            Res.reasontext = "";

            string Err = ValidateSOMandatoryFields(CustomerID, BillingCustomerID, PrincipalCode, CustomerOrderNumber);
            if (!String.IsNullOrEmpty(Err))
            {
                Res.success = false;
                Res.reasoncode = 3;
                Res.reasontext = Err;

                LogTransaction(Rec, Res, Res.success, "SalesOrder", Res.reasontext, CustomerOrderNumber, PrincipalCode);

                db.Dispose();
                return Res;
            }

            if (Priority < 1 || Priority > 3)
            {
                Res.success = false;
                Res.reasoncode = 2;
                Res.reasontext = "Priority must be greater than 0 and less 3";

                LogTransaction(Rec, Res, Res.success, "SalesOrder", Res.reasontext, CustomerOrderNumber, PrincipalCode);

                db.Dispose();
                return Res;
            }

            Err = ValidateSOFieldLengths(CustomerID, BillingCustomerID, CustomerOrderNumber, IDNumber, SalesPerson, SalesCategory, Processor, KYC, CourierName, CourierService);
            if (!String.IsNullOrEmpty(Err))
            {
                Res.success = false;
                Res.reasoncode = 1;
                Res.reasontext = Err;

                LogTransaction(Rec, Res, Res.success, "SalesOrder", Res.reasontext, CustomerOrderNumber, PrincipalCode);

                db.Dispose();
                return Res;
            }

            try
            {
                Prn = db.Principals.AsNoTracking().Where(a => a.PrincipalCode == PrincipalCode).FirstOrDefault();
                if (Prn == null)
                {
                    Res.success = false;
                    Res.reasoncode = 4;
                    Res.reasontext = "Invalid Pricipal Code";

                    LogTransaction(Rec, Res, Res.success, "SalesOrder", Res.reasontext, CustomerOrderNumber, PrincipalCode);

                    db.Dispose();
                    return Res;
                }

                Order Ord = db.Orders.AsNoTracking().Where(a => a.CustomerOrderNumber == CustomerOrderNumber && a.PrincipalID == Prn.PrincipalID).FirstOrDefault();
                if (Ord != null)
                {
                    Res.success = false;
                    Res.reasoncode = 2;
                    Res.reasontext = "Duplicate Order Supplied";

                    LogTransaction(Rec, Res, Res.success, "SalesOrder", Res.reasontext, CustomerOrderNumber, PrincipalCode);

                    db.Dispose();
                    return Res;
                }

                LC = db.LinkedCustomers.AsNoTracking().Where(a => a.RAMCustomerID == CustomerID && a.PrincipalID == Prn.PrincipalID).FirstOrDefault();
                if (LC == null)
                {
                    Res.success = false;
                    Res.reasoncode = 5;
                    Res.reasontext = "Unknown Customer";

                    LogTransaction(Rec, Res, Res.success, "SalesOrder", Res.reasontext, CustomerOrderNumber, PrincipalCode);

                    db.Dispose();
                    return Res;
                }

                LC1 = db.LinkedCustomers.AsNoTracking().Where(a => a.RAMCustomerID == BillingCustomerID && a.PrincipalID == Prn.PrincipalID).FirstOrDefault();
                if (LC1 == null)
                {
                    Res.success = false;
                    Res.reasoncode = 5;
                    Res.reasontext = "Unknown Billing Customer";

                    LogTransaction(Rec, Res, Res.success, "SalesOrder", Res.reasontext, CustomerOrderNumber, PrincipalCode);

                    db.Dispose();
                    return Res;
                }

                ValidateSalesOrderLines(Line, Prn.PrincipalID, out Res);
                if (!Res.success) return Res;

                ObjectParameter OrdNumObj = new ObjectParameter("RamOrderNumber", typeof(string));
                ObjectResult<string> Result = db.usp_NextWmsRamOrderNumber((int)Prn.PrincipalID, OrdNumObj);

                OrderID = "";
                foreach (var i in Result)
                {
                    OrderID = i;
                }
            }
            catch (Exception ex)
            {
                Res.success = false;
                Res.reasoncode = 6;
                Res.reasontext = "Internal Server Error";

                LogTransaction(Rec, Res, Res.success, "SalesOrder", ex.Message, CustomerOrderNumber, PrincipalCode);

                db.Dispose();
                return Res;
            }

            string Error = "";

            Order MyOrd = CreateOrderInPortal(LC, LC1, CustomerID, CourierName, CourierService, CustomerOrderNumber, IDNumber, InsuranceRequired,
                                KYC, OrderDiscount, OrderVAT, Prn, Priority, Processor, OrderID, BillingCustomerID, SalesCategory,
                                SalesPerson, ValueAddedPackaging, Line, out Error);

            if (MyOrd != null)
            {
                Res = SendOrdIntoWMS(CustomerID, BillingCustomerID, MyOrd, Prn, Line);

                if (!Res.success)
                {
                    Error = Res.reasontext;
                    Res.reasontext = "Internal Server Error";

                    LogTransaction(Rec, Res, Res.success, "SalesOrder", Error, CustomerOrderNumber, PrincipalCode);
                }
                else
                {
                    LogTransaction(Rec, Res, Res.success, "SalesOrder", Res.reasontext, CustomerOrderNumber, PrincipalCode);
                }
            }
            else
            {
                Res.success = false;
                Res.reasoncode = 6;
                Res.reasontext = "Internal Server Error";

                LogTransaction(Rec, Res, Res.success, "SalesOrder", Error, CustomerOrderNumber, PrincipalCode);
            }

            db.Dispose();
            return Res;
        }

        #endregion
    }
}
