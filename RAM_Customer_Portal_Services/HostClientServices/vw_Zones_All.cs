//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace HostClientServices
{
    using System;
    using System.Collections.Generic;
    
    public partial class vw_Zones_All
    {
        public string ZoneID { get; set; }
        public string Suburb { get; set; }
        public string Area { get; set; }
        public string Province { get; set; }
        public string PostalCode { get; set; }
        public string HubID { get; set; }
        public string SUB_ID { get; set; }
        public string TWN_ID { get; set; }
        public string PRV_ID { get; set; }
        public Nullable<double> Lat { get; set; }
        public Nullable<double> Lon { get; set; }
        public string Settings { get; set; }
        public Nullable<int> LocalToHub { get; set; }
        public Nullable<int> LocalArea { get; set; }
        public Nullable<int> MainArea { get; set; }
        public Nullable<int> RegionalArea { get; set; }
        public Nullable<int> International { get; set; }
        public Nullable<bool> DAltDays { get; set; }
        public Nullable<bool> DMon { get; set; }
        public Nullable<bool> DTue { get; set; }
        public Nullable<bool> DWed { get; set; }
        public Nullable<bool> DThu { get; set; }
        public Nullable<bool> DFri { get; set; }
        public Nullable<bool> DSat { get; set; }
        public Nullable<bool> DSun { get; set; }
        public Nullable<int> DHour { get; set; }
        public Nullable<int> CHour { get; set; }
        public Nullable<int> Township { get; set; }
        public Nullable<int> Rural { get; set; }
        public int RouteID { get; set; }
        public string RouteName { get; set; }
        public Nullable<int> isActive { get; set; }
        public string MatchingZoneID { get; set; }
    }
}
