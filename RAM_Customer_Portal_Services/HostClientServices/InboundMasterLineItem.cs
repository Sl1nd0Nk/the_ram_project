//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace HostClientServices
{
    using System;
    using System.Collections.Generic;
    
    public partial class InboundMasterLineItem
    {
        public int InboundMasterLineItemID { get; set; }
        public int InboundMasterID { get; set; }
        public int ProductStagingID { get; set; }
        public int ExpectedQuantity { get; set; }
        public double UnitCost { get; set; }
        public System.Guid ItemGuid { get; set; }
        public string EventTerminal { get; set; }
        public System.DateTime EventDate { get; set; }
        public string EventUser { get; set; }
        public bool Deleted { get; set; }
    }
}
