﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ArrSenderService.CustomerReturn {
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="CustomerReturn.CustomerReturnReceiptSoap")]
    public interface CustomerReturnReceiptSoap {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/Return", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        ArrSenderService.CustomerReturn.Response Return(string PrincipalCode, bool Resend, int OrderLineNo, string ProdCode, string MovementRef, string ReturnReferenceNumber, System.DateTime ReceiptDateTime, int RejectCount, string ReasonCode, int AcceptCount, string[] SerialNumbers);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/Return", ReplyAction="*")]
        System.Threading.Tasks.Task<ArrSenderService.CustomerReturn.Response> ReturnAsync(string PrincipalCode, bool Resend, int OrderLineNo, string ProdCode, string MovementRef, string ReturnReferenceNumber, System.DateTime ReceiptDateTime, int RejectCount, string ReasonCode, int AcceptCount, string[] SerialNumbers);
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.8.3761.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://tempuri.org/")]
    public partial class Response : object, System.ComponentModel.INotifyPropertyChanged {
        
        private bool successField;
        
        private int reasoncodeField;
        
        private string reasontextField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=0)]
        public bool success {
            get {
                return this.successField;
            }
            set {
                this.successField = value;
                this.RaisePropertyChanged("success");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=1)]
        public int reasoncode {
            get {
                return this.reasoncodeField;
            }
            set {
                this.reasoncodeField = value;
                this.RaisePropertyChanged("reasoncode");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=2)]
        public string reasontext {
            get {
                return this.reasontextField;
            }
            set {
                this.reasontextField = value;
                this.RaisePropertyChanged("reasontext");
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface CustomerReturnReceiptSoapChannel : ArrSenderService.CustomerReturn.CustomerReturnReceiptSoap, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class CustomerReturnReceiptSoapClient : System.ServiceModel.ClientBase<ArrSenderService.CustomerReturn.CustomerReturnReceiptSoap>, ArrSenderService.CustomerReturn.CustomerReturnReceiptSoap {
        
        public CustomerReturnReceiptSoapClient() {
        }
        
        public CustomerReturnReceiptSoapClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public CustomerReturnReceiptSoapClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public CustomerReturnReceiptSoapClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public CustomerReturnReceiptSoapClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public ArrSenderService.CustomerReturn.Response Return(string PrincipalCode, bool Resend, int OrderLineNo, string ProdCode, string MovementRef, string ReturnReferenceNumber, System.DateTime ReceiptDateTime, int RejectCount, string ReasonCode, int AcceptCount, string[] SerialNumbers) {
            return base.Channel.Return(PrincipalCode, Resend, OrderLineNo, ProdCode, MovementRef, ReturnReferenceNumber, ReceiptDateTime, RejectCount, ReasonCode, AcceptCount, SerialNumbers);
        }
        
        public System.Threading.Tasks.Task<ArrSenderService.CustomerReturn.Response> ReturnAsync(string PrincipalCode, bool Resend, int OrderLineNo, string ProdCode, string MovementRef, string ReturnReferenceNumber, System.DateTime ReceiptDateTime, int RejectCount, string ReasonCode, int AcceptCount, string[] SerialNumbers) {
            return base.Channel.ReturnAsync(PrincipalCode, Resend, OrderLineNo, ProdCode, MovementRef, ReturnReferenceNumber, ReceiptDateTime, RejectCount, ReasonCode, AcceptCount, SerialNumbers);
        }
    }
}
