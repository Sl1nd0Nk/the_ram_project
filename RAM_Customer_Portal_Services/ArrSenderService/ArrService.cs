﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using ServicesPortalDBModel;
using ServicesWmsHostDBModel;
using ArrSenderService.WMSHostWS;
using ArrSenderService.POReceipt;
using ArrSenderService.CustomerReturn;
using System.IO;
using System.Xml.Serialization;
using System.Xml;
using System.Threading;

namespace ArrSenderService
{
    public partial class ArrService : ServiceBase
    {
        WMSHostCustomerPortalEntities PortalDB;
        RAMWMSHOSTEntities WmsHostDB;
        private bool ProcessRunning = false;

        public ArrService()
        {
            InitializeComponent();

            ProcessRunning = true;
            var threadStart = new ThreadStart(RunService);
            var thread = new Thread(threadStart);
            thread.Start();
        }

        #region IGDReceipt
        private void ProcessArr(List<MsgIn_ARR> lst)
        {
            foreach (var Arr in lst)
            {
                try
                {
                    if (!ProcessRunning) break;

                    PortalDB = new WMSHostCustomerPortalEntities();

                    Logger.Info("Process ARR Msg - MoveRef " + Arr.MoveRef + " | LineNumber " + Arr.LineNumber);
                    string ProcErr = "";
                    DateTime ProcStartDT = DateTime.Now;
                    IGDStaging IGD = PortalDB.IGDStagings.Where(a => a.MoveRef == Arr.MoveRef &&
                                                                a.PrincipalCode == Arr.PrincipalCode &&
                                                                a.SiteCode == Arr.SiteCode &&
                                                                a.ReceiptType == Arr.ReceiptType &&
                                                                a.OrderLineNo == Arr.LineNumber &&
                                                                a.PORef == Arr.PORef &&
                                                                a.Deleted == false).FirstOrDefault();
                    if (IGD != null)
                    {
                        IGDReceipt Rec = new IGDReceipt();
                        Rec.AcceptCount = Arr.AcceptCount;
                        Rec.IGDStagingID = IGD.IGDStagingID;
                        Rec.LineNumber = Arr.LineNumber;
                        Rec.MoveRef = Arr.MoveRef;
                        Rec.PORef = Arr.PORef;
                        Rec.PrincipalCode = Arr.PrincipalCode;
                        Rec.ProdCode = Arr.ProdCode;
                        Rec.ReasonCode = Arr.ReasonCode;
                        Rec.ReceiptDT = Arr.ReceiptDT;
                        Rec.ReceiptType = Arr.ReceiptType;
                        Rec.RejectCount = Arr.RejectCount;
                        Rec.SiteCode = Arr.SiteCode;
                        Rec.StartEndStatus = Arr.StartEndStatus;
                        Rec.CreateDate = DateTime.Now;

                        PortalDB.IGDReceipts.Add(Rec);
                        PortalDB.SaveChanges();

                        ProductStaging Prod = PortalDB.ProductStagings.Find(IGD.ProductStagingID);
                        if (Prod != null)
                        {
                            if (Prod.Serialised == true)
                            {
                                List<MsgIn_ARR_SN> SerLst = WmsHostDB.MsgIn_ARR_SN.AsNoTracking().Where(a => a.ARRID == Arr.ARRID).ToList();
                                List<IGDReceiptSerial> IGDSerList = new List<IGDReceiptSerial>();
                                int Count = 0;
                                foreach (var i in SerLst)
                                {
                                    IGDReceiptSerial Ser = new IGDReceiptSerial();
                                    Ser.IGDReceiptID = Rec.ID;
                                    Ser.SerialNumber = i.SerialNumber;
                                    IGDSerList.Add(Ser);
                                    Count++;

                                    if (Count == 1000)
                                    {
                                        PortalDB.IGDReceiptSerials.AddRange(IGDSerList);
                                        PortalDB.SaveChanges();
                                        Count = 0;
                                        IGDSerList = new List<IGDReceiptSerial>();
                                    }
                                }

                                if (IGDSerList.Count > 0)
                                {
                                    PortalDB.IGDReceiptSerials.AddRange(IGDSerList);
                                    PortalDB.SaveChanges();
                                }
                            }
                        }

                        IGD.Status = "ARR";
                        IGD.EventDate = Arr.RecordDT;
                        PortalDB.Entry(IGD).State = System.Data.Entity.EntityState.Modified;
                        PortalDB.SaveChanges();
                    }
                    else
                    {
                        ProcErr = "IGD Not Found In The Customer Portal";
                    }

                    MsgIn_ARR UpdateArr = WmsHostDB.MsgIn_ARR.Find(Arr.ARRID);

                    UpdateArr.RecordState = !String.IsNullOrEmpty(ProcErr) ? "PROCESSERROR" : "PROCESSED";
                    UpdateArr.ProcStartDT = ProcStartDT;
                    UpdateArr.ProcEndDT = DateTime.Now;
                    UpdateArr.ProcErrors = ProcErr;
                    WmsHostDB.Entry(UpdateArr).State = System.Data.Entity.EntityState.Modified;
                    WmsHostDB.SaveChanges();

                    PortalDB.Dispose();
                }
                catch(Exception ex)
                {
                    Logger.Error(ex.Message);
                }
            }
        }

        private void Arr_Msg_ToProcess_List()
        {
            try
            {
                WmsHostDB = new RAMWMSHOSTEntities();

                List<MsgIn_ARR> Lst = WmsHostDB.MsgIn_ARR.AsNoTracking().Where(a => a.RecordState == "READY").Take(100).OrderByDescending(a => a.RecordDT).ToList();

                ProcessArr(Lst);

                WmsHostDB.Dispose();
            }
            catch(Exception ex)
            {
                Logger.Error(ex.Message);
            }
        }
        #endregion

        #region CLC
        private bool SendIGDCLC(IGDStaging i, int Discrepency, string[] SerialNumbers, string ProdCode, bool ProdSerialised)
        {
            try
            {
                HostWSSoapClient cl = new HostWSSoapClient("HostWSSoap");

                if (SerialNumbers.Length > 0 && String.IsNullOrEmpty(SerialNumbers[0]))
                {
                    SerialNumbers = new string[0];
                }

                Result Res = cl.CLCAdd("WMSCustomerPortal", i.SiteCode, i.PrincipalCode,
                                        i.MoveRef, i.OrderLineNo, ProdCode, (int)i.ReceivedQuantity,
                                        i.ReceiptType, Discrepency, ProdSerialised == true ? Discrepency : 0, SerialNumbers);

                Logger.Info("Process CLC Response Msg - MoveRef " + i.MoveRef + " = " + Res.Success);
                return Res.Success;
            }
            catch(Exception ex)
            {
                Logger.Error(ex.Message);
                return false;
            }
        }

        private void ProcessRetrievedIGDS(List<IGDStaging> lst)
        {
            foreach (var i in lst)
            {
                try
                {
                    if (!ProcessRunning) break;

                    PortalDB = new WMSHostCustomerPortalEntities();

                    Logger.Info("Process CLC Msg - MoveRef " + i.MoveRef);

                    string Status = "PENDING";
                    int Discrepency = 0;
                    int AcceptCount = 0;
                    string ProdCode = "";
                    bool Serialised = false;

                    ProductStaging Prod = PortalDB.ProductStagings.Find(i.ProductStagingID);
                    List<IGDReceiptSerial> Serlist = new List<IGDReceiptSerial>();
                    string[] SerialNumbers = new string[(int)i.ReceivedQuantity];

                    IGDReceipt Rec = PortalDB.IGDReceipts.AsNoTracking().Where(a => a.IGDStagingID == i.IGDStagingID).ToList().LastOrDefault();
                    if (Rec != null)
                    {
                        ProdCode = Rec.ProdCode;
                        Serialised = (bool)Prod.Serialised;
                        AcceptCount = (int)Rec.AcceptCount;
                        if (i.ReceivedQuantity == Rec.AcceptCount + Rec.RejectCount)
                        {
                            if (Prod == null)
                            {
                                Discrepency = (int)i.ReceivedQuantity;
                                Status = "RECEIPT REJECTED";
                            }
                            else
                            {
                                if (Prod.Serialised == true)
                                {
                                    Serlist = PortalDB.IGDReceiptSerials.AsNoTracking().Where(a => a.IGDReceiptID == Rec.ID).ToList();
                                    if (Serlist.Count != Rec.AcceptCount)
                                    {
                                        Status = "RECEIPT REJECTED";
                                        Discrepency = Math.Abs((int)Rec.AcceptCount - Serlist.Count);
                                    }
                                    else
                                    {
                                        bool DuplicateSerialsInMessage = false;
                                        int x = 0;
                                        while (x < Serlist.Count)
                                        {
                                            int y = x + 1;
                                            while (y < Serlist.Count)
                                            {
                                                if (Serlist[x].SerialNumber == Serlist[y].SerialNumber)
                                                {
                                                    DuplicateSerialsInMessage = true;
                                                    break;
                                                }

                                                y++;
                                            }

                                            if (DuplicateSerialsInMessage)
                                            {
                                                SerialNumbers[SerialNumbers.Length - 1] = Serlist[x].SerialNumber;
                                                DuplicateSerialsInMessage = false;
                                                Discrepency += 1;
                                            }

                                            x++;
                                        }

                                        if (Discrepency > 0)
                                        {
                                            Status = "RECEIPT REJECTED";
                                        }
                                        else
                                        {
                                            Status = "RECEIPTED";
                                        }
                                    }
                                }
                                else
                                {
                                    Status = "RECEIPTED";
                                }
                            }
                        }
                        else
                        {
                            Status = "RECEIPT REJECTED";
                            Discrepency = (int)i.ReceivedQuantity;
                        }
                    }

                    if (Status == "RECEIPTED" && Prod != null)
                    {
                        if (Prod.Serialised == true && Serlist.Count > 0)
                        {
                            List<Serial> SerList = new List<Serial>();
                            List<int> AvailbleSerList = new List<int>();
                            List<string> MySerList = new List<string>();

                            foreach (var n in Serlist)
                            {
                                MySerList.Add(n.SerialNumber);
                            }

                            List<Serial> ProdSerLst = PortalDB.Serials.AsNoTracking().Where(a => MySerList.Contains(a.SerialNumber) && a.ProductStagingID == Prod.ProductStagingID).ToList();
                            List<Serial> UniqueLst = new List<Serial>();
                            List<string> UniqueLstStr = new List<string>();

                            if(ProdSerLst.Count > 0)
                            {
                                foreach (var s in MySerList)
                                {
                                    var ToAdd = ProdSerLst.Where(a => a.SerialNumber == s).LastOrDefault();
                                    if(ToAdd != null)
                                    {
                                        UniqueLst.Add(ToAdd);
                                    }
                                }
                            }

                            foreach(var u in UniqueLst)
                            {
                                if(u.OrderID == null && u.Status != "SLA DELETE")
                                {
                                    AvailbleSerList.Add(u.ID);
                                    UniqueLstStr.Add(u.SerialNumber);
                                }
                            }

                            foreach(var s in MySerList)
                            {
                                if(UniqueLstStr.IndexOf(s) < 0)
                                {
                                    Serial Ser = new Serial();
                                    Ser.EventDate = DateTime.Now;
                                    Ser.ProductStagingID = Prod.ProductStagingID;
                                    Ser.SerialNumber = s;
                                    Ser.IGDStagingID = i.IGDStagingID;
                                    Ser.Status = "AVAILABLE";
                                    SerList.Add(Ser);

                                    if (SerList.Count == 1000)
                                    {
                                        PortalDB.Serials.AddRange(SerList);
                                        PortalDB.SaveChanges();

                                        PortalDB.Dispose();
                                        PortalDB = new WMSHostCustomerPortalEntities();
                                        SerList = new List<Serial>();
                                    }
                                }
                            }

                            if (SerList.Count > 0)
                            {
                                PortalDB.Serials.AddRange(SerList);
                                PortalDB.SaveChanges();
                                PortalDB.Dispose();
                                PortalDB = new WMSHostCustomerPortalEntities();
                            }

                            if (AvailbleSerList.Count > 0)
                            {
                                List<Serial> ProdSerList = PortalDB.Serials.Where(a => AvailbleSerList.Contains(a.ID)).ToList();
                                int Count = 0;
                                foreach (var a in ProdSerList)
                                {
                                    a.Status = "AVAILABLE";
                                    a.EventDate = DateTime.Now;
                                    a.IGDStagingID = i.IGDStagingID;
                                    //PortalDB.Entry(a).State = System.Data.Entity.EntityState.Modified;
                                    Count++;

                                    if(Count == 1000)
                                    {
                                        Count = 0;
                                        PortalDB.SaveChanges();
                                        PortalDB.Dispose();
                                        PortalDB = new WMSHostCustomerPortalEntities();
                                    }
                                }

                                PortalDB.SaveChanges();
                                PortalDB.Dispose();
                                PortalDB = new WMSHostCustomerPortalEntities();
                            }

                            int StockQty = Prod.StockQuantity == null ? 0 : (int)Prod.StockQuantity;
                            Prod.StockQuantity = StockQty + Serlist.Count;
                        }
                        else
                        {
                            int StockQty = Prod.StockQuantity == null ? 0 : (int)Prod.StockQuantity;
                            Prod.StockQuantity = StockQty + AcceptCount;
                        }

                        Prod.EventDate = DateTime.Now;
                        PortalDB.Entry(Prod).State = System.Data.Entity.EntityState.Modified;
                        PortalDB.SaveChanges();
                    }

                    if (SendIGDCLC(i, Discrepency, SerialNumbers, ProdCode, Serialised))
                    {
                        IGDStaging IGD = PortalDB.IGDStagings.Find(i.IGDStagingID);

                        if(IGD != null)
                        {
                            IGD.ClientConfirmed = false;
                            IGD.EventDate = DateTime.Now;
                            IGD.Status = Status;
                            PortalDB.Entry(IGD).State = System.Data.Entity.EntityState.Modified;
                            PortalDB.SaveChanges();
                        }
                    }

                    PortalDB.Dispose();
                }
                catch(Exception ex)
                {
                    Logger.Error(ex.Message);
                }
            }
        }

        private void Arr_ClC_Msg_ToProcess_list()
        {
            try
            {
                PortalDB = new WMSHostCustomerPortalEntities();

                List<IGDStaging> lst = PortalDB.IGDStagings.AsNoTracking().Where(a => a.Status == "ARR").OrderByDescending(a => a.EventDate).Take(100).ToList();

                PortalDB.Dispose();

                ProcessRetrievedIGDS(lst);
            }
            catch(Exception ex)
            {
                Logger.Error(ex.Message);
            }
        }
        #endregion

        #region POReceipt
        private void ProcessEachAndSend(List<IGDStaging> lst, Principal Prn)
        {
            foreach (var i in lst)
            {
                POReceiptRequest Req = new POReceiptRequest();
                Req.Body = new POReceiptRequestBody();

                try
                {
                    //Principal Prn = PortalDB.Principals.AsNoTracking().Where(a => a.PrincipalID == i.PrincipalID && a.WebServiceIntegration == true).FirstOrDefault(); 

                    Logger.Info("Process PO Receipt Client Msg - MoveRef " + i.MoveRef + " Line [" + i.OrderLineNo + "] Principal Code [" + Prn.PrincipalCode + "]");

                    IGDReceipt IR = PortalDB.IGDReceipts.AsNoTracking().Where(a => a.IGDStagingID == i.IGDStagingID).ToList().LastOrDefault();
                    if (IR != null)
                    {
                        CPOReceiptSoapClient cl = new CPOReceiptSoapClient();

                        List<IGDReceiptSerial> serList = PortalDB.IGDReceiptSerials.AsNoTracking().Where(a => a.IGDReceiptID == IR.ID).ToList();

                        ArrayOfString SerialNumbers = new ArrayOfString();

                        foreach (var x in serList)
                        {
                            SerialNumbers.Add(x.SerialNumber);
                        }

                        //POReceiptRequest Req = new POReceiptRequest();
                        //Req.Body = new POReceiptRequestBody();

                        Req.Body.AcceptCount = (int)IR.AcceptCount;
                        Req.Body.MovementRef = IR.MoveRef;
                        Req.Body.OrderLineNo = Convert.ToInt32(IR.LineNumber);
                        Req.Body.PORef = IR.PORef;
                        Req.Body.PrincipalCode = IR.PrincipalCode;
                        Req.Body.ProdCode = IR.ProdCode;
                        Req.Body.ReasonCode = IR.ReasonCode;
                        Req.Body.ReceiptDateTime = (DateTime)IR.ReceiptDT;
                        Req.Body.RejectCount = (int)IR.RejectCount;
                        Req.Body.Resend = false;
                        Req.Body.SerialNumber = SerialNumbers;

                        POReceipt.Response Res = cl.POReceipt(IR.PrincipalCode, false, Convert.ToInt32(IR.LineNumber), IR.ProdCode, IR.MoveRef, IR.PORef, (DateTime)IR.ReceiptDT, (int)IR.RejectCount, IR.ReasonCode, (int)IR.AcceptCount, SerialNumbers);

                        if (Res.success)
                        {
                            if (Prn != null) i.ClientConfirmed = true;
                            i.EventDate = DateTime.Now;
                            PortalDB.Entry(i).State = System.Data.Entity.EntityState.Modified;
                            PortalDB.SaveChanges();
                        }

                        LogTransaction(Req, Res, Res.success, "PO Receipt", Res.reasontext, IR.MoveRef + "." + IR.LineNumber, IR.PrincipalCode);
                    }
                }
                catch(Exception ex)
                {
                    Logger.Error(ex.Message);

                    POReceipt.Response Res = new POReceipt.Response();
                    Res.reasoncode = 1000;
                    Res.reasontext = ex.Message;
                    Res.success = false;
                    
                    LogTransaction(Req, Res, Res.success, "PO Receipt", Res.reasontext, i.MoveRef + "." + i.OrderLineNo, Prn.PrincipalCode);

                    i.ClientConfirmed = true;
                    i.EventDate = DateTime.Now;
                    PortalDB.Entry(i).State = System.Data.Entity.EntityState.Modified;
                    PortalDB.SaveChanges();
                }
            }
        }

        private void PO_Receipt_Msg_ToProcess_list()
        {
            try
            {
                PortalDB = new WMSHostCustomerPortalEntities();

                List<Principal> PrnList = PortalDB.Principals.AsNoTracking().Where(a => a.WebServiceIntegration == true).ToList();

                foreach (var i in PrnList)
                {
                    List<IGDStaging> lst = PortalDB.IGDStagings.Where(a => a.Status == "RECEIPTED" && a.ClientConfirmed == false && a.PrincipalID == i.PrincipalID && a.ReceiptType == "0").Take(100).ToList();
                    ProcessEachAndSend(lst, i);
                }

                PortalDB.Dispose();
            }
            catch(Exception ex)
            {
                Logger.Error(ex.Message);
            }
        }
        #endregion

        #region CustomerReturnReceipt
        private void ProcessEachAndSendCR(List<IGDStaging> lst)
        {
            foreach (var i in lst)
            {
                try
                {
                    Logger.Info("Process CR Receipt Client Msg - MoveRef " + i.MoveRef);

                    IGDReceipt IR = PortalDB.IGDReceipts.AsNoTracking().Where(a => a.IGDStagingID == i.IGDStagingID).ToList().LastOrDefault();
                    if (IR != null)
                    {
                        CustomerReturnReceiptSoapClient cl = new CustomerReturnReceiptSoapClient();

                        List<IGDReceiptSerial> serList = PortalDB.IGDReceiptSerials.AsNoTracking().Where(a => a.IGDReceiptID == IR.ID).ToList();

                        string[] SerialNumbers = new string[serList.Count];
                        int y = 0;
                        foreach (var x in serList)
                        {
                            SerialNumbers[y++] = x.SerialNumber;
                        }

                        CustomerReturn.Response Res = cl.Return(IR.PrincipalCode, false, Convert.ToInt32(IR.LineNumber), IR.ProdCode, IR.MoveRef, IR.PORef, (DateTime)IR.ReceiptDT, (int)IR.RejectCount, IR.ReasonCode, (int)IR.AcceptCount, SerialNumbers);

                        if (Res.success)
                        {
                            Principal Prn = PortalDB.Principals.AsNoTracking().Where(a => a.PrincipalID == i.PrincipalID && a.WebServiceIntegration == true).FirstOrDefault();

                            if (Prn != null) i.ClientConfirmed = true;
                            i.EventDate = DateTime.Now;
                            PortalDB.Entry(i).State = System.Data.Entity.EntityState.Modified;
                            PortalDB.SaveChanges();
                        }
                    }
                }
                catch(Exception ex)
                {
                    i.ClientConfirmed = true;
                    i.EventDate = DateTime.Now;
                    PortalDB.Entry(i).State = System.Data.Entity.EntityState.Modified;
                    PortalDB.SaveChanges();

                    Logger.Error(ex.Message);
                }
            }
        }

        private void CR_Receipt_Msg_ToProcess_list()
        {
            try
            {
                PortalDB = new WMSHostCustomerPortalEntities();

                List<Principal> PrnList = PortalDB.Principals.AsNoTracking().Where(a => a.WebServiceIntegration == true).ToList();

                foreach (var i in PrnList)
                {
                    List<IGDStaging> lst = PortalDB.IGDStagings.Where(a => a.Status == "RECEIPTED" && a.ClientConfirmed == false && a.PrincipalID == i.PrincipalID && a.ReceiptType != "0").Take(100).ToList();
                    ProcessEachAndSendCR(lst);
                }

                PortalDB.Dispose();
            }
            catch(Exception ex)
            {
                Logger.Error(ex.Message);
            }
        }
        #endregion

        #region Main
        private void RunService()
        {
            Logger.Info("Arr Sender Service Started");

            while (ProcessRunning)
            {
                Logger.Info("Processing Arr Msgs");
                Arr_Msg_ToProcess_List();

                Logger.Info("Processing CLC Msgs");
                Arr_ClC_Msg_ToProcess_list();

                Logger.Info("Processing PO Receipt Msgs");
                PO_Receipt_Msg_ToProcess_list();

                Logger.Info("Processing Customer Return Msgs");
                CR_Receipt_Msg_ToProcess_list();

                Logger.Info(" System inteval of " + (30000 / 1000) + " seconds starting ");
                System.Threading.Thread.Sleep(30000);
                Logger.Info(" System inteval of " + (30000 / 1000) + " seconds ended ");
            }

            Logger.Info("Arr Sender Service Stopped");
        }
        #endregion

        #region Helpers

        private static string GetXMLFromObject(object o)
        {
            StringWriter sw = new StringWriter();
            //XmlTextWriter tw = null;
            try
            {
                XmlSerializer serializer = new XmlSerializer(o.GetType());
                //tw = new XmlTextWriter(sw);
                XmlWriter xw = XmlWriter.Create(sw,
                                              new XmlWriterSettings()
                                              {
                                                  OmitXmlDeclaration = true
                                                   ,
                                                  ConformanceLevel = ConformanceLevel.Auto
                                                   ,
                                                  Indent = true
                                              });
                serializer.Serialize(xw, o);
            }
            catch (Exception)
            {
                //Handle Exception Code
            }
            finally
            {
                sw.Close();
            }
            return sw.ToString();
        }

        private void LogTransaction(object Req, object Res, bool Success, string TrType, string TrErr, string TrID, string PrincipalCode)
        {
            string ReqStr = GetXMLFromObject(Req);
            string ResStr = GetXMLFromObject(Res);

            WMSHostCustomerPortalEntities db = new WMSHostCustomerPortalEntities();

            WebServiceLog Log = new WebServiceLog();
            Log.TransactionError = TrErr;
            Log.TransactionDate = DateTime.Now;
            Log.TransactionID = TrID.ToUpper();
            Log.TransactionResult = !Success ? "FAILED" : "SUCCESS";
            Log.TransactionType = TrType.ToUpper();
            Log.TransactionXml = ReqStr + "\\n" + ResStr;
            Log.PrincipalCode = PrincipalCode.ToUpper();
            Log.Direction = "OUTGOING";
            db.WebServiceLogs.Add(Log);
            db.SaveChanges();

            db.Dispose();
        }

        #endregion

        protected override void OnStart(string[] args)
        {
        }

        protected override void OnStop()
        {
            ProcessRunning = false;
        }
    }
}
