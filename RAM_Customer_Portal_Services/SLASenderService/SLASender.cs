﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ServicesPortalDBModel;
using ServicesWmsHostDBModel;
using SLASenderService.StockLevelAdjustment;

namespace SLASenderService
{
    public partial class SLASender : ServiceBase
    {
        WMSHostCustomerPortalEntities PortalDB;
        RAMWMSHOSTEntities WmsHostDB;
        private bool ProcessRunning = false;

        public SLASender()
        {
            InitializeComponent();

            ProcessRunning = true;
            var threadStart = new ThreadStart(RunService);
            var thread = new Thread(threadStart);
            thread.Start();
        }

        #region Stock Level Adjustment
        private void ProcessSLAs(List<MsgIn_SLA> Lst)
        {
            foreach (var SLA in Lst)
            {
                try
                {
                    if (!ProcessRunning) break;

                    PortalDB = new WMSHostCustomerPortalEntities();

                    Logger.Info("Process SLA Msg - Product " + SLA.ProdCode);

                    string ProcErr = "";
                    DateTime ProcStartDT = DateTime.Now;

                    Principal Prn = PortalDB.Principals.AsNoTracking().Where(a => a.PrincipalCode == SLA.PrincipalCode).FirstOrDefault();

                    if (Prn != null)
                    {
                        ProductStaging Prod = PortalDB.ProductStagings.Where(a => a.ProdCode == SLA.ProdCode && a.Deleted == false && a.PrincipalID == Prn.PrincipalID).FirstOrDefault();

                        if (Prod != null)
                        {
                            SLA sla = new SLA();
                            sla.AvailabilityState = SLA.AvailabilityState;
                            sla.CreateDate = SLA.RecordDT;
                            sla.PrincipalCode = SLA.PrincipalCode;
                            sla.ProdCode = SLA.ProdCode;
                            sla.QuantityChange = SLA.QuantityChange;
                            sla.ReasonCode = SLA.ReasonCode;
                            sla.SerialCount = SLA.SerialCount;
                            sla.SiteCode = SLA.SiteCode;

                            if (Prn.WebServiceIntegration == true) sla.ClientConfirmed = false;

                            PortalDB.SLAs.Add(sla);
                            PortalDB.SaveChanges();

                            if (Prod.Serialised == true)
                            {
                                List<MsgIn_SLA_SN> SerList = WmsHostDB.MsgIn_SLA_SN.AsNoTracking().Where(a => a.SLAID == SLA.SLAID).ToList();
                                List<SLASerial> SlaSerList = new List<SLASerial>();
                                List<Serial> ProdSerList = new List<Serial>();
                                List<string> MySlaSerList = new List<string>();
                                int Count = 0;
                                foreach (var i in SerList)
                                {
                                    SLASerial sSer = new SLASerial();
                                    sSer.SerialNumber = i.SerialNumber;
                                    sSer.SLAID = sla.ID;

                                    SlaSerList.Add(sSer);
                                    Count++;

                                    if (SlaSerList.Count == 1000)
                                    {
                                        Count = 0;
                                        PortalDB.SLASerials.AddRange(SlaSerList);
                                        PortalDB.SaveChanges();
                                        PortalDB.Dispose();
                                        PortalDB = new WMSHostCustomerPortalEntities();
                                        SlaSerList = new List<SLASerial>();
                                    }

                                    MySlaSerList.Add(i.SerialNumber);
                                }

                                if (SlaSerList.Count > 0)
                                {
                                    PortalDB.SLASerials.AddRange(SlaSerList);
                                    PortalDB.SaveChanges();
                                    PortalDB.Dispose();
                                    PortalDB = new WMSHostCustomerPortalEntities();
                                }

                                List<Serial> ProdSerLst = PortalDB.Serials.Where(a => MySlaSerList.Contains(a.SerialNumber) && a.ProductStagingID == Prod.ProductStagingID).ToList();
                                List<Serial> UniqueLst = new List<Serial>();
                                List<string> UniqueLstStr = new List<string>();
                                List<string> DispLstStr = new List<string>();
                                List<int> AvailbleSerList = new List<int>();

                                if (ProdSerLst.Count > 0)
                                {
                                    foreach (var s in MySlaSerList)
                                    {
                                        var ToAdd = ProdSerLst.Where(a => a.SerialNumber == s).LastOrDefault();
                                        if (ToAdd != null)
                                        {
                                            UniqueLst.Add(ToAdd);
                                        }
                                    }
                                }

                                foreach (var u in UniqueLst)
                                {
                                    if (u.OrderID == null)
                                    {
                                        AvailbleSerList.Add(u.ID);
                                        UniqueLstStr.Add(u.SerialNumber);
                                    }
                                    else
                                    {
                                        DispLstStr.Add(u.SerialNumber);
                                    }
                                }

                                foreach (var s in MySlaSerList)
                                {
                                    if ((UniqueLstStr.IndexOf(s) < 0) && (DispLstStr.IndexOf(s) < 0))
                                    {
                                        Serial ProdSer = new Serial();
                                        ProdSer.EventDate = SLA.RecordDT;
                                        ProdSer.ProductStagingID = Prod.ProductStagingID;
                                        ProdSer.SerialNumber = s;
                                        ProdSer.SLAID = sla.ID;
                                        ProdSer.Status = sla.QuantityChange > 0 ? "AVAILABLE" : "SLA DELETE";

                                        ProdSerList.Add(ProdSer);

                                        if (SerList.Count == 1000)
                                        {
                                            PortalDB.Serials.AddRange(ProdSerList);
                                            PortalDB.SaveChanges();

                                            PortalDB.Dispose();
                                            PortalDB = new WMSHostCustomerPortalEntities();
                                            ProdSerList = new List<Serial>();
                                        }
                                    }
                                }

                                if (ProdSerList.Count > 0)
                                {
                                    PortalDB.Serials.AddRange(ProdSerList);
                                    PortalDB.SaveChanges();
                                    PortalDB.Dispose();
                                    PortalDB = new WMSHostCustomerPortalEntities();
                                }

                                if (AvailbleSerList.Count > 0)
                                {
                                    Count = 0;
                                    ProdSerList = PortalDB.Serials.Where(a => AvailbleSerList.Contains(a.ID)).ToList();
                                    foreach (var a in ProdSerList)
                                    {
                                        if (sla.QuantityChange < 0)
                                        {
                                            a.Status = "SLA DELETE";
                                        }
                                        else
                                        {
                                            a.Status = "AVAILABLE";
                                        }

                                        a.EventDate = SLA.RecordDT;
                                        a.SLAID = sla.ID;
                                        Count++;

                                        if (Count == 1000)
                                        {
                                            Count = 0;
                                            PortalDB.SaveChanges();
                                            PortalDB.Dispose();
                                            PortalDB = new WMSHostCustomerPortalEntities();
                                        }
                                    }

                                    PortalDB.SaveChanges();
                                    PortalDB.Dispose();
                                    PortalDB = new WMSHostCustomerPortalEntities();
                                }

                                int StockQty = Prod.StockQuantity == null ? 0 : (int)Prod.StockQuantity;
                                Prod.StockQuantity = StockQty + sla.QuantityChange > 0 ? SerList.Count : -SerList.Count;
                            }
                            else
                            {
                                int StockQty = Prod.StockQuantity == null ? 0 : (int)Prod.StockQuantity;
                                Prod.StockQuantity = StockQty + sla.QuantityChange;
                            }

                            if (Prod.StockQuantity < 0) Prod.StockQuantity = 0;
                            Prod.EventDate = DateTime.Now;
                            PortalDB.Entry(Prod).State = System.Data.Entity.EntityState.Modified;
                            PortalDB.SaveChanges();
                        }
                        else
                        {
                            ProcErr = "Product Not Found In The Customer Portal";
                        }
                    }
                    else
                    {
                        ProcErr = "Principal Not Found In The Customer Portal";
                    }

                    MsgIn_SLA UpdateSla = WmsHostDB.MsgIn_SLA.Find(SLA.SLAID);

                    UpdateSla.RecordState = !String.IsNullOrEmpty(ProcErr) ? "PROCESSERROR" : "PROCESSED";
                    UpdateSla.ProcStartDT = ProcStartDT;
                    UpdateSla.ProcEndDT = DateTime.Now;
                    UpdateSla.ProcErrors = ProcErr;
                    WmsHostDB.Entry(UpdateSla).State = System.Data.Entity.EntityState.Modified;
                    WmsHostDB.SaveChanges();

                    PortalDB.Dispose();
                }
                catch(Exception ex)
                {
                    Logger.Error(ex.Message);
                }
            }
        }

        private void SLA_Msg_ToProcess_List()
        {
            try
            {
                WmsHostDB = new RAMWMSHOSTEntities();

                List<MsgIn_SLA> Lst = WmsHostDB.MsgIn_SLA.AsNoTracking().Where(a => a.RecordState == "READY").Take(100).OrderByDescending(a => a.RecordDT).ToList();

                ProcessSLAs(Lst);

                WmsHostDB.Dispose();
            }
            catch(Exception ex)
            {
                Logger.Error(ex.Message);
            }
        }
        #endregion

        #region Client SLA
        private void ClientSendEach(List<SLA> Lst)
        {
            foreach (var i in Lst)
            {
                PortalDB = new WMSHostCustomerPortalEntities();

                try
                {
                    if (!ProcessRunning) break;

                    Logger.Info("Process SLA Client Msg - Product " + i.ProdCode + " | ID: " + i.ID);

                    List<SLASerial> SerList = PortalDB.SLASerials.AsNoTracking().Where(a => a.SLAID == i.ID).ToList();

                    int SerCount = SerList.Count > 0 ? SerList.Count : (int)i.SerialCount;
                    string[] SerialNumbers = new string[SerList.Count];
                    int x = 0;

                    foreach (var ser in SerList)
                    {
                        SerialNumbers[x++] = ser.SerialNumber;
                    }

                    StockLevelAdjustmentSoapClient cl = new StockLevelAdjustmentSoapClient("StockLevelAdjustmentSoap");
                    Response Res = cl.SLA(i.PrincipalCode, i.ProdCode, (int)i.QuantityChange, i.ReasonCode, i.AvailabilityState, SerCount, SerialNumbers);

                    Logger.Info("SLA Client Msg - Response " + Res.success);

                    if (Res.success)
                    {
                        var Urec = PortalDB.SLAs.Find(i.ID);

                        if(Urec != null)
                        {
                            Urec.ClientConfirmed = true;
                            PortalDB.Entry(Urec).State = System.Data.Entity.EntityState.Modified;
                            PortalDB.SaveChanges();
                        }
                    }

                    PortalDB.Dispose();
                }
                catch(Exception ex)
                {
                    Logger.Error(ex.Message);

                    var Urec = PortalDB.SLAs.Find(i.ID);

                    if (Urec != null)
                    {
                        Urec.ClientConfirmed = true;
                        PortalDB.Entry(Urec).State = System.Data.Entity.EntityState.Modified;
                        PortalDB.SaveChanges();
                    }

                    PortalDB.Dispose();
                }
            }
        }

        private void SLA_ClientSend()
        {
            try
            {
                PortalDB = new WMSHostCustomerPortalEntities();

                List<SLA> Lst = PortalDB.SLAs.AsNoTracking().Where(a => a.ClientConfirmed == false).Take(100).ToList();

                PortalDB.Dispose();

                ClientSendEach(Lst);
            }
            catch(Exception ex)
            {
                Logger.Error(ex.Message);
            }
        }
        #endregion

        #region Main
        private void RunService()
        {
            Logger.Info("SLA Sender Service Started");

            while (ProcessRunning)
            {
                Logger.Info("Processing SLA Msgs");
                SLA_Msg_ToProcess_List();

                Logger.Info("Processing Client SLA Msgs");
                SLA_ClientSend();

                Logger.Info(" System inteval of " + (30000 / 1000) + " seconds starting ");
                System.Threading.Thread.Sleep(30000);
                Logger.Info(" System inteval of " + (30000 / 1000) + " seconds ended ");
            }

            Logger.Info("SLA Sender Service Stopped");
        }
        #endregion

        protected override void OnStart(string[] args)
        {
        }

        protected override void OnStop()
        {
            ProcessRunning = false;
        }
    }
}
