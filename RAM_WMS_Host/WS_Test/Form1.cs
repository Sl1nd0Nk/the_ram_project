﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


 
using System.Xml;
using System.Net;
using System.IO;



namespace WS_Test {
    public partial class Form1 : Form {
        public Form1() {
            InitializeComponent();
        }

        private void btnStart_Click(object sender, EventArgs e) {  
            try {
                BLL.MatfloMessages msg = new BLL.MatfloMessages();

                LogAdd("CLCUpdate");
                BLL.WSResult result = msg.CLCUpdate("001", "OG", "", "1", "", 3, "0", 0, 2, new string[] { "SN001", "SN002" });
                LogAdd("Success:" + result.Success.ToString());
                LogAdd("ReasonText:" + result.ReasonText);
                LogAdd("FaultCode:" + result.FaultCode);
                LogAdd("FaultString:" + result.FaultString);

                LogAdd("IGDUpdate");
                BLL.WSResult result2 = msg.IGDUpdate("001", "OG", "Prod 001", "EAN 001", "Short", "Long", false, "A", "B", "1", 1, "Q", "REF 001", "PO 001", Convert.ToDateTime("2013-08-21"), Convert.ToDateTime("2013-08-21 12:35"));
                LogAdd("Success:" + result2.Success.ToString());
                LogAdd("ReasonText:" + result2.ReasonText);
                LogAdd("FaultCode:" + result2.FaultCode);
                LogAdd("FaultString:" + result2.FaultString);

                LogAdd("ORDUpdate");
                List<BLL.ORD_LN> orderLines = new List<BLL.ORD_LN>();
                orderLines.Add(new BLL.ORD_LN(1, "P", "Prod 001", "Product 001", 1, 15, 2, 0, "1234567890"));
                BLL.WSResult result3 = msg.ORDUpdate("001", "OG", "ORD 001", 1, Convert.ToDateTime("2013-08-21"), "Company 001", "Test order", "CO 001", "ACC001", "Mr Smith", "15 Main Road", "big building", "021 123 1234", "CapeTown", "", "8000", false, "VAT 001", false, "1", true, "Sales", "Test", "Proc", "15 Main Rd", "big building", "021 123 1234", "CapeTown", "", "8000", "8000", 0.0, 12.0,0,"1234567","ra1","ra2","ra3",false,false, "", orderLines);
                LogAdd("Success:" + result3.Success.ToString());
                LogAdd("ReasonText:" + result3.ReasonText);
                LogAdd("FaultCode:" + result3.FaultCode);
                LogAdd("FaultString:" + result3.FaultString);  

            } catch (Exception exc) {
                LogAdd("btnStart_Click Exception:\r\n" + exc.Message);
                LogAdd("             Source:\r\n" + exc.Source);
                LogAdd("             StackTrace:\r\n" + exc.StackTrace);
                return;
            }
        }

        private void LogAdd(string msg) {
            listLog.Items.Add(msg);
            listLog.TopIndex = listLog.Items.Count - 1;
        }

        private void listLog_SelectedIndexChanged(object sender, EventArgs e) {
            try {
                txtLogLine.Text = listLog.SelectedItem.ToString();
            } catch { }
        }


    }
}