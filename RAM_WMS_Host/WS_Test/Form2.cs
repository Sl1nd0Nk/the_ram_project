﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms; 

namespace WS_Test {
    public partial class Form2 : Form {
        public Form2() {
            InitializeComponent();
            timerAuto.Enabled = true;
        }


        private void LogAdd(string msg, bool isError) {
            listLog.Items.Add(DateTime.Now.ToString("HH:mm:ss.fff") + " " + msg);
            Logging.Instance.LogToFile(msg, isError);

            

            if (checkScroll.Checked) {
                if (listLog.Items.Count > 200) listLog.Items.RemoveAt(0);
                listLog.TopIndex = listLog.Items.Count - 1;
            } else {
                if (listLog.Items.Count > 1000) listLog.Items.RemoveAt(0);
            }
        }

        private void LogAdd(string msg) {
             LogAdd(  msg,false);
        }

        private void LogAppend(string msg) {
            if (listLog.Items.Count == 0) LogAdd(msg);
            else {
                string txt = listLog.Items[listLog.Items.Count - 1].ToString();
                listLog.Items.RemoveAt(listLog.Items.Count - 1);
                listLog.Items.Add(txt + " " + msg);
            }

            Logging.Instance.LogToFile(msg, false);
        }

        private void btnStart_Click(object sender, EventArgs e) {
            btnStart.Enabled = false;
            checkAuto.Enabled = false;
            Process("AllOutbound");
            btnStart.Enabled = true;
            checkAuto.Enabled = true;
        }

        private void Process(string MsgTypeToProcess) {
            try {
                LogAdd("----------------");

                DataSet toProcess = BLL.Msg_ToProcess.Msg_ToProcess_List(MsgTypeToProcess);
                if (toProcess.Tables[0].Rows.Count == 0) {
                    LogAdd("Nothing to process");
                } else {
                    BLL.MatfloMessages msgWS = new BLL.MatfloMessages();
                    foreach (DataRow row in toProcess.Tables[0].Rows) {
                        int RecordID = Convert.ToInt32(row["RecordID"]);
                        DateTime RecordDT = Convert.ToDateTime(row["RecordDT"]);
                        DateTime NextStartDT = Convert.ToDateTime(row["NextStartDT"]);
                        string MsgType = row["MsgType"].ToString();
                        int MsgID = Convert.ToInt32(row["MsgID"]);
                        BLL.WSResult result = new BLL.WSResult();
                        LogAdd("ToProcess: " + RecordID.ToString() + " " + RecordDT.ToString("yyyy-MM-dd HH:mm:ss") + " " + NextStartDT.ToString("yyyy-MM-dd HH:mm:ss") + " " + MsgType + " " + MsgID.ToString());
                        try {                             
                            BLL.StartProcessResult startResult = BLL.Msg_ToProcess.Msg_ToProcess_StartProcess(RecordID);                             
                            LogAdd("Start Process " + startResult.MsgType + ": " + MsgID.ToString());
 
                                if (startResult.MsgType == "MsgOut_CLC") {
                                    result = msgWS.CLCUpdate(MsgID);
                                } else if (startResult.MsgType == "MsgOut_IGD") {
                                    result = msgWS.IGDUpdate(MsgID);
                                } else if (startResult.MsgType == "MsgOut_OCA") {
                                    result = msgWS.OCAUpdate(MsgID);
                                } else if (startResult.MsgType == "MsgOut_ORD") {
                                    result = msgWS.ORDUpdate(MsgID);
                                } else {
                                    LogAdd("    MsgType not supported");
                                    result.Success = false;
                                    result.ReasonText = "    MsgType not supported";
                                }

                                LogAppend("    Success:" + result.Success.ToString() + "  " + result.ReasonText);
                                bool updateSuccess = BLL.Msg_ToProcess.Msg_ToProcess_UpdateProcess(RecordID, result.Success, result.ReasonText);
                                LogAppend("    Updated:" + updateSuccess.ToString()); 
                        } catch (Exception exc1) {
                            result.Success = false;
                            result.ReasonText = exc1.Message;
                            LogAdd("      Exception (ex1): "  + exc1.Message, true); 
                            bool updateSuccess = BLL.Msg_ToProcess.Msg_ToProcess_UpdateProcess(RecordID, false, exc1.Message);
                            LogAppend("    Updated:" + updateSuccess.ToString()); 
                        }
                         
                    }
                    msgWS = null;
                }
            } catch (Exception exc2) {
                LogAdd("Exception (ex2): "  + exc2.Message, true);
            }

            
        }


        private DateTime mLastEndDT = DateTime.Now;

        private void timerAuto_Tick(object sender, EventArgs e) {
            timerAuto.Enabled = false;
            if (checkAuto.Checked == false) mLastEndDT = DateTime.Now;
            btnStart.Enabled = !checkAuto.Checked;

            TimeSpan ts = new TimeSpan(DateTime.Now.Ticks - mLastEndDT.Ticks);
            int s = Convert.ToInt32( ts.TotalSeconds);
            checkAuto.Text = "Auto " + s.ToString();

            if (s > Config.SyncInterval) { 
                Process("AllOutbound");
                mLastEndDT = DateTime.Now;
            }

            timerAuto.Enabled = true;
        }
 

    }
}