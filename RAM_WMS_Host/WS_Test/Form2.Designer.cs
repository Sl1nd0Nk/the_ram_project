﻿namespace WS_Test {
    partial class Form2 {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            this.btnStart = new System.Windows.Forms.Button();
            this.listLog = new System.Windows.Forms.ListBox();
            this.checkAuto = new System.Windows.Forms.CheckBox();
            this.timerAuto = new System.Windows.Forms.Timer(this.components);
            this.checkScroll = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(12, 12);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(75, 23);
            this.btnStart.TabIndex = 0;
            this.btnStart.Text = "Start";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // listLog
            // 
            this.listLog.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listLog.FormattingEnabled = true;
            this.listLog.Location = new System.Drawing.Point(12, 53);
            this.listLog.Name = "listLog";
            this.listLog.Size = new System.Drawing.Size(920, 485);
            this.listLog.TabIndex = 1;
            // 
            // checkAuto
            // 
            this.checkAuto.AutoSize = true;
            this.checkAuto.Checked = true;
            this.checkAuto.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkAuto.Location = new System.Drawing.Point(106, 16);
            this.checkAuto.Name = "checkAuto";
            this.checkAuto.Size = new System.Drawing.Size(48, 17);
            this.checkAuto.TabIndex = 2;
            this.checkAuto.Text = "Auto";
            this.checkAuto.UseVisualStyleBackColor = true;
            // 
            // timerAuto
            // 
            this.timerAuto.Interval = 156;
            this.timerAuto.Tick += new System.EventHandler(this.timerAuto_Tick);
            // 
            // checkScroll
            // 
            this.checkScroll.AutoSize = true;
            this.checkScroll.Checked = true;
            this.checkScroll.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkScroll.Location = new System.Drawing.Point(183, 18);
            this.checkScroll.Name = "checkScroll";
            this.checkScroll.Size = new System.Drawing.Size(52, 17);
            this.checkScroll.TabIndex = 3;
            this.checkScroll.Text = "Scroll";
            this.checkScroll.UseVisualStyleBackColor = true;
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(944, 554);
            this.Controls.Add(this.checkScroll);
            this.Controls.Add(this.checkAuto);
            this.Controls.Add(this.listLog);
            this.Controls.Add(this.btnStart);
            this.Name = "Form2";
            this.Text = "WMS Host -> Matflo Sync";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.ListBox listLog;
        private System.Windows.Forms.CheckBox checkAuto;
        private System.Windows.Forms.Timer timerAuto;
        private System.Windows.Forms.CheckBox checkScroll;
    }
}