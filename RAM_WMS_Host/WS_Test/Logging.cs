﻿using System;
using System.IO;
using System.Reflection;
using System.Collections;
using System.Diagnostics;
using System.Xml;
using System.Xml.Serialization;

namespace WS_Test {
    class Logging {

        #region singleton

        private static volatile Logging mInstance;
        private static object mInstanceLock = new Object();
        private Logging() { }
        public static Logging Instance {
            get {
                if (mInstance == null) {
                    lock (mInstanceLock) {
                        if (mInstance == null)
                            mInstance = new Logging();
                    }
                }
                return mInstance;
            }
        }

        #endregion

        #region methods
        private   object mLockObj = new object();

        public   void LogToSystemEventLog(string sEvent, int eventID) {
            lock (mLockObj) {
                try {
                    string sSource;
                    string sLog;
                    sSource = ExecutingAssemblyName_NoLock;
                    sLog = "Application";
                    if (!EventLog.SourceExists(sSource)) EventLog.CreateEventSource(sSource, sLog);
                    EventLog.WriteEntry(sSource, sEvent, EventLogEntryType.Information, eventID);
                } catch (Exception) { }
            }
        }

        public   void LogToSystemEventLog_Warning(string sEvent, int eventID) {
            lock (mLockObj) {
                try {
                    string sSource;
                    string sLog;
                    sSource = ExecutingAssemblyName_NoLock;
                    sLog = "Application";
                    if (!EventLog.SourceExists(sSource)) EventLog.CreateEventSource(sSource, sLog);
                    EventLog.WriteEntry(sSource, sEvent, EventLogEntryType.Warning, eventID);
                } catch (Exception) { }
            }
        }

        private   void LogToSystemEventLog_Warning_NoLock(string sEvent, int eventID) {
            try {
                string sSource;
                string sLog;
                sSource = ExecutingAssemblyName_NoLock;
                sLog = "Application";
                if (!EventLog.SourceExists(sSource)) EventLog.CreateEventSource(sSource, sLog);
                EventLog.WriteEntry(sSource, sEvent, EventLogEntryType.Warning, eventID);
            } catch (Exception) { }
        }

        public   void LogToFile(string msg, bool isError) {
            lock (mLockObj) {
                try {
                    if ((Config.LogToFile_All) | (Config.LogToFile_Errors & isError)) {
                        string filePath = Path.Combine(ExecutingAssemblyPath_NoLock, "Log");
                        if (File_FolderExists(filePath) == false) File_CreateFolder(filePath);
                        filePath = Path.Combine(filePath, DateTime.Now.ToString("yyyy-MM-dd"));
                        if (File_FolderExists(filePath) == false) File_CreateFolder(filePath);
                        string logFileName = ExecutingAssemblyName_NoLock + "_" + DateTime.Now.ToString("yyyy-MM-dd_HH") + ".log";
                        filePath = Path.Combine(filePath, logFileName);
                        if (msg.Length == 0)
                            File_WriteLine(filePath, msg, true);
                        else
                            File_WriteLine(filePath, DateTime.Now.ToString("HH:mm:ss") + " " + msg, true);
                    }
                } catch (Exception exc) {
                    LogToSystemEventLog_Warning_NoLock(exc.Message, 30201);
                }
            }
        }

        public   void LogDataToFile(string filePrefix, object obj) {
            try {
                if (Config.LogToFile_Data) {
                    string filePath = Path.Combine(ExecutingAssemblyPath_NoLock, "Log");
                    if (File_FolderExists(filePath) == false) File_CreateFolder(filePath);
                    filePath = Path.Combine(filePath, DateTime.Now.ToString("yyyy-MM-dd"));
                    if (File_FolderExists(filePath) == false) File_CreateFolder(filePath);
                    string logFileName = filePrefix + "_" + DateTime.Now.ToString("yyyy-MM-dd_HHmmss") + ".xml";
                    filePath = Path.Combine(filePath, logFileName);

                    string xmlText = (obj.GetType() == typeof ( System.String )) ?obj.ToString(): ObjectToXMLText(obj)    ;
                    File_WriteLine(filePath, xmlText);
                }
            } catch (Exception exc) {
                LogToFile("LogDataToFile Err:" + exc.Message, true);
                LogToSystemEventLog_Warning_NoLock(exc.Message, 30202);
            }
        }

        private   string ObjectToXMLText(object obj) {
            string xmlText = "Empty";
            XmlSerializer serializer = new XmlSerializer(obj.GetType());
            using (StringWriter writer = new StringWriter()) {
                serializer.Serialize(writer, obj);
                xmlText = writer.ToString();
            }
            return xmlText;
        }
        #endregion

        #region assembly details
        private string ExecutingAssemblyPath_NoLock {
            //warning - never write to log from this method
            get {
                string path = string.Empty;
                path = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
                if (path.StartsWith(@"file:\")) path = path.Substring(6, path.Length - 6); 
                return path;
            }
        }

        public string ExecutingAssemblyName {
            //warning - never write to log from this method
            get {
                lock (mLockObj) {
                    string name = string.Empty;
                    name = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name.ToString();
                    return name;
                }
            }
        }

        private string ExecutingAssemblyName_NoLock {
            //warning - never write to log from this method
            get {
                string  name = string.Empty;
                name = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name.ToString();
                return  name;
            }
        }
        #endregion


        #region fileops //switch them on as needed

        private   string CrLf {
            //warning - never write to log from this method
            get { return "\r\n"; }
        }

        private   byte[] StrToByteArray(string str) {
            //warning - never write to log from this method
            try {
                System.Text.ASCIIEncoding encoding = new System.Text.ASCIIEncoding();
                return encoding.GetBytes(str);
            } catch {
                return new byte[0];
            }
        }

        /// <summary>
        /// Append UTF-8 text to file. 
        /// Will create the file if not existing, will not overwrite existing file.
        /// </summary>
        /// <param name="FileName">Filename including full path</param>
        /// <param name="Content">UTF-8 text</param>
        private   bool File_WriteLine(string FileName, string Content) {
            //warning - never write to log from this method
            try {
                return File_WriteLine(FileName, Content, false);
            } catch {
                return false;
            }
        }

        /// <summary>
        /// Append UTF-8 text to file. 
        /// Will create the file if not existing, will not overwrite existing file.
        /// </summary>
        /// <param name="FileName">Filename including full path</param>
        /// <param name="Content">UTF-8 text</param>
        ///	<param name="FlushNow">Indicates whether to flush immediately</param>
        private   bool File_WriteLine(string FileName, string Content, Boolean FlushNow) {
            //warning - never write to log from this method
            try {
                Content += CrLf;
                FileStream _fs = File.Open(FileName, FileMode.Append, FileAccess.Write, FileShare.ReadWrite);
                Byte[] _data = StrToByteArray(Content);
                _fs.Write(_data, 0, _data.Length);
                if (FlushNow) _fs.Flush();
                _fs.Close();
                return true;
            } catch {
                return false;
            }
        }


        /// <summary>
        /// Test for folder existence
        /// </summary>
        /// <param name="FolderName">name of folder</param>
        /// <returns>true if folder exists</returns>
        private   bool File_FolderExists(string FolderName) {
            try {
                DirectoryInfo _info = new DirectoryInfo(FolderName);
                return _info.Exists;
            } catch {
                return false;
            }
        }

        /// <summary>
        /// Create a new folder
        /// </summary>
        /// <param name="FolderName">Name of new folder</param>
        private   bool File_CreateFolder(string FolderName) {
            try {
                DirectoryInfo _info = new DirectoryInfo(FolderName);
                _info.Create();
                return true;
            } catch {
                return false;
            }
        }



        /// <summary>
        /// Test for file existence
        /// </summary>
        /// <param name="FileName">Filename including full path</param>
        /// <returns>true if file exists</returns>
        private   bool File_FileExists(string FileName) {
            try {
                FileInfo file = new FileInfo(FileName);
                return file.Exists;
            } catch {
                return false;
            }
        }

        /// <summary>
        /// Rename File
        /// </summary>
        /// <param name="OldFileName">Old Filename including full path</param>
        /// <param name="NewFileName">New Filename including full path</param>
        private   void File_Rename(string OldFileName, string NewFileName) {
            File.Move(OldFileName, NewFileName);
        }

        /// <summary>
        /// Delete File
        /// </summary>
        /// <param name="FileName">Filename including full path</param>
        private   void File_Delete(string FileName) {
            try {
                File.Delete(FileName);
            } catch { }
        }


        /// <summary>
        /// Read UTF-8 text from file
        /// </summary>
        /// <param name="FileName">Filename including full path</param>
        /// <returns>UTF-8 text</returns>
        private   string File_ReadText(string FileName) {
            try {
                StreamReader _sr = File.OpenText(FileName);
                string _out = _sr.ReadToEnd();
                _sr.Close();
                return _out;
            } catch {
                return string.Empty;
            }
        }

        /// <summary>
        /// Write UTF-8 text to file. Will overwrite existing file.
        /// </summary>
        /// <param name="FileName">Filename including full path</param>
        /// <param name="Content">UTF-8 text</param>
        private   void File_WriteText(string FileName, string Content) {
            try {
                StreamWriter _sw = File.CreateText(FileName);
                _sw.Write(Content);
                _sw.Close();
            } catch { }
        }



        #endregion


    }

}