﻿using System; 
using System.Configuration;
using System.Collections.Generic;

namespace WS_Test {
    class Config {

        public static int SyncInterval { get { return GetSettingInt("SyncInterval"); } } 
        
 
        public static string CLC_Uri { get { return GetSetting ("CLC_Uri"); } } 
        public static string CLC_SoapAction { get { return GetSetting ("CLC_SoapAction"); } } 

        public static string IGD_Uri { get { return GetSetting ("IGD_Uri"); } } 
        public static string IGD_SoapAction { get { return GetSetting ("IGD_SoapAction"); } } 

        public static string ORD_Uri { get { return GetSetting ("ORD_Uri"); } } 
        public static string ORD_SoapAction { get { return GetSetting ("ORD_SoapAction"); } } 

        public static string OCA_Uri { get { return GetSetting ("OCA_Uri"); } } 
        public static string OCA_SoapAction { get { return GetSetting ("OCA_SoapAction"); } }



        public static bool LogToFile_Errors { get { return GetSettingBool("LogToFile_Errors"); } }
        public static bool LogToFile_All { get { return GetSettingBool("LogToFile_All"); } }
        public static bool LogToFile_Data { get { return GetSettingBool("LogToFile_Data"); } }
 


 
         #region GetSettings
        private static string GetSetting(string SettingName) {
            try {
                /** Deprecated return ConfigurationSettings.AppSettings[SettingName]; **/
                return ConfigurationManager.AppSettings[SettingName];
            } catch {
                return string.Empty;
            }
        }

        private static List<string> GetSettingList(string SettingName) {
            try {
                string list = GetSetting(SettingName);
                List<string> result = new List<string>();
                foreach (string item in list.Split(new char[] { ","[0], "|"[0] })) {
                    result.Add(item);
                }
                return result;
            } catch {
                return new List<string>();
            }
        }

        private static int GetSettingInt(string settingName, int defaultValue) {
            try {
                return Convert.ToInt32(GetSetting(settingName));
            } catch {
                return defaultValue;
            }
        }
        private static int GetSettingInt(string settingName) {
            return GetSettingInt(settingName, 0);
        }
        private static bool GetSettingBool(string SettingName) {
            try {
                return Convert.ToBoolean(GetSetting(SettingName));
            } catch {
                return false;
            }
        }
        private static DateTime GetSettingDateTime(string SettingName) {
            try {
                return Convert.ToDateTime(GetSetting(SettingName));
            } catch {
                return DateTime.Now;
            }
        }
        #endregion

    }
}
