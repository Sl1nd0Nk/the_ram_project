﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;


using System.Xml;
using System.Xml.Serialization;
using System.Net;
using System.IO;
using System.Security;


namespace WS_Test.BLL {

    [Serializable]
    public class WSResult {
        public bool Success { get; set; }
        public string ReasonText { get; set; }
        public string FaultCode { get; set; }
        public string FaultString { get; set; }
        public WSResult() {
            Success = false; ReasonText = string.Empty;
            FaultCode = string.Empty; FaultString = string.Empty;
        }
        public WSResult(bool success, string reasonText) {
            Success = success; ReasonText = reasonText;
            FaultCode = string.Empty; FaultString = string.Empty;
        }
        public WSResult(string faultCode, string faultString) {
            Success = false; ReasonText = string.Empty;
            FaultCode = faultCode; FaultString = faultString;
        }
    }

    public class ORD_LN {
        public int LineNumber { get; set; }
        public string LineType { get; set; }
        public string ProdCode { get; set; }
        public string LineText { get; set; }
        public int Quantity { get; set; }
        public double UnitCost { get; set; }
        public double Vat { get; set; }
        public double Discount { get; set; }
        public string SerialNumber { get; set; }
        public ORD_LN() { }
        public ORD_LN(int lineNumber, string lineType, string prodCode, string lineText, int quantity, double unitCost, double vat, double discount, string serialNumber) {
            LineNumber = lineNumber;
            LineType = lineType;
            ProdCode = prodCode;
            LineText = lineText;
            Quantity = quantity;
            UnitCost = unitCost;
            Vat = vat;
            Discount = discount;
            SerialNumber = serialNumber;
        }
    }

    class MatfloMessages {

        public MatfloMessages() { }
        private void LogAdd(string msg) {

        }

        public WSResult CLCUpdate(int CLCID) {
            //DataSet ds = MsgOut.MsgOut_CLC.MsgOut_CLC_Get(CLCID);
            DataSet ds = Msg_ToProcess.Msg_Get("MsgOut_CLC", CLCID);

            string SiteCode = Convert.ToString(ds.Tables[0].Rows[0]["SiteCode"]);
            string PrincipalCode = Convert.ToString(ds.Tables[0].Rows[0]["PrincipalCode"]);

            string MoveRef = Convert.ToString(ds.Tables[0].Rows[0]["MoveRef"]);
            string OrderLineNumber = Convert.ToString(ds.Tables[0].Rows[0]["OrderLineNumber"]);
            string ProdCode = Convert.ToString(ds.Tables[0].Rows[0]["ProdCode"]);
            int Quantity = Convert.ToInt32(ds.Tables[0].Rows[0]["Quantity"]);
            string ReceiptType = Convert.ToString(ds.Tables[0].Rows[0]["ReceiptType"]);
            int Discrepancy = Convert.ToInt32(ds.Tables[0].Rows[0]["Discrepancy"]);
            int SerialCount = Convert.ToInt32(ds.Tables[0].Rows[0]["SerialCount"]);
            string[] SerialNumbers = new string[ds.Tables[1].Rows.Count];
            for (int r = 0; r < ds.Tables[1].Rows.Count; r++) {
                SerialNumbers[r] = Convert.ToString(ds.Tables[1].Rows[r]["SerialNumber"]);
            }

            return CLCUpdate(SiteCode,
                                      PrincipalCode,

                                      MoveRef,
                                      OrderLineNumber,
                                      ProdCode,
                                      Quantity,
                                      ReceiptType,
                                      Discrepancy,
                                      SerialCount,
                                    SerialNumbers);
        }

        public WSResult CLCUpdate(string SiteCode,
                                    string PrincipalCode,

                                    string MoveRef,
                                    string OrderLineNumber,
                                    string ProdCode,
                                    int Quantity,
                                    string ReceiptType,
                                    int Discrepancy,
                                    int SerialCount,
                                    string[] SerialNumbers) {
            //build soap envelope
            StringBuilder soapRequest = new StringBuilder();
            soapRequest.AppendLine("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:CLCWs\">");
            soapRequest.AppendLine("<soapenv:Header/>");
            soapRequest.AppendLine("<soapenv:Body>");
            soapRequest.AppendLine("      <urn:CLCUpdate>");
            soapRequest.AppendLine("         <urn:CLC>");
            soapRequest.AppendLine("            <urn:SiteCode>" + SecurityElement.Escape(SiteCode) + "</urn:SiteCode>");
            soapRequest.AppendLine("            <urn:PrincipalCode>" + SecurityElement.Escape(PrincipalCode) + "</urn:PrincipalCode>");
            soapRequest.AppendLine("            <urn:MoveRef>" + SecurityElement.Escape(MoveRef) + "</urn:MoveRef>");
            soapRequest.AppendLine("            <urn:OrderLineNumber>" + SecurityElement.Escape(OrderLineNumber) + "</urn:OrderLineNumber>");
            soapRequest.AppendLine("            <urn:ProdCode>" + SecurityElement.Escape(ProdCode) + "</urn:ProdCode>");
            soapRequest.AppendLine("            <urn:Quantity>" + SecurityElement.Escape(Quantity.ToString()) + "</urn:Quantity>");
            soapRequest.AppendLine("            <urn:ReceiptType>" + SecurityElement.Escape(ReceiptType) + "</urn:ReceiptType>");
            soapRequest.AppendLine("            <urn:Discrepancy>" + SecurityElement.Escape(Discrepancy.ToString()) + "</urn:Discrepancy>");
            soapRequest.AppendLine("            <urn:SerialCount>" + SecurityElement.Escape(SerialCount.ToString()) + "</urn:SerialCount>");
            foreach (string SerialNumber in SerialNumbers) {
                soapRequest.AppendLine("            <urn:SerialNumber>" + SecurityElement.Escape(SerialNumber.ToString()) + "</urn:SerialNumber>");
            }
            soapRequest.AppendLine("         </urn:CLC>");
            soapRequest.AppendLine("      </urn:CLCUpdate>");
            soapRequest.AppendLine("   </soapenv:Body>");
            soapRequest.AppendLine("</soapenv:Envelope>");
            //do request 
            WSResult result = MatfloWebrequest(Config.CLC_Uri, Config.CLC_SoapAction, soapRequest.ToString());
            //return result
            return result;
        }


        public WSResult IGDUpdate(int IGDID) {
           // DataSet ds = MsgOut.MsgOut_IGD.MsgOut_IGD_Get(IGDID);
            DataSet ds = Msg_ToProcess.Msg_Get("MsgOut_IGD", IGDID);

            string SiteCode = Convert.ToString(ds.Tables[0].Rows[0]["SiteCode"]);
            string PrincipalCode = Convert.ToString(ds.Tables[0].Rows[0]["PrincipalCode"]);

            string ProdCode = Convert.ToString(ds.Tables[0].Rows[0]["ProdCode"]);
            string EANCode = Convert.ToString(ds.Tables[0].Rows[0]["EANCode"]);
            string ShortDesc = Convert.ToString(ds.Tables[0].Rows[0]["ShortDesc"]);
            string LongDesc = Convert.ToString(ds.Tables[0].Rows[0]["LongDesc"]);
            bool Serialised = Convert.ToBoolean(ds.Tables[0].Rows[0]["Serialised"]);
            string AnalysisA = Convert.ToString(ds.Tables[0].Rows[0]["AnalysisA"]);
            string AnalysisB = Convert.ToString(ds.Tables[0].Rows[0]["AnalysisB"]);
            string OrderLineNo = Convert.ToString(ds.Tables[0].Rows[0]["OrderLineNo"]);
            int Quantity = Convert.ToInt32(ds.Tables[0].Rows[0]["Quantity"]);
            string ReceiptType = Convert.ToString(ds.Tables[0].Rows[0]["ReceiptType"]);
            string MoveRef = Convert.ToString(ds.Tables[0].Rows[0]["MoveRef"]);
            string PORef = Convert.ToString(ds.Tables[0].Rows[0]["PORef"]);
            DateTime PODate = Convert.ToDateTime(ds.Tables[0].Rows[0]["PODate"]);
            DateTime StockDateTime = Convert.ToDateTime(ds.Tables[0].Rows[0]["StockDateTime"]);

            return IGDUpdate(SiteCode,
                                      PrincipalCode,
                                      ProdCode,
                                      EANCode,
                                      ShortDesc,
                                      LongDesc,
                                      Serialised,
                                      AnalysisA,
                                      AnalysisB,
                                      OrderLineNo,
                                      Quantity,
                                      ReceiptType,
                                      MoveRef,
                                      PORef,
                                      PODate,
                                      StockDateTime);
        }

        public WSResult IGDUpdate(string SiteCode,
                                    string PrincipalCode,
                                    string ProdCode,
                                    string EANCode,
                                    string ShortDesc,
                                    string LongDesc,
                                    bool Serialised,
                                    string AnalysisA,
                                    string AnalysisB,
                                    string OrderLineNo,
                                    int Quantity,
                                    string ReceiptType,
                                    string MoveRef,
                                    string PORef,
                                    DateTime PODate,
                                    DateTime StockDateTime) {
            //build soap envelope
            StringBuilder soapRequest = new StringBuilder();
            soapRequest.AppendLine("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:IGDWs\">");
            soapRequest.AppendLine("   <soapenv:Header/>");
            soapRequest.AppendLine("   <soapenv:Body>");
            soapRequest.AppendLine("      <urn:IGDUpdate>");
            soapRequest.AppendLine("         <urn:IGD>");
            soapRequest.AppendLine("            <urn:SiteCode>" + SecurityElement.Escape(SiteCode) + "</urn:SiteCode>");
            soapRequest.AppendLine("            <urn:PrincipalCode>" + SecurityElement.Escape(PrincipalCode) + "</urn:PrincipalCode>");
            soapRequest.AppendLine("            <urn:ProdCode>" + SecurityElement.Escape(ProdCode) + "</urn:ProdCode>");
            soapRequest.AppendLine("            <urn:EANCode>" + SecurityElement.Escape(EANCode) + "</urn:EANCode>");
            soapRequest.AppendLine("            <urn:ShortDesc>" + SecurityElement.Escape(ShortDesc) + "</urn:ShortDesc>");
            soapRequest.AppendLine("            <urn:LongDesc>" + SecurityElement.Escape(LongDesc) + "</urn:LongDesc>");
            soapRequest.AppendLine("            <urn:Serialised>" + SecurityElement.Escape(Serialised.ToString().ToLower()) + "</urn:Serialised>");
            soapRequest.AppendLine("            <urn:AnalysisA>" + SecurityElement.Escape(AnalysisA) + "</urn:AnalysisA>");
            soapRequest.AppendLine("            <urn:AnalysisB>" + SecurityElement.Escape(AnalysisB) + "</urn:AnalysisB>");
            soapRequest.AppendLine("            <urn:OrderLineNo>" + SecurityElement.Escape(OrderLineNo) + "</urn:OrderLineNo>");
            soapRequest.AppendLine("            <urn:Quantity>" + SecurityElement.Escape(Quantity.ToString()) + "</urn:Quantity>");
            soapRequest.AppendLine("            <urn:ReceiptType>" + SecurityElement.Escape(ReceiptType) + "</urn:ReceiptType>");
            soapRequest.AppendLine("            <urn:MoveRef>" + SecurityElement.Escape(MoveRef) + "</urn:MoveRef>");
            soapRequest.AppendLine("            <urn:PORef>" + SecurityElement.Escape(PORef) + "</urn:PORef>");
            soapRequest.AppendLine("            <urn:PODate>" + SecurityElement.Escape(PODate.ToString("yyyy-MM-dd")) + "</urn:PODate>");
            soapRequest.AppendLine("            <urn:StockDateTime>" + SecurityElement.Escape(StockDateTime.ToString("yyyy-MM-dd HH:mm")) + "</urn:StockDateTime>");
            soapRequest.AppendLine("         </urn:IGD>");
            soapRequest.AppendLine("      </urn:IGDUpdate>");
            soapRequest.AppendLine("   </soapenv:Body>");
            soapRequest.AppendLine("</soapenv:Envelope>");
            //do request 
            WSResult result = MatfloWebrequest(Config.IGD_Uri, Config.IGD_SoapAction, soapRequest.ToString());
            //return result
            return result;
        }


        public WSResult OCAUpdate(int OCAID) {
            //DataSet ds = MsgOut.MsgOut_OCA.MsgOut_OCA_Get(OCAID);
            DataSet ds = Msg_ToProcess.Msg_Get("MsgOut_OCA", OCAID);

            string SiteCode = Convert.ToString(ds.Tables[0].Rows[0]["SiteCode"]);
            string PrincipalCode = Convert.ToString(ds.Tables[0].Rows[0]["PrincipalCode"]);

            string OrderNumber = Convert.ToString(ds.Tables[0].Rows[0]["OrderNumber"]);

            return OCAUpdate(SiteCode,
                                      PrincipalCode,
                                      OrderNumber);
        }

        public WSResult OCAUpdate(string SiteCode,
                                    string PrincipalCode,
                                    string OrderNumber) {
            //build soap envelope
            StringBuilder soapRequest = new StringBuilder();
            soapRequest.AppendLine("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:OCAWs\">");
            soapRequest.AppendLine("   <soapenv:Header/>");
            soapRequest.AppendLine("   <soapenv:Body>");
            soapRequest.AppendLine("      <urn:OCAUpdate>");
            soapRequest.AppendLine("         <urn:OCA>");
            soapRequest.AppendLine("            <urn:SiteCode>" + SecurityElement.Escape(SiteCode) + "</urn:SiteCode>");
            soapRequest.AppendLine("            <urn:PrincipalCode>" + SecurityElement.Escape(PrincipalCode) + "</urn:PrincipalCode>");
            soapRequest.AppendLine("            <urn:OrderNumber>" + SecurityElement.Escape(OrderNumber) + "</urn:OrderNumber>");
            soapRequest.AppendLine("         </urn:OCA>");
            soapRequest.AppendLine("      </urn:OCAUpdate>");
            soapRequest.AppendLine("   </soapenv:Body>");
            soapRequest.AppendLine("</soapenv:Envelope>");
            //do request 
            WSResult result = MatfloWebrequest(Config.OCA_Uri, Config.OCA_SoapAction, soapRequest.ToString());
            //return result
            return result;
        }

        public WSResult ORDUpdate(int ORDID) {
            //DataSet ds = MsgOut.MsgOut_ORD.MsgOut_ORD_Get(ORDID);
            DataSet ds = Msg_ToProcess.Msg_Get("MsgOut_ORD", ORDID);

            string SiteCode = Convert.ToString(ds.Tables[0].Rows[0]["SiteCode"]);
            string PrincipalCode = Convert.ToString(ds.Tables[0].Rows[0]["PrincipalCode"]);

            string OrderNumber = Convert.ToString(ds.Tables[0].Rows[0]["OrderNumber"]);
            int LineCount = Convert.ToInt32(ds.Tables[0].Rows[0]["LineCount"]);
            DateTime DateCreated = Convert.ToDateTime(ds.Tables[0].Rows[0]["DateCreated"]);
            string CompanyName = Convert.ToString(ds.Tables[0].Rows[0]["CompanyName"]);
            string HeaderComment = Convert.ToString(ds.Tables[0].Rows[0]["HeaderComment"]);
            string CustOrderNumber = Convert.ToString(ds.Tables[0].Rows[0]["CustOrderNumber"]);
            string CustAcc = Convert.ToString(ds.Tables[0].Rows[0]["CustAcc"]);
            string InvName = Convert.ToString(ds.Tables[0].Rows[0]["InvName"]);
            string InvAdd1 = Convert.ToString(ds.Tables[0].Rows[0]["InvAdd1"]);
            string InvAdd2 = Convert.ToString(ds.Tables[0].Rows[0]["InvAdd2"]);
            string InvAdd3 = Convert.ToString(ds.Tables[0].Rows[0]["InvAdd3"]);
            string InvAdd4 = Convert.ToString(ds.Tables[0].Rows[0]["InvAdd4"]);
            string InvAdd5 = Convert.ToString(ds.Tables[0].Rows[0]["InvAdd5"]);
            string InvAdd6 = Convert.ToString(ds.Tables[0].Rows[0]["InvAdd6"]);
            bool GSMTest = Convert.ToBoolean(ds.Tables[0].Rows[0]["GSMTest"]);
            string VATNumber = Convert.ToString(ds.Tables[0].Rows[0]["VATNumber"]);
            bool PrintPrice = Convert.ToBoolean(ds.Tables[0].Rows[0]["PrintPrice"]);
            string Priority = Convert.ToString(ds.Tables[0].Rows[0]["Priority"]);
            bool VAP = Convert.ToBoolean(ds.Tables[0].Rows[0]["VAP"]);
            string SalesPerson = Convert.ToString(ds.Tables[0].Rows[0]["SalesPerson"]);
            string SalesCategory = Convert.ToString(ds.Tables[0].Rows[0]["SalesCategory"]);
            string Processor = Convert.ToString(ds.Tables[0].Rows[0]["Processor"]);
            string DeliveryAdd1 = Convert.ToString(ds.Tables[0].Rows[0]["DeliveryAdd1"]);
            string DeliveryAdd2 = Convert.ToString(ds.Tables[0].Rows[0]["DeliveryAdd2"]);
            string DeliveryAdd3 = Convert.ToString(ds.Tables[0].Rows[0]["DeliveryAdd3"]);
            string DeliveryAdd4 = Convert.ToString(ds.Tables[0].Rows[0]["DeliveryAdd4"]);
            string DeliveryAdd5 = Convert.ToString(ds.Tables[0].Rows[0]["DeliveryAdd5"]);
            string DeliveryAdd6 = Convert.ToString(ds.Tables[0].Rows[0]["DeliveryAdd6"]);
            string WMSPostCode = Convert.ToString(ds.Tables[0].Rows[0]["WMSPostCode"]);
            double OrderDiscount = Convert.ToDouble(ds.Tables[0].Rows[0]["OrderDiscount"]);
            double OrderVAT = Convert.ToDouble(ds.Tables[0].Rows[0]["OrderVAT"]);
            //added 2015-01-23 JGE
            int DocToPrint = Convert.ToInt32(ds.Tables[0].Rows[0]["DocToPrint"]);
            //updated
            string IDNumber = Convert.ToString(ds.Tables[0].Rows[0]["IDNumber"]);
            string KYC= Convert.ToString(ds.Tables[0].Rows[0]["KYC"]);
            string CourierName= Convert.ToString(ds.Tables[0].Rows[0]["CourierName"]);
            string CourierService= Convert.ToString(ds.Tables[0].Rows[0]["CourierService"]);
            bool InsuranceRequired= Convert.ToBoolean(ds.Tables[0].Rows[0]["InsuranceRequired"]);
            bool ValidateDelivery = Convert.ToBoolean(ds.Tables[0].Rows[0]["ValidateDelivery"]);
            //added 2020-05-13 SuperSonic Change
            string StoreCode = Convert.ToString(ds.Tables[0].Rows[0]["StoreCode"]);

            //added 2020-05-29 MRP Mobile Change
            VATNumber = !String.IsNullOrEmpty(IDNumber) ? IDNumber : VATNumber;
            int nKYC = 0;
            if (String.IsNullOrEmpty(KYC) || !int.TryParse(KYC, out nKYC))
            {
                nKYC = DocToPrint;
            }

            DocToPrint = nKYC;

            //Lines
            List <ORD_LN> OrderLines = new List<ORD_LN>(ds.Tables[1].Rows.Count);
            for (int r = 0; r < ds.Tables[1].Rows.Count; r++) {
                ORD_LN ln = new ORD_LN(
                   Convert.ToInt32(ds.Tables[1].Rows[r]["LineNumber"]),
                   Convert.ToString(ds.Tables[1].Rows[r]["LineType"]),
                   Convert.ToString(ds.Tables[1].Rows[r]["ProdCode"]),
                   Convert.ToString(ds.Tables[1].Rows[r]["LineText"]),
                   Convert.ToInt32(ds.Tables[1].Rows[r]["Quantity"]),
                   Convert.ToDouble(ds.Tables[1].Rows[r]["UnitCost"]),
                   Convert.ToDouble(ds.Tables[1].Rows[r]["Vat"]),
                   Convert.ToDouble(ds.Tables[1].Rows[r]["Discount"]),
                   Convert.ToString(ds.Tables[1].Rows[r]["SerialNumber"]));
            OrderLines.Add(ln);
            }

            return ORDUpdate(SiteCode,
                                          PrincipalCode,
                                          OrderNumber,
                                          LineCount,
                                          DateCreated,
                                          CompanyName,
                                          HeaderComment,
                                          CustOrderNumber,
                                          CustAcc,
                                          InvName,
                                          InvAdd1,
                                          InvAdd2,
                                          InvAdd3,
                                          InvAdd4,
                                          InvAdd5,
                                          InvAdd6,
                                          GSMTest,
                                          VATNumber,
                                          PrintPrice,
                                          Priority,
                                          VAP,
                                          SalesPerson,
                                          SalesCategory,
                                          Processor,
                                          DeliveryAdd1,
                                          DeliveryAdd2,
                                          DeliveryAdd3,
                                          DeliveryAdd4,
                                          DeliveryAdd5,
                                          DeliveryAdd6,
                                          WMSPostCode,
                                          OrderDiscount,
                                          OrderVAT,
                                          DocToPrint,
                                          IDNumber,
                                          KYC,
                                          CourierName,
                                          CourierService,
                                          InsuranceRequired,
                                          ValidateDelivery,
                                          StoreCode,
                                          OrderLines);
        }

        public WSResult ORDUpdate(string SiteCode,
                                        string PrincipalCode,
                                        string OrderNumber,
                                        int LineCount,
                                        DateTime DateCreated,
                                        string CompanyName,
                                        string HeaderComment,
                                        string CustOrderNumber,
                                        string CustAcc,
                                        string InvName,
                                        string InvAdd1,
                                        string InvAdd2,
                                        string InvAdd3,
                                        string InvAdd4,
                                        string InvAdd5,
                                        string InvAdd6,
                                        bool GSMTest,
                                        string VATNumber,
                                        bool PrintPrice,
                                        string Priority,
                                        bool VAP,
                                        string SalesPerson,
                                        string SalesCategory,
                                        string Processor,
                                        string DeliveryAdd1,
                                        string DeliveryAdd2,
                                        string DeliveryAdd3,
                                        string DeliveryAdd4,
                                        string DeliveryAdd5,
                                        string DeliveryAdd6,
                                        string WMSPostCode,
                                        double OrderDiscount,
                                        double OrderVAT,
                                        int DocToPrint,
                                        string IDNumber,
                                        string KYC,
                                        string CourierName,
                                        string CourierService,
                                        bool InsuranceRequired,
                                        bool ValidateDelivery,
                                        string StoreCode,
        List<ORD_LN> OrderLines) {
            //build soap envelope
            StringBuilder soapRequest = new StringBuilder();
            soapRequest.AppendLine("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:ORDWs\">");
            soapRequest.AppendLine("   <soapenv:Header/>");
            soapRequest.AppendLine("   <soapenv:Body>");
            soapRequest.AppendLine("      <urn:ORDUpdate>");
            soapRequest.AppendLine("         <urn:ORD>");
            soapRequest.AppendLine("            <urn:SiteCode>" + SecurityElement.Escape(SiteCode) + "</urn:SiteCode>");
            soapRequest.AppendLine("            <urn:PrincipalCode>" + SecurityElement.Escape(PrincipalCode) + "</urn:PrincipalCode>");
            soapRequest.AppendLine("            <urn:CustAcc>" + SecurityElement.Escape(CustAcc) + "</urn:CustAcc>");
            soapRequest.AppendLine("            <urn:InvName>" + SecurityElement.Escape(InvName) + "</urn:InvName>");
            soapRequest.AppendLine("            <urn:InvAdd1>" + SecurityElement.Escape(InvAdd1) + "</urn:InvAdd1>");
            soapRequest.AppendLine("            <urn:InvAdd2>" + SecurityElement.Escape(InvAdd2) + "</urn:InvAdd2>");
            soapRequest.AppendLine("            <urn:InvAdd3>" + SecurityElement.Escape(InvAdd3) + "</urn:InvAdd3>");
            soapRequest.AppendLine("            <urn:InvAdd4>" + SecurityElement.Escape(InvAdd4) + "</urn:InvAdd4>");
            soapRequest.AppendLine("            <urn:InvAdd5>" + SecurityElement.Escape(InvAdd5) + "</urn:InvAdd5>");
            soapRequest.AppendLine("            <urn:InvAdd6>" + SecurityElement.Escape(InvAdd6) + "</urn:InvAdd6>");
            soapRequest.AppendLine("            <urn:GSMTest>" + SecurityElement.Escape(GSMTest.ToString().ToLower()) + "</urn:GSMTest>");
            soapRequest.AppendLine("            <urn:VATNumber>" + SecurityElement.Escape(VATNumber) + "</urn:VATNumber>");
            soapRequest.AppendLine("            <urn:PrintPrice>" + SecurityElement.Escape(PrintPrice.ToString().ToLower()) + "</urn:PrintPrice>");
            soapRequest.AppendLine("            <urn:OrderNumber>" + SecurityElement.Escape(OrderNumber) + "</urn:OrderNumber>");
            soapRequest.AppendLine("            <urn:DateCreated>" + SecurityElement.Escape(DateCreated.ToString("yyyy-MM-dd")) + "</urn:DateCreated>");
            soapRequest.AppendLine("            <urn:CompanyName>" + SecurityElement.Escape(CompanyName) + "</urn:CompanyName>");
            soapRequest.AppendLine("            <urn:HeaderComment>" + SecurityElement.Escape(HeaderComment) + "</urn:HeaderComment>");
            soapRequest.AppendLine("            <urn:CustOrderNumber>" + SecurityElement.Escape(CustOrderNumber) + "</urn:CustOrderNumber>");
            soapRequest.AppendLine("            <urn:Priority>" + SecurityElement.Escape(Priority) + "</urn:Priority>");
            soapRequest.AppendLine("            <urn:VAP>" + SecurityElement.Escape(VAP.ToString().ToLower()) + "</urn:VAP>");
            soapRequest.AppendLine("            <urn:SalesPerson>" + SecurityElement.Escape(SalesPerson) + "</urn:SalesPerson>");
            soapRequest.AppendLine("            <urn:SalesCategory>" + SecurityElement.Escape(SalesCategory) + "</urn:SalesCategory>");
            soapRequest.AppendLine("            <urn:Processor>" + SecurityElement.Escape(Processor) + "</urn:Processor>");
            soapRequest.AppendLine("            <urn:DeliveryAdd1>" + SecurityElement.Escape(DeliveryAdd1) + "</urn:DeliveryAdd1>");
            soapRequest.AppendLine("            <urn:DeliveryAdd2>" + SecurityElement.Escape(DeliveryAdd2) + "</urn:DeliveryAdd2>");
            soapRequest.AppendLine("            <urn:DeliveryAdd3>" + SecurityElement.Escape(DeliveryAdd3) + "</urn:DeliveryAdd3>");
            soapRequest.AppendLine("            <urn:DeliveryAdd4>" + SecurityElement.Escape(DeliveryAdd4) + "</urn:DeliveryAdd4>");
            soapRequest.AppendLine("            <urn:DeliveryAdd5>" + SecurityElement.Escape(DeliveryAdd5) + "</urn:DeliveryAdd5>");
            soapRequest.AppendLine("            <urn:DeliveryAdd6>" + SecurityElement.Escape(DeliveryAdd6) + "</urn:DeliveryAdd6>");
            soapRequest.AppendLine("            <urn:WMSPostCode>" + SecurityElement.Escape(WMSPostCode) + "</urn:WMSPostCode>");
            soapRequest.AppendLine("            <urn:OrderDiscount>" + SecurityElement.Escape(OrderDiscount.ToString()) + "</urn:OrderDiscount>");
            soapRequest.AppendLine("            <urn:OrderVAT>" + SecurityElement.Escape(OrderVAT.ToString()) + "</urn:OrderVAT>");
            soapRequest.AppendLine("            <urn:DocToPrint>" + SecurityElement.Escape(DocToPrint.ToString()) + "</urn:DocToPrint>");
            soapRequest.AppendLine("            <urn:StoreCode>" + SecurityElement.Escape(StoreCode) + "</urn:StoreCode>");
          /*soapRequest.AppendLine("            <urn:IDNumber>" + SecurityElement.Escape(IDNumber.ToString()) + "</urn:IDNumber>");
            soapRequest.AppendLine("            <urn:KYC>" + SecurityElement.Escape(KYC.ToString()) + "</urn:KYC>");
            soapRequest.AppendLine("            <urn:CourierName>" + SecurityElement.Escape(CourierName.ToString()) + "</urn:CourierName>");
            soapRequest.AppendLine("            <urn:CourierService>" + SecurityElement.Escape(CourierService.ToString()) + "</urn:CourierService>");
            soapRequest.AppendLine("            <urn:InsuranceRequired>" + SecurityElement.Escape(InsuranceRequired.ToString()) + "</urn:InsuranceRequired>");
            soapRequest.AppendLine("            <urn:ValidateDelivery>" + SecurityElement.Escape(ValidateDelivery.ToString()) + "</urn:ValidateDelivery>");*/
            soapRequest.AppendLine("            <urn:LineCount>" + SecurityElement.Escape(LineCount.ToString()) + "</urn:LineCount>");
            soapRequest.AppendLine("         </urn:ORD>");
            foreach (ORD_LN orderLine in OrderLines) {
                soapRequest.AppendLine("         <urn:LINE>");
                soapRequest.AppendLine("            <urn:LineNumber>" + SecurityElement.Escape(orderLine.LineNumber.ToString()) + "</urn:LineNumber>");
                soapRequest.AppendLine("            <urn:LineType>" + SecurityElement.Escape(orderLine.LineType) + "</urn:LineType>");
                soapRequest.AppendLine("            <urn:ProdCode>" + SecurityElement.Escape(orderLine.ProdCode) + "</urn:ProdCode>");
                soapRequest.AppendLine("            <urn:LineText>" + SecurityElement.Escape(orderLine.LineText) + "</urn:LineText>");
                soapRequest.AppendLine("            <urn:Quantity>" + SecurityElement.Escape(orderLine.Quantity.ToString()) + "</urn:Quantity>");
                soapRequest.AppendLine("            <urn:UnitCost>" + SecurityElement.Escape(orderLine.UnitCost.ToString()) + "</urn:UnitCost>");
                soapRequest.AppendLine("            <urn:Vat>" + SecurityElement.Escape(orderLine.Vat.ToString()) + "</urn:Vat>");
                soapRequest.AppendLine("            <urn:Discount>" + SecurityElement.Escape(orderLine.Discount.ToString()) + "</urn:Discount>");
                soapRequest.AppendLine("            <urn:SerialNumber>" + SecurityElement.Escape(orderLine.SerialNumber.ToString()) + "</urn:SerialNumber>");
                soapRequest.AppendLine("         </urn:LINE>");
            }
            soapRequest.AppendLine("      </urn:ORDUpdate>");
            soapRequest.AppendLine("   </soapenv:Body>");
            soapRequest.AppendLine("</soapenv:Envelope>");
            //do request 
            WSResult result = MatfloWebrequest(Config.ORD_Uri, Config.ORD_SoapAction, soapRequest.ToString());
            //return result
            return result;
        }

        public WSResult MatfloWebrequest(string requestUriString, string soapAction, string soapXML) {
            Logging.Instance.LogDataToFile("MatfloWebrequest." + soapAction + ".soapXML", soapXML);
            //create webRequest 
            Logging.Instance.LogToFile("MatfloWebrequest - create webRequest", false);
            WebRequest webRequest = WebRequest.Create(requestUriString);
            HttpWebRequest httpRequest = (HttpWebRequest)webRequest;
            httpRequest.Method = "POST";
            httpRequest.ContentType = "text/xml; charset=utf-8";
            httpRequest.Headers.Add("SOAPAction: " + soapAction);
            httpRequest.ProtocolVersion = HttpVersion.Version11;
           // httpRequest.Credentials = CredentialCache.DefaultCredentials;
            Stream requestStream = httpRequest.GetRequestStream();
            //Create Stream and Complete Request   
            Logging.Instance.LogToFile("MatfloWebrequest - Create Stream and Complete Request", false);
            StreamWriter streamWriter = new StreamWriter(requestStream, Encoding.ASCII);
            streamWriter.Write(soapXML);
            streamWriter.Close(); 
            //Get the Response   
            Logging.Instance.LogToFile("MatfloWebrequest - Get the Response ", false); 
            HttpWebResponse wr = (HttpWebResponse)httpRequest.GetResponse();
            StreamReader srd = new StreamReader(wr.GetResponseStream());
            string xMLText = srd.ReadToEnd();
            Logging.Instance.LogDataToFile("MatfloWebrequest." + soapAction + ".xMLText", xMLText);
            //Parse the reponse to XML
            Logging.Instance.LogToFile("MatfloWebrequest - Parse the reponse to XML", false);
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(xMLText);
            Logging.Instance.LogDataToFile("MatfloWebrequest." + soapAction + ".xmlDoc", xmlDoc);
            //Parse XML to WSResult
            Logging.Instance.LogToFile("MatfloWebrequest - Parse XML to WSResult", false);
            WSResult result = new WSResult();
            if (xmlDoc.GetElementsByTagName("Success", "*").Count > 0) {
                result.Success = System.Xml.XmlConvert.ToBoolean(xmlDoc.GetElementsByTagName("Success", "*")[0].InnerText);
                result.ReasonText = xmlDoc.GetElementsByTagName("ReasonText", "*")[0].InnerText;
            } else if (xmlDoc.GetElementsByTagName("faultcode", "*").Count > 0) {
                result.FaultCode = xmlDoc.GetElementsByTagName("faultcode", "*")[0].InnerText;
                result.FaultString = xmlDoc.GetElementsByTagName("faultstring", "*")[0].InnerText;
            }
            //close and dispose
            Logging.Instance.LogToFile("MatfloWebrequest - close and dispose", false);
            xmlDoc = null;
            srd.Close();
            srd.Dispose();
            srd = null;
            wr.Close();
            wr = null;

            if (result.ReasonText.IndexOf("not known") >= 0)
            {
                result.Success = true;
            }
            //return result
            Logging.Instance.LogToFile("MatfloWebrequest - result.Success: " + result.Success.ToString() + " ReasonText: " + result.ReasonText, false);
            return result;
        }
    }



}