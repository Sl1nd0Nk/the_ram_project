﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace WS_Test.BLL {


    [Serializable]
    public class StartProcessResult {
        public string MsgType { get; set; }
        public int MsgID { get; set; } 
        public StartProcessResult() {
            MsgType = string.Empty; MsgID = -1; 
        }
        public StartProcessResult(string msgType, int msgID) {
            MsgType = msgType; MsgID = msgID; 
        } 
    }


    class Msg_ToProcess {

        public static DataSet Msg_Get(string MsgType, int MsgID) {
            string sql = @"usp_Msg_Get";
            Database db = new DatabaseProviderFactory().Create("RAMWMSHost");
            DbCommand dbCmd = db.GetStoredProcCommand(sql);
            db.AddInParameter(dbCmd, "@MsgType", DbType.String, MsgType);
            db.AddInParameter(dbCmd, "@MsgID", DbType.Int32, MsgID);
            DataSet ds = db.ExecuteDataSet(dbCmd);
            dbCmd.Dispose(); dbCmd = null; db = null;
            return ds;
        }


        public static DataSet Msg_ToProcess_List(string MsgType) {
            //RecordID, RecordDT, NextStartDT, MsgType, MsgID 
            string sql = @"usp_Msg_ToProcess_List";
            Database db = new DatabaseProviderFactory().Create("RAMWMSHost");
            DbCommand dbCmd = db.GetStoredProcCommand(sql);
            db.AddInParameter(dbCmd, "@MsgType", DbType.String, MsgType);
            DataSet ds = db.ExecuteDataSet(dbCmd);
            dbCmd.Dispose(); dbCmd = null; db = null;
            return ds;
        }


        public static StartProcessResult Msg_ToProcess_StartProcess(int RecordID) {
            //MsgType, MsgID
            string sql = @"usp_Msg_ToProcess_StartProcess";
            Database db = new DatabaseProviderFactory().Create("RAMWMSHost");
            DbCommand dbCmd = db.GetStoredProcCommand(sql);
            db.AddInParameter(dbCmd, "@RecordID", DbType.Int32, RecordID);
            DataSet ds = db.ExecuteDataSet(dbCmd);
            dbCmd.Dispose(); dbCmd = null; db = null; 
            StartProcessResult result = new StartProcessResult(ds.Tables[0].Rows[0]["MsgType"].ToString(), Convert.ToInt32(ds.Tables[0].Rows[0]["MsgID"]));
            ds.Dispose();
            ds = null;
            return result;
        }


        public static bool Msg_ToProcess_UpdateProcess(int RecordID, bool Success, string ProcErrors) { 
            string sql = @"usp_Msg_ToProcess_UpdateProcess";
            Database db = new DatabaseProviderFactory().Create("RAMWMSHost");
            DbCommand dbCmd = db.GetStoredProcCommand(sql);
            db.AddInParameter(dbCmd, "@RecordID", DbType.Int32, RecordID);
            db.AddInParameter(dbCmd, "@Success", DbType.Boolean, Success);
            db.AddInParameter(dbCmd, "@ProcErrors", DbType.String, ProcErrors);
            bool success = Convert.ToBoolean(db.ExecuteScalar(dbCmd));
            dbCmd.Dispose(); dbCmd = null; db = null;
            return success;
        }


        
    }
}

 