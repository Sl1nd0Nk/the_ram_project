﻿using System;

namespace RAMWMSHost_MsgTransfer_Interface {
    public class ProcessMessage {

        private string mMessageText = string.Empty;
        private string mDetails = string.Empty;
        private DateTime mEventDT = DateTime.Now;
        private string mEventSource = string.Empty;
        private int mEventID = 0;
        private bool mIsError = false;

        public string MessageText { get { return mMessageText; } set { mMessageText = value; } }
        public string Details { get { return mDetails; } set { mDetails = value; } }
        public DateTime EventDT { get { return mEventDT; } set { mEventDT = value; } }
        public string EventSource { get { return mEventSource; } set { mEventSource = value; } }
        public int EventID { get { return mEventID; } set { mEventID = value; } }
        public bool IsError { get { return mIsError; } set { mIsError = value; } }

        public ProcessMessage() { }

        public ProcessMessage(string messageText,
                      string details,
                      DateTime eventDT,
                      string eventSource,
                      int eventID,
                      bool isError) {
            mMessageText = messageText;
            mDetails = details;
            mEventDT = eventDT;
            mEventSource = eventSource;
            mEventID = eventID;
            mIsError = isError;
        }

        public ProcessMessage(string messageText ,
              string eventSource,
              int eventID ) {
            mMessageText = messageText;
            mDetails = string.Empty;
            mEventDT = DateTime.Now;
            mEventSource = eventSource;
            mEventID = eventID;
            mIsError = false;
        }

        public ProcessMessage(string messageText,
              string details, 
              string eventSource,
              int eventID,
              bool isError) {
            mMessageText = messageText;
            mDetails = details;
            mEventDT = DateTime.Now;
            mEventSource = eventSource;
            mEventID = eventID;
            mIsError = isError;
        }

    }
}
