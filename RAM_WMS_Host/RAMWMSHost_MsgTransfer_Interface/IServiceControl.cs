﻿using System;
using System.Collections.Generic;  
using System.ServiceModel;

namespace RAMWMSHost_MsgTransfer_Interface {
    [ServiceContract]
    public interface IServiceControl {

        [OperationContract]
        void RequestStart();

        [OperationContract]
        void RequestStop();

        [OperationContract]
        int GetSecondsToNextSync();

        [OperationContract]
        int GetSecondsFromSyncStart();

        [OperationContract]
        int GetSecondsFromSyncLastActivity();

        [OperationContract]
        string GetSyncStatus();

        [OperationContract]
        bool GetSyncBusy();

        [OperationContract]
        void RequestSyncStart();

        [OperationContract]
        bool GetSyncStartRequested();

        [OperationContract]
        List<ProcessMessage> GetMessages();

        [OperationContract]
        DateTime GetServiceStartTime();

        [OperationContract]
        DateTime GetServiceLastActivity();



    }
}

