﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Xml;
using System.Data;


namespace RAMWMSHost_MsgTransfer.BLL {
    public class WorkerProcess {
 
        #region Process parameters, delagates and events

        public delegate void delegateProcessDone(object sender, bool result, string message, int eventID);
        public delegate void delegateProcessMessage(object sender, string message, int eventID);
        public delegate void delegateProcessError(object sender, string message, int eventID, string details);

        public event delegateProcessDone ProcessDone;
        public event delegateProcessMessage ProcessMessage;
        public event delegateProcessError ProcessError;

        protected bool mBusy = false; 
        protected DateTime mStartDT;
        protected DateTime mLastActivityDT;
        protected Thread mWorkerThread;

        public bool Busy { get { return mBusy; } } 
        public DateTime StartDT { get { return mStartDT; } }
        public DateTime LastActivityDT { get { return mLastActivityDT; } }
        public Thread WorkerThread { get { return mWorkerThread; } }

        public int SecondsSinceStart { get { return Convert.ToInt32((DateTime.Now.Ticks - mStartDT.Ticks) / 10000000); } }
        public int SecondsSinceLastActivity { get { return Convert.ToInt32((DateTime.Now.Ticks - mLastActivityDT.Ticks) / 10000000); } }

        private void LogAdd(string message, int eventID) {
            RaiseProcessMessage(this, message, eventID);
        }

        private void LogError(string message, int eventID, string details) {
            RaiseProcessError(this, message, eventID, details);
        }

        public void RaiseProcessMessage(object sender, string message, int eventID) {
            try {
                mLastActivityDT = DateTime.Now;
                this.ProcessMessage(sender, message, eventID);
            } catch { }
        }

        public void RaiseProcessError(object sender, string message, int eventID, string details) {
            try {
                mLastActivityDT = DateTime.Now;
                this.ProcessError(sender, message, eventID, details);
            } catch { }
        }

        public void RaiseProcessDone(object sender, bool result, string message, int eventID) {
            try {
                mLastActivityDT = DateTime.Now; 
                this.ProcessDone(sender, result, message, eventID);
            } catch { }
        }
        
        #endregion

        #region Process control methods
        public WorkerProcess() { }
 
        public void StartThread() {
            try {
                mBusy = true;
                mStartDT = DateTime.Now;
                mLastActivityDT = DateTime.Now;
                mWorkerThread = new Thread(new ThreadStart(StartProcess));
                mWorkerThread.Start();
                Thread.Sleep(10);
            } catch (Exception exc) {
                RaiseProcessError(this, exc.Message, 10101, exc.StackTrace);
            }
        }

        public void KillThread() {
            try {
                RaiseProcessMessage(this, "Killing Thread", 10201);
                if (this.mWorkerThread != null) {
                    try {
                        RaiseProcessMessage(this, "KillThread", 10202);
                        this.mWorkerThread.Abort();
                    } catch (ThreadAbortException exc1) {
                        RaiseProcessError(this, exc1.Message, 10203, exc1.StackTrace);
                    } catch (Exception exc2) {
                        RaiseProcessError(this, exc2.Message, 10204, exc2.StackTrace);
                    }
                    Thread.Sleep(10);
                }
            } catch (Exception exc) {
                RaiseProcessError(this, exc.Message, 10205, exc.StackTrace);
            }
        }

        public virtual void StartProcess() {
            try {
                mBusy = true;
                //do work
                RaiseProcessMessage(this, "StartProcess", 10301);
                bool success = DoMsgTransfer();
                //return thread 
                RaiseProcessDone(this, success, "ProcessDone", 10302);
                mBusy = false;
            } catch (Exception exc) {
                RaiseProcessError(this, exc.Message, 10303, exc.StackTrace);
                mBusy = false;
            }
        }
 
        #endregion

        #region Custom code

        private bool DoMsgTransfer() {
            Process("AllOutbound");
            return true;
        }


        private void Process(string MsgTypeToProcess) {
            try {
                DataSet toProcess = BLL.Msg_ToProcess.Msg_ToProcess_List(MsgTypeToProcess);
                if (toProcess.Tables[0].Rows.Count == 0) { 
                    RaiseProcessMessage(this, "Nothing to process", 50101);
                } else {
                    BLL.MatfloMessages msgWS = new BLL.MatfloMessages();
                    foreach (DataRow row in toProcess.Tables[0].Rows) {
                        int RecordID = Convert.ToInt32(row["RecordID"]);
                        DateTime RecordDT = Convert.ToDateTime(row["RecordDT"]);
                        DateTime NextStartDT = Convert.ToDateTime(row["NextStartDT"]);
                        string MsgType = row["MsgType"].ToString();
                        int MsgID = Convert.ToInt32(row["MsgID"]);
                        BLL.WSResult result = new BLL.WSResult(); 
                        RaiseProcessMessage(this, "ToProcess: " + RecordID.ToString() + " " + RecordDT.ToString("yyyy-MM-dd HH:mm:ss") + " " + NextStartDT.ToString("yyyy-MM-dd HH:mm:ss") + " " + MsgType + " " + MsgID.ToString(), 50101);

                        try {
                            BLL.StartProcessResult startResult = BLL.Msg_ToProcess.Msg_ToProcess_StartProcess(RecordID);
                 
                            RaiseProcessMessage(this, "MsgOut_CLC:" + MsgID.ToString(), 50101);
                            if (startResult.MsgType == "MsgOut_CLC") {
                                result = msgWS.CLCUpdate(MsgID);
                            } else if (startResult.MsgType == "MsgOut_IGD") {
                                result = msgWS.IGDUpdate(MsgID);
                            } else if (startResult.MsgType == "MsgOut_OCA") {
                                result = msgWS.OCAUpdate(MsgID);
                            } else if (startResult.MsgType == "MsgOut_ORD") {
                                result = msgWS.ORDUpdate(MsgID);
                            } else { 
                                RaiseProcessMessage(this, "MsgType not supported", 50101);
                                result.Success = false;
                                result.ReasonText = "MsgType not supported";
                            }
                        } catch (Exception exc1) {
                            result.Success = false;
                            result.ReasonText = exc1.Message;
                        } 
                        RaiseProcessMessage(this, "Success:" + result.Success.ToString() + "  " + result.ReasonText, 50101);
                        bool updateSuccess = BLL.Msg_ToProcess.Msg_ToProcess_UpdateProcess(RecordID, result.Success, result.ReasonText); 
                        RaiseProcessMessage(this, "Updated:" + updateSuccess.ToString(), 50101);
                    }
                    msgWS = null;
                }
            } catch (Exception exc2) { 
                RaiseProcessMessage(this, "Process Exception:" + exc2.Message, 50101);
            }
        }


        #endregion

    }
 

}
