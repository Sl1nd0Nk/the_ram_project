﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RAMWMSHost_MsgTransfer {
    partial class RAMWMSHost_MsgTransfer : ServiceBase {

        #region init

        private System.ServiceModel.ServiceHost mServiceHost = null;
        private System.Timers.Timer mMainTimer = null;


        public RAMWMSHost_MsgTransfer() {
            LogAdd("RAMWMSHost_MsgTransfer.Constructor", false);
            InitializeComponent();
        }

        protected override void OnStart(string[] args) {
            // TODO: Add code here to start your service.
            LogAdd("RAMWMSHost_MsgTransfer.OnStart", false);


            var worker = new Thread(DoWork);
            worker.Name = "MyWorker";
            worker.IsBackground = false;
            worker.Start();


        }

        protected override void OnStop() {
            // TODO: Add code here to perform any tear-down necessary to stop your service.
            LogAdd("RAMWMSHost_MsgTransfer.OnStop", false);
             
            mMainTimer.Enabled = false;
            mMainTimer = null;

            //Stop WCF host service
            if (mServiceHost != null) {
                try {
                    mServiceHost.Close();
                    mServiceHost = null;
                    Logging.Instance.LogToFile("WCF host closed", true);
                } catch (Exception exc) {
                    Logging.Instance.LogToFile("Error: Stop WCF host service" + exc.Message, true);
                }
            }

            LogAdd("RAMWMSHost_MsgTransfer Stopped", true);
            LogAdd("**********************************************************", true);


        }

        private void LogAdd(string message, bool isError) {
            Logging.Instance.LogToFile(message, isError);
            // ServiceControl.Singleton.LogAdd(message);
            ServiceControl.Instance.MessageAdd(message, string.Empty, 0, isError);
        }

        #endregion
         

        #region service loop

        void DoWork() {
            LogAdd("DoWork start", false);
            //Logging.LogEvent("DoWork start", 10301);
            ServiceControl.Instance.ServiceStartTime = DateTime.Now;
            ServiceControl.Instance.ServiceLastActivity = DateTime.Now;



            LogAdd("Start WCF host service", false);
            //Logging.LogEvent("Start WCF host service", 10302);
            //Start WCF host service
            try {
                mServiceHost = new System.ServiceModel.ServiceHost(typeof(ServiceControl));
                mServiceHost.Open();
                LogAdd("WCF host started", false);
            } catch (Exception exc) {
                LogAdd(exc.Message, true);
            }

            LogAdd("Start service control loop", false);
            //Logging.LogEvent("Start service control loop", 10303);
            //Start service control loop
            mMainTimer = new System.Timers.Timer(689);
            mMainTimer.Elapsed += new System.Timers.ElapsedEventHandler(mMainTimer_Elapsed);
            mMainTimer.Enabled = true;

            LogAdd("RAMServiceMessagesClientSvc Started", false);
            LogAdd("DoWork end", false);

            //Logging.LogEvent("DoWork end", 10399);
        }

        private int mRefreshInterval = 1;
        private DateTime mLastRefreshFinished = DateTime.Now;

        void mMainTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e) {
            //LogAdd("mMainTimer_Elapsed", false);
            mMainTimer.Enabled = false;
            try {
                int timeLeft = (mRefreshInterval * 60) - Convert.ToInt32(new TimeSpan(DateTime.Now.Ticks - mLastRefreshFinished.Ticks).TotalSeconds); // Convert.ToInt32(DateTime.Now.Ticks - mLastRefreshFinished.Ticks) / 10000000;
                //LogAdd("timeLeft:" + timeLeft.ToString(), false);
                if (timeLeft < 0) timeLeft = 0;
                ServiceControl.Instance.SecondsToNextSync = timeLeft;
                ServiceControl.Instance.ServiceLastActivity = DateTime.Now;

                //kill workerproces that is stuck
                if (mWorkerProcess != null) {
                    try {
                        if (mWorkerProcess.Busy == false) {
                            LogAdd("Kill mWorkerProcess - not busy", true);
                            mWorkerProcess.KillThread();
                            mWorkerProcess = null;
                        } else if (mWorkerProcess.Busy == true) {
                            if (mWorkerProcess.SecondsSinceLastActivity > 15) {
                                LogAdd("Kill mWorkerProcess - no activity", true);
                                mWorkerProcess.KillThread();
                                mWorkerProcess = null;
                            }
                        }
                    } catch (Exception exc1) {
                        LogAdd(exc1.Message, true);
                    }
                }
                //allow to continue if still running
                if (mWorkerProcess != null) {
                    ServiceControl.Instance.SyncStatus = "Syncing busy";
                    ServiceControl.Instance.SyncBusy = mWorkerProcess.Busy;
                    ServiceControl.Instance.SecondsFromSyncStart = mWorkerProcess.SecondsSinceStart;
                    ServiceControl.Instance.SecondsFromSyncLastActivity = mWorkerProcess.SecondsSinceLastActivity;
                } else {
                    ServiceControl.Instance.SyncBusy = false;
                    ServiceControl.Instance.SecondsFromSyncStart = 0;
                    ServiceControl.Instance.SecondsFromSyncLastActivity = 0;
                    if (timeLeft <= 0 | ServiceControl.Instance.SyncStartRequested == true) {
                        //reset start request
                        ServiceControl.Instance.SyncStartRequested = false;
                        StartDownload();
                    } else {
                        ServiceControl.Instance.SyncStatus = "Waiting";
                    }
                }

            } catch (Exception exc) {
                LogAdd("mMainTimer_Elapse: " + exc.Message, true);
            }
            mMainTimer.Enabled = true;
        }

        private BLL.WorkerProcess mWorkerProcess = null;

        private void StartDownload() {
            try {
                ServiceControl.Instance.SyncStatus = "Syncing start"; 
                mWorkerProcess = new  BLL.WorkerProcess(); 
                mWorkerProcess.ProcessMessage += new BLL.WorkerProcess.delegateProcessMessage(mWorkerProcess_ProcessMessage);
                mWorkerProcess.ProcessError += new BLL.WorkerProcess.delegateProcessError(mWorkerProcess_ProcessError);
                mWorkerProcess.ProcessDone += new BLL.WorkerProcess.delegateProcessDone(mWorkerProcess_ProcessDone);
                mWorkerProcess.StartThread();
                Thread.Sleep(0);
                ServiceControl.Instance.SyncStatus = "Syncing started";
            } catch (Exception exc) {
                LogAdd(exc.Message, true);
            }
        }


        void mWorkerProcess_ProcessMessage(object sender, string message, int eventID) {
            LogAdd("Lib - " + message, false);
            ServiceControl.Instance.SyncStatus = "Syncing busy";
        }

        void mWorkerProcess_ProcessError(object sender, string message, int eventID, string details) {
            LogAdd("Lib - " + message, true);
            ServiceControl.Instance.SyncStatus = "Syncing error";
        }


        void mWorkerProcess_ProcessDone(object sender, bool result, string message, int eventID) {
            LogAdd("Lib - " + message, false); 
            LogAdd("RefreshInterval: " + mRefreshInterval.ToString(), false);

            mLastRefreshFinished = DateTime.Now;
            mWorkerProcess = null;

            ServiceControl.Instance.SyncStatus = "Syncing done";

            //mMainTimer.Enabled = true;
            LogAdd("mWorkerProcess_ProcessDone", false);
        }


        #endregion









    }
}
