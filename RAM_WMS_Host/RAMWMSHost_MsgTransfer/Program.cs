﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RAMWMSHost_MsgTransfer {
    static class Program {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main() {

            try {

                Logging.Instance.LogToFile("", false);
                Logging.Instance.LogToFile("", false);
                Logging.Instance.LogToFile("/**************************************/", false);
                Logging.Instance.LogToFile("RAMWMSHost_MsgTransfer.Main Start", false);
                Logging.Instance.LogToFile("/**************************************/", false);  
                ServiceBase.Run(new RAMWMSHost_MsgTransfer()); 
                Logging.Instance.LogToFile("/**************************************/", false);
                Logging.Instance.LogToFile("RAMWMSHost_MsgTransfer.Main End", false);
                Logging.Instance.LogToFile("/**************************************/", false);
                Logging.Instance.LogToFile("", false);
                Logging.Instance.LogToFile("", false); 

                Thread.Sleep(100);

            } catch (Exception exc) {
                Logging.Instance.LogToSystemEventLog_Warning(exc.Message, 10103);
            }

        }
    }
}