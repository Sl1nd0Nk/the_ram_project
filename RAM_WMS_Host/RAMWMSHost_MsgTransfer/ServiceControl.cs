﻿using System;
using System.Collections.Generic;
using RAMWMSHost_MsgTransfer_Interface;
using System.ServiceModel;

namespace RAMWMSHost_MsgTransfer {
    [ServiceBehavior( InstanceContextMode = InstanceContextMode.Single, ConcurrencyMode=ConcurrencyMode.Single, AddressFilterMode=AddressFilterMode.Any)]
    public sealed class ServiceControl : IServiceControl {

        #region singleton
        //private static ServiceControl mInstance = null;
        //public static ServiceControl Instance {
        //    get {
        //        if (mInstance == null) mInstance = new ServiceControl();
        //        return mInstance;
        //    }
        //}
        //private ServiceControl() { }


        private static volatile ServiceControl mInstance;
        private static object mInstanceLock = new Object();
        private ServiceControl() { }
        public static ServiceControl Instance {
            get {
                if (mInstance == null) {
                    lock (mInstanceLock) {
                        if (mInstance == null)
                            mInstance = new ServiceControl();
                    }
                }
                return mInstance;
            }
        }

        #endregion

        #region events
        public delegate void delegateStartProcess(object sender);
        public delegate void delegateStopProcess(object sender);
        public delegate void delegateLogAdd(object sender, string message);
        public delegate void delegateMessageAdd(object sender, ProcessMessage message);

        public event delegateStartProcess StartProcessEvent;
        public event delegateStopProcess StopProcessEvent;
        public event delegateLogAdd LogAddEvent;
        public event delegateMessageAdd MessageAddEvent;

        private void RaiseStartProcessEvent() {
            try { StartProcessEvent(this); } catch (Exception) { }
        }
        private void RaiseStopProcessEvent() {
            try { StopProcessEvent(this); } catch (Exception) { }
        }
        private void RaiseLogAddEvent(string message) {
            try { LogAddEvent(this, message); } catch (Exception) { }
        }
        private void RaiseMessageAddEvent(ProcessMessage message) {
            try { MessageAddEvent(this, message); } catch (Exception) { }
        }
        #endregion

        #region properties
        //used for threadsafety
        private object mLockObj = new object();

        private bool mStartRequested = false;
        public bool StartRequested {
            get { lock (mLockObj) { return mStartRequested; } }
            set { lock (mLockObj) { mStartRequested = value; } }
        }

        private bool mStopRequested = false;
        public bool StopRequested {
            get {  lock (mLockObj) { return mStopRequested; }}
            set {  lock (mLockObj) {    mStopRequested = value; }}
        }
 
        private List<ProcessMessage> mMessages = new List<ProcessMessage>();
        public List<ProcessMessage> Messages {
            get { lock (mLockObj) { return mMessages; }}
            set { lock (mLockObj) { mMessages = value; } }
        }

        private int mSecondsToNextSync = 0;
        public int SecondsToNextSync {
            get { lock (mLockObj) { return mSecondsToNextSync; } }
            set { lock (mLockObj) { mSecondsToNextSync = value; } }
        }

        private int mSecondsFromSyncStart = 0;
        public int SecondsFromSyncStart {
            get {lock (mLockObj) {  return mSecondsFromSyncStart; }}
            set {lock (mLockObj) {  mSecondsFromSyncStart = value; }}
        }

        private int mSecondsFromSyncLastActivity = 0;
        public int SecondsFromSyncLastActivity {
            get {lock (mLockObj) {  return mSecondsFromSyncLastActivity; }}
            set { lock (mLockObj) { mSecondsFromSyncLastActivity = value; } }
        }


        private string mSyncStatus = "Unknown";
        public string SyncStatus {
            get {  lock (mLockObj) { return mSyncStatus; }}
            set {  lock (mLockObj) { mSyncStatus = value; }}
        }

        private bool mSyncBusy = false;
        public bool SyncBusy {
            get { lock (mLockObj) {  return mSyncBusy; }}
            set { lock (mLockObj) { mSyncBusy = value; } }
        }


        private bool mSyncStartRequested = false;
        public bool SyncStartRequested {
            get { lock (mLockObj) { return mSyncStartRequested; }}
            set {lock (mLockObj) {  mSyncStartRequested = value; }}
        }

        private DateTime mServiceStartTime = System.DateTime.Now;
        public DateTime ServiceStartTime {
            get { lock (mLockObj) { return mServiceStartTime; }}
            set { lock (mLockObj) { mServiceStartTime = value; } }
        }

        private DateTime mServiceLastActivity = System.DateTime.Now;
        public DateTime ServiceLastActivity {
            get { lock (mLockObj) { return mServiceLastActivity; }}
            set { lock (mLockObj) { mServiceLastActivity = value; } }
        }

        #endregion

        #region public methods

        public void MessageAdd(string messageText, string details, int eventID, bool isError) {
            ProcessMessage message = new ProcessMessage(messageText, details, DateTime.Now, Logging.Instance.ExecutingAssemblyName, 0, isError);
            MessageAdd(message);
        }

        public void MessageAdd(string messageText, string details, int eventID) {
            ProcessMessage message = new ProcessMessage(messageText, details, DateTime.Now, Logging.Instance.ExecutingAssemblyName, 0, false);
            MessageAdd(message);
        }

        public void MessageAdd(string messageText) {
            ProcessMessage message = new ProcessMessage(messageText, string.Empty, DateTime.Now, Logging.Instance.ExecutingAssemblyName, 0, false);
            MessageAdd(message);
        }

        public void MessageAdd(ProcessMessage message) {
            //update log list 
            Instance.Messages.Add(message);
            if (Instance.Messages.Count > 1000) Instance.Messages.RemoveAt(0);
            //raise local event
            RaiseMessageAddEvent(message);
        }

        public List<ProcessMessage> PopMessages() {
            List<ProcessMessage> messages = Instance.Messages;
            Instance.Messages = new List<ProcessMessage>();
            return messages;
        }

        #endregion

        #region wcf methods
        public void RequestStart() {
            Instance.StopRequested = false;
            Instance.StartRequested = true;
            Instance.RaiseStartProcessEvent();
        }

        public void RequestStop() {
            Instance.StartRequested = false;
            Instance.StopRequested = true;
            Instance.RaiseStopProcessEvent();
        }

        public int GetSecondsToNextSync() {
            return Instance.SecondsToNextSync;
        }


        public int GetSecondsFromSyncStart() {
            return Instance.SecondsFromSyncStart;
        }

        public int GetSecondsFromSyncLastActivity() {
            return Instance.SecondsFromSyncLastActivity;
        }

        public string GetSyncStatus() {
            return Instance.SyncStatus;
        }

        public bool GetSyncBusy() {
            return Instance.SyncBusy;
        }

        public void RequestSyncStart() {
            Instance.SyncStartRequested = true;
            MessageAdd("Sync Start Requested");
        }

        public bool GetSyncStartRequested() {
            return Instance.SyncStartRequested;
        }

        public List<ProcessMessage> GetMessages() {
            return Instance.PopMessages();
        }
 
        public DateTime GetServiceStartTime() {
            return Instance.ServiceStartTime;
        }

        public DateTime GetServiceLastActivity() {
            return Instance.ServiceLastActivity;
        }

        #endregion

    }
}
