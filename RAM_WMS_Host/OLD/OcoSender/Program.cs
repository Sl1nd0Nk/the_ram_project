﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OcoSender.SalesOrderShipment;

namespace OcoSender
{
    class Program
    {
        private static CustomerPortalEntities PortalDB = new CustomerPortalEntities();
        private static RAMWMSHOSTEntities WMSHostDB = new RAMWMSHOSTEntities();

        static void ProcessOCOs(List<MsgIn_OCO> Lst)
        {
            foreach(var OCO in Lst)
            {
                Console.WriteLine(DateTime.Now.ToString() + "Process OCO Msg - Order " + OCO.OrderNumber);

                string ProcErr = "";
                DateTime ProcStartDT = DateTime.Now;
                Order Ord = PortalDB.Orders.Where(a => a.RAMOrderNumber == OCO.OrderNumber && a.Deleted == false).FirstOrDefault();

                if(Ord != null)
                {
                    OrderOCO OrdOco = new OrderOCO();
                    OrdOco.CreateDate = DateTime.Now;
                    OrdOco.OrderCost = OCO.OrderCost;
                    OrdOco.OrderDiscount = OCO.OrderDiscount;
                    OrdOco.OrderNumber = OCO.OrderNumber;
                    OrdOco.OrderVAT = OCO.OrderVAT;
                    OrdOco.PrincipalCode = OCO.PrincipalCode;
                    OrdOco.Priority = OCO.Priority;
                    OrdOco.SiteCode = OCO.SiteCode;
                    OrdOco.UserID = OCO.UserID;
                    OrdOco.Weight = OCO.Weight;

                    PortalDB.OrderOCOes.Add(OrdOco);
                    PortalDB.SaveChanges();

                    List<int> LineNumbers = WMSHostDB.MsgIn_OCO_LN.AsNoTracking().Where(a => a.OCOID == OCO.OCOID).Select(a => a.LineNumber).Distinct().ToList();

                    foreach (var i in LineNumbers)
                    {
                        List<MsgIn_OCO_LN> LnLst = WMSHostDB.MsgIn_OCO_LN.AsNoTracking().Where(a => a.OCOID == OCO.OCOID && a.LineNumber == i).ToList();

                        int Quantity = 0;
                        foreach(var n in LnLst)
                        {
                            Quantity += (int)n.QuantityPicked;
                        }

                        OrderOCOLine OcoLine = new OrderOCOLine();
                        OcoLine.Discount = LnLst[0].Discount;
                        OcoLine.LineNumber = LnLst[0].LineNumber;
                        OcoLine.OrderOCOID = OrdOco.ID;
                        OcoLine.ProdCode = LnLst[0].ProdCode;
                        OcoLine.UnitCost = LnLst[0].UnitCost;
                        OcoLine.Vat = LnLst[0].Vat;
                        OcoLine.Quantity = Quantity;

                        PortalDB.OrderOCOLines.Add(OcoLine);
                        PortalDB.SaveChanges();

                        ProductStaging Prod = PortalDB.ProductStagings.Where(a => a.ProdCode == OcoLine.ProdCode && a.Deleted == false).FirstOrDefault();
                        bool AddSerials = false;
                        if (Prod != null && Prod.Serialised == true) AddSerials = true;

                        if (AddSerials)
                        {
                            int SerCount = 0;
                            int Count = 0;
                            foreach (var n in LnLst)
                            {
                                Count++;
                                if (!String.IsNullOrEmpty(n.SerialNumber))
                                {
                                    OrderOCOLineSerial lnSer = new OrderOCOLineSerial();
                                    lnSer.OrderOCOLineID = OcoLine.ID;
                                    lnSer.SerialNumber = n.SerialNumber;

                                    PortalDB.OrderOCOLineSerials.Add(lnSer);
                                    PortalDB.SaveChanges();

                                    Serial ProdSer = PortalDB.Serials.Where(a => a.SerialNumber == n.SerialNumber && a.ProductStagingID == Prod.ProductStagingID).ToList().LastOrDefault();
                                    if (ProdSer != null && ProdSer.OrderID == null && ProdSer.Status != "SLA DELETE")
                                    {
                                        ProdSer.Status = "DISPATCHED";
                                        ProdSer.EventDate = DateTime.Now;
                                        ProdSer.OrderID = Ord.OrderID;
                                        PortalDB.Entry(ProdSer).State = System.Data.Entity.EntityState.Modified;
                                        PortalDB.SaveChanges();

                                        SerCount += 1;
                                    }
                                    else
                                    {
                                        ProdSer = new Serial();
                                        ProdSer.EventDate = DateTime.Now;
                                        ProdSer.ProductStagingID = Prod.ProductStagingID;
                                        ProdSer.SerialNumber = n.SerialNumber;
                                        ProdSer.OrderID = Ord.OrderID;
                                        ProdSer.Status = "DISPATCHED";

                                        PortalDB.Serials.Add(ProdSer);
                                        PortalDB.SaveChanges();
                                    }
                                }
                            }

                            int StockQty = Prod.StockQuantity == null ? 0 : (int)Prod.StockQuantity;
                            Prod.StockQuantity = StockQty - SerCount;
                            Prod.StockQuantity = Prod.StockQuantity < 0 ? 0 : Prod.StockQuantity;
                        }
                        else
                        {
                            int StockQty = Prod.StockQuantity == null ? 0 : (int)Prod.StockQuantity;
                            Prod.StockQuantity = StockQty - OcoLine.Quantity;
                            Prod.StockQuantity = Prod.StockQuantity < 0 ? 0 : Prod.StockQuantity;
                        }

                        Prod.EventDate = DateTime.Now;
                        PortalDB.Entry(Prod).State = System.Data.Entity.EntityState.Modified;
                        PortalDB.SaveChanges();
                    }

                    Principal Prn = PortalDB.Principals.AsNoTracking().Where(a => a.PrincipalID == Ord.PrincipalID && a.WebServiceIntegration == true).FirstOrDefault();

                    if (Prn != null) Ord.ClientConfirmed = false;
                    Ord.Status = "DISPATCHED";
                    Ord.EventDate = DateTime.Now;
                    PortalDB.Entry(Ord).State = System.Data.Entity.EntityState.Modified;
                    PortalDB.SaveChanges();
                }
                else
                {
                    ProcErr = "Order Not Found In The Customer Portal";
                }

                MsgIn_OCO UpdateOco = WMSHostDB.MsgIn_OCO.Find(OCO.OCOID);

                UpdateOco.RecordState = !String.IsNullOrEmpty(ProcErr) ? "PROCESSERROR" : "PROCESSED";
                UpdateOco.ProcStartDT = ProcStartDT;
                UpdateOco.ProcEndDT = DateTime.Now;
                UpdateOco.ProcErrors = ProcErr;
                WMSHostDB.Entry(UpdateOco).State = System.Data.Entity.EntityState.Modified;
                WMSHostDB.SaveChanges();
            }
        }

        static void OCO_Msg_ToProcess_List()
        {
            List<MsgIn_OCO> Lst = WMSHostDB.MsgIn_OCO.AsNoTracking().Where(a => a.RecordState == "READY").Take(100).OrderByDescending(a => a.RecordDT).ToList();

            ProcessOCOs(Lst);
        }

        static void ProcessEachAndSend(List<Order> lst)
        {
            foreach(var i in lst)
            {
                Console.WriteLine(DateTime.Now.ToString() + "Process Client Shipment Msg - Order " + i.RAMOrderNumber);

                OrderOCO x = PortalDB.OrderOCOes.AsNoTracking().Where(a => a.OrderNumber == i.RAMOrderNumber).ToList().LastOrDefault();

                if(x != null)
                {
                    SalesOrderShipmentSoapClient cl = new SalesOrderShipmentSoapClient("SalesOrderShipmentSoap");

                    List<OrderOCOLine> lnlist = PortalDB.OrderOCOLines.AsNoTracking().Where(a => a.OrderOCOID == x.ID).ToList();

                    if(lnlist.Count > 0)
                    {
                        OrderLine[] SoLns = new OrderLine[lnlist.Count];
                        int z = 0;
                        foreach (var y in lnlist)
                        {
                            OrderLine SoLn = new OrderLine();
                            SoLn.LineNumber = y.LineNumber.ToString();
                            SoLn.ProdCode = y.ProdCode;
                            SoLn.Quantity = (int)y.Quantity;
                            SoLn.UnitCost = (double)y.UnitCost;
                            SoLn.UnitDiscountAmount = (double)y.Discount;
                            SoLn.VAT = (double)y.Vat;

                            List<OrderOCOLineSerial> serlist = PortalDB.OrderOCOLineSerials.AsNoTracking().Where(a => a.OrderOCOLineID == y.ID).ToList();

                            SoLn.SerialNumbers = new string[serlist.Count];
                            int b = 0;
                            foreach(var a in serlist)
                            {
                                SoLn.SerialNumbers[b++] = a.SerialNumber;
                            }

                            SoLns[z++] = SoLn;
                        }

                        Response Res = cl.SOShipment(x.PrincipalCode, x.OrderNumber, x.Priority, x.UserID, (double)x.Weight, (double)x.OrderCost, (double)x.OrderDiscount, (double)x.OrderVAT, SoLns);

                        Console.WriteLine(DateTime.Now.ToString() + "Process Client Shipment Msg - Order " + i.RAMOrderNumber + " | Response: " + Res.success);

                        if (Res.success)
                        {
                            i.ClientConfirmed = true;
                            i.EventDate = DateTime.Now;
                            PortalDB.Entry(i).State = System.Data.Entity.EntityState.Modified;
                            PortalDB.SaveChanges();
                        }
                    }
                }
                else
                {
                    i.ClientConfirmed = null;
                }
            }
        }

        static void ClientShipmentSend()
        {
            List<Principal> PrnList = PortalDB.Principals.AsNoTracking().Where(a => a.WebServiceIntegration == true).ToList();

            foreach(var i in PrnList)
            {
                List<Order> lst = PortalDB.Orders.Where(a => a.Status == "DISPATCHED" && a.ClientConfirmed == false && a.PrincipalID == i.PrincipalID).ToList();
                ProcessEachAndSend(lst);
            }
        }

        static void Main(string[] args)
        {
            while (true)
            {
                OCO_Msg_ToProcess_List();
                ClientShipmentSend();
                Console.WriteLine(DateTime.Now.ToString() + " System timeout of " + (30000 / 1000) + " seconds starting ");
                System.Threading.Thread.Sleep(30000);
                Console.WriteLine(DateTime.Now.ToString() + " System timeout of " + (30000 / 1000) + " seconds ended ");
            }
        }
    }
}
