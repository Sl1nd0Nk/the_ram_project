﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OcoSender
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class CustomerPortalEntities : DbContext
    {
        public CustomerPortalEntities()
            : base("name=CustomerPortalEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<OrderLineItem> OrderLineItems { get; set; }
        public virtual DbSet<OrderOCO> OrderOCOes { get; set; }
        public virtual DbSet<OrderOCOLine> OrderOCOLines { get; set; }
        public virtual DbSet<OrderOCOLineSerial> OrderOCOLineSerials { get; set; }
        public virtual DbSet<Order> Orders { get; set; }
        public virtual DbSet<Principal> Principals { get; set; }
        public virtual DbSet<ProductStaging> ProductStagings { get; set; }
        public virtual DbSet<Serial> Serials { get; set; }
    }
}
