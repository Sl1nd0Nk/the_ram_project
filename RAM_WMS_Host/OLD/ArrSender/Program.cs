﻿using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ArrSender.WMSHostWS;
using ArrSender.POReceipt;
using ArrSender.CustomerReturn;

namespace ArrSender
{
    class Program
    {
        private static RAMWMSHOSTEntities WmsHostDB = new RAMWMSHOSTEntities();
        private static CustomerPortalEntities PortalDB = new CustomerPortalEntities();

        static void ProcessArr(List<MsgIn_ARR> lst)
        {
            foreach(var Arr in lst)
            {
                Console.WriteLine(DateTime.Now.ToString() + "Process ARR Msg - MoveRef " + Arr.MoveRef);
                string ProcErr = "";
                DateTime ProcStartDT = DateTime.Now;
                IGDStaging IGD = PortalDB.IGDStagings.Where(a => a.MoveRef == Arr.MoveRef &&
                                                            a.PrincipalCode == Arr.PrincipalCode &&
                                                            a.SiteCode == Arr.SiteCode &&
                                                            a.ReceiptType == Arr.ReceiptType &&
                                                            a.PORef == Arr.PORef &&
                                                            a.Deleted == false).FirstOrDefault();
                if(IGD != null)
                {
                    IGDReceipt Rec = new IGDReceipt();
                    Rec.AcceptCount = Arr.AcceptCount;
                    Rec.IGDStagingID = IGD.IGDStagingID;
                    Rec.LineNumber = Arr.LineNumber;
                    Rec.MoveRef = Arr.MoveRef;
                    Rec.PORef = Arr.PORef;
                    Rec.PrincipalCode = Arr.PrincipalCode;
                    Rec.ProdCode = Arr.ProdCode;
                    Rec.ReasonCode = Arr.ReasonCode;
                    Rec.ReceiptDT = Arr.ReceiptDT;
                    Rec.ReceiptType = Arr.ReceiptType;
                    Rec.RejectCount = Arr.RejectCount;
                    Rec.SiteCode = Arr.SiteCode;
                    Rec.StartEndStatus = Arr.StartEndStatus;
                    Rec.CreateDate = DateTime.Now;

                    PortalDB.IGDReceipts.Add(Rec);
                    PortalDB.SaveChanges();

                    ProductStaging Prod = PortalDB.ProductStagings.Find(IGD.ProductStagingID);
                    if(Prod != null)
                    {
                        if(Prod.Serialised == true)
                        {
                            List<MsgIn_ARR_SN> SerLst = WmsHostDB.MsgIn_ARR_SN.AsNoTracking().Where(a => a.ARRID == Arr.ARRID).ToList();

                            foreach (var i in SerLst) {
                                IGDReceiptSerial Ser = new IGDReceiptSerial();
                                Ser.IGDReceiptID = Rec.ID;
                                Ser.SerialNumber = i.SerialNumber;
                                PortalDB.IGDReceiptSerials.Add(Ser);
                                PortalDB.SaveChanges();
                            }
                        }
                    }

                    IGD.Status = "ARR";
                    IGD.EventDate = DateTime.Now;
                    PortalDB.Entry(IGD).State = System.Data.Entity.EntityState.Modified;
                    PortalDB.SaveChanges();
                }
                else
                {
                    ProcErr = "IGD Not Found In The Customer Portal";
                }

                MsgIn_ARR UpdateArr = WmsHostDB.MsgIn_ARR.Find(Arr.ARRID);

                UpdateArr.RecordState = !String.IsNullOrEmpty(ProcErr) ? "PROCESSERROR": "PROCESSED";
                UpdateArr.ProcStartDT = ProcStartDT;
                UpdateArr.ProcEndDT = DateTime.Now;
                UpdateArr.ProcErrors = ProcErr;
                WmsHostDB.Entry(UpdateArr).State = System.Data.Entity.EntityState.Modified;
                WmsHostDB.SaveChanges();
            }
        }

        static void Arr_Msg_ToProcess_List()
        {
            List<MsgIn_ARR> Lst = WmsHostDB.MsgIn_ARR.AsNoTracking().Where(a => a.RecordState == "READY").Take(100).OrderByDescending(a => a.RecordDT).ToList();

            ProcessArr(Lst);
        }

        static bool SendIGDCLC(IGDStaging i, int Discrepency, string[] SerialNumbers)
        {
            ProductStaging Prod = PortalDB.ProductStagings.Find(i.ProductStagingID);

            if(Prod != null)
            {
                ArrSender.WMSHostWS.HostWSSoapClient cl = new HostWSSoapClient("HostWSSoap");

                    
                if(SerialNumbers.Length > 0 && String.IsNullOrEmpty(SerialNumbers[0]))
                {
                    SerialNumbers = new string[0];
                }

                Result Res = cl.CLCAdd("WMSCustomerPortal", i.SiteCode, i.PrincipalCode,
                                       i.MoveRef, i.OrderLineNo, Prod.ProdCode, (int)i.ReceivedQuantity,
                                       i.ReceiptType, Discrepency, Prod.Serialised == true ? Discrepency : 0, SerialNumbers);

                Console.WriteLine(DateTime.Now.ToString() + "Process CLC Response Msg - MoveRef " + i.MoveRef + " = " + Res.Success);
                return Res.Success;

            }

            return false;
        }

        static void Arr_ClC_Msg_ToProcess_list()
        {
            List<IGDStaging> lst = PortalDB.IGDStagings.Where(a => a.Status == "ARR").ToList();

            foreach(var i in lst)
            {
                Console.WriteLine(DateTime.Now.ToString() + "Process CLC Msg - MoveRef " + i.MoveRef);
                int Discrepency = 0;
                int AcceptCount = 0;
                ProductStaging Prod = PortalDB.ProductStagings.Find(i.ProductStagingID);
                List<IGDReceiptSerial> Serlist = new List<IGDReceiptSerial>();
                string[] SerialNumbers = new string[(int)i.ReceivedQuantity];
                IGDReceipt Rec = PortalDB.IGDReceipts.AsNoTracking().Where(a => a.IGDStagingID == i.IGDStagingID).ToList().LastOrDefault();
                if(Rec == null)
                {
                    i.Status = "PENDING";
                }
                else
                {
                    AcceptCount = (int)Rec.AcceptCount;
                    if (i.ReceivedQuantity == Rec.AcceptCount + Rec.RejectCount)
                    {
                        if(Prod == null)
                        {
                            Discrepency = (int)i.ReceivedQuantity;
                            i.Status = "RECEIPT REJECTED";
                        }
                        else
                        {
                            if (Prod.Serialised == true)
                            {
                                Serlist = PortalDB.IGDReceiptSerials.AsNoTracking().Where(a => a.IGDReceiptID == Rec.ID).ToList();
                                if (Serlist.Count != Rec.AcceptCount)
                                {
                                    i.Status = "RECEIPT REJECTED";
                                    Discrepency = Math.Abs((int)Rec.AcceptCount - Serlist.Count);
                                }
                                else
                                {
                                    bool DuplicateSerialsInMessage = false;
                                    int x = 0;
                                    while (x < Serlist.Count)
                                    {
                                        int y = x + 1;
                                        while (y < Serlist.Count)
                                        {
                                            if (Serlist[x].SerialNumber == Serlist[y].SerialNumber)
                                            {
                                                DuplicateSerialsInMessage = true;
                                                break;
                                            }

                                            y++;
                                        }

                                        if (DuplicateSerialsInMessage)
                                        {
                                            SerialNumbers[SerialNumbers.Length - 1] = Serlist[x].SerialNumber;
                                            DuplicateSerialsInMessage = false;
                                            Discrepency += 1;
                                        }

                                        x++;
                                    }

                                    if (Discrepency > 0)
                                    {
                                        i.Status = "RECEIPT REJECTED";
                                    }
                                    else
                                    {
                                        i.Status = "RECEIPTED";
                                    }
                                }
                            }
                            else
                            {
                                i.Status = "RECEIPTED";
                            }
                        }
                    }
                    else
                    {
                        i.Status = "RECEIPT REJECTED";
                        Discrepency = (int)i.ReceivedQuantity;
                    }
                }

                if(i.Status == "RECEIPTED" && Prod != null)
                {
                    if(Prod.Serialised == true && Serlist.Count > 0)
                    {
                        foreach (var n in Serlist)
                        {
                            Serial ProdSer = PortalDB.Serials.Where(a => a.SerialNumber == n.SerialNumber && a.ProductStagingID == Prod.ProductStagingID).ToList().LastOrDefault();
                            if (ProdSer != null && ProdSer.OrderID == null && ProdSer.Status != "SLA DELETE")
                            {
                                ProdSer.Status = "AVAILABLE";
                                ProdSer.EventDate = DateTime.Now;
                                ProdSer.IGDStagingID = i.IGDStagingID;
                                PortalDB.Entry(ProdSer).State = System.Data.Entity.EntityState.Modified;
                                PortalDB.SaveChanges();
                            }
                            else
                            {
                                ProdSer = new Serial();
                                ProdSer.EventDate = DateTime.Now;
                                ProdSer.ProductStagingID = Prod.ProductStagingID;
                                ProdSer.SerialNumber = n.SerialNumber;
                                ProdSer.IGDStagingID = i.IGDStagingID;
                                ProdSer.Status = "AVAILABLE";

                                PortalDB.Serials.Add(ProdSer);
                                PortalDB.SaveChanges();
                            }
                        }

                        int StockQty = Prod.StockQuantity == null ? 0 : (int)Prod.StockQuantity;
                        Prod.StockQuantity = StockQty + Serlist.Count;
                    }
                    else
                    {
                        int StockQty = Prod.StockQuantity == null ? 0 : (int)Prod.StockQuantity;
                        Prod.StockQuantity = StockQty + AcceptCount;
                    }

                    Prod.EventDate = DateTime.Now;
                    PortalDB.Entry(Prod).State = System.Data.Entity.EntityState.Modified;
                    PortalDB.SaveChanges();
                }

                if (SendIGDCLC(i, Discrepency, SerialNumbers))
                {

                    i.ClientConfirmed = false;
                    i.EventDate = DateTime.Now;
                    PortalDB.Entry(i).State = System.Data.Entity.EntityState.Modified;
                    PortalDB.SaveChanges();
                }
            }
        }

        static void ProcessEachAndSend(List<IGDStaging> lst)
        {
            foreach(var i in lst)
            {
                Console.WriteLine(DateTime.Now.ToString() + "Process PO Receipt Client Msg - MoveRef " + i.MoveRef);

                IGDReceipt IR = PortalDB.IGDReceipts.AsNoTracking().Where(a => a.IGDStagingID == i.IGDStagingID).ToList().LastOrDefault();
                if(IR != null)
                {
                    CPOReceiptSoapClient cl = new CPOReceiptSoapClient();

                    List<IGDReceiptSerial> serList = PortalDB.IGDReceiptSerials.AsNoTracking().Where(a => a.IGDReceiptID == IR.ID).ToList();

                    //string[] SerialNumbers = new string[serList.Count];
                    ArrayOfString SerialNumbers = new ArrayOfString();

                    foreach(var x in serList)
                    {
                        SerialNumbers.Add(x.SerialNumber);
                    }

                    POReceipt.Response Res = cl.POReceipt(IR.PrincipalCode, false, Convert.ToInt32(IR.LineNumber), IR.ProdCode, IR.MoveRef, IR.PORef, (DateTime)IR.ReceiptDT, (int)IR.RejectCount, IR.ReasonCode, (int)IR.AcceptCount, SerialNumbers);

                    if (Res.success)
                    {
                        Principal Prn = PortalDB.Principals.AsNoTracking().Where(a => a.PrincipalID == i.PrincipalID && a.WebServiceIntegration == true).FirstOrDefault();

                        if(Prn != null) i.ClientConfirmed = true;
                        i.EventDate = DateTime.Now;
                        PortalDB.Entry(i).State = System.Data.Entity.EntityState.Modified;
                        PortalDB.SaveChanges();
                    }
                }
            }
        }

        static void ProcessEachAndSendCR(List<IGDStaging> lst)
        {
            foreach (var i in lst)
            {
                Console.WriteLine(DateTime.Now.ToString() + "Process CR Receipt Client Msg - MoveRef " + i.MoveRef);

                IGDReceipt IR = PortalDB.IGDReceipts.AsNoTracking().Where(a => a.IGDStagingID == i.IGDStagingID).ToList().LastOrDefault();
                if (IR != null)
                {
                    CustomerReturnReceiptSoapClient cl = new CustomerReturnReceiptSoapClient();

                    List<IGDReceiptSerial> serList = PortalDB.IGDReceiptSerials.AsNoTracking().Where(a => a.IGDReceiptID == IR.ID).ToList();

                    string[] SerialNumbers = new string[serList.Count];
                    int y = 0;
                    foreach (var x in serList)
                    {
                        SerialNumbers[y++] = x.SerialNumber;
                    }

                    CustomerReturn.Response Res = cl.Return(IR.PrincipalCode, false, Convert.ToInt32(IR.LineNumber), IR.ProdCode, IR.MoveRef, IR.PORef, (DateTime)IR.ReceiptDT, (int)IR.RejectCount, IR.ReasonCode, (int)IR.AcceptCount, SerialNumbers);

                    if (Res.success)
                    {
                        Principal Prn = PortalDB.Principals.AsNoTracking().Where(a => a.PrincipalID == i.PrincipalID && a.WebServiceIntegration == true).FirstOrDefault();

                        if (Prn != null) i.ClientConfirmed = true;
                        i.EventDate = DateTime.Now;
                        PortalDB.Entry(i).State = System.Data.Entity.EntityState.Modified;
                        PortalDB.SaveChanges();
                    }
                }
            }
        }

        static void PO_Receipt_Msg_ToProcess_list()
        {
            List<Principal> PrnList = PortalDB.Principals.AsNoTracking().Where(a => a.WebServiceIntegration == true).ToList();

            foreach (var i in PrnList)
            {
                List<IGDStaging> lst = PortalDB.IGDStagings.Where(a => a.Status == "RECEIPTED" && a.ClientConfirmed == false && a.PrincipalID == i.PrincipalID && a.ReceiptType != "R").ToList();
                ProcessEachAndSend(lst);
            }
        }

        static void CR_Receipt_Msg_ToProcess_list()
        {
            List<Principal> PrnList = PortalDB.Principals.AsNoTracking().Where(a => a.WebServiceIntegration == true).ToList();

            foreach (var i in PrnList)
            {
                List<IGDStaging> lst = PortalDB.IGDStagings.Where(a => a.Status == "RECEIPTED" && a.ClientConfirmed == false && a.PrincipalID == i.PrincipalID && a.ReceiptType == "R").ToList();
                ProcessEachAndSendCR(lst);
            }
        }

        static void Main(string[] args)
        {
            while (true)
            {
                Arr_Msg_ToProcess_List();
                Arr_ClC_Msg_ToProcess_list();
                PO_Receipt_Msg_ToProcess_list();
                CR_Receipt_Msg_ToProcess_list();
                Console.WriteLine(DateTime.Now.ToString() + " System timeout of " + (30000 / 1000) + " seconds starting ");
                System.Threading.Thread.Sleep(30000);
                Console.WriteLine(DateTime.Now.ToString() + " System timeout of " + (30000 / 1000) + " seconds ended ");
            }
        }
    }
}
