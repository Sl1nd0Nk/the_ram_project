﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace HostClientServices.WMSHost {
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(Namespace="http://services.ramgroup.co.za/", ConfigurationName="WMSHost.HostWSSoap")]
    public interface HostWSSoap {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://services.ramgroup.co.za/HelloWorld", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        string HelloWorld(string name);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://services.ramgroup.co.za/HelloWorld", ReplyAction="*")]
        System.Threading.Tasks.Task<string> HelloWorldAsync(string name);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://services.ramgroup.co.za/ARRAdd", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        HostClientServices.WMSHost.Result ARRAdd(string SiteCode, string PrincipalCode, bool Resend, string ProdCode, string LineNumber, string ReceiptType, string MoveRef, string PORef, System.DateTime ReceiptDT, string StartEndStatus, int RejectCount, string ReasonCode, int AcceptCount, string[] SerialNumbers);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://services.ramgroup.co.za/ARRAdd", ReplyAction="*")]
        System.Threading.Tasks.Task<HostClientServices.WMSHost.Result> ARRAddAsync(string SiteCode, string PrincipalCode, bool Resend, string ProdCode, string LineNumber, string ReceiptType, string MoveRef, string PORef, System.DateTime ReceiptDT, string StartEndStatus, int RejectCount, string ReasonCode, int AcceptCount, string[] SerialNumbers);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://services.ramgroup.co.za/CLCAdd", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        HostClientServices.WMSHost.Result CLCAdd(string recordSource, string siteCode, string principalCode, string moveRef, string orderLineNumber, string prodCode, int quantity, string receiptType, int discrepancy, int serialCount, string[] SerialNumbers);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://services.ramgroup.co.za/CLCAdd", ReplyAction="*")]
        System.Threading.Tasks.Task<HostClientServices.WMSHost.Result> CLCAddAsync(string recordSource, string siteCode, string principalCode, string moveRef, string orderLineNumber, string prodCode, int quantity, string receiptType, int discrepancy, int serialCount, string[] SerialNumbers);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://services.ramgroup.co.za/SAAAdd", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        HostClientServices.WMSHost.Result SAAAdd(string SiteCode, string PrincipalCode, string ProdCode, string QuantityChange);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://services.ramgroup.co.za/SAAAdd", ReplyAction="*")]
        System.Threading.Tasks.Task<HostClientServices.WMSHost.Result> SAAAddAsync(string SiteCode, string PrincipalCode, string ProdCode, string QuantityChange);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://services.ramgroup.co.za/ORDAdd", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        HostClientServices.WMSHost.Result ORDAdd(
                    string recordSource, 
                    string siteCode, 
                    string principalCode, 
                    string orderNumber, 
                    int lineCount, 
                    System.DateTime dateCreated, 
                    string companyName, 
                    string headerComment, 
                    string custOrderNumber, 
                    string custAcc, 
                    string invName, 
                    string invAdd1, 
                    string invAdd2, 
                    string invAdd3, 
                    string invAdd4, 
                    string invAdd5, 
                    string invAdd6, 
                    bool gsmTest, 
                    string vatNumber, 
                    bool printPrice, 
                    string priority, 
                    bool vap, 
                    string salesPerson, 
                    string salesCategory, 
                    string processor, 
                    string deliveryAdd1, 
                    string deliveryAdd2, 
                    string deliveryAdd3, 
                    string deliveryAdd4, 
                    string deliveryAdd5, 
                    string deliveryAdd6, 
                    string wmsPostCode, 
                    double orderDiscount, 
                    double orderVAT, 
                    int docToPrint, 
                    HostClientServices.WMSHost.ORDLine[] ordLines, 
                    string IDNumber, 
                    string KYC, 
                    string CourierName, 
                    string CourierService, 
                    bool InsuranceRequired, 
                    bool ValidateDelivery);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://services.ramgroup.co.za/ORDAdd", ReplyAction="*")]
        System.Threading.Tasks.Task<HostClientServices.WMSHost.Result> ORDAddAsync(
                    string recordSource, 
                    string siteCode, 
                    string principalCode, 
                    string orderNumber, 
                    int lineCount, 
                    System.DateTime dateCreated, 
                    string companyName, 
                    string headerComment, 
                    string custOrderNumber, 
                    string custAcc, 
                    string invName, 
                    string invAdd1, 
                    string invAdd2, 
                    string invAdd3, 
                    string invAdd4, 
                    string invAdd5, 
                    string invAdd6, 
                    bool gsmTest, 
                    string vatNumber, 
                    bool printPrice, 
                    string priority, 
                    bool vap, 
                    string salesPerson, 
                    string salesCategory, 
                    string processor, 
                    string deliveryAdd1, 
                    string deliveryAdd2, 
                    string deliveryAdd3, 
                    string deliveryAdd4, 
                    string deliveryAdd5, 
                    string deliveryAdd6, 
                    string wmsPostCode, 
                    double orderDiscount, 
                    double orderVAT, 
                    int docToPrint, 
                    HostClientServices.WMSHost.ORDLine[] ordLines, 
                    string IDNumber, 
                    string KYC, 
                    string CourierName, 
                    string CourierService, 
                    bool InsuranceRequired, 
                    bool ValidateDelivery);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://services.ramgroup.co.za/OCOAdd", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        HostClientServices.WMSHost.Result OCOAdd(string SiteCode, string PrincipalCode, string OrderNumber, string Priority, string UserID, double Weight, double OrderCost, double OrderDiscount, double OrderVAT, HostClientServices.WMSHost.OCOLine[] Lines);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://services.ramgroup.co.za/OCOAdd", ReplyAction="*")]
        System.Threading.Tasks.Task<HostClientServices.WMSHost.Result> OCOAddAsync(string SiteCode, string PrincipalCode, string OrderNumber, string Priority, string UserID, double Weight, double OrderCost, double OrderDiscount, double OrderVAT, HostClientServices.WMSHost.OCOLine[] Lines);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://services.ramgroup.co.za/OSTAdd", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        HostClientServices.WMSHost.Result OSTAdd(string SiteCode, string PrincipalCode, string OrderNumber);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://services.ramgroup.co.za/OSTAdd", ReplyAction="*")]
        System.Threading.Tasks.Task<HostClientServices.WMSHost.Result> OSTAddAsync(string SiteCode, string PrincipalCode, string OrderNumber);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://services.ramgroup.co.za/SLAAdd", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        HostClientServices.WMSHost.Result SLAAdd(string SiteCode, string PrincipalCode, string ProdCode, int QuantityChange, string ReasonCode, string AvailabilityState, int SerialCount, string[] SerialNumbers);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://services.ramgroup.co.za/SLAAdd", ReplyAction="*")]
        System.Threading.Tasks.Task<HostClientServices.WMSHost.Result> SLAAddAsync(string SiteCode, string PrincipalCode, string ProdCode, int QuantityChange, string ReasonCode, string AvailabilityState, int SerialCount, string[] SerialNumbers);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://services.ramgroup.co.za/IGDAdd", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        int IGDAdd(
                    string recordSource, 
                    string siteCode, 
                    string principalCode, 
                    string productCode, 
                    string EANCode, 
                    string ShortDesc, 
                    string LongDesc, 
                    bool serialised, 
                    string AnalysisA, 
                    string AnalysisB, 
                    string orderLineNo, 
                    int quantity, 
                    string receiptType, 
                    string moveRef, 
                    string poRef, 
                    System.DateTime poDate, 
                    System.DateTime stockDateTime);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://services.ramgroup.co.za/IGDAdd", ReplyAction="*")]
        System.Threading.Tasks.Task<int> IGDAddAsync(
                    string recordSource, 
                    string siteCode, 
                    string principalCode, 
                    string productCode, 
                    string EANCode, 
                    string ShortDesc, 
                    string LongDesc, 
                    bool serialised, 
                    string AnalysisA, 
                    string AnalysisB, 
                    string orderLineNo, 
                    int quantity, 
                    string receiptType, 
                    string moveRef, 
                    string poRef, 
                    System.DateTime poDate, 
                    System.DateTime stockDateTime);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://services.ramgroup.co.za/ParcelAdd", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        HostClientServices.WMSHost.Result ParcelAdd(string SiteCode, string PrincipalCode, string OrderNumber, int ParcelNo, double Kilograms, double Length, double Breadth, double Height, double InsuredValue, string SPNumber, string ParcelReference, int NoOfParcels);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://services.ramgroup.co.za/ParcelAdd", ReplyAction="*")]
        System.Threading.Tasks.Task<HostClientServices.WMSHost.Result> ParcelAddAsync(string SiteCode, string PrincipalCode, string OrderNumber, int ParcelNo, double Kilograms, double Length, double Breadth, double Height, double InsuredValue, string SPNumber, string ParcelReference, int NoOfParcels);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://services.ramgroup.co.za/ParcelLabel", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        System.Xml.XmlNode ParcelLabel(string cartonID);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://services.ramgroup.co.za/ParcelLabel", ReplyAction="*")]
        System.Threading.Tasks.Task<System.Xml.XmlNode> ParcelLabelAsync(string cartonID);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://services.ramgroup.co.za/ParcelAddLabelRequest", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        System.Xml.XmlNode ParcelAddLabelRequest(string SiteCode, string PrincipalCode, string OrderNumber, int ParcelNo, double Kilograms, double Length, double Breadth, double Height, double InsuredValue, string SPNumber, string ParcelReference, int NoOfParcels);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://services.ramgroup.co.za/ParcelAddLabelRequest", ReplyAction="*")]
        System.Threading.Tasks.Task<System.Xml.XmlNode> ParcelAddLabelRequestAsync(string SiteCode, string PrincipalCode, string OrderNumber, int ParcelNo, double Kilograms, double Length, double Breadth, double Height, double InsuredValue, string SPNumber, string ParcelReference, int NoOfParcels);
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.8.3761.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://services.ramgroup.co.za/")]
    public partial class Result : object, System.ComponentModel.INotifyPropertyChanged {
        
        private bool successField;
        
        private string reasonTextField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=0)]
        public bool Success {
            get {
                return this.successField;
            }
            set {
                this.successField = value;
                this.RaisePropertyChanged("Success");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=1)]
        public string ReasonText {
            get {
                return this.reasonTextField;
            }
            set {
                this.reasonTextField = value;
                this.RaisePropertyChanged("ReasonText");
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.8.3761.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://services.ramgroup.co.za/")]
    public partial class OCOLine : object, System.ComponentModel.INotifyPropertyChanged {
        
        private int lineNumberField;
        
        private string prodCodeField;
        
        private int quantityPickedField;
        
        private string serialNumberField;
        
        private double unitCostField;
        
        private double vatField;
        
        private double discountField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=0)]
        public int LineNumber {
            get {
                return this.lineNumberField;
            }
            set {
                this.lineNumberField = value;
                this.RaisePropertyChanged("LineNumber");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=1)]
        public string ProdCode {
            get {
                return this.prodCodeField;
            }
            set {
                this.prodCodeField = value;
                this.RaisePropertyChanged("ProdCode");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=2)]
        public int QuantityPicked {
            get {
                return this.quantityPickedField;
            }
            set {
                this.quantityPickedField = value;
                this.RaisePropertyChanged("QuantityPicked");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=3)]
        public string SerialNumber {
            get {
                return this.serialNumberField;
            }
            set {
                this.serialNumberField = value;
                this.RaisePropertyChanged("SerialNumber");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=4)]
        public double UnitCost {
            get {
                return this.unitCostField;
            }
            set {
                this.unitCostField = value;
                this.RaisePropertyChanged("UnitCost");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=5)]
        public double Vat {
            get {
                return this.vatField;
            }
            set {
                this.vatField = value;
                this.RaisePropertyChanged("Vat");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=6)]
        public double Discount {
            get {
                return this.discountField;
            }
            set {
                this.discountField = value;
                this.RaisePropertyChanged("Discount");
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.8.3761.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://services.ramgroup.co.za/")]
    public partial class ORDLine : object, System.ComponentModel.INotifyPropertyChanged {
        
        private int lineNumberField;
        
        private string lineTypeField;
        
        private string productCodeField;
        
        private string lineTextField;
        
        private int quantityField;
        
        private double unitCostField;
        
        private double vATField;
        
        private double unitDiscountAmountField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=0)]
        public int LineNumber {
            get {
                return this.lineNumberField;
            }
            set {
                this.lineNumberField = value;
                this.RaisePropertyChanged("LineNumber");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=1)]
        public string LineType {
            get {
                return this.lineTypeField;
            }
            set {
                this.lineTypeField = value;
                this.RaisePropertyChanged("LineType");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=2)]
        public string ProductCode {
            get {
                return this.productCodeField;
            }
            set {
                this.productCodeField = value;
                this.RaisePropertyChanged("ProductCode");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=3)]
        public string LineText {
            get {
                return this.lineTextField;
            }
            set {
                this.lineTextField = value;
                this.RaisePropertyChanged("LineText");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=4)]
        public int Quantity {
            get {
                return this.quantityField;
            }
            set {
                this.quantityField = value;
                this.RaisePropertyChanged("Quantity");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=5)]
        public double UnitCost {
            get {
                return this.unitCostField;
            }
            set {
                this.unitCostField = value;
                this.RaisePropertyChanged("UnitCost");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=6)]
        public double VAT {
            get {
                return this.vATField;
            }
            set {
                this.vATField = value;
                this.RaisePropertyChanged("VAT");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=7)]
        public double UnitDiscountAmount {
            get {
                return this.unitDiscountAmountField;
            }
            set {
                this.unitDiscountAmountField = value;
                this.RaisePropertyChanged("UnitDiscountAmount");
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface HostWSSoapChannel : HostClientServices.WMSHost.HostWSSoap, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class HostWSSoapClient : System.ServiceModel.ClientBase<HostClientServices.WMSHost.HostWSSoap>, HostClientServices.WMSHost.HostWSSoap {
        
        public HostWSSoapClient() {
        }
        
        public HostWSSoapClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public HostWSSoapClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public HostWSSoapClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public HostWSSoapClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public string HelloWorld(string name) {
            return base.Channel.HelloWorld(name);
        }
        
        public System.Threading.Tasks.Task<string> HelloWorldAsync(string name) {
            return base.Channel.HelloWorldAsync(name);
        }
        
        public HostClientServices.WMSHost.Result ARRAdd(string SiteCode, string PrincipalCode, bool Resend, string ProdCode, string LineNumber, string ReceiptType, string MoveRef, string PORef, System.DateTime ReceiptDT, string StartEndStatus, int RejectCount, string ReasonCode, int AcceptCount, string[] SerialNumbers) {
            return base.Channel.ARRAdd(SiteCode, PrincipalCode, Resend, ProdCode, LineNumber, ReceiptType, MoveRef, PORef, ReceiptDT, StartEndStatus, RejectCount, ReasonCode, AcceptCount, SerialNumbers);
        }
        
        public System.Threading.Tasks.Task<HostClientServices.WMSHost.Result> ARRAddAsync(string SiteCode, string PrincipalCode, bool Resend, string ProdCode, string LineNumber, string ReceiptType, string MoveRef, string PORef, System.DateTime ReceiptDT, string StartEndStatus, int RejectCount, string ReasonCode, int AcceptCount, string[] SerialNumbers) {
            return base.Channel.ARRAddAsync(SiteCode, PrincipalCode, Resend, ProdCode, LineNumber, ReceiptType, MoveRef, PORef, ReceiptDT, StartEndStatus, RejectCount, ReasonCode, AcceptCount, SerialNumbers);
        }
        
        public HostClientServices.WMSHost.Result CLCAdd(string recordSource, string siteCode, string principalCode, string moveRef, string orderLineNumber, string prodCode, int quantity, string receiptType, int discrepancy, int serialCount, string[] SerialNumbers) {
            return base.Channel.CLCAdd(recordSource, siteCode, principalCode, moveRef, orderLineNumber, prodCode, quantity, receiptType, discrepancy, serialCount, SerialNumbers);
        }
        
        public System.Threading.Tasks.Task<HostClientServices.WMSHost.Result> CLCAddAsync(string recordSource, string siteCode, string principalCode, string moveRef, string orderLineNumber, string prodCode, int quantity, string receiptType, int discrepancy, int serialCount, string[] SerialNumbers) {
            return base.Channel.CLCAddAsync(recordSource, siteCode, principalCode, moveRef, orderLineNumber, prodCode, quantity, receiptType, discrepancy, serialCount, SerialNumbers);
        }
        
        public HostClientServices.WMSHost.Result SAAAdd(string SiteCode, string PrincipalCode, string ProdCode, string QuantityChange) {
            return base.Channel.SAAAdd(SiteCode, PrincipalCode, ProdCode, QuantityChange);
        }
        
        public System.Threading.Tasks.Task<HostClientServices.WMSHost.Result> SAAAddAsync(string SiteCode, string PrincipalCode, string ProdCode, string QuantityChange) {
            return base.Channel.SAAAddAsync(SiteCode, PrincipalCode, ProdCode, QuantityChange);
        }
        
        public HostClientServices.WMSHost.Result ORDAdd(
                    string recordSource, 
                    string siteCode, 
                    string principalCode, 
                    string orderNumber, 
                    int lineCount, 
                    System.DateTime dateCreated, 
                    string companyName, 
                    string headerComment, 
                    string custOrderNumber, 
                    string custAcc, 
                    string invName, 
                    string invAdd1, 
                    string invAdd2, 
                    string invAdd3, 
                    string invAdd4, 
                    string invAdd5, 
                    string invAdd6, 
                    bool gsmTest, 
                    string vatNumber, 
                    bool printPrice, 
                    string priority, 
                    bool vap, 
                    string salesPerson, 
                    string salesCategory, 
                    string processor, 
                    string deliveryAdd1, 
                    string deliveryAdd2, 
                    string deliveryAdd3, 
                    string deliveryAdd4, 
                    string deliveryAdd5, 
                    string deliveryAdd6, 
                    string wmsPostCode, 
                    double orderDiscount, 
                    double orderVAT, 
                    int docToPrint, 
                    HostClientServices.WMSHost.ORDLine[] ordLines, 
                    string IDNumber, 
                    string KYC, 
                    string CourierName, 
                    string CourierService, 
                    bool InsuranceRequired, 
                    bool ValidateDelivery) {
            return base.Channel.ORDAdd(recordSource, siteCode, principalCode, orderNumber, lineCount, dateCreated, companyName, headerComment, custOrderNumber, custAcc, invName, invAdd1, invAdd2, invAdd3, invAdd4, invAdd5, invAdd6, gsmTest, vatNumber, printPrice, priority, vap, salesPerson, salesCategory, processor, deliveryAdd1, deliveryAdd2, deliveryAdd3, deliveryAdd4, deliveryAdd5, deliveryAdd6, wmsPostCode, orderDiscount, orderVAT, docToPrint, ordLines, IDNumber, KYC, CourierName, CourierService, InsuranceRequired, ValidateDelivery);
        }
        
        public System.Threading.Tasks.Task<HostClientServices.WMSHost.Result> ORDAddAsync(
                    string recordSource, 
                    string siteCode, 
                    string principalCode, 
                    string orderNumber, 
                    int lineCount, 
                    System.DateTime dateCreated, 
                    string companyName, 
                    string headerComment, 
                    string custOrderNumber, 
                    string custAcc, 
                    string invName, 
                    string invAdd1, 
                    string invAdd2, 
                    string invAdd3, 
                    string invAdd4, 
                    string invAdd5, 
                    string invAdd6, 
                    bool gsmTest, 
                    string vatNumber, 
                    bool printPrice, 
                    string priority, 
                    bool vap, 
                    string salesPerson, 
                    string salesCategory, 
                    string processor, 
                    string deliveryAdd1, 
                    string deliveryAdd2, 
                    string deliveryAdd3, 
                    string deliveryAdd4, 
                    string deliveryAdd5, 
                    string deliveryAdd6, 
                    string wmsPostCode, 
                    double orderDiscount, 
                    double orderVAT, 
                    int docToPrint, 
                    HostClientServices.WMSHost.ORDLine[] ordLines, 
                    string IDNumber, 
                    string KYC, 
                    string CourierName, 
                    string CourierService, 
                    bool InsuranceRequired, 
                    bool ValidateDelivery) {
            return base.Channel.ORDAddAsync(recordSource, siteCode, principalCode, orderNumber, lineCount, dateCreated, companyName, headerComment, custOrderNumber, custAcc, invName, invAdd1, invAdd2, invAdd3, invAdd4, invAdd5, invAdd6, gsmTest, vatNumber, printPrice, priority, vap, salesPerson, salesCategory, processor, deliveryAdd1, deliveryAdd2, deliveryAdd3, deliveryAdd4, deliveryAdd5, deliveryAdd6, wmsPostCode, orderDiscount, orderVAT, docToPrint, ordLines, IDNumber, KYC, CourierName, CourierService, InsuranceRequired, ValidateDelivery);
        }
        
        public HostClientServices.WMSHost.Result OCOAdd(string SiteCode, string PrincipalCode, string OrderNumber, string Priority, string UserID, double Weight, double OrderCost, double OrderDiscount, double OrderVAT, HostClientServices.WMSHost.OCOLine[] Lines) {
            return base.Channel.OCOAdd(SiteCode, PrincipalCode, OrderNumber, Priority, UserID, Weight, OrderCost, OrderDiscount, OrderVAT, Lines);
        }
        
        public System.Threading.Tasks.Task<HostClientServices.WMSHost.Result> OCOAddAsync(string SiteCode, string PrincipalCode, string OrderNumber, string Priority, string UserID, double Weight, double OrderCost, double OrderDiscount, double OrderVAT, HostClientServices.WMSHost.OCOLine[] Lines) {
            return base.Channel.OCOAddAsync(SiteCode, PrincipalCode, OrderNumber, Priority, UserID, Weight, OrderCost, OrderDiscount, OrderVAT, Lines);
        }
        
        public HostClientServices.WMSHost.Result OSTAdd(string SiteCode, string PrincipalCode, string OrderNumber) {
            return base.Channel.OSTAdd(SiteCode, PrincipalCode, OrderNumber);
        }
        
        public System.Threading.Tasks.Task<HostClientServices.WMSHost.Result> OSTAddAsync(string SiteCode, string PrincipalCode, string OrderNumber) {
            return base.Channel.OSTAddAsync(SiteCode, PrincipalCode, OrderNumber);
        }
        
        public HostClientServices.WMSHost.Result SLAAdd(string SiteCode, string PrincipalCode, string ProdCode, int QuantityChange, string ReasonCode, string AvailabilityState, int SerialCount, string[] SerialNumbers) {
            return base.Channel.SLAAdd(SiteCode, PrincipalCode, ProdCode, QuantityChange, ReasonCode, AvailabilityState, SerialCount, SerialNumbers);
        }
        
        public System.Threading.Tasks.Task<HostClientServices.WMSHost.Result> SLAAddAsync(string SiteCode, string PrincipalCode, string ProdCode, int QuantityChange, string ReasonCode, string AvailabilityState, int SerialCount, string[] SerialNumbers) {
            return base.Channel.SLAAddAsync(SiteCode, PrincipalCode, ProdCode, QuantityChange, ReasonCode, AvailabilityState, SerialCount, SerialNumbers);
        }
        
        public int IGDAdd(
                    string recordSource, 
                    string siteCode, 
                    string principalCode, 
                    string productCode, 
                    string EANCode, 
                    string ShortDesc, 
                    string LongDesc, 
                    bool serialised, 
                    string AnalysisA, 
                    string AnalysisB, 
                    string orderLineNo, 
                    int quantity, 
                    string receiptType, 
                    string moveRef, 
                    string poRef, 
                    System.DateTime poDate, 
                    System.DateTime stockDateTime) {
            return base.Channel.IGDAdd(recordSource, siteCode, principalCode, productCode, EANCode, ShortDesc, LongDesc, serialised, AnalysisA, AnalysisB, orderLineNo, quantity, receiptType, moveRef, poRef, poDate, stockDateTime);
        }
        
        public System.Threading.Tasks.Task<int> IGDAddAsync(
                    string recordSource, 
                    string siteCode, 
                    string principalCode, 
                    string productCode, 
                    string EANCode, 
                    string ShortDesc, 
                    string LongDesc, 
                    bool serialised, 
                    string AnalysisA, 
                    string AnalysisB, 
                    string orderLineNo, 
                    int quantity, 
                    string receiptType, 
                    string moveRef, 
                    string poRef, 
                    System.DateTime poDate, 
                    System.DateTime stockDateTime) {
            return base.Channel.IGDAddAsync(recordSource, siteCode, principalCode, productCode, EANCode, ShortDesc, LongDesc, serialised, AnalysisA, AnalysisB, orderLineNo, quantity, receiptType, moveRef, poRef, poDate, stockDateTime);
        }
        
        public HostClientServices.WMSHost.Result ParcelAdd(string SiteCode, string PrincipalCode, string OrderNumber, int ParcelNo, double Kilograms, double Length, double Breadth, double Height, double InsuredValue, string SPNumber, string ParcelReference, int NoOfParcels) {
            return base.Channel.ParcelAdd(SiteCode, PrincipalCode, OrderNumber, ParcelNo, Kilograms, Length, Breadth, Height, InsuredValue, SPNumber, ParcelReference, NoOfParcels);
        }
        
        public System.Threading.Tasks.Task<HostClientServices.WMSHost.Result> ParcelAddAsync(string SiteCode, string PrincipalCode, string OrderNumber, int ParcelNo, double Kilograms, double Length, double Breadth, double Height, double InsuredValue, string SPNumber, string ParcelReference, int NoOfParcels) {
            return base.Channel.ParcelAddAsync(SiteCode, PrincipalCode, OrderNumber, ParcelNo, Kilograms, Length, Breadth, Height, InsuredValue, SPNumber, ParcelReference, NoOfParcels);
        }
        
        public System.Xml.XmlNode ParcelLabel(string cartonID) {
            return base.Channel.ParcelLabel(cartonID);
        }
        
        public System.Threading.Tasks.Task<System.Xml.XmlNode> ParcelLabelAsync(string cartonID) {
            return base.Channel.ParcelLabelAsync(cartonID);
        }
        
        public System.Xml.XmlNode ParcelAddLabelRequest(string SiteCode, string PrincipalCode, string OrderNumber, int ParcelNo, double Kilograms, double Length, double Breadth, double Height, double InsuredValue, string SPNumber, string ParcelReference, int NoOfParcels) {
            return base.Channel.ParcelAddLabelRequest(SiteCode, PrincipalCode, OrderNumber, ParcelNo, Kilograms, Length, Breadth, Height, InsuredValue, SPNumber, ParcelReference, NoOfParcels);
        }
        
        public System.Threading.Tasks.Task<System.Xml.XmlNode> ParcelAddLabelRequestAsync(string SiteCode, string PrincipalCode, string OrderNumber, int ParcelNo, double Kilograms, double Length, double Breadth, double Height, double InsuredValue, string SPNumber, string ParcelReference, int NoOfParcels) {
            return base.Channel.ParcelAddLabelRequestAsync(SiteCode, PrincipalCode, OrderNumber, ParcelNo, Kilograms, Length, Breadth, Height, InsuredValue, SPNumber, ParcelReference, NoOfParcels);
        }
    }
}
