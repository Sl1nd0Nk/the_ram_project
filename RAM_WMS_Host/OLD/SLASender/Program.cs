﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SLASender.StockLevelAdjustment;

namespace SLASender
{
    class Program
    {
        private static CustomerPortalEntities PortalDB = new CustomerPortalEntities();
        private static RAMWMSHOSTEntities WMSHostDB = new RAMWMSHOSTEntities();

        static void ProcessSLAs(List<MsgIn_SLA> Lst)
        {
            foreach(var SLA in Lst)
            {
                Console.WriteLine(DateTime.Now.ToString() + "Process SLA Msg - Product " + SLA.ProdCode);

                string ProcErr = "";
                DateTime ProcStartDT = DateTime.Now;

                ProductStaging Prod = PortalDB.ProductStagings.Where(a => a.ProdCode == SLA.ProdCode && a.Deleted == false).FirstOrDefault();
                Principal Prn = PortalDB.Principals.AsNoTracking().Where(a => a.PrincipalCode == SLA.PrincipalCode && a.WebServiceIntegration == true).FirstOrDefault();

                if (Prod != null)
                {
                    SLA sla = new SLA();
                    sla.AvailabilityState = SLA.AvailabilityState;
                    sla.CreateDate = DateTime.Now;
                    sla.PrincipalCode = SLA.PrincipalCode;
                    sla.ProdCode = SLA.ProdCode;
                    sla.QuantityChange = SLA.QuantityChange;
                    sla.ReasonCode = SLA.ReasonCode;
                    sla.SerialCount = SLA.SerialCount;
                    sla.SiteCode = SLA.SiteCode;

                    if(Prn != null) sla.ClientConfirmed = false;

                    PortalDB.SLAs.Add(sla);
                    PortalDB.SaveChanges();

                    if(Prod.Serialised == true)
                    {
                        List<MsgIn_SLA_SN> SerList = WMSHostDB.MsgIn_SLA_SN.AsNoTracking().Where(a => a.SLAID == SLA.SLAID).ToList();

                        foreach(var i in SerList)
                        {
                            SLASerial sSer = new SLASerial();
                            sSer.SerialNumber = i.SerialNumber;
                            sSer.SLAID = sla.ID;

                            PortalDB.SLASerials.Add(sSer);
                            PortalDB.SaveChanges();

                            Serial ProdSer = PortalDB.Serials.Where(a => a.SerialNumber == i.SerialNumber && a.ProductStagingID == Prod.ProductStagingID).ToList().LastOrDefault();
                            if(ProdSer != null && ProdSer.OrderID == null)
                            {
                                if(sla.QuantityChange < 0)
                                {
                                    ProdSer.Status = "SLA DELETE";
                                }
                                else
                                {
                                    ProdSer.Status = "AVAILABLE";
                                }

                                ProdSer.EventDate = DateTime.Now;
                                ProdSer.SLAID = sla.ID;
                                PortalDB.Entry(ProdSer).State = System.Data.Entity.EntityState.Modified;
                                PortalDB.SaveChanges();
                            }
                            else
                            {
                                ProdSer = new Serial();
                                ProdSer.EventDate = DateTime.Now;
                                ProdSer.ProductStagingID = Prod.ProductStagingID;
                                ProdSer.SerialNumber = i.SerialNumber;
                                ProdSer.SLAID = sla.ID;
                                ProdSer.Status = sla.QuantityChange > 0 ? "AVAILABLE" : "SLA DELETE";

                                PortalDB.Serials.Add(ProdSer);
                                PortalDB.SaveChanges();
                            }
                        }

                        int StockQty = Prod.StockQuantity == null ? 0 : (int)Prod.StockQuantity;
                        Prod.StockQuantity = StockQty + sla.QuantityChange > 0? SerList.Count: -SerList.Count;
                    }
                    else
                    {
                        int StockQty = Prod.StockQuantity == null ? 0 : (int)Prod.StockQuantity;
                        Prod.StockQuantity = StockQty + sla.QuantityChange;
                    }

                    Prod.EventDate = DateTime.Now;
                    PortalDB.Entry(Prod).State = System.Data.Entity.EntityState.Modified;
                    PortalDB.SaveChanges();
                }
                else
                {
                    ProcErr = "Order Not Found In The Customer Portal";
                }

                MsgIn_SLA UpdateSla = WMSHostDB.MsgIn_SLA.Find(SLA.SLAID);

                UpdateSla.RecordState = !String.IsNullOrEmpty(ProcErr) ? "PROCESSERROR" : "PROCESSED";
                UpdateSla.ProcStartDT = ProcStartDT;
                UpdateSla.ProcEndDT = DateTime.Now;
                UpdateSla.ProcErrors = ProcErr;
                WMSHostDB.Entry(UpdateSla).State = System.Data.Entity.EntityState.Modified;
                WMSHostDB.SaveChanges();
            }
        }

        static void SLA_Msg_ToProcess_List()
        {
            List<MsgIn_SLA> Lst = WMSHostDB.MsgIn_SLA.AsNoTracking().Where(a => a.RecordState == "READY").Take(100).OrderByDescending(a => a.RecordDT).ToList();

            ProcessSLAs(Lst);
        }

        static void ClientSendEach(List<SLA> Lst)
        {
            foreach(var i in Lst)
            {
                Console.WriteLine(DateTime.Now.ToString() + "Process SLA Client Msg - Product " + i.ProdCode + " | ID: " + i.ID);

                List<SLASerial> SerList = PortalDB.SLASerials.AsNoTracking().Where(a => a.SLAID == i.ID).ToList();

                int SerCount = SerList.Count > 0 ? SerList.Count : (int)i.SerialCount;
                string[] SerialNumbers = new string[SerList.Count];
                int x = 0;
                
                foreach(var ser in SerList)
                {
                    SerialNumbers[x++] = ser.SerialNumber;
                }

                StockLevelAdjustmentSoapClient cl = new StockLevelAdjustmentSoapClient("StockLevelAdjustmentSoap");
                Response Res = cl.SLA(i.PrincipalCode, i.ProdCode, (int)i.QuantityChange, i.ReasonCode, i.AvailabilityState, SerCount, SerialNumbers);

                Console.WriteLine(DateTime.Now.ToString() + "SLA Client Msg - Response " + Res.success);
                if (Res.success)
                {
                    i.ClientConfirmed = true;
                    PortalDB.Entry(i).State = System.Data.Entity.EntityState.Modified;
                    PortalDB.SaveChanges();
                }
            }
        }

        static void SLA_ClientSend()
        {
            List<SLA> Lst = PortalDB.SLAs.Where(a => a.ClientConfirmed == false).ToList();

            ClientSendEach(Lst);
        }

        static void Main(string[] args)
        {
            while (true)
            {
                SLA_Msg_ToProcess_List();
                SLA_ClientSend();
                Console.WriteLine(DateTime.Now.ToString() + " System timeout of " + (30000 / 1000) + " seconds starting ");
                System.Threading.Thread.Sleep(30000);
                Console.WriteLine(DateTime.Now.ToString() + " System timeout of " + (30000 / 1000) + " seconds ended ");
            }
        }
    }
}
