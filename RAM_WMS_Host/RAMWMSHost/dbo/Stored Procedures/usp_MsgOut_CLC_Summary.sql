﻿
create procedure [dbo].[usp_MsgOut_CLC_Summary]  as 


select A.RecordState, A.SortOrder, A.IsError,
	COUNT(distinct B.CLCID) as RecordCount,
	MAX(RecordDT) as LastRecordDT
from Msg_RecordState as A
left outer join dbo.MsgOut_CLC as B on B.RecordState = A.RecordState
group by A.RecordState, A.SortOrder, A.IsError
order by A.SortOrder



