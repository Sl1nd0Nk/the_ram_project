﻿
create procedure [dbo].[usp_MsgIn_ARR_SetREADY] @ARRID int as 
								
/**
RecordState
LOADING -> READY -> PROCESSING - > PROCESSED
CANCELLED / LOADERROR / PROCESSERROR
**/
									
update dbo.MsgIn_ARR 
set RecordState = 'READY'
where ARRID = @ARRID;						
