﻿
create procedure [dbo].[usp_MsgOut_CLC_Add] 
									@CLCID int out, 
									@RecordSource varchar(20),
									
									@SiteCode varchar(4),
									@PrincipalCode varchar(4),
									@MoveRef varchar(10),
									@OrderLineNumber varchar(4),
									@ProdCode varchar(20),
									@Quantity int,
									@ReceiptType char(1),
									@Discrepancy int,
									@SerialCount int as 
									
/**
RecordState
LOADING -> READY -> PROCESSING - > PROCESSED
CANCELLED / LOADERROR / PROCESSERROR
**/
									
insert into dbo.MsgOut_CLC (RecordState, RecordSource, SiteCode, PrincipalCode, MoveRef, OrderLineNumber, ProdCode, Quantity, ReceiptType, Discrepancy, SerialCount)
select					  'LOADING',   @RecordSource,@SiteCode,@PrincipalCode,@MoveRef,@OrderLineNumber,@ProdCode,@Quantity,@ReceiptType,@Discrepancy,@SerialCount;

set @CLCID = SCOPE_IDENTITY();								

 
