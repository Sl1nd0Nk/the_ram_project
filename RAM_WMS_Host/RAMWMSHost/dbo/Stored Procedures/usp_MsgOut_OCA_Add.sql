﻿create procedure [dbo].[usp_MsgOut_OCA_Add] 
									@OCAID int out, 
									@RecordSource varchar(20),
									
									@SiteCode varchar(4),
									@PrincipalCode varchar(4),
									@OrderNumber varchar(10) as 
									
/**
RecordState
LOADING -> READY -> PROCESSING - > PROCESSED
CANCELLED / LOADERROR / PROCESSERROR
**/
									
insert into dbo.MsgOut_OCA (RecordState, RecordSource, SiteCode, PrincipalCode, OrderNumber)
select					  'LOADING',    @RecordSource,@SiteCode,@PrincipalCode,@OrderNumber;

set @OCAID = SCOPE_IDENTITY();								

 
 
	
 

