﻿create procedure [dbo].[usp_MsgOut_IGD_Add] 
									@IGDID int out, 
									@RecordSource varchar(20),
									
									@SiteCode varchar(4),
									@PrincipalCode varchar(4),
									@ProdCode varchar(20),
									@EANCode varchar(20),
									@ShortDesc varchar(20),
									@LongDesc varchar(40),
									@Serialised bit,
									@AnalysisA varchar(10),
									@AnalysisB varchar(10),
									@OrderLineNo varchar(4),
									@Quantity int,
									@ReceiptType char(1),
									@MoveRef varchar(10),
									@PORef varchar(10),
									@PODate datetime,
									@StockDateTime datetime as 
									
/**
RecordState
LOADING -> READY -> PROCESSING - > PROCESSED
CANCELLED / LOADERROR / PROCESSERROR
**/
									
insert into dbo.MsgOut_IGD (RecordState, RecordSource, SiteCode, PrincipalCode, ProdCode, EANCode, ShortDesc, LongDesc, Serialised, AnalysisA, AnalysisB, OrderLineNo, Quantity, ReceiptType, MoveRef, PORef, PODate, StockDateTime)
select					  'LOADING',    @RecordSource,@SiteCode,@PrincipalCode,@ProdCode,@EANCode,@ShortDesc,@LongDesc,@Serialised,@AnalysisA,@AnalysisB,@OrderLineNo,@Quantity,@ReceiptType,@MoveRef,@PORef,@PODate,@StockDateTime;

set @IGDID = SCOPE_IDENTITY();								

 
 
	
 

