﻿
CREATE procedure [dbo].[usp_MsgIn_OST_Add] 
									@OSTID int out, 
									@RecordSource varchar(20),
									
									@SiteCode varchar(4),
									@PrincipalCode varchar(4),
									@OrderNumber varchar(10) as 					
									
/**
RecordState
LOADING -> READY -> PROCESSING - > PROCESSED
CANCELLED / LOADERROR / PROCESSERROR
**/
									
insert into dbo.MsgIn_OST (RecordState, RecordSource, SiteCode, PrincipalCode, OrderNumber)
select					  'READY',     @RecordSource,@SiteCode,@PrincipalCode,@OrderNumber;

set @OSTID = SCOPE_IDENTITY();								


