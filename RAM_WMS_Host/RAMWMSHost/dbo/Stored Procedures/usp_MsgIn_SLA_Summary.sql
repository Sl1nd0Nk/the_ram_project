﻿create procedure [dbo].[usp_MsgIn_SLA_Summary]  as 


select A.RecordState, A.SortOrder, A.IsError,
	COUNT(distinct B.SLAID) as RecordCount,
	MAX(RecordDT) as LastRecordDT
from Msg_RecordState as A
left outer join dbo.MsgIn_SLA as B on B.RecordState = A.RecordState
group by A.RecordState, A.SortOrder, A.IsError
order by A.SortOrder


