﻿

 
 
CREATE procedure [dbo].[usp_MsgIn_SLA_Add] 
									@SLAID int out, 
									@RecordSource varchar(20),
									
									@SiteCode varchar(4),
									@PrincipalCode varchar(4),
									@ProdCode varchar(20),
									@QuantityChange int,
									@ReasonCode char(2),
									@AvailabilityState char(1),
									@SerialCount int as 
									
/**
RecordState
LOADING -> READY -> PROCESSING - > PROCESSED
CANCELLED / LOADERROR / PROCESSERROR
**/
									
insert into dbo.MsgIn_SLA (RecordState, RecordSource, SiteCode, PrincipalCode, ProdCode, QuantityChange, ReasonCode, AvailabilityState, SerialCount)
select					  'LOADING',   @RecordSource,@SiteCode,@PrincipalCode,@ProdCode,@QuantityChange,@ReasonCode,@AvailabilityState,@SerialCount;

set @SLAID = SCOPE_IDENTITY();								

 

 


