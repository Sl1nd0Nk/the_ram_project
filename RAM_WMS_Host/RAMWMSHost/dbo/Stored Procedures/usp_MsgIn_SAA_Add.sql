﻿
CREATE procedure [dbo].[usp_MsgIn_SAA_Add] 
									@SAAID int out, 
									@RecordSource varchar(20),
									
									@SiteCode varchar(4),
									@PrincipalCode varchar(4),
									@ProdCode varchar(20),
									@QuantityChange varchar(10) as 					
									
/**
RecordState
LOADING -> READY -> PROCESSING - > PROCESSED
CANCELLED / LOADERROR / PROCESSERROR
**/
									
insert into dbo.MsgIn_SAA (RecordState, RecordSource, SiteCode, PrincipalCode, ProdCode, QuantityChange)
select					  'READY',     @RecordSource,@SiteCode,@PrincipalCode,@ProdCode,@QuantityChange;

set @SAAID = SCOPE_IDENTITY();								


