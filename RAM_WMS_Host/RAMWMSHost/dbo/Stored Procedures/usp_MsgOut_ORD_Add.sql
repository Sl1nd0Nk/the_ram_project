﻿

create procedure [dbo].[usp_MsgOut_ORD_Add] 
									@ORDID int out, 
									@RecordSource varchar(20),
									
									@SiteCode varchar(4),
									@PrincipalCode varchar(4),
									@OrderNumber varchar(10),
									@LineCount int,
									@DateCreated datetime,
									@CompanyName varchar(10),
									@HeaderComment varchar(200),
									@CustOrderNumber varchar(20),
									@CustAcc varchar(8),
									@InvName varchar(32),
									@InvAdd1 varchar(32),
									@InvAdd2 varchar(32),
									@InvAdd3 varchar(32),
									@InvAdd4 varchar(32),
									@InvAdd5 varchar(32),
									@InvAdd6 varchar(32),
									@GSMTest bit,
									@VATNumber varchar(32),
									@PrintPrice bit,
									@Priority char(1),
									@VAP bit,
									@SalesPerson varchar(6),
									@SalesCategory varchar(6),
									@Processor varchar(10),
									@DeliveryAdd1 varchar(32),
									@DeliveryAdd2 varchar(32),
									@DeliveryAdd3 varchar(32),
									@DeliveryAdd4 varchar(32),
									@DeliveryAdd5 varchar(32),
									@DeliveryAdd6 varchar(32),
									@WMSPostCode varchar(8),
									@OrderDiscount float,
									@OrderVAT float as 
									
/**
RecordState
LOADING -> READY -> PROCESSING - > PROCESSED
CANCELLED / LOADERROR / PROCESSERROR
**/
									
insert into dbo.MsgOut_ORD (RecordState, RecordSource, SiteCode, PrincipalCode, OrderNumber, LineCount, DateCreated, CompanyName, HeaderComment, CustOrderNumber, CustAcc, InvName, InvAdd1, InvAdd2, InvAdd3, InvAdd4, InvAdd5, InvAdd6, GSMTest, VATNumber, PrintPrice, Priority, VAP, SalesPerson, SalesCategory, Processor, DeliveryAdd1, DeliveryAdd2, DeliveryAdd3, DeliveryAdd4, DeliveryAdd5, DeliveryAdd6, WMSPostCode, OrderDiscount, OrderVAT)
select					  'LOADING',    @RecordSource,@SiteCode,@PrincipalCode,@OrderNumber,@LineCount,@DateCreated,@CompanyName,@HeaderComment,@CustOrderNumber,@CustAcc,@InvName,@InvAdd1,@InvAdd2,@InvAdd3,@InvAdd4,@InvAdd5,@InvAdd6,@GSMTest,@VATNumber,@PrintPrice,@Priority,@VAP,@SalesPerson,@SalesCategory,@Processor,@DeliveryAdd1,@DeliveryAdd2,@DeliveryAdd3,@DeliveryAdd4,@DeliveryAdd5,@DeliveryAdd6,@WMSPostCode,@OrderDiscount,@OrderVAT;

set @ORDID = SCOPE_IDENTITY();								

 

