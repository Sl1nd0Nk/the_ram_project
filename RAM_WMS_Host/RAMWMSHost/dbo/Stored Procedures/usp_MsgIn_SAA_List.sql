﻿
create procedure [dbo].[usp_MsgIn_SAA_List] @RecordState varchar(20), @PageSize int, @PageNo int  as 

/**
RecordState
LOADING -> READY -> PROCESSING - > PROCESSED
CANCELLED / LOADERROR / PROCESSERROR
**/

--if (@RecordState = '') set @RecordState = 'ALL';

declare @TotalCount int, @MaxPageNo int;

if (@PageSize < 1) set @PageSize = 1;
if (@PageNo < 1) set @PageNo = 1; 

 
if @RecordState = 'ALL' 
	select @TotalCount = COUNT(SAAID) from dbo.MsgIn_SAA with (nolock);
else
	select @TotalCount = COUNT(SAAID) from dbo.MsgIn_SAA with (nolock) where RecordState = @RecordState;


set @MaxPageNo = ceiling(convert(float, @TotalCount) / convert(float, @PageSize));
if (@PageNo > @MaxPageNo) set @PageNo = @MaxPageNo;

if @RecordState = 'ALL' 
	select *, @PageSize as PageSize, @PageNo as PageNo, @TotalCount as TotalCount
	from ( select row_number() over (order by SAAID desc) as RowNumber,
			SAAID, RecordDT, RecordState, RecordSource, ProcAttempts, ProcStartDT, ProcEndDT, ProcErrors, 
			SiteCode, PrincipalCode, ProdCode, QuantityChange, rowguid	
			from MsgIn_SAA with (nolock) 
		) as A
	where RowNumber between (((@PageNo - 1) * @PageSize) + 1) and (@PageNo * @PageSize)
	order by RowNumber
else 
	select *, @PageSize as PageSize, @PageNo as PageNo, @TotalCount as TotalCount
	from ( select row_number() over (order by SAAID desc) as RowNumber,
				SAAID, RecordDT, RecordState, RecordSource, ProcAttempts, ProcStartDT, ProcEndDT, ProcErrors, 
				SiteCode, PrincipalCode, ProdCode, QuantityChange, rowguid	
			from MsgIn_SAA with (nolock)
			where RecordState = @RecordState 
		) as A
	where RowNumber between (((@PageNo - 1) * @PageSize) + 1) and (@PageNo * @PageSize)
	order by RowNumber



