﻿
 
 
CREATE procedure [dbo].[usp_MsgIn_ARR_Add] 
									@ARRID int out, 
									@RecordSource varchar(20),
									
									@SiteCode varchar(4),
									@PrincipalCode varchar(4),
									@Resend bit,
									@ProdCode varchar(20),
									@LineNumber varchar(4),
									@ReceiptType char(1),
									@MoveRef varchar(10),
									@PORef varchar(10),
									@ReceiptDT datetime,
									@StartEndStatus char(1),
									@RejectCount int,
									@ReasonCode varchar(4),
									@AcceptCount int as 
									
/**\
Edit
RecordState
LOADING -> READY -> PROCESSING - > PROCESSED
CANCELLED / LOADERROR / PROCESSERROR
**/
									
insert into dbo.MsgIn_ARR (RecordState, RecordSource, SiteCode, PrincipalCode, Resend, ProdCode, LineNumber, ReceiptType, MoveRef, PORef, ReceiptDT, StartEndStatus, RejectCount, ReasonCode, AcceptCount)
select					  'LOADING',   @RecordSource,@SiteCode,@PrincipalCode,@Resend,@ProdCode,@LineNumber,@ReceiptType,@MoveRef,@PORef,@ReceiptDT,@StartEndStatus,@RejectCount,@ReasonCode,@AcceptCount;
 
set @ARRID = SCOPE_IDENTITY();								
