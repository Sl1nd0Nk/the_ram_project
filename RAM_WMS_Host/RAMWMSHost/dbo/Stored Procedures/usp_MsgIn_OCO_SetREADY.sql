﻿create procedure [dbo].[usp_MsgIn_OCO_SetREADY] @OCOID int as 
								
/**
RecordState
LOADING -> READY -> PROCESSING - > PROCESSED
CANCELLED / LOADERROR / PROCESSERROR
**/
									
update dbo.MsgIn_OCO 
set RecordState = 'READY'
where OCOID = @OCOID;						
