﻿

create procedure [dbo].[usp_MsgIn_SLA_SetREADY] @SLAID int as 
								
/**
RecordState
LOADING -> READY -> PROCESSING - > PROCESSED
CANCELLED / LOADERROR / PROCESSERROR
**/
									
update dbo.MsgIn_SLA 
set RecordState = 'READY'
where SLAID = @SLAID;						

