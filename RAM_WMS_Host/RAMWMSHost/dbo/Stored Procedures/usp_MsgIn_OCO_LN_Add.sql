﻿create procedure [dbo].[usp_MsgIn_OCO_LN_Add] 
									@OCOID int, 
									@LineNumber int,
									@ProdCode varchar(20),
									@QuantityPicked int,
									@SerialNumber varchar(20),
									@UnitCost float,
									@Vat float,
									@Discount float as 
									
insert into dbo.MsgIn_OCO_LN (OCOID, LineNumber, ProdCode, QuantityPicked, SerialNumber, UnitCost, Vat, Discount)
select					     @OCOID,@LineNumber,@ProdCode,@QuantityPicked,@SerialNumber,@UnitCost,@Vat,@Discount;
 
