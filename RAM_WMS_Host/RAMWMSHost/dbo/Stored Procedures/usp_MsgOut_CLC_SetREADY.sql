﻿


create procedure [dbo].[usp_MsgOut_CLC_SetREADY] @CLCID int as 
								
/**
RecordState
LOADING -> READY -> PROCESSING - > PROCESSED
CANCELLED / LOADERROR / PROCESSERROR
**/
									
update dbo.MsgOut_CLC 
set RecordState = 'READY'
where CLCID = @CLCID;						


