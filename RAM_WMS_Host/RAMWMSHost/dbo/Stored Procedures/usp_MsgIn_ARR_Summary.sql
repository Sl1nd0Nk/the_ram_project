﻿CREATE procedure [dbo].[usp_MsgIn_ARR_Summary]  as 


select A.RecordState, A.SortOrder, A.IsError,
	COUNT(distinct B.ARRID) as RecordCount,
	MAX(RecordDT) as LastRecordDT
from Msg_RecordState as A
left outer join dbo.MsgIn_ARR as B on B.RecordState = A.RecordState
group by A.RecordState, A.SortOrder, A.IsError
order by A.SortOrder


