﻿create procedure [dbo].[usp_MsgIn_OCO_Add] 
									@OCOID int out, 
									@RecordSource varchar(20),
									
									@SiteCode varchar(4),
									@PrincipalCode varchar(4),
									@OrderNumber varchar(10),
									@Priority char(1),
									@UserID varchar(10),
									@Weight float,
									@OrderCost float,
									@OrderDiscount float,
									@OrderVAT float as 
									
/**
RecordState
LOADING -> READY -> PROCESSING - > PROCESSED
CANCELLED / LOADERROR / PROCESSERROR
**/
									
insert into dbo.MsgIn_OCO (RecordState, RecordSource, SiteCode, PrincipalCode, OrderNumber, Priority, UserID, Weight, OrderCost, OrderDiscount, OrderVAT)
select					  'LOADING',   @RecordSource,@SiteCode,@PrincipalCode,@OrderNumber,@Priority,@UserID,@Weight,@OrderCost,@OrderDiscount,@OrderVAT;

set @OCOID = SCOPE_IDENTITY();								

 

 
