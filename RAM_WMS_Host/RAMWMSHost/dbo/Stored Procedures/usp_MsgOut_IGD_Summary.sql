﻿create procedure [dbo].[usp_MsgOut_IGD_Summary]  as 


select A.RecordState, A.SortOrder, A.IsError,
	COUNT(distinct B.IGDID) as RecordCount,
	MAX(RecordDT) as LastRecordDT
from Msg_RecordState as A
left outer join dbo.MsgOut_IGD as B on B.RecordState = A.RecordState
group by A.RecordState, A.SortOrder, A.IsError
order by A.SortOrder



