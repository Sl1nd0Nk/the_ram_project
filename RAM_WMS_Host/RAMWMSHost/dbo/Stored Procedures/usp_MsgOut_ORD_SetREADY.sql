﻿



create procedure [dbo].[usp_MsgOut_ORD_SetREADY] @ORDID int as 
								
/**
RecordState
LOADING -> READY -> PROCESSING - > PROCESSED
CANCELLED / LOADERROR / PROCESSERROR
**/
									
update dbo.MsgOut_ORD 
set RecordState = 'READY'
where ORDID = @ORDID;						



