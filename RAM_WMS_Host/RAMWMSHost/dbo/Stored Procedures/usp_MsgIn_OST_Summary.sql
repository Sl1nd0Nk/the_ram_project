﻿create procedure [dbo].[usp_MsgIn_OST_Summary]  as 


select A.RecordState, A.SortOrder, A.IsError,
	COUNT(distinct B.OSTID) as RecordCount,
	MAX(RecordDT) as LastRecordDT
from Msg_RecordState as A
left outer join dbo.MsgIn_OST as B on B.RecordState = A.RecordState
group by A.RecordState, A.SortOrder, A.IsError
order by A.SortOrder


