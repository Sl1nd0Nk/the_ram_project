﻿

create procedure [dbo].[usp_MsgOut_ORD_Summary]  as 


select A.RecordState, A.SortOrder, A.IsError,
	COUNT(distinct B.ORDID) as RecordCount,
	MAX(RecordDT) as LastRecordDT
from Msg_RecordState as A
left outer join dbo.MsgOut_ORD as B on B.RecordState = A.RecordState
group by A.RecordState, A.SortOrder, A.IsError
order by A.SortOrder




