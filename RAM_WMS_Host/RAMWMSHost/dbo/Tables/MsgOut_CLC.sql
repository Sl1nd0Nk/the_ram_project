﻿CREATE TABLE [dbo].[MsgOut_CLC] (
    [CLCID]           INT              IDENTITY (1, 1) NOT NULL,
    [RecordDT]        DATETIME         CONSTRAINT [DF__MsgOut_CL__Recor__49C3F6B7] DEFAULT (getdate()) NOT NULL,
    [RecordState]     VARCHAR (20)     CONSTRAINT [DF__MsgOut_CL__Recor__4AB81AF0] DEFAULT ('LOADING') NOT NULL,
    [RecordSource]    VARCHAR (20)     CONSTRAINT [DF_MsgOut_CLC_RecordSource] DEFAULT ('') NOT NULL,
    [ProcAttempts]    INT              CONSTRAINT [DF__MsgOut_CL__ProcA__4BAC3F29] DEFAULT ((0)) NOT NULL,
    [ProcStartDT]     DATETIME         NULL,
    [ProcEndDT]       DATETIME         NULL,
    [ProcErrors]      VARCHAR (50)     NULL,
    [SiteCode]        VARCHAR (4)      NULL,
    [PrincipalCode]   VARCHAR (4)      NULL,
    [MoveRef]         VARCHAR (10)     NULL,
    [OrderLineNumber] VARCHAR (4)      NULL,
    [ProdCode]        VARCHAR (20)     NULL,
    [Quantity]        INT              NULL,
    [ReceiptType]     CHAR (1)         NULL,
    [Discrepancy]     INT              NULL,
    [SerialCount]     INT              NULL,
    [rowguid]         UNIQUEIDENTIFIER CONSTRAINT [DF__MsgOut_CL__rowgu__7F2BE32F] DEFAULT (newid()) ROWGUIDCOL NOT NULL,
    CONSTRAINT [PK_MsgOut_CLC] PRIMARY KEY CLUSTERED ([CLCID] ASC)
);

