﻿CREATE TABLE [dbo].[MsgOut_CLC_SN] (
    [CLCID]        INT              NOT NULL,
    [SerialNumber] VARCHAR (20)     NOT NULL,
    [rowguid]      UNIQUEIDENTIFIER DEFAULT (newid()) ROWGUIDCOL NOT NULL,
    CONSTRAINT [PK_MsgOut_CLC_SN] PRIMARY KEY CLUSTERED ([CLCID] ASC, [SerialNumber] ASC)
);

