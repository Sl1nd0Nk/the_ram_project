﻿CREATE TABLE [dbo].[Msg_RecordState] (
    [RecordState] VARCHAR (20)     NOT NULL,
    [SortOrder]   INT              NOT NULL,
    [IsError]     BIT              NOT NULL,
    [rowguid]     UNIQUEIDENTIFIER DEFAULT (newid()) ROWGUIDCOL NOT NULL,
    CONSTRAINT [PK_Msg_RecordState] PRIMARY KEY CLUSTERED ([RecordState] ASC)
);

