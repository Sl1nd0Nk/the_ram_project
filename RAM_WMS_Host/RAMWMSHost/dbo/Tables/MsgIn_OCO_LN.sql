﻿CREATE TABLE [dbo].[MsgIn_OCO_LN] (
    [OCOID]          INT              NOT NULL,
    [LineNumber]     INT              NOT NULL,
    [ProdCode]       VARCHAR (20)     NULL,
    [QuantityPicked] INT              NULL,
    [SerialNumber]   VARCHAR (20)     NULL,
    [UnitCost]       FLOAT (53)       NULL,
    [Vat]            FLOAT (53)       NULL,
    [Discount]       FLOAT (53)       NULL,
    [rowguid]        UNIQUEIDENTIFIER DEFAULT (newid()) ROWGUIDCOL NOT NULL,
    CONSTRAINT [PK_SerialNumber] PRIMARY KEY CLUSTERED ([OCOID] ASC, [LineNumber] ASC)
);

