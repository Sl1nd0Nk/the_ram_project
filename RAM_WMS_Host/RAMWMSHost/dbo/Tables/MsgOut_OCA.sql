﻿CREATE TABLE [dbo].[MsgOut_OCA] (
    [OCAID]         INT              IDENTITY (1, 1) NOT NULL,
    [RecordDT]      DATETIME         CONSTRAINT [DF__MsgOut_OC__Recor__5070F446] DEFAULT (getdate()) NOT NULL,
    [RecordState]   VARCHAR (20)     CONSTRAINT [DF__MsgOut_OC__Recor__5165187F] DEFAULT ('LOADING') NOT NULL,
    [RecordSource]  VARCHAR (20)     CONSTRAINT [DF_MsgOut_OCA_RecordSource] DEFAULT ('') NOT NULL,
    [ProcAttempts]  INT              CONSTRAINT [DF__MsgOut_OC__ProcA__52593CB8] DEFAULT ((0)) NOT NULL,
    [ProcStartDT]   DATETIME         NULL,
    [ProcEndDT]     DATETIME         NULL,
    [ProcErrors]    VARCHAR (50)     NULL,
    [SiteCode]      VARCHAR (4)      NULL,
    [PrincipalCode] VARCHAR (4)      NULL,
    [OrderNumber]   VARCHAR (10)     NULL,
    [rowguid]       UNIQUEIDENTIFIER CONSTRAINT [DF__MsgOut_OC__rowgu__02084FDA] DEFAULT (newid()) ROWGUIDCOL NOT NULL,
    CONSTRAINT [PK_MsgOut_OCA] PRIMARY KEY CLUSTERED ([OCAID] ASC)
);

