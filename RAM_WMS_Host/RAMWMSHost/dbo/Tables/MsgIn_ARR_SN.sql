﻿CREATE TABLE [dbo].[MsgIn_ARR_SN] (
    [ARRID]        INT              NOT NULL,
    [SerialNumber] VARCHAR (20)     NOT NULL,
    [rowguid]      UNIQUEIDENTIFIER DEFAULT (newid()) ROWGUIDCOL NOT NULL,
    CONSTRAINT [PK_MsgIn_ARR_SN] PRIMARY KEY CLUSTERED ([ARRID] ASC, [SerialNumber] ASC)
);

