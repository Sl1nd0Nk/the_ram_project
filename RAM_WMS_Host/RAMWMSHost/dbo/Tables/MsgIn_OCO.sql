﻿CREATE TABLE [dbo].[MsgIn_OCO] (
    [OCOID]         INT              IDENTITY (1, 1) NOT NULL,
    [RecordDT]      DATETIME         CONSTRAINT [DF__MsgIn_OCO__Recor__6754599E] DEFAULT (getdate()) NOT NULL,
    [RecordState]   VARCHAR (20)     CONSTRAINT [DF__MsgIn_OCO__Recor__68487DD7] DEFAULT ('LOADING') NOT NULL,
    [RecordSource]  VARCHAR (20)     CONSTRAINT [DF_MsgIn_OCO_RecordSource] DEFAULT ('') NOT NULL,
    [ProcAttempts]  INT              CONSTRAINT [DF__MsgIn_OCO__ProcA__693CA210] DEFAULT ((0)) NOT NULL,
    [ProcStartDT]   DATETIME         NULL,
    [ProcEndDT]     DATETIME         NULL,
    [ProcErrors]    VARCHAR (50)     NULL,
    [SiteCode]      VARCHAR (4)      NULL,
    [PrincipalCode] VARCHAR (4)      NULL,
    [OrderNumber]   VARCHAR (10)     NULL,
    [Priority]      CHAR (1)         NULL,
    [UserID]        VARCHAR (10)     NULL,
    [Weight]        FLOAT (53)       NULL,
    [OrderCost]     FLOAT (53)       NULL,
    [OrderDiscount] FLOAT (53)       NULL,
    [OrderVAT]      FLOAT (53)       NULL,
    [rowguid]       UNIQUEIDENTIFIER CONSTRAINT [DF__MsgIn_OCO__rowgu__7A672E12] DEFAULT (newid()) ROWGUIDCOL NOT NULL,
    CONSTRAINT [PK_MsgIn_OCO] PRIMARY KEY CLUSTERED ([OCOID] ASC)
);

