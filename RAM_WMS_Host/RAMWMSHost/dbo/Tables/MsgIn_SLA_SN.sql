﻿CREATE TABLE [dbo].[MsgIn_SLA_SN] (
    [SLAID]        INT              NOT NULL,
    [SerialNumber] VARCHAR (20)     NOT NULL,
    [rowguid]      UNIQUEIDENTIFIER DEFAULT (newid()) ROWGUIDCOL NOT NULL,
    CONSTRAINT [PK_MsgIn_SLA_SN] PRIMARY KEY CLUSTERED ([SLAID] ASC, [SerialNumber] ASC)
);

