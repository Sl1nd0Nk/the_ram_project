﻿CREATE TABLE [dbo].[MsgOut_ORD_LN] (
    [ORDID]      INT              NOT NULL,
    [LineNumber] INT              NOT NULL,
    [LineType]   CHAR (1)         NULL,
    [ProdCode]   VARCHAR (20)     NULL,
    [LineText]   VARCHAR (40)     NULL,
    [Quantity]   INT              NULL,
    [UnitCost]   FLOAT (53)       NULL,
    [Vat]        FLOAT (53)       NULL,
    [Discount]   FLOAT (53)       NULL,
    [rowguid]    UNIQUEIDENTIFIER DEFAULT (newid()) ROWGUIDCOL NOT NULL,
    CONSTRAINT [PK_MsgOut_ORD_LN] PRIMARY KEY CLUSTERED ([ORDID] ASC, [LineNumber] ASC)
);

