﻿CREATE TABLE [dbo].[MsgIn_SAA] (
    [SAAID]          INT              IDENTITY (1, 1) NOT NULL,
    [RecordDT]       DATETIME         CONSTRAINT [DF__MsgIn_SAA__Recor__74AE54BC] DEFAULT (getdate()) NOT NULL,
    [RecordState]    VARCHAR (20)     CONSTRAINT [DF__MsgIn_SAA__Recor__75A278F5] DEFAULT ('LOADING') NOT NULL,
    [RecordSource]   VARCHAR (20)     CONSTRAINT [DF_MsgIn_SAA_RecordSource] DEFAULT ('') NOT NULL,
    [ProcAttempts]   INT              CONSTRAINT [DF__MsgIn_SAA__ProcA__76969D2E] DEFAULT ((0)) NOT NULL,
    [ProcStartDT]    DATETIME         NULL,
    [ProcEndDT]      DATETIME         NULL,
    [ProcErrors]     VARCHAR (50)     NULL,
    [SiteCode]       VARCHAR (4)      NULL,
    [PrincipalCode]  VARCHAR (4)      NULL,
    [ProdCode]       VARCHAR (20)     NULL,
    [QuantityChange] VARCHAR (10)     NULL,
    [rowguid]        UNIQUEIDENTIFIER CONSTRAINT [DF__MsgIn_SAA__rowgu__797309D9] DEFAULT (newid()) ROWGUIDCOL NOT NULL,
    CONSTRAINT [PK_MsgIn_SAA] PRIMARY KEY CLUSTERED ([SAAID] ASC)
);

