﻿CREATE TABLE [dbo].[MsgIn_SLA] (
    [SLAID]             INT              IDENTITY (1, 1) NOT NULL,
    [RecordDT]          DATETIME         CONSTRAINT [DF__MsgIn_SLA__Recor__6E01572D] DEFAULT (getdate()) NOT NULL,
    [RecordState]       VARCHAR (20)     CONSTRAINT [DF__MsgIn_SLA__Recor__6EF57B66] DEFAULT ('LOADING') NOT NULL,
    [RecordSource]      VARCHAR (20)     CONSTRAINT [DF_MsgIn_SLA_RecordSource] DEFAULT ('') NOT NULL,
    [ProcAttempts]      INT              CONSTRAINT [DF__MsgIn_SLA__ProcA__6FE99F9F] DEFAULT ((0)) NOT NULL,
    [ProcStartDT]       DATETIME         NULL,
    [ProcEndDT]         DATETIME         NULL,
    [ProcErrors]        VARCHAR (50)     NULL,
    [SiteCode]          VARCHAR (4)      NULL,
    [PrincipalCode]     VARCHAR (4)      NULL,
    [ProdCode]          VARCHAR (20)     NULL,
    [QuantityChange]    INT              NULL,
    [ReasonCode]        CHAR (2)         NULL,
    [AvailabilityState] CHAR (1)         NULL,
    [SerialCount]       INT              NULL,
    [rowguid]           UNIQUEIDENTIFIER CONSTRAINT [DF__MsgIn_SLA__rowgu__7D439ABD] DEFAULT (newid()) ROWGUIDCOL NOT NULL,
    CONSTRAINT [PK_MsgIn_SLA] PRIMARY KEY CLUSTERED ([SLAID] ASC)
);

