﻿CREATE TABLE [dbo].[MsgIn_OST] (
    [OSTID]         INT              IDENTITY (1, 1) NOT NULL,
    [RecordDT]      DATETIME         CONSTRAINT [DF__MsgIn_OST__Recor__628FA481] DEFAULT (getdate()) NOT NULL,
    [RecordState]   VARCHAR (20)     CONSTRAINT [DF__MsgIn_OST__Recor__6383C8BA] DEFAULT ('LOADING') NOT NULL,
    [RecordSource]  VARCHAR (20)     CONSTRAINT [DF_MsgIn_OST_RecordSource] DEFAULT ('') NOT NULL,
    [ProcAttempts]  INT              CONSTRAINT [DF__MsgIn_OST__ProcA__6477ECF3] DEFAULT ((0)) NOT NULL,
    [ProcStartDT]   DATETIME         NULL,
    [ProcEndDT]     DATETIME         NULL,
    [ProcErrors]    VARCHAR (50)     NULL,
    [SiteCode]      VARCHAR (4)      NULL,
    [PrincipalCode] VARCHAR (4)      NULL,
    [OrderNumber]   VARCHAR (10)     NULL,
    [rowguid]       UNIQUEIDENTIFIER CONSTRAINT [DF__MsgIn_OST__rowgu__7C4F7684] DEFAULT (newid()) ROWGUIDCOL NOT NULL,
    CONSTRAINT [PK_MsgIn_OST] PRIMARY KEY CLUSTERED ([OSTID] ASC)
);

