﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MsgOut_CLC_View.aspx.cs" Inherits="RAMWMSHost_Web.MsgQueue.MsgOut.MsgOut_CLC_View" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>CLC Details</title>
    <link href="../../Style1.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <div>

            <% System.Data.DataRow hdr = mCLC.Tables[0].Rows[0]; %>

            <table>
                <tr>
                    <td colspan="3">
                        <h3>CLC #<%=hdr["CLCID"] %></h3>
                    </td>
                </tr>
                <tr>
                    <td style="vertical-align: top">
                        <table class="list">
                            <thead>
                                <tr>
                                    <td colspan="2">Control:</td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="key">CLCID</td>
                                    <td><%=hdr["CLCID"] %></td>
                                </tr>
                                <tr>
                                    <td class="key">RecordDT</td>
                                    <td><%=Convert.ToDateTime( hdr["RecordDT"]).ToString("yyyy-MM-dd HH:mm:ss") %></td>
                                </tr>
                                <tr>
                                    <td class="key">RecordState</td>
                                    <td><%=hdr["RecordState"] %></td>
                                </tr>
                                <tr>
                                    <td class="key">ProcAttempts</td>
                                    <td><%=hdr["ProcAttempts"] %></td>
                                </tr>
                                <tr>
                                    <td class="key">ProcStartDT</td>
                                    <td><%=hdr["ProcStartDT"] %></td>
                                </tr>
                                <tr>
                                    <td class="key">ProcEndDT</td>
                                    <td><%=hdr["ProcEndDT"] %></td>
                                </tr>
                                <tr>
                                    <td class="key">ProcErrors</td>
                                    <td><%=hdr["ProcErrors"] %></td>
                                </tr>
                            </tbody>
                        </table>

                    </td>
                    <td style="vertical-align: top">
                        <table class="list">
                            <thead>
                                <tr>
                                    <td colspan="2">CLC:</td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="key">SiteCode</td>
                                    <td><%=hdr["SiteCode"] %></td>
                                </tr>
                                <tr>
                                    <td class="key">PrincipalCode</td>
                                    <td><%=hdr["PrincipalCode"] %></td>
                                </tr> 

                                <tr>
                                    <td class="key">MoveRef</td>
                                    <td><%=hdr["MoveRef"] %></td>
                                </tr>                                
                                <tr>
                                    <td class="key">OrderLineNumber</td>
                                    <td><%=hdr["OrderLineNumber"] %></td>
                                </tr>
                                <tr>
                                    <td class="key">ProdCode</td>
                                    <td><%=hdr["ProdCode"] %></td>
                                </tr>
                                <tr>
                                    <td class="key">Quantity</td>
                                    <td><%=hdr["Quantity"] %></td>
                                </tr>
                                <tr>
                                    <td class="key">ReceiptType</td>
                                    <td><%=hdr["ReceiptType"] %></td>
                                </tr>
                                <tr>
                                    <td class="key">Discrepancy</td>
                                    <td><%=hdr["Discrepancy"] %></td>
                                </tr>
                                <tr>
                                    <td class="key">SerialCount</td>
                                    <td><%=hdr["SerialCount"] %></td>
                                </tr>
                   
                            </tbody>
                        </table>
                    </td>
                    <td style="vertical-align: top">
                        <table class="list">
                            <thead>
                                <tr>
                                    <td colspan="2">SerialNumbers: <%=mCLC.Tables[1].Rows.Count%></td>
                                </tr>
                            </thead>
                            <tbody>

                                <%foreach (System.Data.DataRow dtl in mCLC.Tables[1].Rows) { %>
                                <tr>
                                    <td class="key">SerialNumber</td>
                                    <td><%=dtl["SerialNumber"] %></td>
                                </tr>
                                <%} %>
                            </tbody>
                        </table>

                    </td>
                </tr>
            </table>

        </div>
    </form>
</body>
</html>
