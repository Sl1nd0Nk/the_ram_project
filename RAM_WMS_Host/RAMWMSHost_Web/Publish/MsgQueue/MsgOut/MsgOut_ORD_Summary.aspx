﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MsgOut_ORD_Summary.aspx.cs" Inherits="RAMWMSHost_Web.MsgQueue.MsgOut.MsgOut_ORD_Summary" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>ORD Summary</title>
    <link href="../../Style1.css" rel="stylesheet" />
    <script type="text/javascript">



        function body_onload() {
            //LoadList('');
        }

        function LoadList(RecordState) {
            document.getElementById('frameList').src = 'MsgOut_ORD_List.aspx?RecordState=' + RecordState;
        }



    </script>
</head>
<body onload="body_onload();">
    <form id="form1" runat="server">
        <div style="  position:fixed; top: 30px; bottom: 30px; left: 30px; right: 30px">


            <table border="0" style="padding: 0px; margin: 0px;  position:absolute; top: 30px; bottom: 30px; left: 0px; width:100%">
                <tr>
                    <td  >
                        <h2 >RAM WMS host</h2>
                        <br />
                        ORD – Order Definition (Host -> Matflo)
                    </td>
                    <td><a href="../Index.aspx">Index</a></td>
                </tr>
                <tr>
                    <td style="border-width: thin; border-color: #C0C0C0; vertical-align: top; width: 300px; border-right-style: solid;">

                        <table border="0">
                            <tr>
                                <td colspan="3">
                                    <h3>ORD Summary</h3>
                                </td>
                            </tr>
                            <tr>
                                <td style="vertical-align: top">
                                    <table class="list" style="width: 450px">
                                        <thead>
                                            <tr>
                                                <td>RecordState</td>
                                                <td>Count</td>
                                                <td>LastRecord</td>
                                                <td><input type="button" value="Refresh" onclick="window.location.reload();"  /></td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <%foreach (System.Data.DataRow dtl in mORD_Summary.Tables[0].Rows) {
                                                  string lastRecordDT = string.Empty;
                                                  try { lastRecordDT = Convert.ToDateTime(dtl["LastRecordDT"]).ToString("yyyy-MM-dd HH:mm:ss"); } catch { }
                                            %>
                                            <tr>
                                                <td><%=dtl["RecordState"] %></td>
                                                <td style="text-align: right;"><%=dtl["RecordCount"] %></td>
                                                <td><%=lastRecordDT %></td>
                                                <td><a target="_self" onclick="LoadList('<%=dtl["RecordState"] %>');" href="#">Show</a></td>
                                            </tr>
                                            <%} %>
                                        </tbody>
                                    </table>
                                </td>


                            </tr>
                        </table>

                    </td>
                    <td style="vertical-align: top;">

                        <iframe marginwidth="0" id="frameList" name="frameList" src="MsgOut_ORD_List.aspx?RecordState=" style="border: none; position: inherit; width: 100%; height: 600px;"></iframe>



                    </td>
                </tr>
            </table>



        </div>

        <%-- <div style="border: 1px solid #999999; padding: 2px; position: fixed; background-color: #DFDFDF; font-size: small; height: 22px; bottom: 0px; right: 0px; color: #666666; text-align: center; vertical-align: middle;"><%=System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") %></div>--%>
    </form>
</body>
</html>

