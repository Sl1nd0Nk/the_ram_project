﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MsgOut_IGD_List.aspx.cs" Inherits="RAMWMSHost_Web.MsgQueue.MsgOut.MsgOut_IGD_List" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>IGD List</title>
    <link href="../../Style1.css" rel="stylesheet" />
    <script type="text/javascript">
        function ShowIGD(IGDID) {
            window.open('MsgOut_IGD_View.aspx?IGDID=' + IGDID, '_blank', 'height=600,width=700');
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div   >
            <table border="0">
                <tr>
                    <td colspan="3">
                        <h3>IGD List: <%=mRecordState %></h3>
                    </td>


                
                </tr>
                <tr>
                    <td style="vertical-align: top">
                        <table class="list">
                            <thead>
                                <tr>
                                    <td>RecordDT</td>
                                    <td>SiteCode</td>
                                    <td>OrderNumber</td>
                                    <td></td>
                                </tr>
                            </thead>
                            <tbody>
                                <%foreach (System.Data.DataRow dtl in mIGD_List.Tables[0].Rows) { %>
                                <tr>
                                    <td><%=dtl["RecordDT"] %></td>
                                    <td><%=dtl["SiteCode"] %></td>
                                    <td><%=dtl["ProdCode"] %></td>
                                    <td><a target="_self" onclick="ShowIGD('<%=dtl["IGDID"] %>');" href="#">Show</a></td>
                                </tr>
                                <%} %>
                            </tbody>
                        </table>

                    </td>

                </tr>
            </table>
        </div>
        <%-- <div style="border: 1px solid #999999; padding: 2px; position: fixed; background-color: #DFDFDF; font-size: small; top: 0px; right: 0px; color: #666666; text-align: center; vertical-align: middle;"><%=System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") %></div>--%>
    </form>
</body>
</html>
