﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MsgIn_Parcel_View.aspx.cs" Inherits="RAMWMSHost_Web.MsgQueue.MsgIn.MsgIn_Parcel_View" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Parcel Details</title>
    <link href="../../Style1.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <div>

            <% System.Data.DataRow hdr = mParcel.Tables[0].Rows[0]; %>

            <table>
                <tr>
                    <td colspan="3">
                        <h3>Parcel #<%=hdr["ParcelID"] %></h3>
                    </td>
                </tr>
                <tr>
                    <td style="vertical-align: top">
                        <table class="list">
                            <thead>
                                <tr>
                                    <td colspan="2">Control:</td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="key">ParcelID</td>
                                    <td><%=hdr["ParcelID"] %></td>
                                </tr>
                                <tr>
                                    <td class="key">RecordDT</td>
                                    <td><%=Convert.ToDateTime( hdr["RecordDT"]).ToString("yyyy-MM-dd HH:mm:ss") %></td>
                                </tr>
                                <tr>
                                    <td class="key">RecordState</td>
                                    <td><%=hdr["RecordState"] %></td>
                                </tr>
                                <tr>
                                    <td class="key">ProcAttempts</td>
                                    <td><%=hdr["ProcAttempts"] %></td>
                                </tr>
                                <tr>
                                    <td class="key">ProcStartDT</td>
                                    <td><%=hdr["ProcStartDT"] %></td>
                                </tr>
                                <tr>
                                    <td class="key">ProcEndDT</td>
                                    <td><%=hdr["ProcEndDT"] %></td>
                                </tr>
                                <tr>
                                    <td class="key">ProcErrors</td>
                                    <td><%=hdr["ProcErrors"] %></td>
                                </tr>
                            </tbody>
                        </table>

                    </td>
                    <td style="vertical-align: top">
                        <table class="list">
                            <thead>
                                <tr>
                                    <td colspan="2">Parcel:</td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="key">SiteCode</td>
                                    <td><%=hdr["SiteCode"] %></td>
                                </tr>
                                <tr>
                                    <td class="key">PrincipalCode</td>
                                    <td><%=hdr["PrincipalCode"] %></td>
                                </tr>

            
                                
                                <tr><td class="key">OrderNumber</td><td><%=hdr["OrderNumber"] %></td></tr> 
                                <tr><td class="key">ParcelNo</td><td><%=hdr["ParcelNo"] %></td></tr> 
                                <tr><td class="key">Kilograms</td><td><%=hdr["Kilograms"] %></td></tr> 
                                <tr><td class="key">Length</td><td><%=hdr["Length"] %></td></tr> 
                                <tr><td class="key">Breadth</td><td><%=hdr["Breadth"] %></td></tr> 
                                <tr><td class="key">Height</td><td><%=hdr["Height"] %></td></tr> 
                                <tr><td class="key">InsuredValue</td><td><%=hdr["InsuredValue"] %></td></tr> 
                                <tr><td class="key">SPNumber</td><td><%=hdr["SPNumber"] %></td></tr> 
                                <tr><td class="key">ParcelReference</td><td><%=hdr["ParcelReference"] %></td></tr>  
                                <tr><td class="key">NoOfParcels</td><td><%=hdr["NoOfParcels"] %></td></tr>    
      
                                
                                 
                            </tbody>
                        </table>
                    </td>
        
                </tr>
            </table>

        </div>
    </form>
</body>
</html>
