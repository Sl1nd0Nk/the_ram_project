﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MsgIn_ARR_View.aspx.cs" Inherits="RAMWMSHost_Web.MsgQueue.MsgIn.MsgIn_ARR_View" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ARR Details</title>
    <link href="../../Style1.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <div>

            <% System.Data.DataRow hdr = mARR.Tables[0].Rows[0]; %>

            <table>
                <tr>
                    <td colspan="3">
                        <h3>ARR #<%=hdr["ARRID"] %></h3>
                    </td>
                </tr>
                <tr>
                    <td style="vertical-align: top">
                        <table class="list">
                            <thead>
                                <tr>
                                    <td colspan="2">Control:</td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="key">ARRID</td>
                                    <td><%=hdr["ARRID"] %></td>
                                </tr>
                                <tr>
                                    <td class="key">RecordDT</td>
                                    <td><%=Convert.ToDateTime( hdr["RecordDT"]).ToString("yyyy-MM-dd HH:mm:ss") %></td>
                                </tr>
                                <tr>
                                    <td class="key">RecordState</td>
                                    <td><%=hdr["RecordState"] %></td>
                                </tr>
                                <tr>
                                    <td class="key">ProcAttempts</td>
                                    <td><%=hdr["ProcAttempts"] %></td>
                                </tr>
                                <tr>
                                    <td class="key">ProcStartDT</td>
                                    <td><%=hdr["ProcStartDT"] %></td>
                                </tr>
                                <tr>
                                    <td class="key">ProcEndDT</td>
                                    <td><%=hdr["ProcEndDT"] %></td>
                                </tr>
                                <tr>
                                    <td class="key">ProcErrors</td>
                                    <td><%=hdr["ProcErrors"] %></td>
                                </tr>
                            </tbody>
                        </table>

                    </td>
                    <td style="vertical-align: top">
                        <table class="list">
                            <thead>
                                <tr>
                                    <td colspan="2">ARR:</td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="key">SiteCode</td>
                                    <td><%=hdr["SiteCode"] %></td>
                                </tr>
                                <tr>
                                    <td class="key">PrincipalCode</td>
                                    <td><%=hdr["PrincipalCode"] %></td>
                                </tr>
                                <tr>
                                    <td class="key">Resend</td>
                                    <td><%=hdr["Resend"] %></td>
                                </tr>
                                <tr>
                                    <td class="key">ProdCode</td>
                                    <td><%=hdr["ProdCode"] %></td>
                                </tr>
                                <tr>
                                    <td class="key">LineNumber</td>
                                    <td><%=hdr["LineNumber"] %></td>
                                </tr>
                                <tr>
                                    <td class="key">ReceiptType</td>
                                    <td><%=hdr["ReceiptType"] %></td>
                                </tr>
                                <tr>
                                    <td class="key">MoveRef</td>
                                    <td><%=hdr["MoveRef"] %></td>
                                </tr>
                                <tr>
                                    <td class="key">PORef</td>
                                    <td><%=hdr["PORef"] %></td>
                                </tr>
                                <tr>
                                    <td class="key">ReceiptDT</td>
                                    <td><%=hdr["ReceiptDT"] %></td>
                                </tr>
                                <tr>
                                    <td class="key">StartEndStatus</td>
                                    <td><%=hdr["StartEndStatus"] %></td>
                                </tr>
                                <tr>
                                    <td class="key">RejectCount</td>
                                    <td><%=hdr["RejectCount"] %></td>
                                </tr>
                                <tr>
                                    <td class="key">ReasonCode</td>
                                    <td><%=hdr["ReasonCode"] %></td>
                                </tr>
                                <tr>
                                    <td class="key">AcceptCount</td>
                                    <td><%=hdr["AcceptCount"] %></td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                    <td style="vertical-align: top">
                        <table class="list">
                            <thead>
                                <tr>
                                    <td colspan="2">SerialNumbers:</td>
                                </tr>
                            </thead>
                            <tbody>

                                <%foreach (System.Data.DataRow dtl in mARR.Tables[1].Rows) { %>
                                <tr>
                                    <td class="key">SerialNumber</td>
                                    <td><%=dtl["SerialNumber"] %></td>
                                </tr>
                                <%} %>
                            </tbody>
                        </table>

                    </td>
                </tr>
            </table>

        </div>
    </form>
</body>
</html>
