﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="RAMWMSHost_Web.MsgQueue.Index" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Message Queue</title>
    <link href="../Style1.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <h2>RAM WMS Host Message Queue</h2>
            <a href="../Default.aspx">Home</a>
            <br />

            <%--<table class="list">
                <thead>
                    <tr>
                        <td style="border-right-style: solid; border-right-width: 1px; border-right-color: #800000">
                            <h3>Messages In (Matflo -> Host)</h3>
                        </td>
                        <td>
                            <h3>Messages Out (Host -> Matflo)</h3>
                        </td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td style="border-right-style: solid; border-right-width: 1px; border-right-color: #800000">
                            <a href="MsgIn/MsgIn_ARR_Summary.aspx">Message In ARR</a>
                        </td>
                        <td>
                            <a href="MsgOut/MsgOut_CLC_Summary.aspx">Message Out CLC</a>
                        </td>
                    </tr>
                    <tr>
                        <td style="border-right-style: solid; border-right-width: 1px; border-right-color: #800000"><a href="MsgIn/MsgIn_OCO_Summary.aspx">Message In OCO</a></td>
                        <td><a href="MsgOut/MsgOut_IGD_Summary.aspx">Message Out IGD</a></td>
                    </tr>
                    <tr>
                        <td style="border-right-style: solid; border-right-width: 1px; border-right-color: #800000"><a href="MsgIn/MsgIn_OST_Summary.aspx">Message In OST</a></td>
                        <td><a href="MsgOut/MsgOut_OCA_Summary.aspx">Message Out OCA</a></td>
                    </tr>
                    <tr>
                        <td style="border-right-style: solid; border-right-width: 1px; border-right-color: #800000"><a href="MsgIn/MsgIn_SAA_Summary.aspx">Message In SAA</a></td>
                        <td><a href="MsgOut/MsgOut_ORD_Summary.aspx">Message Out ORD</a></td>
                    </tr>
                    <tr>
                        <td style="border-right-style: solid; border-right-width: 1px; border-right-color: #800000"><a href="MsgIn/MsgIn_SLA_Summary.aspx">Message In SLA</a></td>
                        <td></td>
                    </tr>
                </tbody>
            </table>--%>

            <table class="list">

                <thead>
                    <tr>
                        <td>Inbound Messages</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><a href="MsgOut/MsgOut_IGD_Summary.aspx">IGD – Incoming Goods Details (Host -> Matflo)</a></td>
                    </tr>
                    <tr>
                        <td><a href="MsgIn/MsgIn_ARR_Summary.aspx">ARR – Accept / Reject Receipt of Stock (Matflo -> Host)</a></td>
                    </tr>
                    <tr>
                        <td><a href="MsgOut/MsgOut_CLC_Summary.aspx">CLC – Consignment Line Completion (Host -> Matflo)</a></td>
                    </tr>
                </tbody>
                <thead>
                    <tr>
                        <td>Outbound Messages</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><a href="MsgOut/MsgOut_ORD_Summary.aspx">ORD – Order Definition (Host -> Matflo)</a></td>
                    </tr>
                    <tr>
                        <td><a href="MsgIn/MsgIn_OST_Summary.aspx">OST – Order Started (Matflo -> Host)</a></td>
                    </tr>
                    <tr>
                        <td><a href="MsgOut/MsgOut_OCA_Summary.aspx">OCA – Order Cancelled (Host -> Matflo)</a></td>
                    </tr>
                    <tr>
                        <td><a href="MsgIn/MsgIn_OCO_Summary.aspx">OCO – Order Completions (Matflo -> Host)</a></td>
                    </tr>
                </tbody>

                <thead>
                    <tr>
                        <td>Inventory Messages</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><a href="MsgIn/MsgIn_SLA_Summary.aspx">SLA – Stock Level Adjustment (Matflo -> Host)</a></td>
                    </tr>
                    <tr>
                        <td><a href="MsgIn/MsgIn_SAA_Summary.aspx">SAA – Stock Availability Adjustment (Matflo -> Host)</a></td>
                    </tr>
                </tbody>

                 <thead>
                    <tr>
                        <td>Other Messages</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><a href="MsgIn/MsgIn_Parcel_Summary.aspx">Parcel – Parcel weights and dims (Matflo -> Host)</a></td>
                    </tr> 
                </tbody> 
            </table>




            <%--            <table class="list">
                <thead><tr><td style="border-right-style: solid; border-right-width: 1px; border-right-color: #800000"><h3>Messages In (Matflo -> Host)</h3></td><td><h3>Messages Out (Host -> Matflo)</h3></td></tr></thead>
                <tbody>
                    <tr>
                        <td style="border-right-style: solid; border-right-width: 1px; border-right-color: #800000"><a href="MsgIn/MsgIn_ARR_Summary.aspx">Message In ARR</a></td><td><a href="#" onclick="alert('To be implemented');" >Message Out CLC</a></td>
                    </tr>
                    <tr>
                        <td style="border-right-style: solid; border-right-width: 1px; border-right-color: #800000"><a href="MsgIn/MsgIn_OCO_Summary.aspx">Message In OCO</a></td><td><a href="#" onclick="alert('To be implemented');" >Message Out IGD</a></td>
                    </tr>
                    <tr>
                        <td style="border-right-style: solid; border-right-width: 1px; border-right-color: #800000"><a href="MsgIn/MsgIn_OST_Summary.aspx">Message In OST</a></td><td><a href="#" onclick="alert('To be implemented');" >Message Out OCA</a></td>
                    </tr>
                    <tr>
                        <td style="border-right-style: solid; border-right-width: 1px; border-right-color: #800000"><a href="MsgIn/MsgIn_SAA_Summary.aspx">Message In SAA</a></td><td><a href="#" onclick="alert('To be implemented');" >Message Out ORD</a></td>
                    </tr>
                    <tr>
                        <td style="border-right-style: solid; border-right-width: 1px; border-right-color: #800000"><a href="MsgIn/MsgIn_SLA_Summary.aspx">Message In SLA</a></td><td></td>
                    </tr>
                </tbody>
            </table>--%>
        </div>
    </form>
</body>
</html>
