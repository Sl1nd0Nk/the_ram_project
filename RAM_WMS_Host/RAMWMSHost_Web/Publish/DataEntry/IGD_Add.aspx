﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="IGD_Add.aspx.cs" Inherits="RAMWMSHost_Web.DataEntry.IGD_Add" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>IGD Add</title>
    <link href="../Style1.css" rel="stylesheet" />

    <script src="../Scripts/datetimepicker/datetimepicker_css.js"></script>
    <script src="../Scripts/Shared.js"></script>
    <script type="text/javascript">
        
        var mForm = new Object();
        mForm.txtShow_IGDID = document.getElementById('txtShow_IGDID');
        mForm.txtSiteCode = document.getElementById('txtSiteCode');
        mForm.txtPrincipalCode = document.getElementById('txtPrincipalCode');
        mForm.txtProdCode = document.getElementById('txtProdCode');
        mForm.txtEANCode = document.getElementById('txtEANCode');
        mForm.txtShortDesc = document.getElementById('txtShortDesc');
        mForm.txtLongDesc = document.getElementById('txtLongDesc');
        mForm.chkSerialised = document.getElementById('chkSerialised');
        mForm.txtAnalysisA = document.getElementById('txtAnalysisA');
        mForm.txtAnalysisB = document.getElementById('txtAnalysisB');
        mForm.txtOrderLineNo = document.getElementById('txtOrderLineNo');
        mForm.txtQuantity = document.getElementById('txtQuantity');
        mForm.selectReceiptType = document.getElementById('selectReceiptType');
        mForm.txtMoveRef = document.getElementById('txtMoveRef');
        mForm.txtPORef = document.getElementById('txtPORef');
        mForm.txtPODate = document.getElementById('txtPODate');
        mForm.txtStockDateTime = document.getElementById('txtStockDateTime');
        mForm.btnSave = document.getElementById('btnSave');
        mForm.divVeil = document.getElementById('divVeil');
 
        function bodyonload() {
            mForm.txtShow_IGDID = document.getElementById('txtShow_IGDID');
            mForm.txtSiteCode = document.getElementById('txtSiteCode');
            mForm.txtPrincipalCode = document.getElementById('txtPrincipalCode');
            mForm.txtProdCode = document.getElementById('txtProdCode');
            mForm.txtEANCode = document.getElementById('txtEANCode');
            mForm.txtShortDesc = document.getElementById('txtShortDesc');
            mForm.txtLongDesc = document.getElementById('txtLongDesc');
            mForm.chkSerialised = document.getElementById('chkSerialised');
            mForm.txtAnalysisA = document.getElementById('txtAnalysisA');
            mForm.txtAnalysisB = document.getElementById('txtAnalysisB');
            mForm.txtOrderLineNo = document.getElementById('txtOrderLineNo');
            mForm.txtQuantity = document.getElementById('txtQuantity');
            mForm.selectReceiptType = document.getElementById('selectReceiptType');
            mForm.txtMoveRef = document.getElementById('txtMoveRef');
            mForm.txtPORef = document.getElementById('txtPORef');
            mForm.txtPODate = document.getElementById('txtPODate');
            mForm.txtStockDateTime = document.getElementById('txtStockDateTime');
            mForm.btnSave = document.getElementById('btnSave');
            mForm.divVeil = document.getElementById('divVeil');

            if (mForm.txtShow_IGDID.value.length > 0) {
                window.open('../MsgQueue/MsgOut/MsgOut_IGD_View.aspx?IGDID=' + mForm.txtShow_IGDID.value, '_blank', 'height=600,width=700');
                mForm.txtShow_IGDID.value = '';
            }

            ClearForm();

            WireValidation();

            mForm.txtSiteCode.value = getCookie('IGD_ADD_txtSiteCode');
            mForm.txtPrincipalCode.value = getCookie('IGD_ADD_txtPrincipalCode');

            Validate();

            setTimeout(HideVeil, 250);
        }
        function HideVeil() {
            mForm.divVeil.style.display = 'none';
        }


        function WireValidation() {
            //wire validation
            var inputs = document.getElementsByTagName('input');
            var i;
            for (i = 0; i < inputs.length; ++i) {
                if (inputs[i].type == 'text') {
                    inputs[i].onkeyup = Validate;
                    inputs[i].onchange = Validate;
                    inputs[i].onclick = Validate;
                    inputs[i].onblur = Validate;
                }
                if (inputs[i].type == 'checkbox') {
                     inputs[i].onkeyup = Validate;
                     inputs[i].onchange = Validate; 
                     inputs[i].onblur = Validate;
                }
            }
            inputs = document.getElementsByTagName('select');
            for (i = 0; i < inputs.length; ++i) {
                inputs[i].onkeyup = Validate;
                inputs[i].onchange = Validate;
                inputs[i].onclick = Validate;
                inputs[i].onblur = Validate;
            }
        }

        function ClearForm() {
            var inputs = document.getElementsByTagName('input');
            var i;
            for (i = 0; i < inputs.length; ++i) {
                if (inputs[i].type == 'text') inputs[i].value = ''; 
            }
            inputs = document.getElementsByTagName('select');
            for (i = 0; i < inputs.length; ++i) {
                inputs[i].selectedIndex = 0;
            }
        }

        function Validate() {
            var valid = true;

            //mForm.txtSiteCode
            if (mForm.txtSiteCode.value.length < 2) {
                valid = false;
                mForm.txtSiteCode.className = 'InputInValid';
            } else {
                mForm.txtSiteCode.className = 'InputValid';
            }

            //mForm.txtPrincipalCode
            if (mForm.txtPrincipalCode.value.length < 2) {
                valid = false;
                mForm.txtPrincipalCode.className = 'InputInValid';
            } else {
                mForm.txtPrincipalCode.className = 'InputValid';
            }

            //mForm.txtProdCode
            mForm.txtProdCode.className = 'InputValid';
            //mForm.txtEANCode 
            mForm.txtEANCode.className = 'InputValid';
            //mForm.txtShortDesc
            mForm.txtShortDesc.className = 'InputValid';
            //mForm.txtLongDesc 
            mForm.txtLongDesc.className = 'InputValid';
            //mForm.chkSerialised
            mForm.chkSerialised.className = 'InputValid';            
            //mForm.txtAnalysisA 
            mForm.txtAnalysisA.className = 'InputValid';
            //mForm.txtAnalysisB 
            mForm.txtAnalysisB.className = 'InputValid';
            //mForm.txtOrderLineNo
            mForm.txtOrderLineNo.className = 'InputValid';

            //mForm.txtQuantity 
            if ((isNumber(mForm.txtQuantity.value) == false) || (mForm.txtQuantity.value < 0 | mForm.txtQuantity.value > 20000)) {
                valid = false;
                mForm.txtQuantity.className = 'InputInValid';
            } else {
                mForm.txtQuantity.className = 'InputValid';
            }

            //mForm.selectReceiptType 
            mForm.selectReceiptType.className = 'InputValid';
            //mForm.txtMoveRef  
            mForm.txtMoveRef.className = 'InputValid';
            //mForm.txtPORef 
            mForm.txtPORef.className = 'InputValid';

            //mForm.txtPODate  
            if (isDate(mForm.txtPODate.value) == false) {
                valid = false;
                mForm.txtPODate.className = 'InputInValid';
            } else {
                mForm.txtPODate.className = 'InputValid';
            }

            //mForm.txtStockDateTime
            if (isDateTime(mForm.txtStockDateTime.value) == false) {
                valid = false;
                mForm.txtStockDateTime.className = 'InputInValid';
            } else {
                mForm.txtStockDateTime.className = 'InputValid';
            }

             
            //mForm.btnSave
            mForm.btnSave.disabled = !valid;
            return valid;
        }

        function form1_onsubmit() {
            try {
                if (Validate() == false) {
                    return false;
                } else {
                    mForm.divVeil.style.display = 'block';

                    setCookie('IGD_ADD_txtSiteCode', mForm.txtSiteCode.value, 7);
                    setCookie('IGD_ADD_txtPrincipalCode', mForm.txtPrincipalCode.value, 7);
                    return true;
                }
            } catch (e) {
                alert(e);
                return false;
            }
        }

         


    </script>

</head>
<body onload="bodyonload();">
    <form id="form1" runat="server" onsubmit="return form1_onsubmit();">
        <input type="hidden" id="txtShow_IGDID" runat="server" />
        <div>
            <table>
                <tr>
                    <td>
                        <h2>IGD – Incoming Goods Details (Host -> Matflo)</h2>
                    </td>
                    <td>&nbsp;</td>
                    <td><a href="Index.aspx">Index</a></td>
                </tr>
            </table>




            <table class="list">
                <thead>
                    <tr>
                        <td colspan="3">IGD Add</td>
                    </tr>
                </thead>
                <tbody>

                    <tr>
                        <td>SiteCode</td>
                        <td style="width: 250px">
                            <input type="text" runat="server" id="txtSiteCode" maxlength="4" value="001" style="width: 100%" />
                        </td>

                    </tr>

                    <tr>
                        <td>PrincipalCode</td>
                        <td>
                            <input type="text" runat="server" id="txtPrincipalCode" maxlength="6" value="OG" style="width: 100%" /></td>


                    </tr>
                    <tr>
                        <td>ProdCode</td>
                        <td>
                            <input type="text" runat="server" id="txtProdCode" maxlength="20" style="width: 100%" /></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>EANCode</td>
                        <td>
                            <input type="text" runat="server" id="txtEANCode" maxlength="20" style="width: 100%" /></td>
                    </tr>
                    <tr>
                        <td>ShortDesc</td>
                        <td>
                            <input type="text" runat="server" id="txtShortDesc" maxlength="20" style="width: 100%" /></td>
                    </tr>
                    <tr>
                        <td>LongDesc</td>
                        <td>
                            <input type="text" runat="server" id="txtLongDesc" maxlength="40" style="width: 100%" /></td>
                    </tr>
                    <tr>
                        <td>Serialised</td>
                        <td>
                            <input   type="checkbox" runat="server" id="chkSerialised"     />
                           
                        </td>
                    </tr>
                    <tr>
                        <td>AnalysisA</td>
                        <td>
                            <input type="text" runat="server" id="txtAnalysisA" maxlength="10" style="width: 100%" /></td>
                    </tr>
                    <tr>
                        <td>AnalysisB</td>
                        <td>
                            <input type="text" runat="server" id="txtAnalysisB" maxlength="10" style="width: 100%" /></td>
                    </tr>
                    <tr>
                        <td>OrderLineNo</td>
                        <td>
                            <input type="text" runat="server" id="txtOrderLineNo" maxlength="4" style="width: 100%" /></td>
                    </tr>
                    <tr>
                        <td>Quantity</td>
                        <td>
                            <input type="text" runat="server" id="txtQuantity" maxlength="6" style="width: 100%" />
                        </td>
                    </tr>
                    <tr>
                        <td>ReceiptType</td>
                        <td>
                            <select id="selectReceiptType" runat="server" style="width: 100%">
                                <option value="0">New Stock</option>
                                <option value="1">Returned Stock</option>
                                <option value="2">Take On</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>MoveRef</td>
                        <td>
                            <input type="text" runat="server" id="txtMoveRef" maxlength="10" style="width: 100%" /></td>
                    </tr>
                    <tr>
                        <td>PORef</td>
                        <td>
                            <input type="text" runat="server" id="txtPORef" maxlength="10" style="width: 100%" /></td>
                    </tr>
                    <tr>
                        <td>PODate</td>
                        <td style="padding: 0px; margin: 0px">





                            <table style="border-style: none; padding: 0px; margin: 0px; width: 100%; border-collapse: collapse; border-spacing: 0px;">
                                <tr>
                                    <td>
                                        <input type="text" runat="server" id="txtPODate" maxlength="10" style="width: 100%; padding: 0px; margin: 0px" /></td>
                                    <td style="width: 20px; text-align: right; padding: 0px; margin: 0px">
                                        <img src="../Scripts/datetimepicker/images2/cal.gif" onclick="javascript:NewCssCal('txtPODate', 'yyyyMMdd', 'dropdown', false)" style="cursor: pointer" /></td>
                                </tr>
                            </table>


                        </td>
                    </tr>
                    <tr>
                        <td>StockDateTime</td>
                        <td style="padding: 0px; margin: 0px">
                            <table style="border-style: none; padding: 0px; margin: 0px; width: 100%; border-collapse: collapse; border-spacing: 0px;">
                                <tr>
                                    <td>
                                        <input type="text" runat="server" id="txtStockDateTime" style="width: 100%; padding: 0px; margin: 0px" maxlength="16" /></td>
                                    <td style="width: 20px; text-align: right; padding: 0px; margin: 0px">
                                        <img src="../Scripts/datetimepicker/images2/cal.gif" onclick="javascript:NewCssCal('txtStockDateTime', 'yyyyMMdd', 'dropdown', true, 24, false)" style="cursor: pointer" /></td>
                                </tr>
                            </table>




                        </td>
                    </tr>

                    <tr>
                        <td id="tdErrorMessage" runat="server" class="validation">.</td>
                        <td style="text-align: right">
                            <asp:Button ID="btnSave"  runat="server"  Text="Save" OnClick="btnSave_Click"    />

                        </td>
                    </tr>
                </tbody>
            </table>

        </div>
    </form>

            <div style="display: block; position: absolute; top: 5px; right: 5px; bottom: 5px; left: 5px; background-color: #FFFFCC; z-index: 10; opacity: .70; filter: alpha(opacity=70); -moz-opacity: 0.7;"
            id="divVeil">
            Busy...
        </div>

</body>
</html>
