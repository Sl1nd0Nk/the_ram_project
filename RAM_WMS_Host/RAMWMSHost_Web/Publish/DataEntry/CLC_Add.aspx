﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CLC_Add.aspx.cs" Inherits="RAMWMSHost_Web.DataEntry.CLC_Add" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>CLC Add</title>
    <link href="../Style1.css" rel="stylesheet" />

    <script src="../Scripts/datetimepicker/datetimepicker_css.js"></script>
    <script src="../Scripts/Shared.js"></script>
    <script type="text/javascript">

        if (!String.prototype.trim) {
            String.prototype.trim = function () { return this.replace(/^\s+|\s+$/g, ''); };
        }

        var JSON = JSON || {};
        // implement JSON.stringify serialization
        JSON.stringify = JSON.stringify || function (obj) {
            var t = typeof (obj);
            if (t != "object" || obj === null) {
                // simple data type
                if (t == "string") obj = '"' + obj + '"';
                return String(obj);
            }
            else {
                // recurse array or object
                var n, v, json = [], arr = (obj && obj.constructor == Array);
                for (n in obj) {
                    v = obj[n]; t = typeof (v);
                    if (t == "string") v = '"' + v + '"';
                    else if (t == "object" && v !== null) v = JSON.stringify(v);
                    json.push((arr ? "" : '"' + n + '":') + String(v));
                }
                return (arr ? "[" : "{") + String(json) + (arr ? "]" : "}");
            }
        };
        // implement JSON.parse de-serialization
        JSON.parse = JSON.parse || function (str) {
            if (str === "") str = '""';
            eval("var p=" + str + ";");
            return p;
        };

        var mForm = new Object();
        mForm.txtShow_CLCID = document.getElementById('txtShow_CLCID');
        mForm.txtSiteCode = document.getElementById('txtSiteCode');
        mForm.txtPrincipalCode = document.getElementById('txtPrincipalCode');

        mForm.txtSiteCode = document.getElementById('txtSiteCode');
        mForm.txtPrincipalCode = document.getElementById('txtPrincipalCode');
        mForm.txtMoveRef = document.getElementById('txtMoveRef');
        mForm.txtOrderLineNumber = document.getElementById('txtOrderLineNumber');
        mForm.txtProdCode = document.getElementById('txtProdCode');
        mForm.txtQuantity = document.getElementById('txtQuantity');
        mForm.selectReceiptType = document.getElementById('selectReceiptType');
        mForm.txtDiscrepancy = document.getElementById('txtDiscrepancy');
        mForm.txtSerialCount = document.getElementById('txtSerialCount');

        mForm.btnSerialnumberAdd = document.getElementById('btnSerialnumberAdd');
        mForm.txtSerialNumberAdd = document.getElementById('txtSerialNumberAdd');
        mForm.tblSerialNumbers = document.getElementById('tblSerialNumbers');

        mForm.btnSave = document.getElementById('btnSave');
        mForm.divVeil = document.getElementById('divVeil');

        function bodyonload() {
            mForm.txtShow_CLCID = document.getElementById('txtShow_CLCID');
            mForm.txtSiteCode = document.getElementById('txtSiteCode');
            mForm.txtPrincipalCode = document.getElementById('txtPrincipalCode');

            mForm.txtSiteCode = document.getElementById('txtSiteCode');
            mForm.txtPrincipalCode = document.getElementById('txtPrincipalCode');
            mForm.txtMoveRef = document.getElementById('txtMoveRef');
            mForm.txtOrderLineNumber = document.getElementById('txtOrderLineNumber');
            mForm.txtProdCode = document.getElementById('txtProdCode');
            mForm.txtQuantity = document.getElementById('txtQuantity');
            mForm.selectReceiptType = document.getElementById('selectReceiptType');
            mForm.txtDiscrepancy = document.getElementById('txtDiscrepancy');
            mForm.txtSerialCount = document.getElementById('txtSerialCount');

            mForm.btnSerialnumberAdd = document.getElementById('btnSerialnumberAdd');
            mForm.txtSerialNumberAdd = document.getElementById('txtSerialNumberAdd');
            mForm.tblSerialNumbers = document.getElementById('tblSerialNumbers');

            mForm.btnSave = document.getElementById('btnSave');
            mForm.divVeil = document.getElementById('divVeil');

            if (mForm.txtShow_CLCID.value.length > 0) {
                window.open('../MsgQueue/MsgOut/MsgOut_CLC_View.aspx?CLCID=' + mForm.txtShow_CLCID.value, '_blank', 'height=600,width=700');
                mForm.txtShow_CLCID.value = '';
            }

            ClearForm();

            WireValidation();

            mForm.txtSiteCode.value = getCookie('CLC_ADD_txtSiteCode');
            mForm.txtPrincipalCode.value = getCookie('CLC_ADD_txtPrincipalCode');

            Validate();


            //setTimeout(LoadDummyData, 250);

            setTimeout(HideVeil, 250);
        }
        function HideVeil(){
            mForm.divVeil.style.display = 'none';
        }


        function LoadDummyData() { 
            var i = 0;
            for (i = 0; i < 100; i++) {
                mForm.txtSerialNumberAdd.value = 'SN' + Math.random();
                mForm.btnSerialnumberAdd.disabled = true;
                btnSerialnumberAdd_onclick();
                mForm.btnSerialnumberAdd.disabled = false;
            }
            
            setTimeout(HideVeil, 250);
        }


        function WireValidation() {
            //wire validation
            var inputs = document.getElementsByTagName('input');
            var i;
            for (i = 0; i < inputs.length; ++i) {
                if (inputs[i].type == 'text') {
                    inputs[i].onkeyup = Validate;
                    inputs[i].onchange = Validate;
                    inputs[i].onclick = Validate;
                    inputs[i].onblur = Validate;
                }
                if (inputs[i].type == 'checkbox') {
                    inputs[i].onkeyup = Validate;
                    inputs[i].onchange = Validate;
                    inputs[i].onblur = Validate;
                }
            }
            inputs = document.getElementsByTagName('select');
            for (i = 0; i < inputs.length; ++i) {
                inputs[i].onkeyup = Validate;
                inputs[i].onchange = Validate;
                inputs[i].onclick = Validate;
                inputs[i].onblur = Validate;
            }
        }

        function ClearForm() {
            var inputs = document.getElementsByTagName('input');
            var i;
            for (i = 0; i < inputs.length; ++i) {
                if (inputs[i].type == 'text') inputs[i].value = '';
            }
            inputs = document.getElementsByTagName('select');
            for (i = 0; i < inputs.length; ++i) {
                inputs[i].selectedIndex = 0;
            }

            while (mForm.tblSerialNumbers.tBodies[0].rows.length > 0) {
                mForm.tblSerialNumbers.tBodies[0].deleteRow(0);
            }
        }

        function Validate() {
            var valid = true;

            mForm.txtSerialCount.value = mForm.tblSerialNumbers.tBodies[0].rows.length;

            //mForm.txtSiteCode
            if (mForm.txtSiteCode.value.length < 2) {
                valid = false;
                mForm.txtSiteCode.className = 'InputInValid';
            } else {
                mForm.txtSiteCode.className = 'InputValid';
            }

            //mForm.txtPrincipalCode
            if (mForm.txtPrincipalCode.value.length < 2) {
                valid = false;
                mForm.txtPrincipalCode.className = 'InputInValid';
            } else {
                mForm.txtPrincipalCode.className = 'InputValid';
            }

            //mForm.txtMoveRef
            mForm.txtMoveRef.className = 'InputValid';
            //mForm.txtOrderLineNumber 
            mForm.txtOrderLineNumber.className = 'InputValid';
            //mForm.txtProdCode
            mForm.txtProdCode.className = 'InputValid';

            //mForm.txtQuantity 
            if ((isNumber(mForm.txtQuantity.value) == false) || (mForm.txtQuantity.value < 0 | mForm.txtQuantity.value > 20000)) {
                valid = false;
                mForm.txtQuantity.className = 'InputInValid';
            } else {
                mForm.txtQuantity.className = 'InputValid';
            }

            //mForm.selectReceiptType 
            mForm.selectReceiptType.className = 'InputValid';


            //mForm.txtDiscrepancy 
            if ((isNumber(mForm.txtDiscrepancy.value) == false) || (mForm.txtDiscrepancy.value < 0 | mForm.txtDiscrepancy.value > 20000)) {
                valid = false;
                mForm.txtDiscrepancy.className = 'InputInValid';
            } else {
                mForm.txtDiscrepancy.className = 'InputValid';
            }

            //mForm.txtSerialCount 
            if ((isNumber(mForm.txtSerialCount.value) == false) || (mForm.txtSerialCount.value < 0 | mForm.txtSerialCount.value > 20000)) {
                valid = false;
                mForm.txtSerialCount.className = 'InputInValid';
            } else {
                mForm.txtSerialCount.className = 'InputValid';
            }




            //mForm.btnSave
            mForm.btnSave.disabled = !valid;
            return valid;
        }

        function form1_onsubmit() {
            try {
                if (Validate() == false) {
                    return false;
                } else {
                     
                     mForm.divVeil.style.display = 'block';

                     

                    var serialNumbers = new Array();
                    var r = 0;
                    for (r = 0; r < mForm.tblSerialNumbers.tBodies[0].rows.length; r++) {
                        var serialNumber = mForm.tblSerialNumbers.tBodies[0].rows[r].CLC_Serialnumber;
                        serialNumbers[r] = serialNumber;
                    }
                    document.getElementById('txtSerialNumbers').value = JSON.stringify(serialNumbers);


                    setCookie('CLC_ADD_txtSiteCode', mForm.txtSiteCode.value, 7);
                    setCookie('CLC_ADD_txtPrincipalCode', mForm.txtPrincipalCode.value, 7);
                    return true;
                }
            } catch (e) {
                alert(e);
                return false;
            }
        }

        function btnSerialnumberAdd_onclick() {
            var sn = mForm.txtSerialNumberAdd.value.trim();
            mForm.txtSerialNumberAdd.value = '';
            if (sn.length > 0) {
                var tr = mForm.tblSerialNumbers.tBodies[0].insertRow(0);
                var td1 = tr.insertCell(0);
                td1.innerHTML = sn;
                var td2 = tr.insertCell(1); td2.style.width = '15px';
                td2.innerHTML = '';
                td2.style.cursor = 'pointer';
               // td2.style.color = 'red';

                tr.CLC_Serialnumber = sn;
                td2.Row = tr;
                tr.Table = mForm.tblSerialNumbers.tBodies[0];
                tr.normalbackgroundColor = tr.style.backgroundColor;
                tr.Cell2 = td2;

                td2.onclick = function () {
                    this.Row.Table.removeChild(this.Row);
                    mForm.txtSerialCount.value = this.Row.Table.rows.length;
                };
                tr.onmouseover = function () {
                    this.style.backgroundColor = "lightgray";
                    this.Cell2.innerHTML = 'x';
                };
                tr.onmouseout = function () {
                    this.style.backgroundColor = this.normalbackgroundColor;
                    this.Cell2.innerHTML = '';
                };
            }

            mForm.txtSerialCount.value = mForm.tblSerialNumbers.tBodies[0].rows.length;

            mForm.txtSerialNumberAdd.focus();
        }






    </script>

</head>
<body onload="bodyonload();">
    <form id="form1" runat="server" onsubmit="return form1_onsubmit();">
        <input type="hidden" id="txtShow_CLCID" runat="server" />
        <input type="hidden" id="txtSerialNumbers" runat="server" />
        <div>
            <table>
                <tr>
                    <td>
                        <h2>CLC – Consignment Line Completion (Host -> Matflo)</h2>
                    </td>
                    <td>&nbsp;</td>
                    <td><a href="Index.aspx">Index</a></td>
                </tr>
            </table>

            <table class="list">
                <thead>
                    <tr>
                        <td colspan="3">CLC Add</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>SiteCode</td>
                        <td style="width: 250px">
                            <input type="text" runat="server" id="txtSiteCode" maxlength="4" value="001" style="width: 100%" />
                        </td>

                    </tr>
                    <tr>
                        <td>PrincipalCode</td>
                        <td>
                            <input type="text" runat="server" id="txtPrincipalCode" maxlength="6" value="OG" style="width: 100%" /></td>
                    </tr>

                    <tr>
                        <td>MoveRef</td>
                        <td>
                            <input type="text" runat="server" id="txtMoveRef" maxlength="10" style="width: 100%" /></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>OrderLineNumber</td>
                        <td>
                            <input type="text" runat="server" id="txtOrderLineNumber" maxlength="4" style="width: 100%" /></td>
                    </tr>
                    <tr>
                        <td>ProdCode</td>
                        <td>
                            <input type="text" runat="server" id="txtProdCode" maxlength="20" style="width: 100%" /></td>
                    </tr>
                    <tr>
                        <td>Quantity</td>
                        <td>
                            <input type="text" runat="server" id="txtQuantity" maxlength="10" style="width: 100%" /></td>
                    </tr>
                    <tr>
                        <td>ReceiptType</td>
                        <td>
                            <select id="selectReceiptType" runat="server" style="width: 100%">
                                <option value="0">New Stock</option>
                                <option value="1">Returned Stock</option>
                                <option value="2">Take On</option>
                            </select>
                        </td>
                    </tr>

                    <tr>
                        <td>Discrepancy</td>
                        <td>
                            <input type="text" runat="server" id="txtDiscrepancy" maxlength="10" style="width: 100%" /></td>
                    </tr>
                    <tr>
                        <td>SerialCount</td>
                        <td>
                            <input type="text" runat="server" id="txtSerialCount" maxlength="10" style="width: 100%" readonly="readonly" /></td>
                    </tr>

                    <tr>
                        <td>SerialNumbers</td>
                        <td style="padding: 0px; margin: 0px">
                            <table style="border-style: none; padding: 0px; margin: 0px; width: 100%; border-collapse: collapse; border-spacing: 0px;">
                                <tr>
                                    <td>
                                        <input type="text" runat="server" id="txtSerialNumberAdd" maxlength="20" style="width: 100%; padding: 0px; margin: 0px"
                                            onkeypress="var key = window.event ? event.keyCode : event.which; if ((key == 32)|(key == 13)) btnSerialnumberAdd_onclick();" />
                                    </td>
                                    <td style="width: 20px; text-align: right; padding: 0px; margin: 0px">
                                        <input type="button" value="Add" id="btnSerialnumberAdd" onclick="btnSerialnumberAdd_onclick();" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>

                        <td style="padding: 0px; margin: 0px" colspan="2">
                            <table class="list" id="tblSerialNumbers" style="border-style: none; padding: 0px; margin: 0px; width: 100%; border-collapse: collapse; border-spacing: 0px;">
                                <thead>
                                    <tr>
                                        <td colspan="2">SerialNumbers</td>
                                    </tr>

                                </thead>
                                <tbody></tbody>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td id="tdErrorMessage" runat="server" class="validation"></td>
                        <td style="text-align: right">
                            <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" />

                        </td>

                    </tr>
                </tbody>
            </table>

        </div>
    </form>
        <div style="display: block; position: absolute; top: 5px; right: 5px; bottom: 5px; left: 5px; background-color: #FFFFCC; z-index: 10; opacity: .70; filter: alpha(opacity=70); -moz-opacity: 0.7;"
            id="divVeil">
            Busy...
        </div>
</body>
</html>
