﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="RAMWMSHost_Web.DataEntry.Index" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Data Entry</title>
    <link href="../Style1.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <h2>Data Entry</h2>
            <a href="../Default.aspx">Home</a>
            <br />


          
            <table class="list">

                <thead>
                    <tr>
                        <td>Inbound Messages</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><a href="IGD_Add.aspx">IGD – Incoming Goods Details (Host -> Matflo)</a></td>
                    </tr>

                    <tr>
                        <td><a href="CLC_Add.aspx">CLC – Consignment Line Completion (Host -> Matflo)</a></td>
                    </tr>
                </tbody>
                <thead>
                    <tr>
                        <td>Outbound Messages</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><a href="ORD_Add.aspx">ORD – Order Definition (Host -> Matflo)</a></td>
                    </tr>

                    <tr>
                        <td><a href="OCA_Add.aspx">OCA – Order Cancelled (Host -> Matflo)</a></td>
                    </tr>

                </tbody>



            </table>




        </div>
    </form>
</body>
</html>
