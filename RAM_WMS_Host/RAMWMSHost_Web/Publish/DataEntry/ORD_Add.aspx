﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ORD_Add.aspx.cs" Inherits="RAMWMSHost_Web.DataEntry.ORD_Add" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>ORD Add</title>
    <link href="../Style1.css" rel="stylesheet" />

    <script src="../Scripts/datetimepicker/datetimepicker_css.js"></script>
    <script src="../Scripts/Shared.js"></script>
    <script type="text/javascript">

        if (!String.prototype.trim) {
            String.prototype.trim = function () { return this.replace(/^\s+|\s+$/g, ''); };
        }

        var JSON = JSON || {};
        // implement JSON.stringify serialization
        JSON.stringify = JSON.stringify || function (obj) {
            var t = typeof (obj);
            if (t != "object" || obj === null) {
                // simple data type
                if (t == "string") obj = '"' + obj + '"';
                return String(obj);
            }
            else {
                // recurse array or object
                var n, v, json = [], arr = (obj && obj.constructor == Array);
                for (n in obj) {
                    v = obj[n]; t = typeof (v);
                    if (t == "string") v = '"' + v + '"';
                    else if (t == "object" && v !== null) v = JSON.stringify(v);
                    json.push((arr ? "" : '"' + n + '":') + String(v));
                }
                return (arr ? "[" : "{") + String(json) + (arr ? "]" : "}");
            }
        };
        // implement JSON.parse de-serialization
        JSON.parse = JSON.parse || function (str) {
            if (str === "") str = '""';
            eval("var p=" + str + ";");
            return p;
        };

        var mForm = new Object();
        mForm.txtShow_ORDID = document.getElementById('txtShow_ORDID');

        mForm.txtSiteCode = document.getElementById('txtSiteCode');
        mForm.txtPrincipalCode = document.getElementById('txtPrincipalCode');
        mForm.txtOrderNumber = document.getElementById('txtOrderNumber');
        mForm.txtLineCount = document.getElementById('txtLineCount');
        mForm.txtDateCreated = document.getElementById('txtDateCreated');
        mForm.txtCompanyName = document.getElementById('txtCompanyName');
        mForm.txtHeaderComment = document.getElementById('txtHeaderComment');
        mForm.txtCustOrderNumber = document.getElementById('txtCustOrderNumber');
        mForm.txtCustAcc = document.getElementById('txtCustAcc');
        mForm.txtInvName = document.getElementById('txtInvName');
        mForm.txtInvAdd1 = document.getElementById('txtInvAdd1');
        mForm.txtInvAdd2 = document.getElementById('txtInvAdd2');
        mForm.txtInvAdd3 = document.getElementById('txtInvAdd3');
        mForm.txtInvAdd4 = document.getElementById('txtInvAdd4');
        mForm.txtInvAdd5 = document.getElementById('txtInvAdd5');
        mForm.txtInvAdd6 = document.getElementById('txtInvAdd6');
        mForm.chkGSMTest = document.getElementById('chkGSMTest');
        mForm.txtVATNumber = document.getElementById('txtVATNumber');
        mForm.chkPrintPrice = document.getElementById('chkPrintPrice');
        mForm.selectPriority = document.getElementById('selectPriority');
        mForm.chkVAP = document.getElementById('chkVAP');
        mForm.txtSalesPerson = document.getElementById('txtSalesPerson');
        mForm.txtSalesCategory = document.getElementById('txtSalesCategory');
        mForm.txtProcessor = document.getElementById('txtProcessor');
        mForm.txtDeliveryAdd1 = document.getElementById('txtDeliveryAdd1');
        mForm.txtDeliveryAdd2 = document.getElementById('txtDeliveryAdd2');
        mForm.txtDeliveryAdd3 = document.getElementById('txtDeliveryAdd3');
        mForm.txtDeliveryAdd4 = document.getElementById('txtDeliveryAdd4');
        mForm.txtDeliveryAdd5 = document.getElementById('txtDeliveryAdd5');
        mForm.txtDeliveryAdd6 = document.getElementById('txtDeliveryAdd6');
        mForm.txtWMSPostCode = document.getElementById('txtWMSPostCode');
        mForm.txtOrderDiscount = document.getElementById('txtOrderDiscount');
        mForm.txtOrderVAT = document.getElementById('txtOrderVAT');
        mForm.txtDocToPrint = document.getElementById('txtDocToPrint'); 

        mForm.selectLineType = document.getElementById('selectLineType');
        mForm.txtProdCode = document.getElementById('txtProdCode');
        mForm.txtLineText = document.getElementById('txtLineText');
        mForm.txtQuantity = document.getElementById('txtQuantity');
        mForm.txtUnitCost = document.getElementById('txtUnitCost');
        mForm.txtVat = document.getElementById('txtVat');
        mForm.txtDiscount = document.getElementById('txtDiscount');
        mForm.btnOrderLineAdd = document.getElementById('btnOrderLineAdd');
        mForm.tblOrderLines = document.getElementById('tblOrderLines');

        mForm.txtIDNumber = document.getElementById('txtIDNumber');
        mForm.txtKYC = document.getElementById('KYC');
        mForm.txtCourierName = document.getElementById('txtCourierName');
        mForm.txtCourierService = document.getElementById('txtCourierService');
        mForm.txtInsuranceRequired = document.getElementById('txtInsuranceRequired');
        mForm.txtValidateDelivery = document.getElementById('txtValidateDelivery');
       


        mForm.btnSave = document.getElementById('btnSave');
        mForm.divVeil = document.getElementById('divVeil');
        

        function bodyonload() {
            mForm.txtShow_ORDID = document.getElementById('txtShow_ORDID');

            mForm.txtSiteCode = document.getElementById('txtSiteCode');
            mForm.txtPrincipalCode = document.getElementById('txtPrincipalCode');
            mForm.txtOrderNumber = document.getElementById('txtOrderNumber');
            mForm.txtLineCount = document.getElementById('txtLineCount');
            mForm.txtDateCreated = document.getElementById('txtDateCreated');
            mForm.txtCompanyName = document.getElementById('txtCompanyName');
            mForm.txtHeaderComment = document.getElementById('txtHeaderComment');
            mForm.txtCustOrderNumber = document.getElementById('txtCustOrderNumber');
            mForm.txtCustAcc = document.getElementById('txtCustAcc');
            mForm.txtInvName = document.getElementById('txtInvName');
            mForm.txtInvAdd1 = document.getElementById('txtInvAdd1');
            mForm.txtInvAdd2 = document.getElementById('txtInvAdd2');
            mForm.txtInvAdd3 = document.getElementById('txtInvAdd3');
            mForm.txtInvAdd4 = document.getElementById('txtInvAdd4');
            mForm.txtInvAdd5 = document.getElementById('txtInvAdd5');
            mForm.txtInvAdd6 = document.getElementById('txtInvAdd6');
            mForm.chkGSMTest = document.getElementById('chkGSMTest');
            mForm.txtVATNumber = document.getElementById('txtVATNumber');
            mForm.chkPrintPrice = document.getElementById('chkPrintPrice');
            mForm.selectPriority = document.getElementById('selectPriority');
            mForm.chkVAP = document.getElementById('chkVAP');
            mForm.txtSalesPerson = document.getElementById('txtSalesPerson');
            mForm.txtSalesCategory = document.getElementById('txtSalesCategory');
            mForm.txtProcessor = document.getElementById('txtProcessor');
            mForm.txtDeliveryAdd1 = document.getElementById('txtDeliveryAdd1');
            mForm.txtDeliveryAdd2 = document.getElementById('txtDeliveryAdd2');
            mForm.txtDeliveryAdd3 = document.getElementById('txtDeliveryAdd3');
            mForm.txtDeliveryAdd4 = document.getElementById('txtDeliveryAdd4');
            mForm.txtDeliveryAdd5 = document.getElementById('txtDeliveryAdd5');
            mForm.txtDeliveryAdd6 = document.getElementById('txtDeliveryAdd6');
            mForm.txtWMSPostCode = document.getElementById('txtWMSPostCode');
            mForm.txtOrderDiscount = document.getElementById('txtOrderDiscount');
            mForm.txtOrderVAT = document.getElementById('txtOrderVAT');
            mForm.txtDocToPrint = document.getElementById('txtDocToPrint');

            mForm.selectLineType = document.getElementById('selectLineType');
            mForm.txtProdCode = document.getElementById('txtProdCode');
            mForm.txtLineText = document.getElementById('txtLineText');
            mForm.txtQuantity = document.getElementById('txtQuantity');
            mForm.txtUnitCost = document.getElementById('txtUnitCost');
            mForm.txtVat = document.getElementById('txtVat');
            mForm.txtDiscount = document.getElementById('txtDiscount');
            mForm.btnOrderLineAdd = document.getElementById('btnOrderLineAdd');
            mForm.tblOrderLines = document.getElementById('tblOrderLines');


        mForm.txtIDNumber = document.getElementById('txtIDNumber');
        mForm.txtKYC = document.getElementById('KYC');
        mForm.txtCourierName = document.getElementById('txtCourierName');
        mForm.txtCourierService = document.getElementById('txtCourierService');
            mForm.txtInsuranceRequired = document.getElementById('txtInsuranceRequired');
            mForm.txtValidateDelivery = document.getElementById('txtValidateDelivery');
            

            mForm.btnSave = document.getElementById('btnSave');
            mForm.divVeil = document.getElementById('divVeil');

            if (mForm.txtShow_ORDID.value.length > 0) {
                window.open('../MsgQueue/MsgOut/MsgOut_ORD_View.aspx?ORDID=' + mForm.txtShow_ORDID.value, '_blank', 'height=600,width=950');
                mForm.txtShow_ORDID.value = '';
            }

            ClearForm();

            WireValidation();

            mForm.txtSiteCode.value = getCookie('ORD_ADD_txtSiteCode');
            mForm.txtPrincipalCode.value = getCookie('ORD_ADD_txtPrincipalCode');

            Validate();


            //setTimeout(LoadDummyData, 250);

            setTimeout(HideVeil, 250);
        }
        function HideVeil() {
            mForm.divVeil.style.display = 'none';
        }


        function LoadDummyData() {
            var i = 0;
            for (i = 0; i < 100; i++) {
                mForm.txtOrderLineAdd.value = 'SN' + Math.random();
                mForm.btnOrderLineAdd.disabled = true;
                btnOrderLineAdd_onclick();
                mForm.btnOrderLineAdd.disabled = false;
            }

            setTimeout(HideVeil, 250);
        }


        function WireValidation() {
            //wire validation
            var inputs = document.getElementsByTagName('input');
            var i;
            for (i = 0; i < inputs.length; ++i) {
                if (inputs[i].type == 'text') {
                    inputs[i].onkeyup = Validate;
                    inputs[i].onchange = Validate;
                    inputs[i].onclick = Validate;
                    inputs[i].onblur = Validate;
                }
                if (inputs[i].type == 'checkbox') {
                    inputs[i].onkeyup = Validate;
                    inputs[i].onchange = Validate;
                    inputs[i].onblur = Validate;
                }
            }
            inputs = document.getElementsByTagName('select');
            for (i = 0; i < inputs.length; ++i) {
                inputs[i].onkeyup = Validate;
                inputs[i].onchange = Validate;
                inputs[i].onclick = Validate;
                inputs[i].onblur = Validate;
            }
        }

        function ClearForm() {
            var inputs = document.getElementsByTagName('input');
            var i;
            for (i = 0; i < inputs.length; ++i) {
                if ((inputs[i].type == 'text') && (inputs[i].id != 'txtDateCreated')) inputs[i].value = '';
            }
            inputs = document.getElementsByTagName('select');
            for (i = 0; i < inputs.length; ++i) {
                inputs[i].selectedIndex = 0;
            }

            while (mForm.tblOrderLines.tBodies[0].rows.length > 0) {
                mForm.tblOrderLines.tBodies[0].deleteRow(0);
            }

            mForm.txtDocToPrint.value = 0;
        }

        function Validate() {
            var valid = true;

            mForm.txtLineCount.value = mForm.tblOrderLines.tBodies[0].rows.length;

            //mForm.txtSiteCode
            if (mForm.txtSiteCode.value.length < 2) {
                valid = false;
                mForm.txtSiteCode.className = 'InputInValid';
            } else {
                mForm.txtSiteCode.className = 'InputValid';
            }

            //mForm.txtPrincipalCode
            if (mForm.txtPrincipalCode.value.length < 2) {
                valid = false;
                mForm.txtPrincipalCode.className = 'InputInValid';
            } else {
                mForm.txtPrincipalCode.className = 'InputValid';
            }


            mForm.txtOrderNumber.className = 'InputValid';

            //mForm.txtLineCount 
            valid = valid & ValidateNumber(mForm.txtLineCount, 0, 20000);


            //mForm.txtDateCreated  
            if (isDate(mForm.txtDateCreated.value) == false) {
                valid = false;
                mForm.txtDateCreated.className = 'InputInValid';
            } else {
                mForm.txtDateCreated.className = 'InputValid';
            }

            mForm.txtCompanyName.className = 'InputValid';
            mForm.txtHeaderComment.className = 'InputValid';
            mForm.txtCustOrderNumber.className = 'InputValid';
            mForm.txtCustAcc.className = 'InputValid';
            mForm.txtInvName.className = 'InputValid';
            mForm.txtInvAdd1.className = 'InputValid';
            mForm.txtInvAdd2.className = 'InputValid';
            mForm.txtInvAdd3.className = 'InputValid';
            mForm.txtInvAdd4.className = 'InputValid';
            mForm.txtInvAdd5.className = 'InputValid';
            mForm.txtInvAdd6.className = 'InputValid';
            mForm.chkGSMTest.className = 'InputValid';
            mForm.txtVATNumber.className = 'InputValid';
            mForm.chkPrintPrice.className = 'InputValid';
            mForm.selectPriority.className = 'InputValid';
            mForm.chkVAP.className = 'InputValid';
            mForm.txtSalesPerson.className = 'InputValid';
            mForm.txtSalesCategory.className = 'InputValid';
            mForm.txtProcessor.className = 'InputValid';
            mForm.txtDeliveryAdd1.className = 'InputValid';
            mForm.txtDeliveryAdd2.className = 'InputValid';
            mForm.txtDeliveryAdd3.className = 'InputValid';
            mForm.txtDeliveryAdd4.className = 'InputValid';
            mForm.txtDeliveryAdd5.className = 'InputValid';
            mForm.txtDeliveryAdd6.className = 'InputValid';
            mForm.txtWMSPostCode.className = 'InputValid';

            mForm.txtIDNumber.className = 'InputValid';
            mForm.txtKYC.className = 'InputValid';
            mForm.txtCourierName.className = 'InputValid';
            mForm.txtCourierService.className = 'InputValid';
            mForm.txtInsuranceRequired.className = 'InputValid';
            mForm.txtValidateDelivery.className = 'ValidateDelivery';

            //mForm.txtOrderDiscount 
            valid = valid & ValidateNumber(mForm.txtOrderDiscount, 0, 2000000);
            //mForm.txtOrderVAT 
            valid = valid & ValidateNumber(mForm.txtOrderVAT, 0, 2000000); 

            valid = valid & ValidateNumber(mForm.txtDocToPrint, 0, 2000000);


            ValidateOrderLine();

            //mForm.btnSave
            mForm.btnSave.disabled = !valid;
            return valid;
        }

        function ValidateOrderLine() {
            var lineValid = true;
            mForm.selectLineType.className = 'InputValid';
            mForm.txtProdCode.className = 'InputValid';
            mForm.txtLineText.className = 'InputValid';
            lineValid = lineValid & ValidateNumber(mForm.txtQuantity, 0, 9999);
            lineValid = lineValid & ValidateNumber(mForm.txtUnitCost, 0, 2000000);
            lineValid = lineValid & ValidateNumber(mForm.txtVat, 0, 2000000);
            lineValid = lineValid & ValidateNumber(mForm.txtDiscount, 0, 2000000);
            //mForm.btnOrderLineAdd
            mForm.btnOrderLineAdd.disabled = !lineValid;
            return lineValid;
        }

        function ValidateNumber(el, min, max) {
            var valid = true;
            if ((isNumber(el.value) == false) || (el.value < min | el.value > max)) {
                valid = false;
                el.className = 'InputInValid';
            } else {
                el.className = 'InputValid';
            }
            return valid;
        }

        function form1_onsubmit() {
            try {
                if (Validate() == false) {
                    return false;
                } else {

                    mForm.divVeil.style.display = 'block';
                    var OrderLines = new Array();
                    var r = 0;
                    for (r = mForm.tblOrderLines.tBodies[0].rows.length - 1 ; r >= 0; r--) {
                        var LineNumber = r + 1;
                        var orderLine = mForm.tblOrderLines.tBodies[0].rows[r].ORD_OrderLine;
                        OrderLines[r] = { LineNumber: LineNumber, LineType: orderLine.LineType, ProdCode: orderLine.ProdCode, LineText: orderLine.LineText, Quantity: orderLine.Quantity, UnitCost: orderLine.UnitCost, Vat: orderLine.Vat, Discount: orderLine.Discount };
                    }
                    document.getElementById('txtOrderLines').value = JSON.stringify(OrderLines);
                    setCookie('ORD_ADD_txtSiteCode', mForm.txtSiteCode.value, 7);
                    setCookie('ORD_ADD_txtPrincipalCode', mForm.txtPrincipalCode.value, 7);
                    return true;
                }
            } catch (e) {
                alert(e);
                return false;
            }
        }

        function btnOrderLineAdd_onclick() {
            if (ValidateOrderLine() == true) {
                var LineType = mForm.selectLineType.value.trim();
                var ProdCode = mForm.txtProdCode.value.trim();
                var LineText = mForm.txtLineText.value.trim();
                var Quantity = mForm.txtQuantity.value.trim();
                var UnitCost = mForm.txtUnitCost.value.trim();
                var Vat = mForm.txtVat.value.trim();
                var Discount = mForm.txtDiscount.value.trim();

                var LineTypeText = mForm.selectLineType.options[mForm.selectLineType.selectedIndex].text.trim();

                var tr = mForm.tblOrderLines.tBodies[0].insertRow(0);

                var td1 = tr.insertCell(); td1.innerHTML = 'Type: ' + LineTypeText + '<br />Qty: ' + Quantity;
                var td2 = tr.insertCell(); td2.innerHTML = 'ProdCode: ' + ProdCode + '<br />Cost: ' + UnitCost;
                var td3 = tr.insertCell(); td3.innerHTML = 'Text: ' + LineText + '<br />VAT: ' + Vat + ' Discount: ' + Discount;

                var tdDelete = tr.insertCell(); td2.style.width = '15px';
                tdDelete.innerHTML = '';
                tdDelete.style.cursor = 'pointer';

                tdDelete.Row = tr;
                tr.Table = mForm.tblOrderLines.tBodies[0];
                tr.normalbackgroundColor = tr.style.backgroundColor;
                tr.CellDelete = tdDelete;
                tr.ORD_OrderLine = new Object();
                tr.ORD_OrderLine.LineType = LineType;
                tr.ORD_OrderLine.ProdCode = ProdCode;
                tr.ORD_OrderLine.LineText = LineText;
                tr.ORD_OrderLine.Quantity = Quantity;
                tr.ORD_OrderLine.UnitCost = UnitCost;
                tr.ORD_OrderLine.Vat = Vat;
                tr.ORD_OrderLine.Discount = Discount;

                tdDelete.onclick = function () {
                    this.Row.Table.removeChild(this.Row);
                    mForm.txtLineCount.value = this.Row.Table.rows.length;
                };
                tr.onmouseover = function () {
                    this.style.backgroundColor = "lightgray";
                    this.CellDelete.innerHTML = 'x';
                };
                tr.onmouseout = function () {
                    this.style.backgroundColor = this.normalbackgroundColor;
                    this.CellDelete.innerHTML = '';
                };
            }

            mForm.txtLineCount.value = mForm.tblOrderLines.tBodies[0].rows.length;

            mForm.txtProdCode.focus();
        }






    </script>

</head>
<body onload="bodyonload();">
    <form id="form1" runat="server" onsubmit="return form1_onsubmit();">
        <input type="hidden" id="txtShow_ORDID" runat="server" />
        <input type="hidden" id="txtOrderLines" runat="server" />
        <div>
            <table>
                <tr>
                    <td>
                        <h2>ORD – Order Definition (Host -> Matflo)</h2>
                    </td>
                    <td>&nbsp;</td>
                    <td><a href="Index.aspx">Index</a></td>
                </tr>
            </table>

            <table class="list">
                <thead>
                    <tr>
                        <td colspan="6">ORD Add</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>SiteCode</td>
                        <td style="width: 250px">
                            <input type="text" runat="server" id="txtSiteCode" maxlength="4" style="width: 100%" /></td>
                        <td></td>
                        <td>CustAcc</td>
                        <td style="width: 250px">
                            <input type="text" runat="server" id="txtCustAcc" maxlength="20" style="width: 100%" /></td>
                        <td></td>

                    </tr>
                    <tr>
                        <td>PrincipalCode</td>
                        <td>
                            <input type="text" runat="server" id="txtPrincipalCode" maxlength="6" style="width: 100%" /></td>
                        <td></td>
                        <td>InvName</td>
                        <td>
                            <input type="text" runat="server" id="txtInvName" maxlength="32" style="width: 100%" /></td>
                        <td></td>

                    </tr>
                    <tr>
                        <td>OrderNumber</td>
                        <td>
                            <input type="text" runat="server" id="txtOrderNumber" maxlength="10" style="width: 100%" /></td>
                        <td></td>
                        <td>InvAdd1</td>
                        <td>
                            <input type="text" runat="server" id="txtInvAdd1" maxlength="32" style="width: 100%" /></td>
                        <td></td>

                    </tr>
                    <tr>
                        <td>DateCreated</td>
                        <td style="padding: 0px; margin: 0px">
                            <table style="border-style: none; padding: 0px; margin: 0px; width: 100%; border-collapse: collapse; border-spacing: 0px;">
                                <tr>
                                    <td>
                                        <input type="text" runat="server" id="txtDateCreated" maxlength="10" style="width: 100%; padding: 0px; margin: 0px" /></td>
                                    <td style="width: 20px; text-align: right; padding: 0px; margin: 0px">
                                        <img src="../Scripts/datetimepicker/images2/cal.gif" onclick="javascript:NewCssCal('txtDateCreated', 'yyyyMMdd', 'dropdown', false)" style="cursor: pointer" /></td>
                                </tr>
                            </table>
                        </td>
                        <td></td>
                        <td>InvAdd2</td>
                        <td>
                            <input type="text" runat="server" id="txtInvAdd2" maxlength="32" style="width: 100%" /></td>
                        <td></td>

                    </tr>

                    <tr>
                        <td>CompanyName</td>
                        <td>
                            <input type="text" runat="server" id="txtCompanyName" maxlength="10" style="width: 100%" /></td>
                        <td></td>
                        <td>InvAdd3</td>
                        <td>
                            <input type="text" runat="server" id="txtInvAdd3" maxlength="32" style="width: 100%" /></td>
                        <td></td>

                    </tr>


                    <tr>
                        <td>HeaderComment</td>
                        <td>
                            <input type="text" runat="server" id="txtHeaderComment" maxlength="200" style="width: 100%" /></td>
                        <td></td>
                        <td>InvAdd4</td>
                        <td>
                            <input type="text" runat="server" id="txtInvAdd4" maxlength="32" style="width: 100%" /></td>
                        <td></td>

                    </tr>
                    <tr>
                        <td>CustOrderNumber</td>
                        <td>
                            <input type="text" runat="server" id="txtCustOrderNumber" maxlength="20" style="width: 100%" /></td>
                        <td></td>
                        <td>InvAdd5</td>
                        <td>
                            <input type="text" runat="server" id="txtInvAdd5" maxlength="32" style="width: 100%" /></td>
                        <td></td>

                    </tr>
                    <tr>
                        <td>GSMTest</td>
                        <td>
                            <input type="checkbox" runat="server" id="chkGSMTest" />
                        </td>
                        <td></td>
                        <td>InvAdd6</td>
                        <td>
                            <input type="text" runat="server" id="txtInvAdd6" maxlength="32" style="width: 100%" /></td>
                        <td></td>

                    </tr>

                    <tr>
                        <td>VATNumber</td>
                        <td>
                            <input type="text" runat="server" id="txtVATNumber" maxlength="32" style="width: 100%" /></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>PrintPrice</td>
                        <td>
                            <input type="checkbox" runat="server" id="chkPrintPrice" />
                        </td>
                        <td></td>
                        <td>DeliveryAdd1</td>
                        <td>
                            <input type="text" runat="server" id="txtDeliveryAdd1" maxlength="32" style="width: 100%" /></td>
                        <td></td>

                    </tr>
                    <tr>
                        <td>Priority</td>
                        <td>
                            <select id="selectPriority" runat="server" style="width: 100%">
                                <option value="3">Normal</option>
                                <option value="2">Rapid</option>
                            </select>
                        </td>
                        <td></td>
                        <td>DeliveryAdd2</td>
                        <td>
                            <input type="text" runat="server" id="txtDeliveryAdd2" maxlength="32" style="width: 100%" /></td>
                        <td></td>

                    </tr>
                    <tr>
                        <td>VAP</td>
                        <td>
                            <input type="checkbox" runat="server" id="chkVAP" />
                        </td>
                        <td></td>
                        <td>DeliveryAdd3</td>
                        <td>
                            <input type="text" runat="server" id="txtDeliveryAdd3" maxlength="32" style="width: 100%" /></td>
                        <td></td>

                    </tr>
                    <tr>
                        <td>SalesPerson</td>
                        <td>
                            <input type="text" runat="server" id="txtSalesPerson" maxlength="6" style="width: 100%" /></td>
                        <td></td>
                        <td>DeliveryAdd4</td>
                        <td>
                            <input type="text" runat="server" id="txtDeliveryAdd4" maxlength="32" style="width: 100%" /></td>
                        <td></td>

                    </tr>
                    <tr>
                        <td>SalesCategory</td>
                        <td>
                            <input type="text" runat="server" id="txtSalesCategory" maxlength="6" style="width: 100%" /></td>
                        <td></td>
                        <td>DeliveryAdd5</td>
                        <td>
                            <input type="text" runat="server" id="txtDeliveryAdd5" maxlength="32" style="width: 100%" /></td>
                        <td></td>

                    </tr>
                    <tr>
                        <td>Processor</td>
                        <td>
                            <input type="text" runat="server" id="txtProcessor" maxlength="10" style="width: 100%" /></td>
                        <td></td>
                        <td>DeliveryAdd6</td>
                        <td>
                            <input type="text" runat="server" id="txtDeliveryAdd6" maxlength="32" style="width: 100%" /></td>
                        <td></td>

                    </tr>


                    <tr>
                        <td>WMSPostCode</td>
                        <td>
                            <input type="text" runat="server" id="txtWMSPostCode" maxlength="8" style="width: 100%" /></td>
                        <td></td>

                        
                        <td>DocToPrint</td>
                        <td>
                            <input type="text" runat="server" id="txtDocToPrint" maxlength="8" style="width: 100%" /></td>
                        <td></td>

                    </tr>
                    <tr>
                        <td>OrderDiscount</td>
                        <td>
                            <input type="text" runat="server" id="txtOrderDiscount" maxlength="10" style="width: 100%" /></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>OrderVAT</td>
                        <td>
                            <input type="text" runat="server" id="txtOrderVAT" maxlength="10" style="width: 100%" /></td>
                        <td></td>


                        <td>LineCount</td>
                        <td>
                            <input type="text" runat="server" id="txtLineCount" maxlength="6" style="width: 100%" readonly /></td>
                        <td></td>

                    </tr>
                    <tr>
                        <td>IDNumber</td>
                        <td>
                            <input type="text" runat="server" id="txtIDNumber" maxlength="10" style="width: 100%" /></td>
                        <td></td>
                    </tr>
                     <tr>
                        <td>KYC</td>
                        <td>
                            <input type="text" runat="server" id="txtKYC" maxlength="10" style="width: 100%" /></td>
                        <td></td>
                    </tr>
                     <tr>
                        <td>CourierName</td>
                        <td>
                            <input type="text" runat="server" id="txtCourierName" maxlength="10" style="width: 100%" /></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>CourierService</td>
                        <td>
                            <input type="text" runat="server" id="txtCourierService" maxlength="10" style="width: 100%" /></td>
                        <td></td>
                    </tr>
                     <tr>
                        <td>InsuranceRequired</td>
                        <td>
                            <input type="text" runat="server" id="txtInsuranceRequired" maxlength="10" style="width: 100%" /></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>ValidateDelivery</td>
                        <td><input type="text" runat="server" id="txtValidateDelivery" maxlength="10" style="width:100%" /></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td colspan="6" class="divider"></td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top">OrderLines</td>
                        <td style="padding: 0px; margin: 0px" colspan="5">
                            <table style="border-style: none; padding: 0px; margin: 0px; width: 100%; border-collapse: collapse; border-spacing: 0px;">

                                <tbody>
                                    <tr>
                                        <%--<td>LineNumber</td>--%>
                                        <td>LineType</td>
                                        <td>ProdCode</td>
                                        <td colspan="3">LineText</td>

                                    </tr>
                                    <tr>
                                        <%--<td>
                                            <input type="text" runat="server" id="txtLineNumber" maxlength="6" />
                                        </td>--%>
                                        <td>
                                            <select id="selectLineType" runat="server">
                                                <option value="P">Product</option>
                                                <option value="S">Service</option>
                                                <option value="C">Comment</option>
                                            </select>
                                        </td>
                                        <td>
                                            <input type="text" runat="server" id="txtProdCode" maxlength="20" style="width: 100%" /></td>
                                        <td colspan="3">
                                            <input type="text" runat="server" id="txtLineText" maxlength="40" style="width: 99%" /></td>

                                    </tr>
                                    <tr>
                                        <td>Quantity</td>
                                        <td>UnitCost</td>
                                        <td>Vat</td>
                                        <td>Discount</td>
                                        <td></td>
                                    </tr>

                                    <tr>

                                        <td>
                                            <input type="text" runat="server" id="txtQuantity" maxlength="6" /></td>
                                        <td>
                                            <input type="text" runat="server" id="txtUnitCost" maxlength="10" /></td>
                                        <td>
                                            <input type="text" runat="server" id="txtVat" maxlength="10" /></td>
                                        <td>
                                            <input type="text" runat="server" id="txtDiscount" maxlength="10" /></td>

                                        <td style="width: 20px; text-align: right; padding: 0px; margin: 0px">
                                            <input type="button" value="Add" id="btnOrderLineAdd" onclick="btnOrderLineAdd_onclick();" />
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr>

                        <td style="padding: 0px; margin: 0px" colspan="6">
                            <table class="list" id="tblOrderLines" style="border-style: none; padding: 0px; margin: 0px; width: 100%; border-collapse: collapse; border-spacing: 0px;">
                                <thead>
                                    <tr>
                                        <td colspan="4">OrderLines</td>
                                    </tr>

                                </thead>
                                <tbody></tbody>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td id="tdErrorMessage" colspan="4" runat="server" class="validation"></td>
                        <td style="text-align: right" colspan="2">
                            <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" />
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </form>
    <div style="display: block; position: absolute; top: 5px; right: 5px; bottom: 5px; left: 5px; background-color: #FFFFCC; z-index: 10; opacity: .70; filter: alpha(opacity=70); -moz-opacity: 0.7;"
        id="divVeil">
        Busy...
    </div>
</body>
</html>
