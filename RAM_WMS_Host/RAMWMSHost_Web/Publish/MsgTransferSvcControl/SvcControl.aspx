﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SvcControl.aspx.cs" Inherits="RAMWMSHost_Web.MsgTransferSvcControl.SvcControl" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>MsgTransferSvcControl</title>
    <meta http-equiv="refresh" content="1">

    <link href="../Style1.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <h2>Host->Matflo Synchronisation Service</h2>  
            <a href="../Default.aspx">Home</a> 

            <%=System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") %>
             &nbsp;&nbsp;
            <asp:Button ID="btnStartSync" runat="server" Text="Start Sync" OnClick="btnStartSync_Click" />
 
 

            <table class="list" style="width: 100%">
                <thead>
                    <tr>
                        <td style="width: 300px">Service Status</td>
                        <td class="verticaldivider">Service Log</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td style="vertical-align: top">
                            <div id="divStatus" runat="server"></div>
                        </td>
                        <td style="vertical-align: top" class="verticaldivider">
                            <div id="divLog" runat="server">
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>

        </div>
    </form>
</body>
</html>
