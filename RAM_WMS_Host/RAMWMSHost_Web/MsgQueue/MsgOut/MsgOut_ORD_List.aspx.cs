﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace RAMWMSHost_Web.MsgQueue.MsgOut {
    public partial class MsgOut_ORD_List : System.Web.UI.Page {

        public string mRecordState = string.Empty;
        public DataSet mORD_List = null;

        protected void Page_Load(object sender, EventArgs e) {

            Refresh(string.Empty);

        }

        private void Refresh(string orderNumber) {
            mRecordState = Request["RecordState"];
            mORD_List = BLL.MsgOut.MsgOut_ORD.MsgOut_ORD_List(mRecordState, 20, 1, orderNumber);
        }

        protected void btnFilter_Click(object sender, EventArgs e) {
            string orderNumber =   textFilter.Value.Trim(); 

            Refresh(orderNumber);
        }
    }
}