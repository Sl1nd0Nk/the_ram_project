﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace RAMWMSHost_Web.MsgQueue.MsgOut {
    public partial class MsgOut_CLC_List : System.Web.UI.Page {

        public string mRecordState = string.Empty;
        public DataSet mCLC_List = null;

        protected void Page_Load(object sender, EventArgs e) {

            mRecordState = Request["RecordState"];
            mCLC_List = BLL.MsgOut.MsgOut_CLC.MsgOut_CLC_List(mRecordState, 20, 1);

        }
    }
}

