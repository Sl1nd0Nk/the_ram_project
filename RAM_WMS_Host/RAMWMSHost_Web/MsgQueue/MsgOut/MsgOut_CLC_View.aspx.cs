﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace RAMWMSHost_Web.MsgQueue.MsgOut {
    public partial class MsgOut_CLC_View : System.Web.UI.Page {

        public DataSet mCLC = null;

        protected void Page_Load(object sender, EventArgs e) {
            int CLCID = Convert.ToInt32(Request["CLCID"]);
            mCLC = BLL.MsgOut.MsgOut_CLC.MsgOut_CLC_Get(CLCID);
        }

    }
}