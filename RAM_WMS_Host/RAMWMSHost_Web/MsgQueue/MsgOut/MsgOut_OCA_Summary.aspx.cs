﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;


namespace RAMWMSHost_Web.MsgQueue.MsgOut {
    public partial class MsgOut_OCA_Summary : System.Web.UI.Page {


        public DataSet mOCA_Summary = null;

        protected void Page_Load(object sender, EventArgs e) {
            mOCA_Summary = BLL.MsgOut.MsgOut_OCA.MsgOut_OCA_Summary();
        }
    }
}