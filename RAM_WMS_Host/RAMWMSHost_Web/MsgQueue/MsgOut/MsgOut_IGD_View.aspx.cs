﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace RAMWMSHost_Web.MsgQueue.MsgOut {
    public partial class MsgOut_IGD_View : System.Web.UI.Page {

        public DataSet mIGD = null;

        protected void Page_Load(object sender, EventArgs e) {
            int IGDID = Convert.ToInt32(Request["IGDID"]);
            mIGD = BLL.MsgOut.MsgOut_IGD.MsgOut_IGD_Get(IGDID);
        }

    }
}