﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace RAMWMSHost_Web.MsgQueue.MsgOut {
    public partial class MsgOut_ORD_View : System.Web.UI.Page {

        public DataSet mORD = null;

        protected void Page_Load(object sender, EventArgs e) {
            int ORDID = Convert.ToInt32(Request["ORDID"]);
            mORD = BLL.MsgOut.MsgOut_ORD.MsgOut_ORD_Get(ORDID);
        }

    }
}