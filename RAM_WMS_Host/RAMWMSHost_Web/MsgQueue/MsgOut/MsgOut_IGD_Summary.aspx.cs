﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;


namespace RAMWMSHost_Web.MsgQueue.MsgOut {
    public partial class MsgOut_IGD_Summary : System.Web.UI.Page {


        public DataSet mIGD_Summary = null;

        protected void Page_Load(object sender, EventArgs e) {
            mIGD_Summary = BLL.MsgOut.MsgOut_IGD.MsgOut_IGD_Summary();
        }
    }
}