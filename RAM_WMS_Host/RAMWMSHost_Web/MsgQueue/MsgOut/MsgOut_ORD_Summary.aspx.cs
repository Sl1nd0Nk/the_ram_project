﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;


namespace RAMWMSHost_Web.MsgQueue.MsgOut {
    public partial class MsgOut_ORD_Summary : System.Web.UI.Page {


        public DataSet mORD_Summary = null;

        protected void Page_Load(object sender, EventArgs e) {
            mORD_Summary = BLL.MsgOut.MsgOut_ORD.MsgOut_ORD_Summary();
        }
    }
}