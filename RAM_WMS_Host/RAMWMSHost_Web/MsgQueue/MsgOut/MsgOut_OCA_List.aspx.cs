﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace RAMWMSHost_Web.MsgQueue.MsgOut {
    public partial class MsgOut_OCA_List : System.Web.UI.Page {

        public string mRecordState = string.Empty;
        public DataSet mOCA_List = null;

        protected void Page_Load(object sender, EventArgs e) {

            mRecordState = Request["RecordState"];
            mOCA_List = BLL.MsgOut.MsgOut_OCA.MsgOut_OCA_List(mRecordState, 20, 1);

        }
    }
}