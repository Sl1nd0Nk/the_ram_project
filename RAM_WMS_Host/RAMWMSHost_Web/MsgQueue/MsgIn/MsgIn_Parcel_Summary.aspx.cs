﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;


namespace RAMWMSHost_Web.MsgQueue.MsgIn {
    public partial class MsgIn_Parcel_Summary : System.Web.UI.Page {


        public DataSet mParcel_Summary = null;

        protected void Page_Load(object sender, EventArgs e) {
            mParcel_Summary = BLL.MsgIn.MsgIn_Parcel.MsgIn_Parcel_Summary();
        }
    }
}