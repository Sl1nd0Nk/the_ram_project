﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;


namespace RAMWMSHost_Web.MsgQueue.MsgIn {
    public partial class MsgIn_SLA_Summary : System.Web.UI.Page {


        public DataSet mSLA_Summary = null;

        protected void Page_Load(object sender, EventArgs e) {
            mSLA_Summary = BLL.MsgIn.MsgIn_SLA.MsgIn_SLA_Summary();
        }
    }
}