﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace RAMWMSHost_Web.MsgQueue.MsgIn {
    public partial class MsgIn_SLA_View : System.Web.UI.Page {

        public DataSet mSLA = null;

        protected void Page_Load(object sender, EventArgs e) {
            int SLAID = Convert.ToInt32(Request["SLAID"]);
            mSLA = BLL.MsgIn.MsgIn_SLA.MsgIn_SLA_Get(SLAID);
        }

    }
}