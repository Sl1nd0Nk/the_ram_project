﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace RAMWMSHost_Web.MsgQueue.MsgIn {
    public partial class MsgIn_OCO_View : System.Web.UI.Page {

        public DataSet mOCO = null;

        protected void Page_Load(object sender, EventArgs e) {
            int OCOID = Convert.ToInt32(Request["OCOID"]);
            mOCO = BLL.MsgIn.MsgIn_OCO.MsgIn_OCO_Get(OCOID);
        }

    }
}