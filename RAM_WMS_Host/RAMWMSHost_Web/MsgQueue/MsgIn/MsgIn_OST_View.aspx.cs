﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace RAMWMSHost_Web.MsgQueue.MsgIn {
    public partial class MsgIn_OST_View : System.Web.UI.Page {

        public DataSet mOST = null;

        protected void Page_Load(object sender, EventArgs e) {
            int OSTID = Convert.ToInt32(Request["OSTID"]);
            mOST = BLL.MsgIn.MsgIn_OST.MsgIn_OST_Get(OSTID);
        }

    }
}