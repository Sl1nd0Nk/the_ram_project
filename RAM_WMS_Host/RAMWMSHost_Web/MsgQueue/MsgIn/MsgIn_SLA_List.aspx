﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MsgIn_SLA_List.aspx.cs" Inherits="RAMWMSHost_Web.MsgQueue.MsgIn.MsgIn_SLA_List" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>SLA List</title>
    <link href="../../Style1.css" rel="stylesheet" />
    <script type="text/javascript">
        function ShowSLA(SLAID) {
            window.open('MsgIn_SLA_View.aspx?SLAID=' + SLAID, '_blank', 'height=500,width=700');
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div   >
            <table border="0">
                <tr>
                    <td colspan="3">
                        <h3>SLA List: <%=mRecordState %></h3>
                    </td>
                </tr>
                <tr>
                    <td style="vertical-align: top">
                        <table class="list">
                            <thead>
                                <tr>
                                    <td>RecordDT</td>
                                    <td>SiteCode</td>
                                    <td>ProdCode</td>
                                    <td>QuantityChange</td>
                                    <td></td>
                                </tr>
                            </thead>
                            <tbody>
                                <%foreach (System.Data.DataRow dtl in mSLA_List.Tables[0].Rows) { %>
                                <tr>
                                    <td><%=dtl["RecordDT"] %></td>
                                    <td><%=dtl["SiteCode"] %></td>
                                    <td><%=dtl["ProdCode"] %></td>
                                    <td><%=dtl["QuantityChange"] %></td>
                                    <td><a target="_self" onclick="ShowSLA('<%=dtl["SLAID"] %>');" href="#">Show</a></td>
                                </tr>
                                <%} %>
                            </tbody>
                        </table>

                    </td>

                </tr>
            </table>
        </div>
        <%-- <div style="border: 1px solid #999999; padding: 2px; position: fixed; background-color: #DFDFDF; font-size: small; top: 0px; right: 0px; color: #666666; text-align: center; vertical-align: middle;"><%=System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") %></div>--%>
    </form>
</body>
</html>
