﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace RAMWMSHost_Web.MsgQueue.MsgIn {
    public partial class MsgIn_SAA_View : System.Web.UI.Page {

        public DataSet mSAA = null;

        protected void Page_Load(object sender, EventArgs e) {
            int SAAID = Convert.ToInt32(Request["SAAID"]);
            mSAA = BLL.MsgIn.MsgIn_SAA.MsgIn_SAA_Get(SAAID);
        }

    }
}