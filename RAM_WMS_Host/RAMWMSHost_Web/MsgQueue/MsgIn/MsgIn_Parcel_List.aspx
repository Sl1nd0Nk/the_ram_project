﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MsgIn_Parcel_List.aspx.cs" Inherits="RAMWMSHost_Web.MsgQueue.MsgIn.MsgIn_Parcel_List" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Parcel List</title>
    <link href="../../Style1.css" rel="stylesheet" />
    <script type="text/javascript">
        function ShowParcel(ParcelID) {
            window.open('MsgIn_Parcel_View.aspx?ParcelID=' + ParcelID, '_blank', 'height=500,width=700');
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table border="0">
                <tr>
                    <td colspan="3">
                        <h3>Parcel List: <%=mRecordState %></h3>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <table>
                            <tr>
                                <td>
                                    <input type="radio" name="FilterType" id="radioOrderNumber" runat="server" style="width: 25px" />
                                </td>
                                <td>OrderNumber</td>
                                <td>
                                    <input type="radio" name="FilterType" id="radioParcelReference" runat="server" style="width: 25px" />
                                </td>
                                <td>ParcelReference</td>
                             
                                <td  >
                                    <input type="text" id="textFilter" runat="server"  style="width: 100px" /></td>
                                <td>
                                    <asp:Button ID="btnFilter" runat="server" Text="Filter" OnClick="btnFilter_Click" /></td>
                            </tr>
                        </table>

                    </td>
                </tr>
                <tr>
                    <td style="vertical-align: top">
                        <table class="list">
                            <thead>
                                <tr>
                                    <td>RecordDT</td>
                                    <td>SiteCode</td>
                                    <td>OrderNumber</td>
                                    <td>ParcelReference</td>
                                    <td>ParcelNo</td>
                                    <td></td>
                                </tr>
                            </thead>
                            <tbody>
                                <%foreach (System.Data.DataRow dtl in mParcel_List.Tables[0].Rows) { %>
                                <tr>
                                    <td><%=dtl["RecordDT"] %></td>
                                    <td><%=dtl["SiteCode"] %></td>
                                    <td><%=dtl["OrderNumber"] %></td>
                                    <td><%=dtl["ParcelReference"] %></td>
                                    <td><%=dtl["ParcelNo"] %>/<%=dtl["NoOfParcels"] %></td>
                                    <td><a target="_self" onclick="ShowParcel('<%=dtl["ParcelID"] %>');" href="#">Show</a></td>
                                </tr>
                                <%} %>
                            </tbody>
                        </table>

                    </td>

                </tr>
            </table>
        </div>
        <%-- <div style="border: 1px solid #999999; padding: 2px; position: fixed; background-color: #DFDFDF; font-size: small; top: 0px; right: 0px; color: #666666; text-align: center; vertical-align: middle;"><%=System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") %></div>--%>
    </form>
</body>
</html>
