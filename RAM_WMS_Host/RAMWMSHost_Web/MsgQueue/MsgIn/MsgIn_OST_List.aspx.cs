﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace RAMWMSHost_Web.MsgQueue.MsgIn {
    public partial class MsgIn_OST_List : System.Web.UI.Page {

        public string mRecordState = string.Empty;
        public DataSet mOST_List = null;

        protected void Page_Load(object sender, EventArgs e) {

            mRecordState = Request["RecordState"];
            mOST_List = BLL.MsgIn.MsgIn_OST.MsgIn_OST_List(mRecordState, 20, 1);

        }
    }
}