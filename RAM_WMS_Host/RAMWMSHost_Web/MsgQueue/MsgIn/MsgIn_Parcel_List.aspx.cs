﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace RAMWMSHost_Web.MsgQueue.MsgIn {
    public partial class MsgIn_Parcel_List : System.Web.UI.Page {

        public string mRecordState = string.Empty;
        public DataSet mParcel_List = null;

        protected void Page_Load(object sender, EventArgs e) {

            if (!radioOrderNumber.Checked & !radioParcelReference.Checked) radioOrderNumber.Checked = true;
            Refresh(string.Empty, string.Empty);

        }

        protected void btnFilter_Click(object sender, EventArgs e) {
            string orderNumber = string.Empty;
            string parcelReference = string.Empty;

            if (radioOrderNumber.Checked) orderNumber = textFilter.Value.Trim();
            else if (radioParcelReference.Checked) parcelReference = textFilter.Value.Trim();

            Refresh(orderNumber, parcelReference);

        }

        private void Refresh(string orderNumber, string parcelReference) {
            mRecordState = Request["RecordState"];
            mParcel_List = BLL.MsgIn.MsgIn_Parcel.MsgIn_Parcel_List(mRecordState, 20, 1, orderNumber, parcelReference);
        }

    }
}