﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace RAMWMSHost_Web.MsgQueue.MsgIn {
    public partial class MsgIn_Parcel_View : System.Web.UI.Page {

        public DataSet mParcel = null;

        protected void Page_Load(object sender, EventArgs e) {
            int ParcelID = Convert.ToInt32(Request["ParcelID"]);
            mParcel = BLL.MsgIn.MsgIn_Parcel.MsgIn_Parcel_Get(ParcelID);
        }

    }
}