﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace RAMWMSHost_Web.MsgQueue.MsgIn {
    public partial class MsgIn_ARR_List : System.Web.UI.Page {

        public string mRecordState = string.Empty;
        public DataSet mARR_List = null;

        protected void Page_Load(object sender, EventArgs e) {

            mRecordState = Request["RecordState"]; 
            mARR_List = BLL.MsgIn.MsgIn_ARR.MsgIn_ARR_List(mRecordState, 20, 1);

        }
    }
}