﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.ServiceModel;
using RAMWMSHost_MsgTransfer_Interface;
using System.ServiceProcess;


namespace RAMWMSHost_Web.MsgTransferSvcControl {
    public partial class SvcControl : System.Web.UI.Page {
        protected void Page_Load(object sender, EventArgs e) {
            if (Application["SvcProcessMessages"] == null) {
                Application["SvcProcessMessages"] = new List<ProcessMessage>();
            }
                RefreshState(); 
        }

        private void RefreshState() {
            if (Application["SvcProcessMessages"] == null) {
                Application["SvcProcessMessages"] = new List<ProcessMessage>();
            }
            List<ProcessMessage> cachedMsgs = (List<ProcessMessage>)Application["SvcProcessMessages"];
            string statusHTML = string.Empty;

            try {
                using (ChannelFactory<IServiceControl> serverControlFactory = new ChannelFactory<IServiceControl>("RAMWMSHost_MsgTransfer.ServiceControl")) {

                    statusHTML = "<u><b>Service details</b></u><br/>" +
                                    "&emsp;<b>" + mServiceName + "</b><br/>" +
                                    "&emsp;<b>Address:</b><br/>&emsp;&emsp;" + serverControlFactory.Endpoint.Address.ToString() + "<br/>";

                    serverControlFactory.Credentials.Windows.ClientCredential = new System.Net.NetworkCredential(BLL.Config.WcfCredentials_Username, BLL.Config.WcfCredentials_Password, BLL.Config.WcfCredentials_Domain);


                    statusHTML += "&emsp;<b>UserName:</b> " + serverControlFactory.Credentials.Windows.ClientCredential.Domain + @"\" + serverControlFactory.Credentials.Windows.ClientCredential.UserName + "<br/>";


                    IServiceControl serverControl = serverControlFactory.CreateChannel();


                    DateTime serviceStartTime = serverControl.GetServiceStartTime();
                    DateTime serviceLastActivity = serverControl.GetServiceLastActivity();

                    string syncStatus = serverControl.GetSyncStatus();
                    bool syncBusy = serverControl.GetSyncBusy();
                    bool syncStartRequested = serverControl.GetSyncStartRequested();

                    int secondsToNextSync = serverControl.GetSecondsToNextSync();
                    int secondsFromSyncStart = serverControl.GetSecondsFromSyncStart();
                    int secondsFromSyncLastActivity = serverControl.GetSecondsFromSyncLastActivity();

                    List<ProcessMessage> msgs = serverControl.GetMessages();

                    statusHTML += "&emsp;<b>StartTime:</b> " + serviceStartTime.ToString("yyyy-MM-dd HH:mm:ss") + "<br/>" +
                                    "&emsp;<b>LastActivity:</b> " + serviceLastActivity.ToString("yyyy-MM-dd HH:mm:ss") + "<br/>" +
                                "<u><b>Sync details</b></u><br/>" +
                                    "&emsp;<b>Status:</b> " + syncStatus.ToString() + "<br/>" +
                                    "&emsp;<b>Busy:</b> " + syncBusy.ToString() + "<br/>" +
                                    "&emsp;<b>StartRequested:</b> " + syncStartRequested.ToString() + "<br/>" +
                                    "&emsp;<b>TimeToNextSync:</b> " + secondsToNextSync.ToString() + "<br/>" +
                                    "&emsp;<b>Duration:</b> " + secondsFromSyncStart.ToString() + "<br/>" +
                                    "&emsp;<b>LastActivity:</b> " + secondsFromSyncLastActivity.ToString();

                    foreach (ProcessMessage msg in msgs) {
                        cachedMsgs.Add(msg);
                    }
                }


            } catch (System.TimeoutException e ) {
                statusHTML = LogError(cachedMsgs, statusHTML, e);
            } catch (System.ServiceModel.CommunicationException e ) {
                statusHTML = LogError(cachedMsgs, statusHTML, e);
            }  catch (System.ServiceProcess.TimeoutException e) {
                statusHTML = LogError(cachedMsgs, statusHTML, e);
            } catch (ObjectDisposedException e) {
                statusHTML = LogError(cachedMsgs, statusHTML, e);
            } catch (Exception e) {
                statusHTML = LogError(cachedMsgs, statusHTML, e);
            }
             


            divStatus.InnerHtml = statusHTML;

            ClearLog();
            foreach (ProcessMessage msg in cachedMsgs) {
                LogAdd(msg);
            }


            while (cachedMsgs.Count > 30) cachedMsgs.RemoveAt(0);

        }

        private static string LogError(List<ProcessMessage> cachedMsgs,  string statusHTML,  Exception e) {
            cachedMsgs.Add(new ProcessMessage("Exception:" + e.Message, "RefreshState", 20011));
            if (e.InnerException != null) cachedMsgs.Add(new ProcessMessage("InnerException:" + e.InnerException.Message, "RefreshState", 20012));

            statusHTML += "&emsp;<b>" + e.GetType().ToString() + ":</b><br/>&emsp;&emsp;" + e.Message + "<br/>";
            if (e.InnerException != null) statusHTML += "&emsp;<b>InnerException:</b><br/>&emsp;&emsp;" + e.InnerException.Message + "<br/>";
            return statusHTML;
        }


        #region service control

        private string mServiceName = "RAMWMSHost_MsgTransfer";

        private bool ServiceExists() {
            bool serviceExists = ServiceController.GetServices().Any(s => s.ServiceName == mServiceName);
            return serviceExists;
        }

        private bool ServiceIsRunning() {
            bool found = false;
            foreach (System.Diagnostics.Process process in System.Diagnostics.Process.GetProcesses()) {
                if (process.ProcessName == mServiceName) {
                    found = true;
                }
            }
            return found;
        }

        //public void StopService(int timeoutMilliseconds) {
        //    LogAdd("Stopping " + mServiceName, 10801);
        //    try {
        //        ServiceController service = new ServiceController(mServiceName);
        //        TimeSpan timeout = TimeSpan.FromMilliseconds(timeoutMilliseconds);

        //        service.Stop();
        //        service.WaitForStatus(ServiceControllerStatus.Stopped, timeout);
        //        LogAdd("Stopped " + mServiceName, 10802);
        //    } catch (Exception exc) {
        //        LogAdd(exc.Message, exc.StackTrace, 10803, true);
        //    }
        //}

        //public void StartService(int timeoutMilliseconds) {
        //    LogAdd("Starting " + mServiceName, 10901);
        //    try {
        //        ServiceController service = new ServiceController(mServiceName);
        //        TimeSpan timeout = TimeSpan.FromMilliseconds(timeoutMilliseconds);

        //        service.Start();
        //        service.WaitForStatus(ServiceControllerStatus.Running, timeout);
        //        LogAdd("Started " + mServiceName, 10902);
        //    } catch (Exception exc) {
        //        LogAdd(exc.Message, exc.StackTrace, 10903, true);
        //    }
        //}

        private void StartSync ( ) {
                List<ProcessMessage> cachedMsgs = (List<ProcessMessage>)Application["SvcProcessMessages"];
            //LogAdd("Request Start Sync", "green");

                cachedMsgs.Add(new ProcessMessage("Request Start Sync", "StartSync", 20011));

            try {
                using (ChannelFactory<IServiceControl> serverControlFactory = new ChannelFactory<IServiceControl>("RAMWMSHost_MsgTransfer.ServiceControl")) {
                     serverControlFactory.Credentials.Windows.ClientCredential = new System.Net.NetworkCredential(BLL.Config.WcfCredentials_Username, BLL.Config.WcfCredentials_Password, BLL.Config.WcfCredentials_Domain);

                    IServiceControl serverControl = serverControlFactory.CreateChannel();
                    serverControl.RequestSyncStart();
                }
            } catch (Exception exc) {
                //LogAdd(exc.Message,  "red");
                //LogAdd( exc.StackTrace, "red");


                cachedMsgs.Add(new ProcessMessage("StartSync Exception:" + exc.Message, "StartSync", 20011));
                if (exc.InnerException != null) cachedMsgs.Add(new ProcessMessage("StartSync InnerException:" + exc.InnerException.Message, "StartSync", 20012));

            }
                ClearLog();
                foreach (ProcessMessage msg in cachedMsgs) {
                    LogAdd(msg);
                }
        }
 

        private void LogAdd(ProcessMessage msg) {
            if (msg.IsError) {
                divLog.InnerHtml = "<span style=\"color:red\">" + msg.EventDT.ToString("HH:mm:ss.fff") + " " + msg.MessageText + " " + msg.Details + "</span><br />" + divLog.InnerHtml;
            } else {
                divLog.InnerHtml = msg.EventDT.ToString("HH:mm:ss.fff") + " " + msg.MessageText + " " + msg.Details + "<br />" + divLog.InnerHtml;
            }
        }


        private void LogAdd(string msg ) {
            LogAdd(msg, string.Empty);
        }

        private void LogAdd(string msg,  string  color ) {
            if (color.Length > 0) {
                divLog.InnerHtml =   "<span style=\"color:" + color + "\">" + msg + "</span><br />" + divLog.InnerHtml ;
            } else {
                divLog.InnerHtml = msg + "<br />" + divLog.InnerHtml;
            }
        }
        private void ClearLog ( ) {
            divLog.InnerHtml = "";
        }
 

        #endregion

 
        protected void btnStartSync_Click(object sender, EventArgs e) {
            StartSync(); 
        }
 


    }
}