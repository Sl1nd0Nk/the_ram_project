﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Text;


namespace RAMWMSHost_Web.BLL {
    public class RAMTnT {


        public static DataSet usp_ParcelV1_Info_WMSImport(string CartonID) {
            //ConsignmentID TrackingNo Kilograms Length Breadth Height InsuredValue 
            //SecurityPackNo ParcelReference EventDateTime EmployeeID EmployeeName ParcelNo 
            //ParcelState (NEW/DIMMED) ImportState (IMPORTED/NOTFOUND) AltTrackingNo AltCartonID  Prints AllowZeroWeight AllowZeroDims 
            //ConsignmentState (CONSIGNED/NOTFOUND)
            string sql = @"usp_ParcelV1_Info_WMSImport";
            Database db = new DatabaseProviderFactory().Create("RAMTnTv1");
            DbCommand dbCmd = db.GetStoredProcCommand(sql);
            db.AddInParameter(dbCmd, "@CartonID", DbType.String, CartonID);
            DataSet ds = db.ExecuteDataSet(dbCmd);
            dbCmd.Dispose(); dbCmd = null; db = null;
            return ds;
        }

        public static DataSet usp_ParcelV1_Save(string ConsignmentID,
                                                string TrackingNo,
                                                double Kilograms,
                                                double Length,
                                                double Breadth,
                                                double Height,
                                                Boolean UpdateWeights,
                                                string HubID,
                                                string EmployeeID,
                                                string TerminalID,
                                                string SecurityPackNo,
                                                Boolean PreserveConsignDT,
                                                string ParcelReference,
                                                double InsuredValue,
                                                Boolean AllowUnDimmed) {
            //Saved, ConsignmentID, TrackingNo, Details;
            string sql = @"usp_ParcelV1_Save";
            Database db = new DatabaseProviderFactory().Create("RAMTnTv1");
            DbCommand dbCmd = db.GetStoredProcCommand(sql);
            db.AddInParameter(dbCmd, "@ConsignmentID", DbType.String, ConsignmentID);
            db.AddInParameter(dbCmd, "@TrackingNo", DbType.String, TrackingNo);
            db.AddInParameter(dbCmd, "@Kilograms", DbType.Double, Kilograms);
            db.AddInParameter(dbCmd, "@Length", DbType.Double, Length);
            db.AddInParameter(dbCmd, "@Breadth", DbType.Double, Breadth);
            db.AddInParameter(dbCmd, "@Height", DbType.Double, Height);
            db.AddInParameter(dbCmd, "@UpdateWeights", DbType.Boolean, UpdateWeights);
            db.AddInParameter(dbCmd, "@HubID", DbType.String, HubID);
            db.AddInParameter(dbCmd, "@EmployeeID", DbType.String, EmployeeID);
            db.AddInParameter(dbCmd, "@TerminalID", DbType.String, TerminalID);
            db.AddInParameter(dbCmd, "@SecurityPackNo", DbType.String, SecurityPackNo);
            db.AddInParameter(dbCmd, "@PreserveConsignDT", DbType.Boolean, PreserveConsignDT);
            db.AddInParameter(dbCmd, "@ParcelReference", DbType.String, ParcelReference);
            db.AddInParameter(dbCmd, "@InsuredValue", DbType.Double, InsuredValue);
            db.AddInParameter(dbCmd, "@AllowUnDimmed", DbType.Boolean, AllowUnDimmed);
            DataSet ds = db.ExecuteDataSet(dbCmd);
            dbCmd.Dispose(); dbCmd = null; db = null;
            return ds;
        }

        //public static string usp_LabelPrintTextZPL_Vodacom(string TrackingNo) {
        //    string sql = @"usp_LabelPrintTextZPL_Vodacom";
        //    Database db = new DatabaseProviderFactory().Create("RAMTnTv1");
        //    DbCommand dbCmd = db.GetStoredProcCommand(sql);
        //    db.AddInParameter(dbCmd, "@TrackingNo", DbType.String, TrackingNo);
        //    string label = db.ExecuteScalar(dbCmd).ToString();
        //    dbCmd.Dispose(); dbCmd = null; db = null;
        //    return label;
        //}

        //public static string usp_LabelPrintTextEPL(string TrackingNo) {
        //    string sql = @"usp_LabelPrintTextEPL";
        //    Database db = new DatabaseProviderFactory().Create("RAMTnTv1");
        //    DbCommand dbCmd = db.GetStoredProcCommand(sql);
        //    db.AddInParameter(dbCmd, "@TrackingNo", DbType.String, TrackingNo);
        //    string label = db.ExecuteScalar(dbCmd).ToString();
        //    dbCmd.Dispose(); dbCmd = null; db = null;
        //    return label;
        //}


        public static byte[] usp_LabelPrintTextEPL(string TrackingNo) {
            string sql = @"usp_LabelPrintTextEPL";
            Database db = new DatabaseProviderFactory().Create("RAMTnTv1");
            DbCommand dbCmd = db.GetStoredProcCommand(sql);
            db.AddInParameter(dbCmd, "@TrackingNo", DbType.String, TrackingNo);
            string eplText = db.ExecuteScalar(dbCmd).ToString();
            dbCmd.Dispose(); dbCmd = null; db = null;
        
            //get the bytes and use only the first byte of each pair
            byte[] eplBytes = Encoding.Unicode.GetBytes(eplText);
            //only use first byte of evey pair
            byte[] eplBytes2 = new byte[eplBytes.Length / 2];
            for (int i = 0; i < eplBytes2.Length; i++) {
                eplBytes2[i] = eplBytes[i * 2];
            }
            return eplBytes2;  
        }
         

        public static bool usp_PrintLog_Label(string ConsignmentID, string TrackingNo, string RecordSource, string TerminalID) {
            string sql = @"usp_PrintLog";
            Database db = new DatabaseProviderFactory().Create("RAMTnTv1");
            DbCommand dbCmd = db.GetStoredProcCommand(sql);
            db.AddInParameter(dbCmd, "@PrintType", DbType.String, "LABEL");
            db.AddInParameter(dbCmd, "@ItemID1", DbType.String, ConsignmentID);
            db.AddInParameter(dbCmd, "@ItemID2", DbType.String, TrackingNo);
            db.AddInParameter(dbCmd, "@EmployeeID", DbType.String, RecordSource);
            db.AddInParameter(dbCmd, "@HubID", DbType.String, RecordSource);
            db.AddInParameter(dbCmd, "@TerminalID", DbType.String, TerminalID);
            bool saved = db.ExecuteNonQuery(dbCmd) == 1; 
            dbCmd.Dispose(); dbCmd = null; db = null;
            return saved;
        }

        public static string GenerateZplLabel(string trackingNo, bool isReprint, string employeeNo)
        {
            string sql = string.Format("select dbo.udf_Label_ZPL('{0}', {1}, '{2}') as zpl",
                trackingNo, isReprint ? 1 : 0, employeeNo);

            Database db = new DatabaseProviderFactory().Create("RAMTnTv1");
            DbCommand dbCmd = db.GetSqlStringCommand(sql);
            string zplText = db.ExecuteScalar(dbCmd).ToString();
            dbCmd.Dispose(); dbCmd = null; db = null;

            //get the bytes and use only the first byte of each pair
            /*byte[] zplBytes = Encoding.Unicode.GetBytes(zplText);
            //only use first byte of evey pair
            byte[] zplBytes2 = new byte[zplBytes.Length / 2];
            for (int i = 0; i < zplBytes2.Length; i++)
            {
                zplBytes2[i] = zplBytes[i * 2];
            }
            return zplBytes2;*/
            return zplText;
        }
    }
}
