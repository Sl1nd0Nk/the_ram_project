﻿using System; 
using System.Configuration;
using System.Collections.Generic;

namespace RAMWMSHost_Web.BLL {
    class Config {
 

        public static string WcfCredentials_Username { get { return GetSetting ("WcfCredentials_Username"); } } 
        public static string WcfCredentials_Password { get { return GetSetting ("WcfCredentials_Password"); } } 
        public static string WcfCredentials_Domain { get { return GetSetting ("WcfCredentials_Domain"); } } 

 
 
         #region GetSettings
        private static string GetSetting(string SettingName) {
            try {
                /** Deprecated return ConfigurationSettings.AppSettings[SettingName]; **/
                return ConfigurationManager.AppSettings[SettingName];
            } catch {
                return string.Empty;
            }
        }

        private static List<string> GetSettingList(string SettingName) {
            try {
                string list = GetSetting(SettingName);
                List<string> result = new List<string>();
                foreach (string item in list.Split(new char[] { ","[0], "|"[0] })) {
                    result.Add(item);
                }
                return result;
            } catch {
                return new List<string>();
            }
        }

        private static int GetSettingInt(string settingName, int defaultValue) {
            try {
                return Convert.ToInt32(GetSetting(settingName));
            } catch {
                return defaultValue;
            }
        }
        private static int GetSettingInt(string settingName) {
            return GetSettingInt(settingName, 0);
        }
        private static bool GetSettingBool(string SettingName) {
            try {
                return Convert.ToBoolean(GetSetting(SettingName));
            } catch {
                return false;
            }
        }
        private static DateTime GetSettingDateTime(string SettingName) {
            try {
                return Convert.ToDateTime(GetSetting(SettingName));
            } catch {
                return DateTime.Now;
            }
        }
        #endregion

    }
}
