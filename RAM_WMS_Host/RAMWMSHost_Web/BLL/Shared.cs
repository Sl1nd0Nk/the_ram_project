﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace RAMWMSHost_Web.BLL {
    public class Shared {
 

        public static DataSet Hubs_List() {
            //Database provider factory not set for the static DatabaseFactory. 
            //Set a provider factory invoking the DatabaseFactory.SetProviderFactory method or by specifying 
            //custom mappings by calling the DatabaseFactory.SetDatabases method.

            
            //HubID, Description, TelephoneNo, StreetAddress1, StreetAddress2, ZoneID, 
            //DirtyID, EventDateTime, EmployeeID, TerminalID, LastUpdate, isDirty, rowguid, HubType, 
            //GEOID, Lat, Lon, CanFly, AirportName, TelephoneNo2, FaxNo, EmailAddress, BranchManager, IsActive, OpenedDT, ClosedDT
            string sql = @"select * from dbo.Hubs order by HubID";
            //Database db = DatabaseFactory.CreateDatabase();
            Database db = new DatabaseProviderFactory().Create("RAMWMSHost");
            DbCommand dbCmd = db.GetSqlStringCommand(sql);
            //db.AddInParameter(dbCmd, "@MsgType", DbType.String, MsgType);
            //db.AddInParameter(dbCmd, "@MovementType", DbType.String, "InOculus");
            DataSet ds = db.ExecuteDataSet(dbCmd);
            dbCmd.Dispose(); dbCmd = null; db = null;
            return ds;
        }

        public static string MiddlePackNo_from_CaseNo(string CaseNo) {
            string sql = @"usp_Admin_MiddlePackNo_from_CaseNo"; 
            Database db = new DatabaseProviderFactory().Create("RAMWMSHost");
            DbCommand dbCmd = db.GetStoredProcCommand(sql);
            db.AddInParameter(dbCmd, "@CaseNo", DbType.String, CaseNo); 
            string packNo = db.ExecuteScalar(dbCmd).ToString();
            dbCmd.Dispose(); dbCmd = null; db = null;
            return packNo;
        }
 

    }
}