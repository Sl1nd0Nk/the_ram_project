﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace RAMWMSHost_Web.BLL.MsgOut {
    public class MsgOut_IGD {
        
        public static string MsgType = "MsgOut_IGD";

        public static DataSet MsgOut_IGD_Summary() {
            //RecordState	SortOrder	IsError	RecordCount	LastRecordDT
            return Msg.Msg_Summary(MsgType);
        }

        public static DataSet MsgOut_IGD_List(string RecordState, int PageSize, int PageNo) {
            //RowNumber,
            //IGDID, RecordDT, RecordState, RecordSource, ProcAttempts, ProcStartDT, ProcEndDT, ProcErrors, 
            //SiteCode, PrincipalCode, ProdCode, EANCode, ShortDesc, LongDesc, Serialised, AnalysisA, AnalysisB, 
            //OrderLineNo, Quantity, ReceiptType, MoveRef, PORef, PODate, StockDateTime, rowguid
            //PageSize	PageNo	TotalCount
            return Msg.Msg_List(MsgType, RecordState, PageSize, PageNo);
        }

        public static DataSet MsgOut_IGD_Get(int IGDID) {
            //IGDID, RecordDT, RecordState, RecordSource, ProcAttempts, ProcStartDT, ProcEndDT, ProcErrors, 
            //SiteCode, PrincipalCode, ProdCode, EANCode, ShortDesc, LongDesc, Serialised, AnalysisA, AnalysisB, 
            //OrderLineNo, Quantity, ReceiptType, MoveRef, PORef, PODate, StockDateTime, rowguid
            return Msg.Msg_Get(MsgType, IGDID);
        }

        public static int MsgOut_IGD_Add(string RecordSource,
                                            string SiteCode,
                                            string PrincipalCode,

                                            string ProdCode,
                                            string EANCode,
                                            string ShortDesc,
                                            string LongDesc,
                                            bool Serialised,
                                            string AnalysisA,
                                            string AnalysisB,
                                            string OrderLineNo,
                                            int Quantity,
                                            string ReceiptType,
                                            string MoveRef,
                                            string PORef,
                                            DateTime PODate,
                                            DateTime StockDateTime) {
            string sql = @"usp_MsgOut_IGD_Add";
            Database db = new DatabaseProviderFactory().Create("RAMWMSHost");
            DbCommand dbCmd = db.GetStoredProcCommand(sql);
            db.AddOutParameter(dbCmd, "@IGDID", DbType.Int32, 16);
            db.AddInParameter(dbCmd, "@RecordSource", DbType.String, RecordSource);
            db.AddInParameter(dbCmd, "@SiteCode", DbType.String, SiteCode);
            db.AddInParameter(dbCmd, "@PrincipalCode", DbType.String, PrincipalCode);

            db.AddInParameter(dbCmd, "@ProdCode", DbType.String, ProdCode);
            db.AddInParameter(dbCmd, "@EANCode", DbType.String, EANCode);
            db.AddInParameter(dbCmd, "@ShortDesc", DbType.String, ShortDesc);
            db.AddInParameter(dbCmd, "@LongDesc", DbType.String, LongDesc);
            db.AddInParameter(dbCmd, "@Serialised", DbType.Boolean, Serialised);
            db.AddInParameter(dbCmd, "@AnalysisA", DbType.String, AnalysisA);
            db.AddInParameter(dbCmd, "@AnalysisB", DbType.String, AnalysisB);
            db.AddInParameter(dbCmd, "@OrderLineNo", DbType.String, OrderLineNo);
            db.AddInParameter(dbCmd, "@Quantity", DbType.Int32, Quantity);
            db.AddInParameter(dbCmd, "@ReceiptType", DbType.String, ReceiptType);
            db.AddInParameter(dbCmd, "@MoveRef", DbType.String, MoveRef);
            db.AddInParameter(dbCmd, "@PORef", DbType.String, PORef);
            db.AddInParameter(dbCmd, "@PODate", DbType.DateTime, PODate);
            db.AddInParameter(dbCmd, "@StockDateTime", DbType.DateTime, StockDateTime);

            int rowsAffected = Convert.ToInt32(db.ExecuteNonQuery(dbCmd));
            int IGDID = Convert.ToInt32(dbCmd.Parameters["@IGDID"].Value);
            dbCmd.Dispose(); dbCmd = null; db = null;
            return IGDID;
        }


    }
}
