﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace RAMWMSHost_Web.BLL.MsgOut {
    public class MsgOut_ORD {
         
        public static string MsgType = "MsgOut_ORD"; 

        public static DataSet MsgOut_ORD_Summary() {
            //RecordState	SortOrder	IsError	RecordCount	LastRecordDT
            return Msg.Msg_Summary(MsgType);
        }

        //public static DataSet MsgOut_ORD_List(string RecordState, int PageSize, int PageNo, string orderNumber) {
        //    //RowNumber,
        //    //ORDID, RecordDT, RecordState, RecordSource, ProcAttempts, ProcStartDT, ProcEndDT, ProcErrors, 
        //    //SiteCode, PrincipalCode, OrderNumber, LineCount, DateCreated, CompanyName, HeaderComment, CustOrderNumber, CustAcc, 
        //    //InvName, InvAdd1, InvAdd2, InvAdd3, InvAdd4, InvAdd5, InvAdd6, GSMTest, VATNumber, PrintPrice, Priority, VAP, SalesPerson, SalesCategory, Processor, 
        //    //DeliveryAdd1, DeliveryAdd2, DeliveryAdd3, DeliveryAdd4, DeliveryAdd5, DeliveryAdd6, WMSPostCode, OrderDiscount, OrderVAT, rowguid
        //    //PageSize	PageNo	TotalCount
        //    return Msg.Msg_List(MsgType, RecordState, PageSize, PageNo, orderNumber);
        //}

        public static DataSet MsgOut_ORD_List( string RecordState, int PageSize, int PageNo, string orderNumber) {
            string sql = @"usp_MsgOut_ORD_List";
            Database db = new DatabaseProviderFactory().Create("RAMWMSHost");
            DbCommand dbCmd = db.GetStoredProcCommand(sql);
            db.AddInParameter(dbCmd, "@RecordState", DbType.String, RecordState);
            db.AddInParameter(dbCmd, "@PageSize", DbType.Int32, PageSize);
            db.AddInParameter(dbCmd, "@PageNo", DbType.Int32, PageNo);
            db.AddInParameter(dbCmd, "@OrderNumber", DbType.String, orderNumber);
            DataSet ds = db.ExecuteDataSet(dbCmd);
            dbCmd.Dispose(); dbCmd = null; db = null;
            return ds;
        }


        public static DataSet MsgOut_ORD_Get(int ORDID) {
            //Table 1
            //ORDID	RecordDT	RecordState	RecordSource	ProcAttempts	ProcStartDT	ProcEndDT	ProcErrors	
            //SiteCode	PrincipalCode	OrderNumber	LineCount	DateCreated	CompanyName	HeaderComment	CustOrderNumber	
            //CustAcc	InvName	InvAdd1	InvAdd2	InvAdd3	InvAdd4	InvAdd5	InvAdd6	GSMTest	VATNumber	PrintPrice	Priority	
            //VAP	SalesPerson	SalesCategory	Processor	DeliveryAdd1	DeliveryAdd2	DeliveryAdd3	DeliveryAdd4	DeliveryAdd5	DeliveryAdd6	WMSPostCode	
            //OrderDiscount	OrderVAT	rowguid
            //Table 2
            //ORD_LNID	ORDID	LineNumber	LineType	ProdCode	LineText	Quantity	UnitCost	Vat	Discount	rowguid
            return Msg.Msg_Get(MsgType, ORDID);
        }
         
        public static int MsgOut_ORD_SetREADY(int ORDID) {
            return Msg.Msg_SetREADY(MsgType, ORDID);
        }
         
        public static int MsgOut_ORD_LN_Add(int ORDID,
                                            int LineNumber,

                                            string LineType,
                                            string ProdCode,
                                            string LineText,
                                            int Quantity,
                                            double UnitCost,
                                            double Vat,
                                            double Discount) {
            string sql = @"usp_MsgOut_ORD_LN_Add";
            Database db = new DatabaseProviderFactory().Create("RAMWMSHost");
            DbCommand dbCmd = db.GetStoredProcCommand(sql);
            db.AddInParameter(dbCmd, "@ORDID", DbType.Int32, ORDID);
            db.AddInParameter(dbCmd, "@LineNumber", DbType.Int32, LineNumber);

            db.AddInParameter(dbCmd, "@LineType", DbType.String, LineType);
            db.AddInParameter(dbCmd, "@ProdCode", DbType.String, ProdCode);
            db.AddInParameter(dbCmd, "@LineText", DbType.String, LineText);
            db.AddInParameter(dbCmd, "@Quantity", DbType.Int32, Quantity);
            db.AddInParameter(dbCmd, "@UnitCost", DbType.Double, UnitCost);
            db.AddInParameter(dbCmd, "@Vat", DbType.Double, Vat);
            db.AddInParameter(dbCmd, "@Discount", DbType.Double, Discount);

            int rowsAffected = Convert.ToInt32(db.ExecuteNonQuery(dbCmd));
            dbCmd.Dispose(); dbCmd = null; db = null;
            return rowsAffected;
        } 

        public static int MsgOut_ORD_Add(string RecordSource,
                                        string SiteCode,
                                        string PrincipalCode,

                                        string OrderNumber,
                                        int LineCount,
                                        DateTime DateCreated,
                                        string CompanyName,
                                        string HeaderComment,
                                        string CustOrderNumber,
                                        string CustAcc,
                                        string InvName,
                                        string InvAdd1,
                                        string InvAdd2,
                                        string InvAdd3,
                                        string InvAdd4,
                                        string InvAdd5,
                                        string InvAdd6,
                                        bool GSMTest,
                                        string VATNumber,
                                        bool PrintPrice,
                                        string Priority,
                                        bool VAP,
                                        string SalesPerson,
                                        string SalesCategory,
                                        string Processor,
                                        string DeliveryAdd1,
                                        string DeliveryAdd2,
                                        string DeliveryAdd3,
                                        string DeliveryAdd4,
                                        string DeliveryAdd5,
                                        string DeliveryAdd6,
                                        string WMSPostCode,
                                        double OrderDiscount,
                                        double OrderVAT,
                                        int DocToPrint,
                                        string IDNumber,
                                        string KYC,
                                        string CourierName,
                                        string CourierService,
                                        bool InsuranceRequired,
                                        bool ValidateDelivery,
                                        string StoreCode) {
            string sql = @"usp_MsgOut_ORD_Add";
            Database db = new DatabaseProviderFactory().Create("RAMWMSHost");
            DbCommand dbCmd = db.GetStoredProcCommand(sql);
            db.AddOutParameter(dbCmd, "@ORDID", DbType.Int32, 16);
            db.AddInParameter(dbCmd, "@RecordSource", DbType.String, RecordSource);
            db.AddInParameter(dbCmd, "@SiteCode", DbType.String, SiteCode);
            db.AddInParameter(dbCmd, "@PrincipalCode", DbType.String, PrincipalCode);

            db.AddInParameter(dbCmd, "@OrderNumber", DbType.String, OrderNumber);
            db.AddInParameter(dbCmd, "@LineCount", DbType.Int32, LineCount);
            db.AddInParameter(dbCmd, "@DateCreated", DbType.DateTime, DateCreated);
            db.AddInParameter(dbCmd, "@CompanyName", DbType.String, CompanyName);
            db.AddInParameter(dbCmd, "@HeaderComment", DbType.String, HeaderComment);
            db.AddInParameter(dbCmd, "@CustOrderNumber", DbType.String, CustOrderNumber);
            db.AddInParameter(dbCmd, "@CustAcc", DbType.String, CustAcc);
            db.AddInParameter(dbCmd, "@InvName", DbType.String, InvName); 
            db.AddInParameter(dbCmd, "@InvAdd1", DbType.String, InvAdd1);
            db.AddInParameter(dbCmd, "@InvAdd2", DbType.String, InvAdd2);
            db.AddInParameter(dbCmd, "@InvAdd3", DbType.String, InvAdd3);
            db.AddInParameter(dbCmd, "@InvAdd4", DbType.String, InvAdd4);
            db.AddInParameter(dbCmd, "@InvAdd5", DbType.String, InvAdd5);
            db.AddInParameter(dbCmd, "@InvAdd6", DbType.String, InvAdd6);
            db.AddInParameter(dbCmd, "@GSMTest", DbType.Boolean, GSMTest);
            db.AddInParameter(dbCmd, "@VATNumber", DbType.String, VATNumber);
            db.AddInParameter(dbCmd, "@PrintPrice", DbType.Boolean, PrintPrice);
            db.AddInParameter(dbCmd, "@Priority", DbType.String, Priority);
            db.AddInParameter(dbCmd, "@VAP", DbType.Boolean, VAP);
            db.AddInParameter(dbCmd, "@SalesPerson", DbType.String, SalesPerson);
            db.AddInParameter(dbCmd, "@SalesCategory", DbType.String, SalesCategory);
            db.AddInParameter(dbCmd, "@Processor", DbType.String, Processor);
            db.AddInParameter(dbCmd, "@DeliveryAdd1", DbType.String, DeliveryAdd1);
            db.AddInParameter(dbCmd, "@DeliveryAdd2", DbType.String, DeliveryAdd2);
            db.AddInParameter(dbCmd, "@DeliveryAdd3", DbType.String, DeliveryAdd3);
            db.AddInParameter(dbCmd, "@DeliveryAdd4", DbType.String, DeliveryAdd4);
            db.AddInParameter(dbCmd, "@DeliveryAdd5", DbType.String, DeliveryAdd5);
            db.AddInParameter(dbCmd, "@DeliveryAdd6", DbType.String, DeliveryAdd6);
            db.AddInParameter(dbCmd, "@WMSPostCode", DbType.String, WMSPostCode);
            db.AddInParameter(dbCmd, "@OrderDiscount", DbType.Double, OrderDiscount);
            db.AddInParameter(dbCmd, "@OrderVAT", DbType.Double, OrderVAT);
            db.AddInParameter(dbCmd, "@DocToPrint", DbType.Int32, DocToPrint);
            db.AddInParameter(dbCmd, "@IDNumber", DbType.String, IDNumber);
            db.AddInParameter(dbCmd, "@KYC", DbType.String, KYC);
            db.AddInParameter(dbCmd, "@CourierName", DbType.String, CourierName);
            db.AddInParameter(dbCmd, "@CourierService", DbType.String, CourierService);
            db.AddInParameter(dbCmd, "@InsuranceRequired", DbType.Boolean, InsuranceRequired);
            db.AddInParameter(dbCmd, "@ValidateDelivery", DbType.Boolean, ValidateDelivery);
            db.AddInParameter(dbCmd, "@StoreCode", DbType.String, StoreCode);

            int rowsAffected = Convert.ToInt32(db.ExecuteNonQuery(dbCmd));
            int ORDID = Convert.ToInt32(dbCmd.Parameters["@ORDID"].Value);
            dbCmd.Dispose(); dbCmd = null; db = null;
            return ORDID;
        }
 

    }

    public class MsgOut_ORD_LN {
        public int LineNumber { get; set; }
        public string LineType { get; set; }
        public string ProdCode { get; set; }
        public string LineText { get; set; }
        public int Quantity { get; set; }
        public double UnitCost { get; set; }
        public double Vat { get; set; }
        public double Discount { get; set; }
        public MsgOut_ORD_LN() { }
    }

}