﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace RAMWMSHost_Web.BLL.MsgOut {
    public class MsgOut_CLC {

        public static string MsgType = "MsgOut_CLC";
        
        public static DataSet MsgOut_CLC_Summary() {
            //RecordState	SortOrder	IsError	RecordCount	LastRecordDT
            return Msg.Msg_Summary(MsgType);
        }
        
        public static DataSet MsgOut_CLC_List(string RecordState, int PageSize, int PageNo) {
            //RowNumber,
			//CLCID, RecordDT, RecordState, RecordSource, ProcAttempts, ProcStartDT, ProcEndDT, ProcErrors, 
			//SiteCode, PrincipalCode, MoveRef, OrderLineNumber, ProdCode, Quantity, ReceiptType, Discrepancy, SerialCount, rowguid	
            //PageSize	PageNo	TotalCount
            return Msg.Msg_List(MsgType, RecordState, PageSize, PageNo);
        }

        public static DataSet MsgOut_CLC_Get(int CLCID) {
            //Table 1
            //CLCID	RecordDT	RecordState	RecordSource	ProcAttempts	ProcStartDT	ProcEndDT	ProcErrors	
            //SiteCode	PrincipalCode   MoveRef	OrderLineNumber	ProdCode	Quantity	ReceiptType	Discrepancy	SerialCount	rowguid
            //Table 2
            ///CLC_SNID	CLCID	SerialNumber	rowguid
            return Msg.Msg_Get(MsgType, CLCID);
        }

        public static int MsgOut_CLC_SetREADY(int CLCID) {
            return Msg.Msg_SetREADY(MsgType, CLCID);
        }

        public static int MsgOut_CLC_SN_Add(int CLCID, string SerialNumber) {
            string sql = @"usp_MsgOut_CLC_SN_Add";
            Database db = new DatabaseProviderFactory().Create("RAMWMSHost");
            DbCommand dbCmd = db.GetStoredProcCommand(sql);
            db.AddInParameter(dbCmd, "@CLCID", DbType.Int32, CLCID);
            db.AddInParameter(dbCmd, "@SerialNumber", DbType.String, SerialNumber);
            int rowsAffected = Convert.ToInt32(db.ExecuteNonQuery(dbCmd));
            dbCmd.Dispose(); dbCmd = null; db = null;
            return rowsAffected;
        }
                
        public static int MsgOut_CLC_Add(string RecordSource,
                                        string SiteCode,
                                        string PrincipalCode,
             
                                        string MoveRef,
                                        string OrderLineNumber,
                                        string ProdCode,
                                        int Quantity,
                                        string ReceiptType,
                                        int Discrepancy,
                                        int SerialCount) {
            string sql = @"usp_MsgOut_CLC_Add";
            Database db = new DatabaseProviderFactory().Create("RAMWMSHost");
            DbCommand dbCmd = db.GetStoredProcCommand(sql);
            db.AddOutParameter(dbCmd, "@CLCID", DbType.Int32, 16);
            db.AddInParameter(dbCmd, "@RecordSource", DbType.String, RecordSource);
            db.AddInParameter(dbCmd, "@SiteCode", DbType.String, SiteCode);
            db.AddInParameter(dbCmd, "@PrincipalCode", DbType.String, PrincipalCode); 
             
            db.AddInParameter(dbCmd, "@MoveRef", DbType.String, MoveRef);
            db.AddInParameter(dbCmd, "@OrderLineNumber", DbType.String, OrderLineNumber);
            db.AddInParameter(dbCmd, "@ProdCode", DbType.String, ProdCode);
            db.AddInParameter(dbCmd, "@Quantity", DbType.Int32, Quantity);
            db.AddInParameter(dbCmd, "@ReceiptType", DbType.String, ReceiptType);
            db.AddInParameter(dbCmd, "@Discrepancy", DbType.Int32, Discrepancy);
            db.AddInParameter(dbCmd, "@SerialCount", DbType.Int32, SerialCount); 

            int rowsAffected = Convert.ToInt32(db.ExecuteNonQuery(dbCmd));
            int CLCID = Convert.ToInt32(dbCmd.Parameters["@CLCID"].Value);
            dbCmd.Dispose(); dbCmd = null; db = null;
            return CLCID;
        }




    }
}
