﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace RAMWMSHost_Web.BLL.MsgOut {
    public class MsgOut_OCA {

        public static string MsgType = "MsgOut_OCA";

        public static DataSet MsgOut_OCA_Summary() {
            //RecordState	SortOrder	IsError	RecordCount	LastRecordDT
            return Msg.Msg_Summary(MsgType);
        }
        
        public static DataSet MsgOut_OCA_List(string RecordState, int PageSize, int PageNo) {
            //RowNumber,
			//OCAID, RecordDT, RecordState, RecordSource, ProcAttempts, ProcStartDT, ProcEndDT, ProcErrors, 
			//SiteCode, PrincipalCode, OrderNumber
            //PageSize	PageNo	TotalCount
            return Msg.Msg_List(MsgType, RecordState, PageSize, PageNo);
        }

        public static DataSet MsgOut_OCA_Get(int OCAID) {
            //OCAID, RecordDT, RecordState, RecordSource, ProcAttempts, ProcStartDT, ProcEndDT, ProcErrors, 
            //SiteCode, PrincipalCode, OrderNumber
            return Msg.Msg_Get(MsgType, OCAID);
        }
        
        public static int MsgOut_OCA_Add(string RecordSource,
                                            string SiteCode,
                                            string PrincipalCode,

                                            string OrderNumber) {
            string sql = @"usp_MsgOut_OCA_Add";
            Database db = new DatabaseProviderFactory().Create("RAMWMSHost");
            DbCommand dbCmd = db.GetStoredProcCommand(sql);
            db.AddOutParameter(dbCmd, "@OCAID", DbType.Int32, 16);
            db.AddInParameter(dbCmd, "@RecordSource", DbType.String, RecordSource);
            db.AddInParameter(dbCmd, "@SiteCode", DbType.String, SiteCode);
            db.AddInParameter(dbCmd, "@PrincipalCode", DbType.String, PrincipalCode);

            db.AddInParameter(dbCmd, "@OrderNumber", DbType.String,  OrderNumber); 
 
            int rowsAffected = Convert.ToInt32(db.ExecuteNonQuery(dbCmd));
            int OCAID = Convert.ToInt32(dbCmd.Parameters["@OCAID"].Value);
            dbCmd.Dispose(); dbCmd = null; db = null;
            return OCAID;
        }


    }
}
