﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace RAMWMSHost_Web.BLL {
    public class Msg {


        public static int Msg_SetREADY(string MsgType, int MsgID) {
            string sql = @"usp_Msg_SetREADY";
            Database db = new DatabaseProviderFactory().Create("RAMWMSHost");
            DbCommand dbCmd = db.GetStoredProcCommand(sql);
            db.AddInParameter(dbCmd, "@MsgType", DbType.String, MsgType);
            db.AddInParameter(dbCmd, "@MsgID", DbType.Int32, MsgID);
            int rowsAffected = Convert.ToInt32(db.ExecuteNonQuery(dbCmd));
            dbCmd.Dispose(); dbCmd = null; db = null;
            return rowsAffected;
        }

        public static DataSet Msg_Get(string MsgType, int MsgID) {
            string sql = @"usp_Msg_Get";
            Database db = new DatabaseProviderFactory().Create("RAMWMSHost");
            DbCommand dbCmd = db.GetStoredProcCommand(sql);
            db.AddInParameter(dbCmd, "@MsgType", DbType.String, MsgType);
            db.AddInParameter(dbCmd, "@MsgID", DbType.Int32, MsgID);
            DataSet ds = db.ExecuteDataSet(dbCmd);
            dbCmd.Dispose(); dbCmd = null; db = null;
            return ds;
        }
        
        public static DataSet Msg_Summary(string MsgType ) {
            string sql = @"usp_Msg_Summary";
            Database db = new DatabaseProviderFactory().Create("RAMWMSHost");
            DbCommand dbCmd = db.GetStoredProcCommand(sql);
            db.AddInParameter(dbCmd, "@MsgType", DbType.String, MsgType); 
            DataSet ds = db.ExecuteDataSet(dbCmd);
            dbCmd.Dispose(); dbCmd = null; db = null;
            return ds;
        }



        public static DataSet Msg_List(string MsgType, string RecordState, int PageSize, int PageNo ) { 
            string sql = @"usp_Msg_List";
            Database db = new DatabaseProviderFactory().Create("RAMWMSHost");
            DbCommand dbCmd = db.GetStoredProcCommand(sql);
            db.AddInParameter(dbCmd, "@MsgType", DbType.String, MsgType);
            db.AddInParameter(dbCmd, "@RecordState", DbType.String, RecordState);
            db.AddInParameter(dbCmd, "@PageSize", DbType.Int32, PageSize);
            db.AddInParameter(dbCmd, "@PageNo", DbType.Int32, PageNo); 
            DataSet ds = db.ExecuteDataSet(dbCmd);
            dbCmd.Dispose(); dbCmd = null; db = null;
            return ds;
        }


   


    }
}