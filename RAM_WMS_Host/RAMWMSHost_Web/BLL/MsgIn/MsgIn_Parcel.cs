﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace RAMWMSHost_Web.BLL.MsgIn {
    public class MsgIn_Parcel {

        public static string MsgType = "MsgIn_Parcel";
        
        public static DataSet MsgIn_Parcel_Summary() {
            //RecordState	SortOrder	IsError	RecordCount	LastRecordDT
            return Msg.Msg_Summary(MsgType);
        }

        //public static DataSet MsgIn_Parcel_List(string RecordState, int PageSize, int PageNo, string orderNumber, string parcelReference) {
        //    //RowNumber,
        //    //ParcelID, RecordDT, RecordState, RecordSource, ProcAttempts, ProcStartDT, ProcEndDT, ProcErrors, 
        //    //SiteCode, PrincipalCode, OrderNumber, ParcelNo, Kilograms, Length, Breadth, Height, InsuredValue, SPNumber, ParcelReference, rowguid
        //    //PageSize	PageNo	TotalCount
        //    return Msg.Msg_List(MsgType, RecordState, PageSize, PageNo,  orderNumber,   parcelReference);
        //}

        public static DataSet MsgIn_Parcel_List(string RecordState, int PageSize, int PageNo, string orderNumber, string parcelReference) {
            string sql = @"usp_MsgIn_Parcel_List";
            Database db = new DatabaseProviderFactory().Create("RAMWMSHost");
            DbCommand dbCmd = db.GetStoredProcCommand(sql);
            db.AddInParameter(dbCmd, "@RecordState", DbType.String, RecordState);
            db.AddInParameter(dbCmd, "@PageSize", DbType.Int32, PageSize);
            db.AddInParameter(dbCmd, "@PageNo", DbType.Int32, PageNo);
            db.AddInParameter(dbCmd, "@OrderNumber", DbType.String, orderNumber);
            db.AddInParameter(dbCmd, "@ParcelReference", DbType.String, parcelReference);
            DataSet ds = db.ExecuteDataSet(dbCmd);
            dbCmd.Dispose(); dbCmd = null; db = null;
            return ds;
        }

        public static DataSet MsgIn_Parcel_Get(int ParcelID) {
            //ParcelID, RecordDT, RecordState, RecordSource, ProcAttempts, ProcStartDT, ProcEndDT, ProcErrors, 
            //SiteCode, PrincipalCode, OrderNumber, ParcelNo, Kilograms, Length, Breadth, Height, InsuredValue, SPNumber, ParcelReference, rowguid
            return Msg.Msg_Get(MsgType, ParcelID);
        }
        
        public static int MsgIn_Parcel_Add(string RecordSource,
                                            string SiteCode,
                                            string PrincipalCode,
                                            string OrderNumber,

                                            int ParcelNo,
                                            double Kilograms,
                                            double Length,
                                            double Breadth,
                                            double Height,
                                            double InsuredValue,
                                            string SPNumber,
                                            string ParcelReference,
                                            int NoOfParcels) {
            string sql = @"usp_MsgIn_Parcel_Add";
            Database db = new DatabaseProviderFactory().Create("RAMWMSHost");
            DbCommand dbCmd = db.GetStoredProcCommand(sql);
            db.AddOutParameter(dbCmd, "@ParcelID", DbType.Int32, 16);
            db.AddInParameter(dbCmd, "@RecordSource", DbType.String, RecordSource);
            db.AddInParameter(dbCmd, "@SiteCode", DbType.String, SiteCode);
            db.AddInParameter(dbCmd, "@PrincipalCode", DbType.String, PrincipalCode);
            db.AddInParameter(dbCmd, "@OrderNumber", DbType.String, OrderNumber);


            db.AddInParameter(dbCmd, "@ParcelNo", DbType.Int32, ParcelNo);
            db.AddInParameter(dbCmd, "@Kilograms", DbType.Double, Kilograms);
            db.AddInParameter(dbCmd, "@Length", DbType.Double, Length);
            db.AddInParameter(dbCmd, "@Breadth", DbType.Double, Breadth);
            db.AddInParameter(dbCmd, "@Height", DbType.Double, Height);
            db.AddInParameter(dbCmd, "@InsuredValue", DbType.Double, InsuredValue);
            db.AddInParameter(dbCmd, "@SPNumber", DbType.String, SPNumber);
            db.AddInParameter(dbCmd, "@ParcelReference", DbType.String, ParcelReference);
            db.AddInParameter(dbCmd, "@NoOfParcels", DbType.Int32, NoOfParcels);


            int rowsAffected = Convert.ToInt32(db.ExecuteNonQuery(dbCmd));
            int ParcelID = Convert.ToInt32(dbCmd.Parameters["@ParcelID"].Value);
            dbCmd.Dispose(); dbCmd = null; db = null;
            return ParcelID;
        }

        		 
							   


    }
}
