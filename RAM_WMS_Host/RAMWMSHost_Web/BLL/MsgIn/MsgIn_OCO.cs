﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace RAMWMSHost_Web.BLL.MsgIn {
    public class MsgIn_OCO {

        public static string MsgType = "MsgIn_OCO";

        public static DataSet MsgIn_OCO_Summary() {
            //RecordState	SortOrder	IsError	RecordCount	LastRecordDT
            return Msg.Msg_Summary(MsgType);
        }

        public static DataSet MsgIn_OCO_List(string RecordState, int PageSize, int PageNo) {
            //RowNumber,
            //OCOID, RecordDT, RecordState, RecordSource, ProcAttempts, ProcStartDT, ProcEndDT, ProcErrors, 
            //SiteCode, PrincipalCode, OrderNumber, Priority, UserID, Weight, OrderCost, OrderDiscount, OrderVAT, rowguid	
            //PageSize	PageNo	TotalCount
            return Msg.Msg_List(MsgType, RecordState, PageSize, PageNo);
        }

        public static DataSet MsgIn_OCO_Get(int OCOID) {
            //OCOID, RecordDT, RecordState, RecordSource, ProcAttempts, ProcStartDT, ProcEndDT, ProcErrors, 
            //SiteCode, PrincipalCode, OrderNumber, Priority, UserID, Weight, OrderCost, OrderDiscount, OrderVAT, rowguid
            return Msg.Msg_Get(MsgType, OCOID);
        }

        public static int MsgIn_OCO_SetREADY(int OCOID) {
            return Msg.Msg_SetREADY(MsgType, OCOID);
        }

        public static int MsgIn_OCO_LN_Add(int OCOID,
                                            int LineNumber,
                                            string ProdCode,
                                            int QuantityPicked,
                                            string SerialNumber,
                                            double UnitCost,
                                            double Vat,
                                            double Discount) {
            string sql = @"usp_MsgIn_OCO_LN_Add";
            Database db = new DatabaseProviderFactory().Create("RAMWMSHost");
            DbCommand dbCmd = db.GetStoredProcCommand(sql);
            db.AddInParameter(dbCmd, "@OCOID", DbType.Int32, OCOID);
            db.AddInParameter(dbCmd, "@LineNumber", DbType.Int32, LineNumber);
            db.AddInParameter(dbCmd, "@ProdCode", DbType.String, ProdCode);
            db.AddInParameter(dbCmd, "@QuantityPicked", DbType.Int32, QuantityPicked);
            db.AddInParameter(dbCmd, "@SerialNumber", DbType.String, SerialNumber);
            db.AddInParameter(dbCmd, "@UnitCost", DbType.Double, UnitCost);
            db.AddInParameter(dbCmd, "@Vat", DbType.Double, Vat);
            db.AddInParameter(dbCmd, "@Discount", DbType.Double, Discount);
            int rowsAffected = Convert.ToInt32(db.ExecuteNonQuery(dbCmd));
            dbCmd.Dispose(); dbCmd = null; db = null;
            return rowsAffected;
        }

        public static int MsgIn_OCO_Add(string RecordSource,
                                        string SiteCode,
                                        string PrincipalCode,
                                        string OrderNumber,
                                        string Priority,
                                        string UserID,
                                        double Weight,
                                        double OrderCost,
                                        double OrderDiscount,
                                        double OrderVAT) {
            string sql = @"usp_MsgIn_OCO_Add";
            Database db = new DatabaseProviderFactory().Create("RAMWMSHost");
            DbCommand dbCmd = db.GetStoredProcCommand(sql);
            db.AddOutParameter(dbCmd, "@OCOID", DbType.Int32, 16);
            db.AddInParameter(dbCmd, "@RecordSource", DbType.String, RecordSource);
            db.AddInParameter(dbCmd, "@SiteCode", DbType.String, SiteCode);
            db.AddInParameter(dbCmd, "@PrincipalCode", DbType.String, PrincipalCode);
            db.AddInParameter(dbCmd, "@OrderNumber", DbType.String, OrderNumber);
            db.AddInParameter(dbCmd, "@Priority", DbType.String, Priority);
            db.AddInParameter(dbCmd, "@UserID", DbType.String, UserID);
            db.AddInParameter(dbCmd, "@Weight", DbType.Double, Weight);
            db.AddInParameter(dbCmd, "@OrderCost", DbType.Double, OrderCost);
            db.AddInParameter(dbCmd, "@OrderDiscount", DbType.Double, OrderDiscount);
            db.AddInParameter(dbCmd, "@OrderVAT", DbType.Double, OrderVAT);
            int rowsAffected = Convert.ToInt32(db.ExecuteNonQuery(dbCmd));
            int OCOID = Convert.ToInt32(dbCmd.Parameters["@OCOID"].Value);
            dbCmd.Dispose(); dbCmd = null; db = null;
            return OCOID;
        }


    }
}
