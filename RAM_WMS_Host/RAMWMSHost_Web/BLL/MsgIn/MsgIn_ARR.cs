﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace RAMWMSHost_Web.BLL.MsgIn {
    public class MsgIn_ARR {

        public static string MsgType = "MsgIn_ARR";
 
        public static DataSet  MsgIn_ARR_Summary() {
            //RecordState	SortOrder	IsError	RecordCount	LastRecordDT
            return Msg.Msg_Summary(MsgType);
        }


        public static DataSet MsgIn_ARR_List(string RecordState, int PageSize, int PageNo) {
            //RowNumber	ARRID	RecordDT	RecordState	ProcAttempts	ProcStartDT	ProcEndDT	ProcErrors	
            //SiteCode	PrincipalCode	Resend	ProdCode	LineNumber	ReceiptType	MoveRef	PORef	ReceiptDT	
            //StartEndStatus	RejectCount	ReasonCode	AcceptCount	
            //PageSize	PageNo	TotalCount
            return Msg.Msg_List(MsgType, RecordState, PageSize, PageNo);
        }

        public static DataSet MsgIn_ARR_Get(int ARRID) {
            //ARRID, SerialNumber
            return Msg.Msg_Get(MsgType, ARRID);
        } 
        public static int MsgIn_ARR_SetREADY(int ARRID) {
            return Msg.Msg_SetREADY(MsgType, ARRID);
        }
         
        public static int MsgIn_ARR_SN_Add(int ARRID, string SerialNumber) {
            string sql = @"usp_MsgIn_ARR_SN_Add";
            Database db = new DatabaseProviderFactory().Create("RAMWMSHost");
            DbCommand dbCmd = db.GetStoredProcCommand(sql);
            db.AddInParameter(dbCmd, "@ARRID", DbType.Int32, ARRID);
            db.AddInParameter(dbCmd, "@SerialNumber", DbType.String, SerialNumber);
            int rowsAffected = Convert.ToInt32(db.ExecuteNonQuery(dbCmd));
            dbCmd.Dispose(); dbCmd = null; db = null;
            return rowsAffected;
        }
        
        public static int  MsgIn_ARR_Add(string RecordSource,
                                            string SiteCode,
                                            string PrincipalCode,
                                            bool Resend,
                                            string ProdCode,
                                            string LineNumber,
                                            string ReceiptType,
                                            string MoveRef,
                                            string PORef,
                                            DateTime ReceiptDT,
                                            string StartEndStatus,
                                            int RejectCount,
                                            string ReasonCode,
                                            int AcceptCount) {
            string sql = @"usp_MsgIn_ARR_Add";
            Database db = new DatabaseProviderFactory().Create("RAMWMSHost");
            DbCommand dbCmd = db.GetStoredProcCommand(sql);
            db.AddOutParameter(dbCmd, "@ARRID", DbType.Int32, 16);
            db.AddInParameter(dbCmd, "@RecordSource", DbType.String, RecordSource);
            db.AddInParameter(dbCmd, "@SiteCode", DbType.String, SiteCode);
            db.AddInParameter(dbCmd, "@PrincipalCode", DbType.String, PrincipalCode);
            db.AddInParameter(dbCmd, "@Resend", DbType.Boolean, Resend);
            db.AddInParameter(dbCmd, "@ProdCode", DbType.String, ProdCode);
            db.AddInParameter(dbCmd, "@LineNumber", DbType.String, LineNumber);
            db.AddInParameter(dbCmd, "@ReceiptType", DbType.String, ReceiptType);
            db.AddInParameter(dbCmd, "@MoveRef", DbType.String, MoveRef);
            db.AddInParameter(dbCmd, "@PORef", DbType.String, PORef);
            db.AddInParameter(dbCmd, "@ReceiptDT", DbType.DateTime, ReceiptDT);
            db.AddInParameter(dbCmd, "@StartEndStatus", DbType.String, StartEndStatus);
            db.AddInParameter(dbCmd, "@RejectCount", DbType.Int32, RejectCount);
            db.AddInParameter(dbCmd, "@ReasonCode", DbType.String, ReasonCode);
            db.AddInParameter(dbCmd, "@AcceptCount", DbType.Int32, AcceptCount);
            int rowsAffected = Convert.ToInt32(db.ExecuteNonQuery(dbCmd));
            int ARRID = Convert.ToInt32(dbCmd.Parameters["@ARRID"].Value);
            dbCmd.Dispose(); dbCmd = null; db = null;
            return ARRID;
        }
 



    }
}
