﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace RAMWMSHost_Web.BLL.MsgIn {
    public class MsgIn_SLA {

        public static string MsgType = "MsgIn_SLA";
        
        public static DataSet MsgIn_SLA_Summary() {
            //RecordState	SortOrder	IsError	RecordCount	LastRecordDT
            return Msg.Msg_Summary(MsgType);
        }

        public static DataSet MsgIn_SLA_List(string RecordState, int PageSize, int PageNo) {
            //RowNumber,
            //SLAID, RecordDT, RecordState, RecordSource, ProcAttempts, ProcStartDT, ProcEndDT, ProcErrors, 
            //SiteCode, PrincipalCode, ProdCode, QuantityChange, ReasonCode, AvailabilityState, SerialCount, rowguid
            //PageSize	PageNo	TotalCount
            return Msg.Msg_List(MsgType, RecordState, PageSize, PageNo);
        }

        public static DataSet MsgIn_SLA_Get(int SLAID) {
            //SLAID, RecordDT, RecordState, RecordSource, ProcAttempts, ProcStartDT, ProcEndDT, ProcErrors, 
            //SiteCode, PrincipalCode, ProdCode, QuantityChange, ReasonCode, AvailabilityState, SerialCount, rowguid
            return Msg.Msg_Get(MsgType, SLAID);
        }

        public static int MsgIn_SLA_SetREADY(int SLAID) {
            return Msg.Msg_SetREADY(MsgType, SLAID);
        }

        public static int MsgIn_SLA_SN_Add(int SLAID, string SerialNumber) {
            string sql = @"usp_MsgIn_SLA_SN_Add";
            Database db = new DatabaseProviderFactory().Create("RAMWMSHost");
            DbCommand dbCmd = db.GetStoredProcCommand(sql);
            db.AddInParameter(dbCmd, "@SLAID", DbType.Int32, SLAID);
            db.AddInParameter(dbCmd, "@SerialNumber", DbType.String, SerialNumber);
            int rowsAffected = Convert.ToInt32(db.ExecuteNonQuery(dbCmd));
            dbCmd.Dispose(); dbCmd = null; db = null;
            return rowsAffected;
        }

        public static int MsgIn_SLA_Add(string RecordSource,
                                        string SiteCode,
                                        string PrincipalCode,
                                        string ProdCode,
                                        int QuantityChange,
                                        string ReasonCode,
                                        string AvailabilityState,
                                        int SerialCount) {
            string sql = @"usp_MsgIn_SLA_Add";
            Database db = new DatabaseProviderFactory().Create("RAMWMSHost");
            DbCommand dbCmd = db.GetStoredProcCommand(sql);
            db.AddOutParameter(dbCmd, "@SLAID", DbType.Int32, 16);
            db.AddInParameter(dbCmd, "@RecordSource", DbType.String, RecordSource);
            db.AddInParameter(dbCmd, "@SiteCode", DbType.String, SiteCode);
            db.AddInParameter(dbCmd, "@PrincipalCode", DbType.String, PrincipalCode);
            db.AddInParameter(dbCmd, "@ProdCode", DbType.String, ProdCode);
            db.AddInParameter(dbCmd, "@QuantityChange", DbType.Int32, QuantityChange);
            db.AddInParameter(dbCmd, "@ReasonCode", DbType.String, ReasonCode);
            db.AddInParameter(dbCmd, "@AvailabilityState", DbType.String, AvailabilityState);
            db.AddInParameter(dbCmd, "@SerialCount", DbType.Int32, SerialCount);
            int rowsAffected = Convert.ToInt32(db.ExecuteNonQuery(dbCmd));
            int SLAID = Convert.ToInt32(dbCmd.Parameters["@SLAID"].Value);
            dbCmd.Dispose(); dbCmd = null; db = null;
            return SLAID;
        }


    }
}