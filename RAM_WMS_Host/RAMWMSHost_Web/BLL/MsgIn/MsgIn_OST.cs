﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace RAMWMSHost_Web.BLL.MsgIn {
    public class MsgIn_OST {

        public static string MsgType = "MsgIn_OST";

        public static DataSet MsgIn_OST_Summary() {
            //RecordState	SortOrder	IsError	RecordCount	LastRecordDT
            return Msg.Msg_Summary(MsgType);
        }

        public static DataSet MsgIn_OST_List(string RecordState, int PageSize, int PageNo) {
            //RowNumber,
			//OSTID, RecordDT, RecordState, RecordSource, ProcAttempts, ProcStartDT, ProcEndDT, ProcErrors, 
            //SiteCode, PrincipalCode, OrderNumber, rowguid	
            return Msg.Msg_List(MsgType, RecordState, PageSize, PageNo);
        }

        public static DataSet MsgIn_OST_Get(int OSTID) {
            //OSTID, RecordDT, RecordState, RecordSource, ProcAttempts, ProcStartDT, ProcEndDT, ProcErrors, 
            //SiteCode, PrincipalCode, OrderNumber, rowguid	
            return Msg.Msg_Get(MsgType, OSTID);
        }

        public static int MsgIn_OST_Add(string RecordSource,
                                            string SiteCode,
                                            string PrincipalCode,
                                            string OrderNumber) {
            string sql = @"usp_MsgIn_OST_Add";
            Database db = new DatabaseProviderFactory().Create("RAMWMSHost");
            DbCommand dbCmd = db.GetStoredProcCommand(sql);
            db.AddOutParameter(dbCmd, "@OSTID", DbType.Int32, 16);
            db.AddInParameter(dbCmd, "@RecordSource", DbType.String, RecordSource);
            db.AddInParameter(dbCmd, "@SiteCode", DbType.String, SiteCode);
            db.AddInParameter(dbCmd, "@PrincipalCode", DbType.String, PrincipalCode);
            db.AddInParameter(dbCmd, "@OrderNumber", DbType.String, OrderNumber);
            int rowsAffected = Convert.ToInt32(db.ExecuteNonQuery(dbCmd));
            int OSTID = Convert.ToInt32(dbCmd.Parameters["@OSTID"].Value);
            dbCmd.Dispose(); dbCmd = null; db = null;
            return OSTID;
        }


    }
}
