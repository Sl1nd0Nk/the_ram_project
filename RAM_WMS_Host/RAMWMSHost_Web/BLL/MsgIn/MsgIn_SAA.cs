﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace RAMWMSHost_Web.BLL.MsgIn {
    public class MsgIn_SAA {

        public static string MsgType = "MsgIn_SAA";

        public static DataSet MsgIn_SAA_Summary() {
            //RecordState	SortOrder	IsError	RecordCount	LastRecordDT
            return Msg.Msg_Summary(MsgType);
        }

        public static DataSet MsgIn_SAA_List(string RecordState, int PageSize, int PageNo) {
            //RowNumber, SAAID, RecordDT, RecordState, RecordSource, ProcAttempts, ProcStartDT, ProcEndDT, ProcErrors, 
            //SiteCode, PrincipalCode, ProdCode, QuantityChange, rowguid
            return Msg.Msg_List(MsgType, RecordState, PageSize, PageNo);
        }

        public static DataSet MsgIn_SAA_Get(int SAAID) {
            //SAAID, RecordDT, RecordState, RecordSource, ProcAttempts, ProcStartDT, ProcEndDT, ProcErrors, 
            //SiteCode, PrincipalCode, ProdCode, QuantityChange, rowguid
            return Msg.Msg_Get(MsgType, SAAID);
        }

        public static int MsgIn_SAA_Add(string RecordSource,
                                            string SiteCode,
                                            string PrincipalCode,
                                            string ProdCode,
                                            string QuantityChange) {
            string sql = @"usp_MsgIn_SAA_Add";
            Database db = new DatabaseProviderFactory().Create("RAMWMSHost");
            DbCommand dbCmd = db.GetStoredProcCommand(sql);
            db.AddOutParameter(dbCmd, "@SAAID", DbType.Int32, 16);
            db.AddInParameter(dbCmd, "@RecordSource", DbType.String, RecordSource);
            db.AddInParameter(dbCmd, "@SiteCode", DbType.String, SiteCode);
            db.AddInParameter(dbCmd, "@PrincipalCode", DbType.String, PrincipalCode);
            db.AddInParameter(dbCmd, "@ProdCode", DbType.String, ProdCode);
            db.AddInParameter(dbCmd, "@QuantityChange", DbType.String, QuantityChange);
            int rowsAffected = Convert.ToInt32(db.ExecuteNonQuery(dbCmd));
            int SAAID = Convert.ToInt32(dbCmd.Parameters["@SAAID"].Value);
            dbCmd.Dispose(); dbCmd = null; db = null;
            return SAAID;
        }


    }
}
