﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PrintTest.aspx.cs" Inherits="RAMWMSHost_Web.Other.PrintTest" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>frmPrintTest</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="C#" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <object id="clsPrint" classid="CLSID:3564822F-7B51-4D06-A6D0-1318CD0E3F0C" codebase="RAMLabel.CAB#version=1,0,0,38"
        viewastext>
    </object>

    <script language="javascript" type="text/javascript">
        //Public Sub PrintToPort(ByVal port As String, ByVal data As String)
        //Public Sub PrintToDefaultPort(ByVal data As String)
        //Public Function Version() As String
        //Public Function DefaultPort() As String
        //Public Sub SetDefaultPort()
        function PrintToPort() {
            try {
                var port = document.getElementById('printerPort').value;
                var data = 'GI\r\nEI\r\n';
                var printObj = new ActiveXObject('RAMLabel.clsPrint');
                printObj.PrintToPort(port, data);
                printObj = null;
            } catch (e) {
                alert(e);
            }
        }
        function PrintToDefaultPort() {
            try {
                var data = 'GI\r\nEI\r\n';

                 data = 'N\r\nq832\r\nQ1184, 24 + 0\r\nS6\r\nD12\r\nZT\r\nB24, 24, 0, 1, 3, 3, 200, N, "12345"\r\nA24, 250, 0, 2, 1, 1, N, "CaseNo: 0012   PackNo: 12345"\r\nP1\r\n';
                 data = 'N\r\nB24,24,0,1,3,3,200,N,"12345"\r\nA24,250,0,2,1,1,N,"CaseNo: 0012   PackNo: 12345"\r\nP1\r\n';


                var printObj = new ActiveXObject('RAMLabel.clsPrint');
                printObj.PrintToDefaultPort(data);
                printObj = null;
            } catch (e) {
                alert(e);
            }
        }
        function Version() {
            try {
                var printObj = new ActiveXObject('RAMLabel.clsPrint');
                var version = printObj.Version();
                printObj = null;
                alert('RAMLabel.DLL V' + version);
            } catch (e) {
                alert(e);
            }
        }
        function DefaultPort() {
            try {
                var printObj = new ActiveXObject('RAMLabel.clsPrint');
                var port = printObj.DefaultPort();
                printObj = null;
                alert('Default Port: ' + port);
            } catch (e) {
                alert(e);
            }
        }
        function SetDefaultPort() {
            try {
                var printObj = new ActiveXObject('RAMLabel.clsPrint');
                printObj.SetDefaultPort();
                printObj = null;
            } catch (e) {
                alert(e);
            }
        }
    </script>

</head>
<body  >
    <form id="Form1" method="post" runat="server">
    <table>
        <!--<tr>
				<td>
					<select id="printerPort" style="WIDTH: 100%">
						<option value="LPT1" selected>LPT1</option>
						<option value="COM1">COM1</option>
						<option value="COM2">COM2</option>
						<option value="COM3">COM3</option>
						<option value="COM4">COM4</option>
					</select>
				</td>
				<td><INPUT type="button" value="PrintToPort" onclick="PrintToPort();" style="WIDTH: 100%"></td>
			</tr>-->
        <tr>
            <td>
                <input type="button" value="Print To Default Port" onclick="PrintToDefaultPort();"
                    style="width: 100%" />
            </td>
            <td>
                <input type="button" value="DLL Version" onclick="Version();" style="width: 100%" />
            </td>
        </tr>
        <tr>
            <td>
                <input type="button" value="Get Default Port" onclick="DefaultPort();" style="width: 100%" />
            </td>
            <td>
                <input type="button" value="Set Default Port" onclick="SetDefaultPort();" style="width: 100%" />
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
