﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="RAMWMSHost_Web.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <h2>RAM WMS Host</h2> 
            <br />

            <a href="MsgQueue/Index.aspx">Message Queue</a>
            <br />
            <a href="DataEntry/Index.aspx">Data Entry</a>
            <br />
            <a href="MsgTransferSvcControl/SvcControl.aspx">Message Transfer Service</a>
             <br />
            <a href="Other/PrintPackNoBarcode.aspx">Print PackNo Barcode</a>
        </div>
    </form>
</body>
</html>
