﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OCA_Add.aspx.cs" Inherits="RAMWMSHost_Web.DataEntry.OCA_Add" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>OCA Add</title>
    <link href="../Style1.css" rel="stylesheet" />

    <script src="../Scripts/datetimepicker/datetimepicker_css.js"></script>
    <script src="../Scripts/Shared.js"></script>
    <script type="text/javascript">

        var mForm = new Object();
        mForm.txtShow_OCAID = document.getElementById('txtShow_OCAID');
        mForm.txtSiteCode = document.getElementById('txtSiteCode');
        mForm.txtPrincipalCode = document.getElementById('txtPrincipalCode');
        mForm.txtOrderNumber = document.getElementById('txtOrderNumber');
        mForm.btnSave = document.getElementById('btnSave');
        mForm.divVeil = document.getElementById('divVeil');

        function bodyonload() {
            mForm.txtShow_OCAID = document.getElementById('txtShow_OCAID');
            mForm.txtSiteCode = document.getElementById('txtSiteCode');
            mForm.txtPrincipalCode = document.getElementById('txtPrincipalCode');
            mForm.txtOrderNumber = document.getElementById('txtOrderNumber');
            mForm.btnSave = document.getElementById('btnSave');
            mForm.divVeil = document.getElementById('divVeil');

            if (mForm.txtShow_OCAID.value.length > 0) {
                window.open('../MsgQueue/MsgOut/MsgOut_OCA_View.aspx?OCAID=' + mForm.txtShow_OCAID.value, '_blank', 'height=400,width=400');
                mForm.txtShow_OCAID.value = '';
            }

            ClearForm();

            WireValidation();

            mForm.txtSiteCode.value = getCookie('OCA_ADD_txtSiteCode');
            mForm.txtPrincipalCode.value = getCookie('OCA_ADD_txtPrincipalCode');

            Validate();

 

            //setTimeout(LoadDummyData, 250);

            setTimeout(HideVeil, 250);
        }

        function HideVeil() {
            mForm.divVeil.style.display = 'none';
        }


        function WireValidation() {
            //wire validation
            var inputs = document.getElementsByTagName('input');
            var i;
            for (i = 0; i < inputs.length; ++i) {
                if (inputs[i].type == 'text') {
                    inputs[i].onkeyup = Validate;
                    inputs[i].onchange = Validate;
                    inputs[i].onclick = Validate;
                    inputs[i].onblur = Validate;
                }
                if (inputs[i].type == 'checkbox') {
                    inputs[i].onkeyup = Validate;
                    inputs[i].onchange = Validate;
                    inputs[i].onblur = Validate;
                }
            }
            inputs = document.getElementsByTagName('select');
            for (i = 0; i < inputs.length; ++i) {
                inputs[i].onkeyup = Validate;
                inputs[i].onchange = Validate;
                inputs[i].onclick = Validate;
                inputs[i].onblur = Validate;
            }
        }

        function ClearForm() {
            var inputs = document.getElementsByTagName('input');
            var i;
            for (i = 0; i < inputs.length; ++i) {
                if (inputs[i].type == 'text') inputs[i].value = '';
            }
            inputs = document.getElementsByTagName('select');
            for (i = 0; i < inputs.length; ++i) {
                inputs[i].selectedIndex = 0;
            }
        }

        function Validate() {
            var valid = true;

            //mForm.txtSiteCode
            if (mForm.txtSiteCode.value.length < 2) {
                valid = false;
                mForm.txtSiteCode.className = 'InputInValid';
            } else {
                mForm.txtSiteCode.className = 'InputValid';
            }

            //mForm.txtPrincipalCode
            if (mForm.txtPrincipalCode.value.length < 2) {
                valid = false;
                mForm.txtPrincipalCode.className = 'InputInValid';
            } else {
                mForm.txtPrincipalCode.className = 'InputValid';
            }

            //mForm.txtOrderNumber
            mForm.txtOrderNumber.className = 'InputValid';


            //mForm.btnSave
            mForm.btnSave.disabled = !valid;
            return valid;
        }
 

        function form1_onsubmit() {
            try {
                if (Validate() == false) {
                    return false;
                } else { 
                    mForm.divVeil.style.display = 'block'; 
                    setCookie('OCA_ADD_txtSiteCode', mForm.txtSiteCode.value, 7);
                    setCookie('OCA_ADD_txtPrincipalCode', mForm.txtPrincipalCode.value, 7);
                    return true;
                }
            } catch (e) {
                alert(e);
                return false;
            }
        }



    </script>

</head>
<body onload="bodyonload();">
    <form id="form1" runat="server" onsubmit="return form1_onsubmit();">
        <input type="hidden" id="txtShow_OCAID" runat="server" />
        <div>
            <table>
                <tr>
                    <td>
                        <h2>OCA – Incoming Goods Details (Host -> Matflo)</h2>
                    </td>
                    <td>&nbsp;</td>
                    <td><a href="Index.aspx">Index</a></td>
                </tr>
            </table>




            <table class="list">
                <thead>
                    <tr>
                        <td colspan="3">OCA Add</td>
                    </tr>
                </thead>
                <tbody>

                    <tr>
                        <td>SiteCode</td>
                        <td style="width: 250px">
                            <input type="text" runat="server" id="txtSiteCode" maxlength="4" value="001" style="width: 100%" />
                        </td>

                    </tr>

                    <tr>
                        <td>PrincipalCode</td>
                        <td>
                            <input type="text" runat="server" id="txtPrincipalCode" maxlength="6" value="OG" style="width: 100%" /></td>


                    </tr>
                    <tr>
                        <td>OrderNumber</td>
                        <td>
                            <input type="text" runat="server" id="txtOrderNumber" maxlength="10" style="width: 100%" /></td>
                        <td></td>
                    </tr>


                    <tr>
                        <td id="tdErrorMessage" runat="server" class="validation">.</td>
                        <td style="text-align: right">
                            <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" />

                        </td>
                    </tr>
                </tbody>
            </table>

        </div>
        <div style="display: block; position: absolute; top: 5px; right: 5px; bottom: 5px; left: 5px; background-color: #FFFFCC; z-index: 10; opacity: .70; filter: alpha(opacity=70); -moz-opacity: 0.7;"
            id="divVeil">
            Busy...
        </div>
    </form>
</body>
</html>
