﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace RAMWMSHost_Web.DataEntry {
    public partial class IGD_Add : System.Web.UI.Page {



        protected void Page_Load(object sender, EventArgs e) {

        }
         
        protected void btnSave_Click(object sender, EventArgs e) {

            try { 
                string SiteCode = txtSiteCode.Value;
                string PrincipalCode = txtPrincipalCode.Value;
                string ProdCode = txtProdCode.Value;
                string EANCode = txtEANCode.Value;
                string ShortDesc = txtShortDesc.Value;
                string LongDesc = txtLongDesc.Value;
                bool Serialised = chkSerialised.Checked;
                string AnalysisA = txtAnalysisA.Value;
                string AnalysisB = txtAnalysisB.Value;
                string OrderLineNo = txtOrderLineNo.Value;
                int Quantity = Convert.ToInt32(txtQuantity.Value);
                string ReceiptType = selectReceiptType.Value;
                string MoveRef = txtMoveRef.Value;
                string PORef = txtPORef.Value;
                DateTime PODate = Convert.ToDateTime(txtPODate.Value);
                DateTime StockDateTime = Convert.ToDateTime(txtStockDateTime.Value);

                int IGDID = BLL.MsgOut.MsgOut_IGD.MsgOut_IGD_Add("RAMWMSHost",
                                              SiteCode,
                                              PrincipalCode,

                                              ProdCode,
                                              EANCode,
                                              ShortDesc,
                                              LongDesc,
                                              Serialised,
                                              AnalysisA,
                                              AnalysisB,
                                              OrderLineNo,
                                              Quantity,
                                              ReceiptType,
                                              MoveRef,
                                              PORef,
                                              PODate,
                                              StockDateTime);


                tdErrorMessage.InnerHtml = "Saved IGD #" + IGDID.ToString();

                txtShow_IGDID.Value = IGDID.ToString();
                

            }catch(Exception exc){
                tdErrorMessage.InnerHtml = exc.Message;
            }


        }


    }
}