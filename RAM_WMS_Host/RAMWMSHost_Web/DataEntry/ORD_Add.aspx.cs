﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Script.Serialization;

namespace RAMWMSHost_Web.DataEntry {


    public partial class ORD_Add : System.Web.UI.Page {



        protected void Page_Load(object sender, EventArgs e) {
            if (!Page.IsPostBack) {
                txtDateCreated.Value = System.DateTime.Now.ToString("yyyy-MM-dd");
            }
        }

        protected void btnSave_Click(object sender, EventArgs e) {

            try {

                string SiteCode = txtSiteCode.Value;
                string PrincipalCode = txtPrincipalCode.Value;
                string OrderNumber = txtOrderNumber.Value;
                int LineCount = Convert.ToInt32(txtLineCount.Value);
                DateTime DateCreated = Convert.ToDateTime( txtDateCreated.Value);
                string CompanyName = txtCompanyName.Value;
                string HeaderComment = txtHeaderComment.Value;
                string CustOrderNumber = txtCustOrderNumber.Value;
                string CustAcc = txtCustAcc.Value;
                string InvName = txtInvName.Value;
                string InvAdd1 = txtInvAdd1.Value;
                string InvAdd2 = txtInvAdd2.Value;
                string InvAdd3 = txtInvAdd3.Value;
                string InvAdd4 = txtInvAdd4.Value;
                string InvAdd5 = txtInvAdd5.Value;
                string InvAdd6 = txtInvAdd6.Value;
                bool GSMTest = chkGSMTest.Checked;
                string VATNumber = txtVATNumber.Value;
                bool PrintPrice = chkPrintPrice.Checked;
                string Priority = selectPriority.Value;
                bool VAP = chkVAP.Checked;
                string SalesPerson = txtSalesPerson.Value;
                string SalesCategory = txtSalesCategory.Value;
                string Processor = txtProcessor.Value;
                string DeliveryAdd1 = txtDeliveryAdd1.Value;
                string DeliveryAdd2 = txtDeliveryAdd2.Value;
                string DeliveryAdd3 = txtDeliveryAdd3.Value;
                string DeliveryAdd4 = txtDeliveryAdd4.Value;
                string DeliveryAdd5 = txtDeliveryAdd5.Value;
                string DeliveryAdd6 = txtDeliveryAdd6.Value;
                string WMSPostCode = txtWMSPostCode.Value;
                double OrderDiscount = Convert.ToDouble(txtOrderDiscount.Value);
                double OrderVAT = Convert.ToDouble(txtOrderVAT.Value);
                int DocToPrint = Convert.ToInt32(txtDocToPrint.Value);
                string IDNumber = txtIDNumber.Value;
                string KYC = txtKYC.Value;
                string CourierName = txtCompanyName.Value;
                string CourierService = txtCourierName.Value;
                bool InsuranceRequired = Convert.ToBoolean(txtInsuranceRequired.Value);
                bool ValidateDelivery = Convert.ToBoolean(txtValidateDelivery);


                string OrderLines = txtOrderLines.Value;
                var jss = new JavaScriptSerializer(); //http://procbits.com/2011/04/21/quick-json-serializationdeserialization-in-c
                BLL.MsgOut.MsgOut_ORD_LN[] arr = jss.Deserialize<BLL.MsgOut.MsgOut_ORD_LN[]>(OrderLines);


                int ORDID = BLL.MsgOut.MsgOut_ORD.MsgOut_ORD_Add("RAMWMSHost",
                                                                SiteCode,
                                                                PrincipalCode,
                                                                OrderNumber,
                                                                LineCount,
                                                                DateCreated,
                                                                CompanyName,
                                                                HeaderComment,
                                                                CustOrderNumber,
                                                                CustAcc,
                                                                InvName,
                                                                InvAdd1,
                                                                InvAdd2,
                                                                InvAdd3,
                                                                InvAdd4,
                                                                InvAdd5,
                                                                InvAdd6,
                                                                GSMTest,
                                                                VATNumber,
                                                                PrintPrice,
                                                                Priority,
                                                                VAP,
                                                                SalesPerson,
                                                                SalesCategory,
                                                                Processor,
                                                                DeliveryAdd1,
                                                                DeliveryAdd2,
                                                                DeliveryAdd3,
                                                                DeliveryAdd4,
                                                                DeliveryAdd5,
                                                                DeliveryAdd6,
                                                                WMSPostCode,
                                                                OrderDiscount,
                                                                OrderVAT,
                                                                DocToPrint,
                                                                IDNumber,
                                                                KYC,
                                                                CourierName,
                                                                CourierService,
                                                                InsuranceRequired,
                                                                ValidateDelivery,
                                                                "");
                foreach (BLL.MsgOut.MsgOut_ORD_LN orderLine in arr) {
                    BLL.MsgOut.MsgOut_ORD.MsgOut_ORD_LN_Add(ORDID,
                                             orderLine.LineNumber,
                                             orderLine.LineType,
                                             orderLine.ProdCode,
                                             orderLine.LineText,
                                             orderLine.Quantity,
                                             orderLine.UnitCost,
                                             orderLine.Vat,
                                             orderLine.Discount);
                }


                BLL.MsgOut.MsgOut_ORD.MsgOut_ORD_SetREADY(ORDID);

                tdErrorMessage.InnerHtml = "Saved ORD #" + ORDID.ToString();// + "<BR />" + OrderLines.Length.ToString();

                txtShow_ORDID.Value = ORDID.ToString();


            } catch (Exception exc) {
                tdErrorMessage.InnerHtml = exc.Message;
            }


        }

    }
}