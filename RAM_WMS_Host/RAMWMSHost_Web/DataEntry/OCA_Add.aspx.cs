﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace RAMWMSHost_Web.DataEntry {
    public partial class OCA_Add : System.Web.UI.Page {



        protected void Page_Load(object sender, EventArgs e) {

        }

        protected void btnSave_Click(object sender, EventArgs e) {

            try {
                string SiteCode = txtSiteCode.Value;
                string PrincipalCode = txtPrincipalCode.Value;
                string OrderNumber = txtOrderNumber.Value;
 

                int OCAID = BLL.MsgOut.MsgOut_OCA.MsgOut_OCA_Add("RAMWMSHost",
                                              SiteCode,
                                              PrincipalCode,

                                              OrderNumber );


                tdErrorMessage.InnerHtml = "Saved OCA #" + OCAID.ToString();

                txtShow_OCAID.Value = OCAID.ToString();


            } catch (Exception exc) {
                tdErrorMessage.InnerHtml = exc.Message;
            }


        }


    }
}