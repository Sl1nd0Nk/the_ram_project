﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Script.Serialization;

namespace RAMWMSHost_Web.DataEntry {
    public partial class CLC_Add : System.Web.UI.Page {



        protected void Page_Load(object sender, EventArgs e) {

        }

        protected void btnSave_Click(object sender, EventArgs e) {

            try {
                string SiteCode = txtSiteCode.Value;
                string PrincipalCode = txtPrincipalCode.Value;

                string MoveRef = txtMoveRef.Value;
                string OrderLineNumber = txtOrderLineNumber.Value;
                string ProdCode = txtProdCode.Value;


                int Quantity = Convert.ToInt32(txtQuantity.Value);
                string ReceiptType = selectReceiptType.Value;

                int Discrepancy = Convert.ToInt32(txtDiscrepancy.Value);
                int SerialCount = Convert.ToInt32(txtSerialCount.Value);

                string SerialNumbers = txtSerialNumbers.Value;
                var jss = new JavaScriptSerializer(); //http://procbits.com/2011/04/21/quick-json-serializationdeserialization-in-c
                string[] arr = jss.Deserialize<string[]>(SerialNumbers);


                int CLCID = BLL.MsgOut.MsgOut_CLC.MsgOut_CLC_Add("RAMWMSHost",
                                                                SiteCode,
                                                                PrincipalCode,
                                                                MoveRef,
                                                                OrderLineNumber,
                                                                ProdCode,
                                                                Quantity,
                                                                ReceiptType,
                                                                Discrepancy,
                                                                SerialCount);
                foreach (string serialnumber in arr) {
                    if (serialnumber.Length > 0) BLL.MsgOut.MsgOut_CLC.MsgOut_CLC_SN_Add(CLCID, serialnumber);
                }


                BLL.MsgOut.MsgOut_CLC.MsgOut_CLC_SetREADY(CLCID);

                tdErrorMessage.InnerHtml = "Saved CLC #" + CLCID.ToString();// + "<BR />" + SerialNumbers.Length.ToString();

                txtShow_CLCID.Value = CLCID.ToString();


            } catch (Exception exc) {
                tdErrorMessage.InnerHtml = exc.Message;
            }


        }

    }
}