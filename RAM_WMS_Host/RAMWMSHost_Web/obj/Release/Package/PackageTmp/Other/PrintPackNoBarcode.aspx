﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PrintPackNoBarcode.aspx.cs" Inherits="RAMWMSHost_Web.Other.PrintPackNoBarcode" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Print PackNo Barcode</title>
    <link href="../Style1.css" rel="stylesheet" />
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="C#" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />

    <object id="Object1" classid="CLSID:3564822F-7B51-4D06-A6D0-1318CD0E3F0C" codebase="RAMLabel.CAB#version=1,0,0,38"
        viewastext>
    </object>


    <script language="javascript" type="text/javascript">

        function body_onload() {
            document.getElementById('txtCaseNo').value = '';
        }

        function btnPrint_onclick() {
            var caseNo = document.getElementById('txtCaseNo').value;
            caseNo = trimString(caseNo);
            if (caseNo.length > 0) {
                // alert(caseNo);
                document.getElementById('txtCaseNo').disabled = true;
                document.getElementById('btnPrint').disabled = true;
                PageMethods.MiddlePackNo_from_CaseNo(caseNo, MiddlePackNo_from_CaseNo_CallBack, MiddlePackNo_from_CaseNo_Error, caseNo);
            } else {
                document.getElementById('txtCaseNo').value = '';
                document.getElementById('txtCaseNo').focus();
            }

        }

        function MiddlePackNo_from_CaseNo_CallBack(response, context) {

            if (response.length == 0) {
                alert("Packno could not be determined!");
            } else {
                //alert('Print Label CaseNo:' + context + ' PackNo:' + response);
                var label = CreateLabel(context, response);
                // alert(label);
                PrintToDefaultPort(label);
            }


            document.getElementById('txtCaseNo').disabled = false;
            document.getElementById('btnPrint').disabled = false;
            document.getElementById('txtCaseNo').value = '';
            document.getElementById('txtCaseNo').focus();

        }
        function MiddlePackNo_from_CaseNo_Error(error) {
            alert('Error: ' + error.get_message());

            document.getElementById('txtCaseNo').disabled = false;
            document.getElementById('btnPrint').disabled = false;
            document.getElementById('txtCaseNo').focus();

        }

        function trimString(str) { return str.replace(/^\s+/g, '').replace(/\s+$/g, ''); }

        function CreateLabel(CaseNo, PackNo) {
            //var label = 'N\r\nq832\r\nQ1184, 24 + 0\r\nS6\r\nD12\r\nZT\r\nB24, 24, 0, 1, 3, 3, 200, N, "' + PackNo + '"\r\nA24, 250, 0, 2, 1, 1, N, "CaseNo: ' + CaseNo + '   PackNo: ' + PackNo + '"\r\nP1\r\n';
            var label = 'N\r\nB24,24,0,1,3,3,200,N,"' + PackNo + '"\r\nA24,250,0,2,1,1,N,"CaseNo: ' + CaseNo + '   PackNo: ' + PackNo + '"\r\nP1\r\n';
            return label;
        }

        function PrintToDefaultPort(data) {
            try { 
                var printObj = new ActiveXObject('RAMLabel.clsPrint');
                printObj.PrintToDefaultPort(data);
                printObj = null;
            } catch (e) {
                alert(e);
            }
        }








    </script>
</head>
<body onload="body_onload();">
    <form id="form1" runat="server">




        <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true"></asp:ScriptManager>

        <div>


            <h3>Print PackNo Barcode</h3>
            <br />

            CaseNo:
            <input type="text" id="txtCaseNo" value="" maxlength="5" runat="server" style="width: 100px" autocomplete="off" onkeypress="var key = window.event ? event.keyCode : event.which; if ((key == 32)|(key == 13)) btnPrint_onclick();" />&nbsp; &nbsp;
            <input type="button" id="btnPrint" value="Print" onclick="btnPrint_onclick();" />

            <br />
            <br />
            <br />
            <a href="PrintTest.aspx" style="font-size: x-small; color: #808080">Configure Label Printing</a>
        </div>
        <%-- <div style="border: 1px solid #999999; padding: 2px; position: fixed; background-color: #DFDFDF; font-size: small; top: 0px; right: 0px; color: #666666; text-align: center; vertical-align: middle;"><%=System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") %></div>--%>
    </form>


</body>
</html>
