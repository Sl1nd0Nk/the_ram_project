﻿

function isNumber(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}

function isDate(val) {
    var pattDate = /^[0-9]{4}-[0-9]{2}-[0-9]{2}$/;
    if (pattDate.test(val) == false) return false;
    var d = new Date(val);
    return !isNaN(d.valueOf());
}
function isDateTime(val) {
    var pattDateTime = /^[0-9]{4}-[0-9]{2}-[0-9]{2} (0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$/;
    if (pattDateTime.test(val) == false) return false;
    var d = new Date(val.substr(0, 10));
    return !isNaN(d.valueOf());
}



function setCookie(c_name, value, expiredays) {
    var test = getCookie(c_name);
    if (test != '') delCookie(c_name);
    var exdate = new Date();
    exdate.setDate(exdate.getDate() + expiredays);
    document.cookie = c_name + '=' + escape(value) + ((expiredays == null) ? '' : ';expires=' + exdate.toGMTString());
}
function getCookie(c_name) {
    if (document.cookie.length > 0) {
        c_start = document.cookie.indexOf(c_name + '=');
        if (c_start != -1) {
            c_start = c_start + c_name.length + 1;
            c_end = document.cookie.indexOf(';', c_start);
            if (c_end == -1) c_end = document.cookie.length;
            return unescape(document.cookie.substring(c_start, c_end));
        }
    }
    return '';
}
function delCookie(c_name) {
    var cookie_date = new Date();  // current date & time
    cookie_date.setTime(cookie_date.getTime() - (10 * 24 * 60 * 60 * 1000));
    document.cookie = c_name += '=; expires=' + cookie_date.toGMTString();
}
