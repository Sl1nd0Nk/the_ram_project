﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MsgIn_ARR_List.aspx.cs" Inherits="RAMWMSHost_Web.MsgQueue.MsgIn.MsgIn_ARR_List" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ARR List</title>
    <link href="../../Style1.css" rel="stylesheet" />

    
    <script type="text/javascript">
        function ShowARR(ARRID) {
            window.open('MsgIn_ARR_View.aspx?ARRID=' + ARRID, '_blank', 'height=500,width=700');
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div   >
            <table border="0">
                <tr>
                    <td colspan="3">
                        <h3>ARR List: <%=mRecordState %></h3>
                    </td>
                </tr>
                <tr>
                    <td style="vertical-align: top">
                        <table class="list">
                            <thead>
                                <tr>
                                    <td>RecordDT</td>
                                    <td>SiteCode</td>
                                    <td>ProdCode</td>
                                    <td></td>
                                </tr>
                            </thead>
                            <tbody>
                                <%foreach (System.Data.DataRow dtl in mARR_List.Tables[0].Rows) { %>
                                <tr>
                                    <td><%=dtl["RecordDT"] %></td>
                                    <td><%=dtl["SiteCode"] %></td>
                                    <td><%=dtl["ProdCode"] %></td>
                                    <td><a target="_self" onclick="ShowARR('<%=dtl["ARRID"] %>');" href="#">Show</a></td>
                                </tr>
                                <%} %>
                            </tbody>
                        </table>

                    </td>

                </tr>
            </table>
        </div>
        <%-- <div style="border: 1px solid #999999; padding: 2px; position: fixed; background-color: #DFDFDF; font-size: small; top: 0px; right: 0px; color: #666666; text-align: center; vertical-align: middle;"><%=System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") %></div>--%>
    </form>
</body>
</html>
