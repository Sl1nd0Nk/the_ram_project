﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MsgIn_OCO_View.aspx.cs" Inherits="RAMWMSHost_Web.MsgQueue.MsgIn.MsgIn_OCO_View" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>OCO Details</title>
    <link href="../../Style1.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <div>

            <% System.Data.DataRow hdr = mOCO.Tables[0].Rows[0]; %>

            <table>
                <tr>
                    <td colspan="3">
                        <h3>OCO #<%=hdr["OCOID"] %></h3>
                    </td>
                </tr>
                <tr>
                    <td style="vertical-align: top">
                        <table class="list">
                            <thead>
                                <tr>
                                    <td colspan="2">Control:</td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="key">OCOID</td>
                                    <td><%=hdr["OCOID"] %></td>
                                </tr>
                                <tr>
                                    <td class="key">RecordDT</td>
                                    <td><%=Convert.ToDateTime( hdr["RecordDT"]).ToString("yyyy-MM-dd HH:mm:ss") %></td>
                                </tr>
                                <tr>
                                    <td class="key">RecordState</td>
                                    <td><%=hdr["RecordState"] %></td>
                                </tr>
                                <tr>
                                    <td class="key">ProcAttempts</td>
                                    <td><%=hdr["ProcAttempts"] %></td>
                                </tr>
                                <tr>
                                    <td class="key">ProcStartDT</td>
                                    <td><%=hdr["ProcStartDT"] %></td>
                                </tr>
                                <tr>
                                    <td class="key">ProcEndDT</td>
                                    <td><%=hdr["ProcEndDT"] %></td>
                                </tr>
                                <tr>
                                    <td class="key">ProcErrors</td>
                                    <td><%=hdr["ProcErrors"] %></td>
                                </tr>
                            </tbody>
                        </table>

                    </td>
                    <td style="vertical-align: top">
                        <table class="list">
                            <thead>
                                <tr>
                                    <td colspan="2">OCO:</td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="key">SiteCode</td>
                                    <td><%=hdr["SiteCode"] %></td>
                                </tr>
                                <tr>
                                    <td class="key">PrincipalCode</td>
                                    <td><%=hdr["PrincipalCode"] %></td>
                                </tr>
                                <tr>
                                    <td class="key">OrderNumber</td>
                                    <td><%=hdr["OrderNumber"] %></td>
                                </tr>
                                <tr>
                                    <td class="key">Priority</td>
                                    <td><%=hdr["Priority"] %></td>
                                </tr>
                                <tr>
                                    <td class="key">UserID</td>
                                    <td><%=hdr["UserID"] %></td>
                                </tr>
                                <tr>
                                    <td class="key">Weight</td>
                                    <td><%=hdr["Weight"] %></td>
                                </tr>
                                <tr>
                                    <td class="key">OrderCost</td>
                                    <td><%=hdr["OrderCost"] %></td>
                                </tr>
                                <tr>
                                    <td class="key">OrderDiscount</td>
                                    <td><%=hdr["OrderDiscount"] %></td>
                                </tr>
                                <tr>
                                    <td class="key">OrderVAT</td>
                                    <td><%=hdr["OrderVAT"] %></td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                    <td style="vertical-align: top">
                        <table class="list">
                            <thead>
                                <tr>
                                    <td colspan="2">SerialNumbers:</td>
                                </tr>
                            </thead>
                            <tbody>
                                <%
                                    int r = 0;
                                    foreach (System.Data.DataRow dtl in mOCO.Tables[1].Rows) { %>
                                <tr>
                                    <td class="key">LineNumber</td>
                                    <td><%=dtl["LineNumber"] %></td>
                                </tr>
                                <tr>
                                    <td class="key">ProdCode</td>
                                    <td><%=dtl["ProdCode"] %></td>
                                </tr>
                                <tr>
                                    <td class="key">QuantityPicked</td>
                                    <td><%=dtl["QuantityPicked"] %></td>
                                </tr>
                                <tr>
                                    <td class="key">SerialNumber</td>
                                    <td><%=dtl["SerialNumber"] %></td>
                                </tr>
                                <tr>
                                    <td class="key">UnitCost</td>
                                    <td><%=dtl["UnitCost"] %></td>
                                </tr>
                                <tr>
                                    <td class="key">Vat</td>
                                    <td><%=dtl["Vat"] %></td>
                                </tr>
                                <tr>
                                    <td class="key">Discount</td>
                                    <td><%=dtl["Discount"] %></td>
                                </tr>
                                <%if (r < mOCO.Tables[1].Rows.Count - 1) { %>
                                <tr>
                                    <td class="divider key"></td>
                                    <td class="divider"></td>
                                </tr>
                                <%} %>
                                <%
                                  r = r + 1;
                                    } %>
                            </tbody>
                        </table>

                    </td>
                </tr>
            </table>

        </div>
    </form>
</body>
</html>
