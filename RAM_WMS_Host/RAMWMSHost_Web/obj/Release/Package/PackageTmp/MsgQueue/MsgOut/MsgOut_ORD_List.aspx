﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MsgOut_ORD_List.aspx.cs" Inherits="RAMWMSHost_Web.MsgQueue.MsgOut.MsgOut_ORD_List" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>ORD List</title>
    <link href="../../Style1.css" rel="stylesheet" />
    <script type="text/javascript">
        function ShowORD(ORDID) {
            window.open('MsgOut_ORD_View.aspx?ORDID=' + ORDID, '_blank', 'height=600,width=950');
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div   >
            <table border="0">
                <tr>
                    <td colspan="3">
                        <h3>ORD List: <%=mRecordState %></h3>
                    </td>
                </tr>

                <tr>
                    <td colspan="3">
                        <table>
                            <tr>
                           
                                <td>OrderNumber</td>
                                 
                             
                                <td  >
                                    <input type="text" id="textFilter" runat="server"  style="width: 100px" /></td>
                                <td>
                                    <asp:Button ID="btnFilter" runat="server" Text="Filter" OnClick="btnFilter_Click" /></td>
                            </tr>
                        </table>

                    </td>
                </tr>

                <tr>
                    <td style="vertical-align: top">
                        <table class="list">
                            <thead>
                                <tr>
                                    <td>RecordDT</td>
                                    <td>SiteCode</td>
                                    <td>OrderNumber</td>
                                    <td></td>
                                </tr>
                            </thead>
                            <tbody>
                                <%foreach (System.Data.DataRow dtl in mORD_List.Tables[0].Rows) { %>
                                <tr>
                                    <td><%=dtl["RecordDT"] %></td>
                                    <td><%=dtl["SiteCode"] %></td>
                                    <td><%=dtl["OrderNumber"] %></td>
                                    <td><a target="_self" onclick="ShowORD('<%=dtl["ORDID"] %>');" href="#">Show</a></td>
                                </tr>
                                <%} %>
                            </tbody>
                        </table>

                    </td>

                </tr>
            </table>
        </div>
        <%-- <div style="border: 1px solid #999999; padding: 2px; position: fixed; background-color: #DFDFDF; font-size: small; top: 0px; right: 0px; color: #666666; text-align: center; vertical-align: middle;"><%=System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") %></div>--%>
    </form>
</body>
</html>
