﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MsgOut_IGD_View.aspx.cs" Inherits="RAMWMSHost_Web.MsgQueue.MsgOut.MsgOut_IGD_View" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>IGD Details</title>
    <link href="../../Style1.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <div>

            <% System.Data.DataRow hdr = mIGD.Tables[0].Rows[0]; %>

            <table>
                <tr>
                    <td colspan="3">
                        <h3>IGD #<%=hdr["IGDID"] %></h3>
                    </td>
                </tr>
                <tr>
                    <td style="vertical-align: top">
                        <table class="list">
                            <thead>
                                <tr>
                                    <td colspan="2">Control:</td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="key">IGDID</td>
                                    <td><%=hdr["IGDID"] %></td>
                                </tr>
                                <tr>
                                    <td class="key">RecordDT</td>
                                    <td><%=Convert.ToDateTime( hdr["RecordDT"]).ToString("yyyy-MM-dd HH:mm:ss") %></td>
                                </tr>
                                <tr>
                                    <td class="key">RecordState</td>
                                    <td><%=hdr["RecordState"] %></td>
                                </tr>
                                <tr>
                                    <td class="key">ProcAttempts</td>
                                    <td><%=hdr["ProcAttempts"] %></td>
                                </tr>
                                <tr>
                                    <td class="key">ProcStartDT</td>
                                    <td><%=hdr["ProcStartDT"] %></td>
                                </tr>
                                <tr>
                                    <td class="key">ProcEndDT</td>
                                    <td><%=hdr["ProcEndDT"] %></td>
                                </tr>
                                <tr>
                                    <td class="key">ProcErrors</td>
                                    <td><%=hdr["ProcErrors"] %></td>
                                </tr>
                            </tbody>
                        </table>

                    </td>
                    <td style="vertical-align: top">
                        <table class="list">
                            <thead>
                                <tr>
                                    <td colspan="2">IGD:</td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="key">SiteCode</td>
                                    <td><%=hdr["SiteCode"] %></td>
                                </tr>
                                <tr>
                                    <td class="key">PrincipalCode</td>
                                    <td><%=hdr["PrincipalCode"] %></td>
                                </tr>


                                <tr>
                                    <td class="key">ProdCode</td>
                                    <td><%=hdr["ProdCode"] %></td>
                                </tr>
                                <tr>
                                    <td class="key">EANCode</td>
                                    <td><%=hdr["EANCode"] %></td>
                                </tr>
                                <tr>
                                    <td class="key">ShortDesc</td>
                                    <td><%=hdr["ShortDesc"] %></td>
                                </tr>
                                <tr>
                                    <td class="key">LongDesc</td>
                                    <td><%=hdr["LongDesc"] %></td>
                                </tr>
                                <tr>
                                    <td class="key">Serialised</td>
                                    <td><%=hdr["Serialised"] %></td>
                                </tr>
                                <tr>
                                    <td class="key">AnalysisA</td>
                                    <td><%=hdr["AnalysisA"] %></td>
                                </tr>
                                <tr>
                                    <td class="key">AnalysisB</td>
                                    <td><%=hdr["AnalysisB"] %></td>
                                </tr>
                                <tr>
                                    <td class="key">OrderLineNo</td>
                                    <td><%=hdr["OrderLineNo"] %></td>
                                </tr>
                                <tr>
                                    <td class="key">Quantity</td>
                                    <td><%=hdr["Quantity"] %></td>
                                </tr>
                                <tr>
                                    <td class="key">ReceiptType</td>
                                    <td><%=hdr["ReceiptType"] %>
                                        &nbsp;
                                        <%if (hdr["ReceiptType"].ToString() == "0") { %>New Stock <% } %>
                                        <%if (hdr["ReceiptType"].ToString() == "1") { %>Returned Stock<% } %>
                                        <%if (hdr["ReceiptType"].ToString() == "2") { %>Take On<% } %>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="key">MoveRef</td>
                                    <td><%=hdr["MoveRef"] %></td>
                                </tr>
                                <tr>
                                    <td class="key">PORef</td>
                                    <td><%=hdr["PORef"] %></td>
                                </tr>
                                <tr>
                                    <td class="key">PODate</td>
                                    <td><%=Convert.ToDateTime(  hdr["PODate"]).ToString("yyyy-MM-dd") %></td>
                                </tr>
                                <tr>
                                    <td class="key">StockDateTime</td>
                                    <td><%=Convert.ToDateTime( hdr["StockDateTime"]).ToString("yyyy-MM-dd HH:mm") %></td>
                                </tr>



                            </tbody>
                        </table>
                    </td>

                </tr>
            </table>

        </div>
    </form>
</body>
</html>
