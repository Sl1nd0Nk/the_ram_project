﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MsgOut_ORD_View.aspx.cs" Inherits="RAMWMSHost_Web.MsgQueue.MsgOut.MsgOut_ORD_View" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>ORD Details</title>
    <link href="../../Style1.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <div>

            <% System.Data.DataRow hdr = mORD.Tables[0].Rows[0]; %>

            <table>
                <tr>
                    <td colspan="3">
                        <h3>ORD #<%=hdr["ORDID"] %></h3>
                    </td>
                </tr>
                <tr>
                    <td style="vertical-align: top">
                        <table class="list">
                            <thead>
                                <tr>
                                    <td colspan="2">Control:</td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="key">ORDID</td>
                                    <td><%=hdr["ORDID"] %></td>
                                </tr>
                                <tr>
                                    <td class="key">RecordDT</td>
                                    <td><%=Convert.ToDateTime( hdr["RecordDT"]).ToString("yyyy-MM-dd HH:mm:ss") %></td>
                                </tr>
                                <tr>
                                    <td class="key">RecordState</td>
                                    <td><%=hdr["RecordState"] %></td>
                                </tr>
                                <tr>
                                    <td class="key">ProcAttempts</td>
                                    <td><%=hdr["ProcAttempts"] %></td>
                                </tr>
                                <tr>
                                    <td class="key">ProcStartDT</td>
                                    <td><%=hdr["ProcStartDT"] %></td>
                                </tr>
                                <tr>
                                    <td class="key">ProcEndDT</td>
                                    <td><%=hdr["ProcEndDT"] %></td>
                                </tr>
                                <tr>
                                    <td class="key">ProcErrors</td>
                                    <td><%=hdr["ProcErrors"] %></td>
                                </tr>
                            </tbody>

                            <thead>
                                <tr>
                                    <td colspan="2">RMS Import Status:</td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="key">RMS_RecordState</td>
                                    <td><%=hdr["RMS_RecordState"] %></td>
                                </tr>
                                <tr>
                                    <td class="key">RMS_ProcAttempts</td>
                                    <td><%=hdr["RMS_ProcAttempts"] %></td>
                                </tr>
                                <tr>
                                    <td class="key">RMS_ProcStartDT</td>
                                    <td><%=hdr["RMS_ProcStartDT"] %></td>
                                </tr>
                                <tr>
                                    <td class="key">RMS_ProcEndDT</td>
                                    <td><%=hdr["RMS_ProcEndDT"] %></td>
                                </tr>
                                <tr>
                                    <td class="key">RMS_ProcErrors</td>
                                    <td><%=hdr["RMS_ProcErrors"] %></td>
                                </tr>
                            </tbody>


                        </table>

                    </td>
                    <td style="vertical-align: top">
                        <table class="list">
                            <thead>
                                <tr>
                                    <td colspan="4">ORD:</td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="key">SiteCode</td>
                                    <td><%=hdr["SiteCode"] %></td>
                                    <td class="key">CustAcc</td>
                                    <td><%=hdr["CustAcc"] %></td>
                                </tr>
                                <tr>
                                    <td class="key">PrincipalCode</td>
                                    <td><%=hdr["PrincipalCode"] %></td>
                                    <td class="key">InvName</td>
                                    <td><%=hdr["InvName"] %></td>
                                </tr>
                                <tr>
                                    <td class="key">OrderNumber</td>
                                    <td><%=hdr["OrderNumber"] %></td>
                                    <td class="key">InvAdd1</td>
                                    <td><%=hdr["InvAdd1"] %></td>
                                </tr>
                                <tr>
                                    <td class="key">DateCreated</td>
                                    <td><%= Convert.ToDateTime( hdr["DateCreated"]).ToString("yyyy-MM-dd") %></td>
                                    <td class="key">InvAdd2</td>
                                    <td><%=hdr["InvAdd2"] %></td>
                                </tr>
                                <tr>
                                    <td class="key">CompanyName</td>
                                    <td><%=hdr["CompanyName"] %></td>
                                    <td class="key">InvAdd3</td>
                                    <td><%=hdr["InvAdd3"] %></td>
                                </tr>
                                <tr>
                                    <td class="key">HeaderComment</td>
                                    <td><%=hdr["HeaderComment"] %></td>
                                    <td class="key">InvAdd4</td>
                                    <td><%=hdr["InvAdd4"] %></td>
                                </tr>
                                <tr>
                                    <td class="key">CustOrderNumber</td>
                                    <td><%=hdr["CustOrderNumber"] %></td>
                                    <td class="key">InvAdd5</td>
                                    <td><%=hdr["InvAdd5"] %></td>
                                </tr>
                                <tr>
                                    <td class="key">GSMTest</td>
                                    <td><%=hdr["GSMTest"] %></td>
                                    <td class="key">InvAdd6</td>
                                    <td><%=hdr["InvAdd6"] %></td>
                                </tr>
                                <tr>
                                    <td class="key">VATNumber</td>
                                    <td><%=hdr["VATNumber"] %></td>
                                </tr>
                                <tr>
                                    <td class="key">PrintPrice</td>
                                    <td><%=hdr["PrintPrice"] %></td>
                                    <td class="key">DeliveryAdd1</td>
                                    <td><%=hdr["DeliveryAdd1"] %></td>
                                </tr>
                                <tr>
                                    <td class="key">Priority</td>
                                    <td><%=hdr["Priority"] %></td>
                                    <td class="key">DeliveryAdd2</td>
                                    <td><%=hdr["DeliveryAdd2"] %></td>
                                </tr>
                                <tr>
                                    <td class="key">VAP</td>
                                    <td><%=hdr["VAP"] %></td>
                                    <td class="key">DeliveryAdd3</td>
                                    <td><%=hdr["DeliveryAdd3"] %></td>
                                </tr>
                                <tr>
                                    <td class="key">SalesPerson</td>
                                    <td><%=hdr["SalesPerson"] %></td>
                                    <td class="key">DeliveryAdd4</td>
                                    <td><%=hdr["DeliveryAdd4"] %></td>
                                </tr>
                                <tr>
                                    <td class="key">SalesCategory</td>
                                    <td><%=hdr["SalesCategory"] %></td>
                                    <td class="key">DeliveryAdd5</td>
                                    <td><%=hdr["DeliveryAdd5"] %></td>
                                </tr>
                                <tr>
                                    <td class="key">Processor</td>
                                    <td><%=hdr["Processor"] %></td>
                                    <td class="key">DeliveryAdd6</td>
                                    <td><%=hdr["DeliveryAdd6"] %></td>
                                </tr>
                                <tr>
                                    <td class="key">WMSPostCode</td>
                                    <td><%=hdr["WMSPostCode"] %></td>
                                </tr>
                                <tr>
                                    <td class="key">OrderDiscount</td>
                                    <td><%=hdr["OrderDiscount"] %></td>
                                </tr>
                                <tr>
                                    <td class="key">OrderVAT</td>
                                    <td><%=hdr["OrderVAT"] %></td>
                                    <td class="key">LineCount</td>
                                    <td><%=hdr["LineCount"] %></td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                    <td style="vertical-align: top">
                        <table class="list">
                            <thead>
                                <tr>
                                    <td colspan="8">Order Lines:</td>
                                </tr>
                            </thead>
                            <tbody>
                                <%int r = 0;
                                  foreach (System.Data.DataRow dtl in mORD.Tables[1].Rows) { %>
                                <tr>
                                    <td class="key">LineNumber</td>
                                    <td><%=dtl["LineNumber"] %></td>
                                    <td class="key">LineType</td>
                                    <td><%=dtl["LineType"] %></td>
                                    <td class="key">ProdCode</td>
                                    <td><%=dtl["ProdCode"] %></td>
                                    <td class="key">LineText</td>
                                    <td><%=dtl["LineText"] %></td>
                                </tr>
                                <tr>
                                    <td class="key">Quantity</td>
                                    <td><%=dtl["Quantity"] %></td>
                                    <td class="key">UnitCost</td>
                                    <td><%=dtl["UnitCost"] %></td>
                                    <td class="key">Vat</td>
                                    <td><%=dtl["Vat"] %></td>
                                    <td class="key">Discount</td>
                                    <td><%=dtl["Discount"] %></td>
                                </tr>
                                <%if (r < mORD.Tables[1].Rows.Count - 1) { %>
                                <tr>
                                    <td class="divider key"></td>
                                    <td class="divider"></td>
                                    <td class="divider key"></td>
                                    <td class="divider"></td>
                                    <td class="divider key"></td>
                                    <td class="divider"></td>
                                    <td class="divider key"></td>
                                    <td class="divider"></td>
                                </tr>
                                <%} %>
                                <%
                                  r = r + 1;
                                  } %>
                            </tbody>
                        </table>

                    </td>
                </tr>
            </table>

        </div>
    </form>
</body>
</html>
