﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace RAMWMSHost_Web.ClientWS
{
    /// <summary>
    /// Summary description for CustomerReturnReceipt
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class CustomerReturnReceipt : System.Web.Services.WebService
    {
        public class Response
        {
            public bool success { get; set; }
            public int reasoncode { get; set; }
            public string reasontext { get; set; }
        }

        [WebMethod]
        public Response Return(string PrincipalCode,
                               bool Resend,
                               int OrderLineNo,
                               string ProdCode,
                               string MovementRef,
                               string ReturnReferenceNumber,
                               DateTime ReceiptDateTime,
                               int RejectCount,
                               string ReasonCode,
                               int AcceptCount,
                               string[] SerialNumbers)
        {
            Response Res = new Response();
            Res.success = true;
            Res.reasoncode = 0;
            Res.reasontext = "";

            return Res;
        }
    }
}
