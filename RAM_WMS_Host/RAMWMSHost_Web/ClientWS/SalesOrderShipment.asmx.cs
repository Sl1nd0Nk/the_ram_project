﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Xml.Serialization;

namespace RAMWMSHost_Web.ClientWS
{
    /// <summary>
    /// Summary description for SalesOrderShipment
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class SalesOrderShipment : System.Web.Services.WebService
    {

        public class Response
        {
            public bool success { get; set; }
            public int reasoncode { get; set; }
            public string reasontext { get; set; }
        }

        public class OrderLine
        {
            public string LineNumber { get; set; }
            public string ProdCode { get; set; }
            public int Quantity { get; set; }
            public double UnitCost { get; set; }
            public double VAT { get; set; }
            public double UnitDiscountAmount { get; set; }
            public string[] SerialNumbers { get; set; }
        }

        [WebMethod]
        public Response SOShipment(string PrincipalCode,
                                  string OrderNumber,
                                  string Priority,
                                  string UserID,
                                  double Weight,
                                  double OrderTotal,
                                  double OrderDiscount,
                                  double OrderVAT,
                                  [XmlElementAttribute(IsNullable = true)] List<OrderLine> Lines)
        {
            Response Res = new Response();
            Res.success = true;
            Res.reasoncode = 0;
            Res.reasontext = "";

            return Res;
        }
    }
}
