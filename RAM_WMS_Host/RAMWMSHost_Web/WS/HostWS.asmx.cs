﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Xml;
using System.Data;


namespace RAMWMSHost_Web.WS {
    /// <summary>
    /// Summary description for HostWS
    /// </summary>
    [WebService(Namespace = "http://services.ramgroup.co.za/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class HostWS : System.Web.Services.WebService {

        #region subclasses
        public class Result {
            public bool Success { get; set; }
            public string ReasonText { get; set; }
            public Result() { }
            public Result(bool success, string reasonText) { Success = success; ReasonText = reasonText; }
        }

        public class OCOLine {
            public int LineNumber { get; set; }
            public string ProdCode { get; set; }
            public int QuantityPicked { get; set; }
            public string SerialNumber { get; set; }
            public double UnitCost { get; set; }
            public double Vat { get; set; }
            public double Discount { get; set; }
        }

        //JMD
        public class ORDLine
        {
            public int LineNumber { get; set; }
            public string LineType { get; set; }
            public string ProductCode { get; set; }
            public string LineText { get; set; }
            public int Quantity { get; set; }
            public double UnitCost { get; set; }
            public double VAT {get;set;}
            public double UnitDiscountAmount {get ; set;}


        }


        #endregion

        
        [WebMethod]
        public string HelloWorld(string name) {
           return "Hello " + name.ToUpper() + " @ "+  System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
        }

        #region ARR
        [WebMethod]
        public Result ARRAdd(string SiteCode,
                                    string PrincipalCode,
                                    bool Resend,
                                    string ProdCode,
                                    string LineNumber,
                                    string ReceiptType,
                                    string MoveRef,
                                    string PORef,
                                    DateTime ReceiptDT,
                                    string StartEndStatus,
                                    int RejectCount,
                                    string ReasonCode,
                                    int AcceptCount,
                                    string[] SerialNumbers) {

            int ARRID = 0;

            ARRID = BLL.MsgIn.MsgIn_ARR.MsgIn_ARR_Add(
                                      "HostWS",
                                      SiteCode,
                                      PrincipalCode,
                                      Resend,
                                      ProdCode,
                                      LineNumber,
                                      ReceiptType,
                                      MoveRef,
                                      PORef,
                                      ReceiptDT,
                                      StartEndStatus,
                                      RejectCount,
                                      ReasonCode,
                                      AcceptCount);
            foreach (string SerialNumber in SerialNumbers) {
                bool rowAdded = BLL.MsgIn.MsgIn_ARR.MsgIn_ARR_SN_Add(ARRID, SerialNumber) == 1;
            }
            bool rowUpdated = BLL.MsgIn.MsgIn_ARR.MsgIn_ARR_SetREADY(ARRID) == 1;
            return new Result(true, string.Empty);
        }

        #endregion

        #region CLC
        //JMD 20150126
        [WebMethod]
        public Result CLCAdd(   string recordSource,
                                string siteCode,
                                string principalCode,  
                                string moveRef,
                                string orderLineNumber,
                                string prodCode,
                                int quantity,
                                string receiptType,
                                int discrepancy,
                                 int serialCount,
                                 string[] SerialNumbers)
        {

            int CLCID = 0;

           CLCID = BLL.MsgOut.MsgOut_CLC.MsgOut_CLC_Add(
                                        recordSource,
                                        siteCode,
                                        principalCode,
                                        moveRef,
                                        orderLineNumber,
                                        prodCode,
                                        quantity,
                                        receiptType,
                                        discrepancy,
                                        serialCount); 
                                        

            foreach (string SerialNumber in SerialNumbers)
            {
                bool rowAdded = BLL.MsgOut.MsgOut_CLC.MsgOut_CLC_SN_Add(CLCID, SerialNumber) == 1;
            }
            bool rowUpdated = BLL.MsgOut.MsgOut_CLC.MsgOut_CLC_SetREADY(CLCID) == 1;
            return new Result(true, string.Empty);
        }

        #endregion

        #region SAA
        [WebMethod]
        public Result SAAAdd(string SiteCode,
                                    string PrincipalCode,
                                    string ProdCode,
                                    string QuantityChange) {

            int SAAID = 0;

            SAAID = BLL.MsgIn.MsgIn_SAA.MsgIn_SAA_Add(
                                      "HostWS",
                                      SiteCode,
                                      PrincipalCode,
                                      ProdCode,
                                      QuantityChange);
            return new Result(true, string.Empty);
        }

        #endregion

        #region ORD
        [WebMethod]
        public Result ORDAdd(string recordSource,
                                        string siteCode,
                                        string principalCode,

                                        string orderNumber,
                                        int lineCount,
                                        DateTime dateCreated,
                                        string companyName,
                                        string headerComment,
                                        string custOrderNumber,
                                        string custAcc,
                                        string invName,
                                        string invAdd1,
                                        string invAdd2,
                                        string invAdd3,
                                        string invAdd4,
                                        string invAdd5,
                                        string invAdd6,
                                        bool gsmTest,
                                        string vatNumber,
                                        bool printPrice,
                                        string priority,
                                        bool vap,
                                        string salesPerson,
                                        string salesCategory,
                                        string processor,
                                        string deliveryAdd1,
                                        string deliveryAdd2,
                                        string deliveryAdd3,
                                        string deliveryAdd4,
                                        string deliveryAdd5,
                                        string deliveryAdd6,
                                        string wmsPostCode,
                                        double orderDiscount,
                                        double orderVAT,
                                        int docToPrint,
                                        ORDLine[] ordLines,
                                        string  IDNumber,
                                        string KYC,
                                        string CourierName,
                                        string CourierService,
                                        bool InsuranceRequired,
                                        bool ValidateDelivery,
                                        string StoreCode
                                        )
        {

            int ORDID = 0;

            //WE MUST STILL add the order lines

            ORDID = BLL.MsgOut.MsgOut_ORD.MsgOut_ORD_Add( recordSource,
                                         siteCode,
                                         principalCode,
                                         orderNumber,
                                         lineCount,
                                         dateCreated,
                                         companyName,
                                         headerComment,
                                         custOrderNumber,
                                         custAcc,
                                         invName,
                                         invAdd1,
                                         invAdd2,
                                         invAdd3,
                                         invAdd4,
                                         invAdd5,
                                         invAdd6,
                                         gsmTest,
                                         vatNumber,
                                         printPrice,
                                         priority,
                                         vap,
                                         salesPerson,
                                         salesCategory,
                                         processor,
                                         deliveryAdd1,
                                         deliveryAdd2,
                                         deliveryAdd3,
                                         deliveryAdd4,
                                         deliveryAdd5,
                                         deliveryAdd6,
                                         wmsPostCode,
                                         orderDiscount,
                                         orderVAT,
                                         docToPrint,
                                         IDNumber,
                                         KYC,
                                         CourierName,
                                         CourierService,
                                         InsuranceRequired,
                                         ValidateDelivery,
                                         StoreCode
                                        ); 
                
               
            foreach (ORDLine ln in ordLines)
            {
                bool rowAdded = BLL.MsgOut.MsgOut_ORD.MsgOut_ORD_LN_Add(ORDID,
                                                                ln.LineNumber,
                                                                ln.LineType, 
                                                                ln.ProductCode,
                                                                ln.LineText,
                                                                ln.Quantity,
                                                                ln.UnitCost,
                                                                ln.VAT,
                                                                ln.UnitDiscountAmount
                                                              
                ) == 1;
            }
            bool rowUpdated = BLL.MsgOut.MsgOut_ORD.MsgOut_ORD_SetREADY(ORDID) == 1;
            return new Result(true, string.Empty);
        }

        #endregion

        #region OCO
        [WebMethod]
        public Result OCOAdd(string SiteCode,
                            string PrincipalCode,
                            string OrderNumber,
                            string Priority,
                            string UserID,
                            double Weight,
                            double OrderCost,
                            double OrderDiscount,
                            double OrderVAT,
                            OCOLine[] Lines) {

            int OCOID = 0;

            OCOID = BLL.MsgIn.MsgIn_OCO.MsgIn_OCO_Add("HostWS",
                                                SiteCode,
                                                PrincipalCode,
                                                OrderNumber,
                                                Priority,
                                                UserID,
                                                Weight,
                                                OrderCost,
                                                OrderDiscount,
                                                OrderVAT);
            foreach (OCOLine ln in Lines) {
                bool rowAdded = BLL.MsgIn.MsgIn_OCO.MsgIn_OCO_LN_Add(OCOID,
                                                                ln.LineNumber,
                                                                ln.ProdCode,
                                                                ln.QuantityPicked,
                                                                ln.SerialNumber,
                                                                ln.UnitCost,
                                                                ln.Vat,
                                                                ln.Discount
                ) == 1;
            }
            bool rowUpdated = BLL.MsgIn.MsgIn_OCO.MsgIn_OCO_SetREADY(OCOID) == 1;
            return new Result(true, string.Empty);
        }

        #endregion

        #region OST
        [WebMethod]
        public Result OSTAdd(string SiteCode,
                                    string PrincipalCode,
                                    string OrderNumber) {

            int OSTID = 0;

            OSTID = BLL.MsgIn.MsgIn_OST.MsgIn_OST_Add(
                                      "HostWS",
                                      SiteCode,
                                      PrincipalCode,
                                      OrderNumber);
            return new Result(true, string.Empty);
        }

        #endregion

        #region SLA
        [WebMethod]
        public Result SLAAdd(string SiteCode,
                            string PrincipalCode,
                            string ProdCode,
                            int QuantityChange,
                            string ReasonCode,
                            string AvailabilityState,
                            int SerialCount,
                            string[] SerialNumbers) {

            int SLAID = 0;

            SLAID = BLL.MsgIn.MsgIn_SLA.MsgIn_SLA_Add("HostWS",
                                                SiteCode,
                                                PrincipalCode,
                                                ProdCode,
                                                QuantityChange,
                                                ReasonCode,
                                                AvailabilityState,
                                                SerialCount);
            foreach (string SerialNumber in SerialNumbers) {
                bool rowAdded = BLL.MsgIn.MsgIn_SLA.MsgIn_SLA_SN_Add(SLAID, SerialNumber) == 1;
            }
            bool rowUpdated = BLL.MsgIn.MsgIn_SLA.MsgIn_SLA_SetREADY(SLAID) == 1;
            return new Result(true, string.Empty);
        }

        #endregion

        #region IGD 

        //JMD 20150109
        [WebMethod]
        public int IGDAdd(string recordSource,
                            string siteCode,
                            string principalCode,
                            string productCode,
                            string EANCode,
                            string ShortDesc,
                            string LongDesc,
                            bool serialised,
                            string AnalysisA,
                            string AnalysisB,
                            string orderLineNo,
                            int quantity,
                            string receiptType,
                            string moveRef,
                            string poRef,
                            DateTime poDate,
                            DateTime stockDateTime)
        {

            int retVal = 0;
            int IGDID = 0;
            
          try { 
                ////string SiteCode = txtSiteCode.Value;
                ////string PrincipalCode = txtPrincipalCode.Value;
                ////string ProdCode = txtProdCode.Value;
                ////string EANCode = txtEANCode.Value;
                ////string ShortDesc = txtShortDesc.Value;
                ////string LongDesc = txtLongDesc.Value;
                ////bool Serialised = chkSerialised.Checked;
                ////string AnalysisA = txtAnalysisA.Value;
                ////string AnalysisB = txtAnalysisB.Value;
                ////string OrderLineNo = txtOrderLineNo.Value;
                ////int Quantity = Convert.ToInt32(txtQuantity.Value);
                ////string ReceiptType = selectReceiptType.Value;
                ////string MoveRef = txtMoveRef.Value;
                ////string PORef = txtPORef.Value;
                ////DateTime PODate = Convert.ToDateTime(txtPODate.Value);
                ////DateTime StockDateTime = Convert.ToDateTime(txtStockDateTime.Value);

               // int IGDID = BLL.MsgOut.MsgOut_IGD.MsgOut_IGD_Add("RAMWMSHost",

                IGDID = BLL.MsgOut.MsgOut_IGD.MsgOut_IGD_Add(recordSource,
                                              siteCode,
                                              principalCode,

                                              productCode,
                                              EANCode,
                                              ShortDesc,
                                              LongDesc,
                                              serialised,
                                              AnalysisA,
                                              AnalysisB,
                                              orderLineNo,
                                              quantity,
                                              receiptType,
                                              moveRef,
                                              poRef,
                                              poDate,
                                              stockDateTime);


               //dErrorMessage.InnerHtml = "Saved IGD #" + IGDID.ToString();

               
                

            }catch(Exception exc){
                throw new System.Exception(exc.Message.ToString());
            }


          retVal = IGDID;
          return retVal;
            
        }


        #endregion

        #region Parcel
        [WebMethod]
        public Result ParcelAdd(string SiteCode,
                                    string PrincipalCode,
                                    string OrderNumber,

                                            int ParcelNo,
                                            double Kilograms,
                                            double Length,
                                            double Breadth,
                                            double Height,
                                            double InsuredValue,
                                            string SPNumber,
                                            string ParcelReference,
                                            int NoOfParcels) {

            int ParcelID = 0;

            ParcelID = BLL.MsgIn.MsgIn_Parcel.MsgIn_Parcel_Add(
                                        "HostWS",
                                        SiteCode,
                                        PrincipalCode,
                                        OrderNumber,
                                        ParcelNo,
                                        Kilograms,
                                        Length,
                                        Breadth,
                                        Height,
                                        InsuredValue,
                                        SPNumber,
                                        ParcelReference, NoOfParcels);
            return new Result(true, string.Empty);
        }


        [WebMethod]
        public XmlDocument ParcelLabel(string cartonID) {
            string iPAddress = base.Context.Request.UserHostAddress;

            try {
                /** get the parcel info - will consign if needed and possible **/
                DataSet parcelInfo = BLL.RAMTnT.usp_ParcelV1_Info_WMSImport(cartonID);
                //fail immediately if no data returned
                if (parcelInfo == null || parcelInfo.Tables.Count == 0 || parcelInfo.Tables[0].Rows.Count == 0) {
                    return CreateFailedResponse("Could not determine parcel info");
                }
                //parcel status
                string ImportState = parcelInfo.Tables[0].Rows[0]["ImportState"].ToString();
                string ConsignmentState = parcelInfo.Tables[0].Rows[0]["ConsignmentState"].ToString();
                string ParcelState = parcelInfo.Tables[0].Rows[0]["ParcelState"].ToString();
                //fail if parcel not imported 
                if (ImportState != "IMPORTED") {
                    return CreateFailedResponse("Parcel import not found");
                } else if (ConsignmentState != "CONSIGNED") {
                    return CreateFailedResponse("Not consigned");
                }
                //safe to get the rest of the parameters
                string ConsignmentID = parcelInfo.Tables[0].Rows[0]["ConsignmentID"].ToString();
                string TrackingNo = parcelInfo.Tables[0].Rows[0]["TrackingNo"].ToString();
                double Kilograms = Convert.ToDouble(parcelInfo.Tables[0].Rows[0]["Kilograms"]);
                double Length = Convert.ToDouble(parcelInfo.Tables[0].Rows[0]["Length"]);
                double Breadth = Convert.ToDouble(parcelInfo.Tables[0].Rows[0]["Breadth"]);
                double Height = Convert.ToDouble(parcelInfo.Tables[0].Rows[0]["Height"]);
                double InsuredValue = Convert.ToDouble(parcelInfo.Tables[0].Rows[0]["InsuredValue"]);
                string SecurityPackNo = parcelInfo.Tables[0].Rows[0]["SecurityPackNo"].ToString();
                string ParcelReference = parcelInfo.Tables[0].Rows[0]["ParcelReference"].ToString();
                int ParcelNo = Convert.ToInt32(parcelInfo.Tables[0].Rows[0]["ParcelNo"]);
                int Prints = Convert.ToInt32(parcelInfo.Tables[0].Rows[0]["Prints"]);

                /** save the parcel record if needed **/
                if (ParcelState == "NEW") {
                    //Saved, ConsignmentID, TrackingNo, Details;
                    DataSet saveResponse = BLL.RAMTnT.usp_ParcelV1_Save(ConsignmentID,
                                                TrackingNo,
                                                Kilograms,
                                                Length,
                                                Breadth,
                                                Height,
                                                true, //UpdateWeights
                                                "HostWS", //HubID
                                                "HostWS", //EmployeeID
                                                iPAddress, //TerminalID
                                                SecurityPackNo,
                                                false, //PreserveConsignDT
                                                ParcelReference,
                                                InsuredValue,
                                                false //AllowUnDimmed
                                                );
                    //fail immediately if no data returned
                    if (saveResponse == null || saveResponse.Tables.Count == 0 || saveResponse.Tables[0].Rows.Count == 0) {
                        return CreateFailedResponse("Parcel save failed with no response");
                    }
                    bool Saved = Convert.ToBoolean(saveResponse.Tables[0].Rows[0]["Saved"]);
                    string Details = saveResponse.Tables[0].Rows[0]["Details"].ToString();
                    //fail if not saved
                    if (Saved == false) {
                        return CreateFailedResponse("Parcel save failed: " + Details);
                    }
                }

                /** retrieve the label data **/
                string labelData = BLL.RAMTnT.GenerateZplLabel(TrackingNo, false, "HostWS");
                //fail if labelText empty
                if (labelData.Length == 0) {
                    return CreateFailedResponse("No label returned");
                }

                /** save the print log **/
                BLL.RAMTnT.usp_PrintLog_Label(ConsignmentID, TrackingNo, "HostWS", iPAddress);

                /** return the label data **/
                XmlDocument doc = CreateSuccessResponse();
                XmlNode docElement = doc.ChildNodes[0];
                AddTextNode(docElement, "ConsignmentID", ConsignmentID);
                AddTextNode(docElement, "TrackingNo", TrackingNo);
                AddTextNode(docElement, "Kilograms", Kilograms);
                AddTextNode(docElement, "Length", Length);
                AddTextNode(docElement, "Breadth", Breadth);
                AddTextNode(docElement, "Height", Height);
                AddTextNode(docElement, "InsuredValue", InsuredValue);
                AddTextNode(docElement, "SecurityPackNo", SecurityPackNo);
                AddTextNode(docElement, "ParcelReference", ParcelReference);
                AddTextNode(docElement, "ParcelNo", ParcelNo);
                AddTextNode(docElement, "Prints", Prints + 1);
                AddTextNode(docElement, "Label", labelData);
                return doc;
            } catch (Exception exc) {
                return CreateFailedResponse(exc.Message);
            }

        }

        [WebMethod]
        public XmlDocument ParcelAddLabelRequest(string SiteCode, string PrincipalCode, string OrderNumber, int ParcelNo, double Kilograms, double Length, double Breadth,
                                            double Height, double InsuredValue, string SPNumber, string ParcelReference, int NoOfParcels)
        {
            ParcelAdd(SiteCode, PrincipalCode, OrderNumber, ParcelNo, Kilograms, Length, Breadth, Height, InsuredValue, SPNumber, ParcelReference, NoOfParcels);
            return ParcelLabel(ParcelReference);
        }

        #endregion

        #region internal methods

        private string GetDocumentContents(System.Web.HttpRequest Request) {
            string documentContents;
            using (System.IO.Stream receiveStream = Request.InputStream) {
                using (System.IO.StreamReader readStream = new System.IO.StreamReader(receiveStream, System.Text.Encoding.UTF8)) {
                    documentContents = readStream.ReadToEnd();
                }
            }
            return documentContents;
        }

        private void AddTextNode(XmlNode parentNode, string name, object value) {
            XmlNode node = parentNode.OwnerDocument.CreateElement(name);
            if (value.GetType() == typeof(DateTime)) node.InnerText = System.Security.SecurityElement.Escape(Convert.ToDateTime(value).ToString("yyyy-MM-ddTHH:mm:ss"));
            else if (value.GetType() == typeof(byte[])) node.InnerText = Convert.ToBase64String((byte[])value);
            else node.InnerText = System.Security.SecurityElement.Escape(value.ToString());
            parentNode.AppendChild(node);
        }

        private XmlDocument CreateFailedResponse(string Errors) {
            XmlDocument doc = new XmlDocument();
            XmlNode Response = doc.CreateElement("Response");
            AddTextNode(Response, "ReturnCode", "Failed");
            AddTextNode(Response, "Errors", Errors);
            doc.AppendChild(Response);
            return doc;
        }

        private XmlDocument CreateSuccessResponse() {
            XmlDocument doc = new XmlDocument();
            XmlNode Response = doc.CreateElement("Response");
            AddTextNode(Response, "ReturnCode", "Success");
            AddTextNode(Response, "Errors", "");
            doc.AppendChild(Response);
            return doc;
        }



        #endregion

    }
}