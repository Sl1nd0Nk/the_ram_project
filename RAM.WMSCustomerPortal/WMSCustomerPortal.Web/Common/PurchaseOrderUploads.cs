﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WMSCustomerPortal.Business;
using WMSCustomerPortal.Models.DataAccess;
using WMSCustomerPortal.Models.Entities;

namespace WMSCustomerPortal.Models.Common
{
    public class PurchaseOrderUploads
    {

        public string POupload(List<string> lines, int principalID, string eventTerminal, string eventUser, string FileName, string transectionType, string fileDelimiter)
        {
            InboundService inboundService = new InboundService();
            InboundMaster stagingRecord = new InboundMaster();
            InboundMasterLineItem items = new InboundMasterLineItem();

            AuditRecord auditRecord = new AuditRecord();
            AuditMail auditMail = new AuditMail();
            SQLDataManager qLDataManager = new SQLDataManager();


            List<string> auditStatus = new List<string>();
            List<InboundMaster> inboundheader = new List<InboundMaster>();
            List<InboundMasterLineItem> inboundMasterLineItems = new List<InboundMasterLineItem>();
            List<int> successRow = new List<int>(); // couting number of rows
            List<int> failsRow = new List<int>(); // couting number of rows
            List<int> header = new List<int>(); // couting number of rows
            List<string> err = new List<string>();


            string pRecordSource = RAM.Utilities.Common.ConfigSettings.ReadConfigValue("WMSRecordSource", "UNKNOWN");
            string pSiteCode = RAM.Utilities.Common.ConfigSettings.ReadConfigValue("WMSSiteCode", "000");

            auditRecord.TransactionType = transectionType;
            auditRecord.FileName = FileName;
            auditRecord.User = eventUser;

            DateTime date;
            int NumericalValue;
            string Status;
            var prodstaging = 0;
            var quantity = 0;
            string pTable = "";
            string combined = "";
            string POHeader="";
            int x = 1;
            // double UnitPrice;


            int i = 0;
            int row = 0;// files rows start from row 1
            int SuccessStatus = 0;
            int FailsStatus = 0;

          
            //remove tablename from list
            lines.RemoveAt(0);
            int loopingHeader = 0;

            try
            {
                foreach (var obj in lines)
                {
                    row++;
                    Object[] objects;
                    if (obj.ToString().Equals(""))// checking for empty row
                    {
                        auditRecord.EventStatus = "Failed".ToUpper();
                        auditRecord.Message = "Row ".ToUpper() + row + " is empty".ToUpper();
                        auditStatus.Add(auditRecord.Message);
                        Status = "Failed";
                        auditRecord.Save(principalID);
                    }
                    else
                    {
                        //splitting
                        if (fileDelimiter == null)
                        {
                            objects = obj.Split(',');// default delimiter 
                        }
                        else
                        {
                            objects = obj.Split(Convert.ToChar(fileDelimiter));// return string
                           // objects = objects.Where(x => !string.IsNullOrEmpty(x)).ToArray();
                            objects = objects.Except(new List<string> { string.Empty }).ToArray();




                        }
                        if (objects[0].ToString().ToUpper() == "H")
                        {

                            if (inboundMasterLineItems.Count() != 0)
                            {
                                //FailsStatus = FailsStatus + failsRow.Count();// successfull product rollback
                                if (stagingRecord.PORef != "")
                                {
                                    //SuccessStatus = 0;
                                    //    SuccessStatus++;
                                    //    auditRecord.EventStatus = "SUCCESS";
                                    //auditRecord.Message = " " +POHeader + " SUCCESSFUL SUCCESSFULLY IMPORTED".ToUpper();
                                    //auditStatus.Add(auditRecord.Message);
                                    //auditRecord.Transaction = POHeader;
                                    //auditRecord.Save();
                                    SuccessStatus++;

                                foreach (var s in inboundMasterLineItems)
                                {


                                    string tbl = s.ProductStagingID + "|" + s.UnitCost + "|" + s.ExpectedQuantity + ",";

                                    if (successRow.Count() <= row)
                                    {

                                        auditRecord.EventStatus = "SUCCESS";
                                       // auditRecord.Message = "ROW " + successRow[x] + " , " + s.ProdCode + " SUCCESSFULLY IMPORTED".ToUpper();
                                        auditRecord.Message = "ROW " + successRow[x] + " PO REF = " + POHeader + " ,  PRODUCT = " + s.ProdCode + "  SUCCESSFULLY IMPORTED ".ToUpper();
                                        auditStatus.Add(auditRecord.Message);
                                        auditRecord.Transaction = s.ProdCode;
                                        auditRecord.Save(principalID);
                                        x++;
                                    }

                                    combined += tbl;
                                    SuccessStatus++;
                                }
                                  //  successRow.Clear();
                                loopingHeader = 0;
                                if (combined != "")
                                {
                                    combined = combined.Substring(0, combined.Length - 1);
                                    inboundService.SaveNewOrderWithOrderLine(stagingRecord, eventTerminal, eventUser, combined);
                                        combined = "";
                                }
                                }
                                else
                                {

                                    //SuccessStatus=0;
                                    FailsStatus = FailsStatus + successRow.Count();
                                    auditRecord.EventStatus = "FAILED";
                                    auditRecord.Message = POHeader + " FAILED UPLOADS".ToUpper();
                                    auditStatus.Add(auditRecord.Message);
                                    auditRecord.Transaction = POHeader;
                                    auditRecord.Save(principalID);
                                    loopingHeader = 0;
                                    FailsStatus++;
                                    successRow.Clear();
                                }

                            }

                            inboundMasterLineItems.Clear();
                            //else
                            //{

                            //    auditRecord.EventStatus = "FAILED";
                            //    auditRecord.Message = POHeader + " FAILED UPLOADS".ToUpper();
                            //    auditStatus.Add(auditRecord.Message);
                            //    auditRecord.Transaction = POHeader;
                            //    auditRecord.Save();
                            //    SuccessStatus--;
                            //    FailsStatus++; ;
                            //}

                        }
                        if (objects[0].ToString().ToUpper() == "H" || objects[0].ToString().ToUpper() == "D")
                        {
                            if (loopingHeader == 0)
                            {
                                if (objects.Count() == 5)
                                {
                                    if (objects[0].ToString().ToUpper() == "H")
                                    {
                                        bool PODate = DateTime.TryParse(objects[3].ToString(), out date);
                                        bool ExpectedDeliveryDateTime = DateTime.TryParse(objects[4].ToString(), out date);

                                        if (objects[1].ToString().ToUpper() != "")
                                        {
                                            if (objects[2].ToString().ToUpper() != "") { 
                                            if (PODate == true && ExpectedDeliveryDateTime == true)
                                            {

                                                stagingRecord.PORef = objects[1].ToString().ToUpper();
                                                stagingRecord.SupplierName = objects[2].ToString().ToUpper();
                                                stagingRecord.PODate = Convert.ToDateTime(objects[3]);

                                                stagingRecord.ExpectedDeliveryDateTime = Convert.ToDateTime(objects[4]);
                                                stagingRecord.SiteCode = pSiteCode;
                                                stagingRecord.RecordSource = pRecordSource;
                                                stagingRecord.PrincipalID = principalID;

                                                inboundheader.Add(stagingRecord);
                                                loopingHeader++;
                                                //SuccessStatus++;

                                                if (stagingRecord != null)
                                                {
                                                    auditRecord.EventStatus = "SUCCESS";
                                                    successRow.Add(row);
                                                    header.Add(row);
                                                    POHeader = objects[1].ToString().ToUpper();
                                                    auditRecord.Message = "PURCHASE ORDER REFERENCE " + POHeader.ToUpper();
                                                    auditStatus.Add(auditRecord.Message);
                                                    auditRecord.Transaction = objects[1].ToString().ToUpper();
                                                    auditRecord.Save(principalID);
                                                }
                                                else
                                                {
                                                    auditRecord.EventStatus = "FAILED";
                                                    auditRecord.Message = "ROW " + row + " PURCHASE ORDER HEADER NOT SUCCESSFUL".ToUpper();
                                                    auditStatus.Add(auditRecord.Message);
                                                    auditRecord.Transaction = "NOT APPLICABLE";
                                                     FailsStatus++;
                                                    auditRecord.Save(principalID);
                                                    continue;
                                                }

                                            }
                                            else
                                            {
                                                FailsStatus++;
                                                    auditRecord.EventStatus = "FAILED";
                                                auditRecord.Message = "ROW " + row + " INCORRECT DATE FORMAT.".ToUpper();
                                                auditStatus.Add(auditRecord.Message);
                                                auditRecord.Transaction = "NOT APPLICABLE";
                                                auditRecord.Save(principalID);
                                                continue;
                                            }
                                        }
                                            else
                                            {
                                               FailsStatus++;
                                                auditRecord.EventStatus = "FAILED";
                                                auditRecord.Message = "ROW " + row + " SUPPLIER NAME NOT PROVIDED.".ToUpper();
                                                auditStatus.Add(auditRecord.Message);
                                                auditRecord.Transaction = "NOT APPLICABLE";
                                                auditRecord.Save(principalID);
                                                continue;
                                            }
                                        }
                                        else
                                        {
                                           FailsStatus++;
                                            auditRecord.EventStatus = "FAILED";
                                            auditRecord.Message = "ROW " + row + " POREF NOT PROVIDED.".ToUpper();
                                            auditStatus.Add(auditRecord.Message);
                                            auditRecord.Transaction = "NOT APPLICABLE";
                                            auditRecord.Save(principalID);
                                            continue;
                                        }
                                    }
                                }
                                else
                                {
                                    FailsStatus++;
                                    auditRecord.EventStatus = "FAILED";
                                    auditRecord.Message = "ROW " + row + " PURCHASE ORDER HEADER NOT PROVIDED".ToUpper();
                                    auditStatus.Add(auditRecord.Message);
                                    auditRecord.Transaction =objects[1].ToString().ToUpper();
                                    auditRecord.TransactionType = "PURCHASE ORDER";
                                    auditRecord.Save(principalID);
                                    continue;
                                }
                            }
                            if (loopingHeader >= 1 && objects[0].ToString().ToUpper() == "D")
                            {

                                List<ProductAutocomplete> results = qLDataManager.Product_Lookup(principalID, objects[1].ToString(), 20, "ProdCode"); //looking up for the product code 
                                bool Quantit = Int32.TryParse(objects[2].ToString(), out NumericalValue);
                                if (objects.Count() == 3)
                                {
                                    if (objects[2].ToString() != "" && Quantit == true)
                                    {
                                        if (results.Count() == 0)
                                        {
                                            // invalid product code
                                            auditRecord.EventStatus = "FAILED";
                                            FailsStatus++;
                                            err.Add("ROW " + row + objects[1].ToString().ToUpper() + ", INVALID PRODUCT CODE".ToUpper());
                                            auditRecord.Message = "ROW " + row + " PO REF = "+POHeader+" ,  PRODUCT = " + objects[1].ToString().ToUpper() +" INVALID ".ToUpper();
                                            auditStatus.Add(auditRecord.Message);
                                            auditRecord.Transaction = objects[1].ToString().ToUpper();
                                            failsRow.Add(row);
                                            auditRecord.Save(principalID);
                                            stagingRecord.PORef = "";
                                            
                                            combined = null;
                                            continue;
                                        }
                                        else
                                        {
                                            items.ProductStagingID = results[0].ProductStagingID;
                                            items.ExpectedQuantity = Convert.ToInt16(objects[2].ToString());
                                            items.UnitCost = Convert.ToDouble(results[0].UnitCost);
                                            //pTable = prodstaging + "|" + UnitPrice + "|" + quantity;

                                            inboundMasterLineItems.Add(new InboundMasterLineItem(items.ProductStagingID, items.ExpectedQuantity, items.UnitCost, objects[1].ToString()));
                                            List<string> prodlines = new List<string>();

                                            //foreach (var s in inboundMasterLineItems)
                                            //{

                                            //    string tbl = s.ProductStagingID + "|" + s.SalesPrice + "|" + s.ExpectedQuantity +",";

                                            //   // prodlines.Add(tbl);

                                            //        combined += tbl;
                                            //}
                                            // combined += prodlines[0]+",";


                                            // inboundService.SaveNewOrderWithOrderLine(stagingRecord, eventTerminal, eventUser, combined); // send the header to the database
                                            // SuccessStatus++;
                                            successRow.Add(row);
                                            //  auditRecord.Transaction = objects[1].ToString().ToUpper();
                                            //  auditRecord.EventStatus = "SUCCESS";
                                            //  auditRecord.Message = "ROW " + row + " IMPORT SUCCESS";
                                            //  auditStatus.Add(auditRecord.Message);
                                            //  auditRecord.Save();// save to auditRecord
                                            // inboundMasterLineItems.RemoveAt(0);
                                        }
                                    }
                                    else
                                    {
                                        // quantity cannot be null
                                        auditRecord.EventStatus = "FAILED";
                                        FailsStatus++;
                                        auditRecord.Message = "ROW " + row + " PO REF = " + POHeader + " ,  PRODUCT = " + objects[1].ToString().ToUpper() + "  Non Nagative number only required ".ToUpper();
                                        auditStatus.Add(auditRecord.Message);
                                        auditRecord.Transaction = objects[1].ToString().ToUpper();
                                        auditRecord.Save(principalID);
                                        failsRow.Add(row);
                                        stagingRecord.PORef = "";
                                        combined = null;
                                        continue;
                                    }
                                }
                                else
                                {
                                    auditRecord.EventStatus = "FAILED";
                                    FailsStatus++;
                                    auditRecord.Message = "ROW " + row + "Incorrect Number of fileds".ToUpper();
                                    auditStatus.Add(auditRecord.Message);
                                    auditRecord.Transaction = POHeader;
                                    auditRecord.Save(principalID);
                                    stagingRecord.PORef = "";
                                    failsRow.Add(row);
                                    combined = null;
                                    continue;
                                }
                            }
                        }
                        else
                        {
                            auditRecord.EventStatus = "FAILED";
                            auditRecord.Message = "ROW " + row + " - EACH ROW SHOULD START WITH EITHER A HEADER (H) OR DETAIL (D).".ToUpper();
                            auditRecord.Transaction = "NOT APPLICABLE";
                            auditStatus.Add(auditRecord.Message);
                            auditRecord.TransactionType = "PURCHASE ORDER";
                            auditRecord.Save(principalID);
                           // inboundMasterLineItems = null;
                            combined = null;
                            stagingRecord.PORef ="";
                            continue;
                        }
                    }
                }
                if (inboundMasterLineItems.Count() != 0)
                {
                 // FailsStatus= FailsStatus+ failsRow.Count();// successfull product rollback
                    if (stagingRecord.PORef != "")
                    {
                       //// SuccessStatus = 0;

                       // auditRecord.EventStatus = "SUCCESS";
                       // auditRecord.Message = "ROW " + POHeader + " SUCCESSFUL SUCCESSFULLY UPLOADED".ToUpper();
                       // auditStatus.Add(auditRecord.Message);
                       // auditRecord.Transaction = POHeader;
                       // auditRecord.Save();
                        

                        foreach (var s in inboundMasterLineItems)
                        {
                            string tbl = s.ProductStagingID + "|" + s.SalesPrice + "|" + s.ExpectedQuantity + ",";

                            if (successRow.Count() <= row)
                            {
                                auditRecord.EventStatus = "SUCCESS";
                                auditRecord.Message = "ROW " + successRow[x] + " PO REF = " + POHeader + " ,  PRODUCT = " + s.ProdCode + "  SUCCESSFULLY IMPORTED ".ToUpper();
                                auditStatus.Add(auditRecord.Message);
                                auditRecord.Transaction = s.ProdCode;
                                auditRecord.Save(principalID);
                                SuccessStatus++;
                                x++;
                            }

                            combined += tbl;
                            SuccessStatus++;
                        }
                        // SuccessStatus = 0;

                        //auditRecord.EventStatus = "SUCCESS";
                        //auditRecord.Message = "ROW " + POHeader + " SUCCESSFUL SUCCESSFULLY UPLOADED".ToUpper();
                        //auditStatus.Add(auditRecord.Message);
                        //auditRecord.Transaction = POHeader;
                        //auditRecord.Save();


                        loopingHeader = 0;
                        if (combined != "")
                        {
                            combined = combined.Substring(0, combined.Length - 1);
                            inboundService.SaveNewOrderWithOrderLine(stagingRecord, eventTerminal, eventUser, combined);
                        }
                        SuccessStatus = successRow.Count();
                    }
                    else
                    {
                        //SuccessStatus--;
                        FailsStatus++;
                        auditRecord.EventStatus = "FAILED";
                        auditRecord.Message = POHeader + " FAILED UPLOADS".ToUpper();
                        auditStatus.Add(auditRecord.Message);
                        auditRecord.Transaction = POHeader;
                        auditRecord.Save(principalID);
                        loopingHeader = 0;
                    }

                }
                else
                {
                    auditRecord.EventStatus = "FAILED";
                    auditRecord.Message = POHeader + " FAILED UPLOADS".ToUpper();
                    auditStatus.Add(auditRecord.Message);
                    auditRecord.Transaction = POHeader;
                    auditRecord.Save(principalID);
                   // SuccessStatus=0;
                     FailsStatus++; ;
                }

               
            }
            catch (Exception Ex)
            {
                // Reading error to the database table Audit Table 
                auditRecord.EventStatus = "FAILED";
                auditRecord.Message = Ex.Message.ToUpper();
                auditStatus.Add(Ex.Message.ToUpper());
                Status = "Failed";
                auditRecord.Save(principalID);
                Console.WriteLine("Error  details" + Ex);
            }          
            // call the Email send;
            //auditMail.Sendmail(auditStatus, eventUser, FileName);


            if (FailsStatus ==0)// return all successful rows
            {
                return SuccessStatus + " Row(s) Uploaded Successfully ".ToUpper() + " And ".ToUpper() + FailsStatus + " Row(s) Failed".ToUpper();
            }
            if (SuccessStatus == 0)// return all failed rows
            {
                --row;
                return SuccessStatus + " Row(s) Uploaded Successfully ".ToUpper() + " And ".ToUpper() + FailsStatus + " Row(s) Failed".ToUpper();
            }
            // Partialy successfully , failed rows and succeeded rows 
            return SuccessStatus + " Row(s) Uploaded Successfully ".ToUpper() + " And ".ToUpper() + FailsStatus + " Row(s) Failed".ToUpper();

        }
        
    }
}